-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 07, 2019 at 11:03 PM
-- Server version: 10.3.17-MariaDB-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bwssula1_cop`
--

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id` int(11) NOT NULL,
  `id_provinsi` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`id`, `id_provinsi`, `name`) VALUES
(1, '27', 'Donggala'),
(2, '27', 'Sigi'),
(3, '27', 'Parigi Moutong'),
(4, '27', 'Banggai'),
(5, '27', 'Banggai Kepulauan'),
(6, '27', 'Banggai Laut'),
(7, '27', 'Buol'),
(8, '27', 'Morowali'),
(9, '27', 'Poso'),
(10, '27', 'Morowali Utara'),
(11, '27', 'Tojo Una-una'),
(12, '27', 'Toli - Toli'),
(13, '27', 'Kota Palu'),
(14, '25', 'Mamuju'),
(15, '25', 'Majene'),
(16, '25', 'Mamasa'),
(17, '25', 'Mamuju Tengah'),
(18, '25', 'Mamuju Utara'),
(19, '25', 'Polewali Mandar');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `id_kabupaten`, `name`, `deskripsi`) VALUES
(1, 13, 'Palu  Barat', ''),
(2, 13, 'Palu Selatan', ''),
(3, 13, 'Palu Timur', ''),
(4, 13, 'Palu Utara', ''),
(5, 2, 'Balantak', ''),
(6, 2, 'Batui', ''),
(7, 2, 'Bualemo', ''),
(8, 2, 'Bunta', ''),
(9, 2, 'Kintom', ''),
(10, 2, 'Lamala', ''),
(11, 2, 'Luwuk', ''),
(12, 2, 'LuwukTimur', ''),
(13, 2, 'Masama', ''),
(14, 2, 'Nuhon', ''),
(15, 2, 'Pagimana', ''),
(16, 2, 'Toili', ''),
(17, 2, 'Toili Barat', ''),
(18, 3, 'Banggai', ''),
(19, 3, 'Banggai Selatan', ''),
(20, 3, 'Banggai Tengah', ''),
(21, 3, 'Banggai Utara', ''),
(22, 3, 'Bangkurung', ''),
(23, 3, 'BokanKepulauan', ''),
(24, 3, 'Buko', ''),
(25, 3, 'BukoSelatan', ''),
(26, 3, 'Bulagi', ''),
(27, 3, 'Bulagi Selatan', ''),
(28, 3, 'Bulagi Utara', ''),
(29, 3, 'Labodo/Lobangkurung', ''),
(30, 3, 'Liang', ''),
(31, 3, 'Peling Tengah', ''),
(32, 3, 'Tinangkung', ''),
(33, 3, 'TinangkungSelatan', ''),
(34, 3, 'Tinangkung Utara', ''),
(35, 3, 'Torikum/Totikung', ''),
(36, 3, 'Totikum Selatan', ''),
(37, 4, 'Biau', ''),
(38, 4, 'Bokat', ''),
(39, 4, 'Bukal', ''),
(40, 4, 'Bunobogu/Bonobogu', ''),
(41, 4, 'Gadung', ''),
(42, 4, 'Karamat', ''),
(43, 4, 'Lipunoto', ''),
(44, 4, 'Momunu', ''),
(45, 4, 'Paleleh', ''),
(46, 4, 'PalelehBarat', ''),
(47, 4, 'Tiloan', ''),
(48, 5, 'Balaesang', ''),
(49, 5, 'Banawa', ''),
(50, 5, 'BanawaSelatan', ''),
(51, 5, 'BanawaTengah', ''),
(52, 5, 'Damsol/DampelasSojol', ''),
(53, 5, 'Labuan', ''),
(54, 5, 'Pinembani', ''),
(55, 5, 'RioPakava/Riopakawa', ''),
(56, 5, 'Sindue', ''),
(57, 5, 'Sindue Tobata', ''),
(58, 5, 'SindueTombusabora', ''),
(59, 5, 'Sirenja', ''),
(60, 5, 'Sojol', ''),
(61, 5, 'SojolUtara', ''),
(62, 5, 'Tanantovea', ''),
(63, 6, 'Bahodopi', ''),
(64, 6, 'Bumi Raya', ''),
(65, 6, 'Bungku Barat', ''),
(66, 6, 'Bungku Selatan', ''),
(67, 6, 'BungkuTengah', ''),
(68, 6, 'Bungku Utara', ''),
(69, 6, 'Lembo', ''),
(70, 6, 'Mamosalato', ''),
(71, 6, 'MenuiKepulauan', ''),
(72, 6, 'Mori Atas', ''),
(73, 6, 'Petasia', ''),
(74, 6, 'Soyo Jaya', ''),
(75, 6, 'Wita Ponda', ''),
(76, 7, 'Ampibabo', ''),
(77, 7, 'Balinggi', ''),
(78, 7, 'Bolano Lambunu', ''),
(79, 7, 'Kasimbar', ''),
(80, 7, 'Mepanga', ''),
(81, 7, 'Moutong', ''),
(82, 7, 'Palasa', ''),
(83, 7, 'Parigi', ''),
(84, 7, 'ParigiBarat', ''),
(85, 7, 'Parigi Selatan', ''),
(86, 7, 'Parigi Tengah', ''),
(87, 7, 'Parigi Utara', ''),
(88, 7, 'Sausu', ''),
(89, 7, 'Siniu', ''),
(90, 7, 'Taopa', ''),
(91, 7, 'Tinombo', ''),
(92, 7, 'Tinombo Selatan', ''),
(93, 7, 'Tomini', ''),
(94, 7, 'Toribulu', ''),
(95, 7, 'Torue', ''),
(96, 8, 'Lage', ''),
(97, 8, 'Lore Barat', ''),
(98, 8, 'Lore Piore', ''),
(99, 8, 'Lore Selatan', ''),
(100, 8, 'Lore Tengah', ''),
(101, 8, 'Lore Timur', ''),
(102, 8, 'Lore Utara', ''),
(103, 8, 'Pamona Barat', ''),
(104, 8, 'Pamona Selatan', ''),
(105, 8, 'Pamona Tenggara', ''),
(106, 8, 'Pamona Timur', ''),
(107, 8, 'PamonaUtara', ''),
(108, 8, 'Poso  Kota', ''),
(109, 8, 'Poso  Kota Selatan', ''),
(110, 8, 'Poso Kota Utara', ''),
(111, 8, 'Poso Pesisir', ''),
(112, 8, 'Poso Pesisir Selatan', ''),
(113, 8, 'Poso Pesisi Utara', ''),
(114, 9, 'Dolo', ''),
(115, 9, 'Dolo Barat', ''),
(116, 9, 'Dolo Selatan', ''),
(117, 9, 'Gumbasa', ''),
(118, 9, 'Kinovaru', ''),
(119, 9, 'Kulawi', ''),
(120, 9, 'Kulawi Selatan', ''),
(121, 9, 'Lindu', ''),
(122, 9, 'Marawola', ''),
(123, 9, 'Marawo laBarat', ''),
(124, 9, 'Nokilalaki', ''),
(125, 9, 'Palolo', ''),
(126, 9, 'Pipikoro', ''),
(127, 9, 'Sigi Biromaru', ''),
(128, 9, 'Tanambuluva', ''),
(129, 10, 'AmpanaKota', ''),
(130, 10, 'Ampana Tete', ''),
(131, 10, 'Togean', ''),
(132, 10, 'Tojo', ''),
(133, 10, 'Tojo Barat', ''),
(134, 10, 'Ulu Bongka', ''),
(135, 10, 'Una-Una', ''),
(136, 10, 'Walea Besar', ''),
(137, 10, 'Walea Kepulauan', ''),
(138, 11, 'Baolan', ''),
(139, 11, 'Basidondo', ''),
(140, 11, 'Dako Pamean', ''),
(141, 11, 'Dampa lSelatan', ''),
(142, 11, 'DamPalu tara', ''),
(143, 11, 'Dondo', ''),
(144, 11, 'Galang', ''),
(145, 11, 'Lampasio', ''),
(146, 11, 'OgoDeide', ''),
(147, 11, 'Tolitoli Utara', '');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `id_kecamatan`, `name`, `deskripsi`) VALUES
(1, 1, 'Kelurahan Baru', ''),
(2, 1, 'Kelurahan Boyaoge', ''),
(3, 1, 'Kelurahan Lere', ''),
(4, 1, 'Kelurahan Siranindi', ''),
(5, 1, 'Kelurahan Nunu', ''),
(6, 1, 'Kelurahan Ujuna', ''),
(7, 1, 'Kelurahan Kamonji', ''),
(8, 1, 'Kelurahan Duyu', ''),
(9, 1, 'Kelurahan Balaroa', ''),
(10, 1, 'Kelurahan Donggala Kodi', ''),
(11, 1, 'Kelurahan Kabonena', ''),
(12, 1, 'Kelurahan Silae', ''),
(13, 1, 'Kelurahan Buluri', ''),
(14, 1, 'Kelurahan Tipo', ''),
(15, 1, 'Kelurahan Watusampu', ''),
(16, 2, 'Kelurahan BirobuliSelatan', ''),
(17, 2, 'Kelurahan BirobuliUtara', ''),
(18, 2, 'Kelurahan Petobo', ''),
(19, 2, 'Kelurahan Kawatuna', ''),
(20, 2, 'Kelurahan Tanamodindi', ''),
(21, 2, 'Kelurahan LoluSelatan', ''),
(22, 2, 'Kelurahan LoluUtara', ''),
(23, 2, 'Kelurahan TaturaSelatan', ''),
(24, 2, 'Kelurahan TaturaUtara', ''),
(25, 2, 'Kelurahan Tawanjuka', ''),
(26, 2, 'Kelurahan Palu pi', ''),
(27, 2, 'Kelurahan Pengawu', ''),
(28, 3, 'Kelurahan BesusuBara', ''),
(29, 3, 'Kelurahan BesusuTengah', ''),
(30, 3, 'Kelurahan BesusuTimur', ''),
(31, 3, 'Kelurahan LayanaIndah', ''),
(32, 3, 'Kelurahan Poboya', ''),
(33, 3, 'Kelurahan Lasoani', ''),
(34, 3, 'Kelurahan Talise', ''),
(35, 3, 'Kelurahan Tondo', ''),
(36, 4, 'Kelurahan Lambara', ''),
(37, 4, 'Kelurahan Panau', ''),
(38, 4, 'Kelurahan Baiya', ''),
(39, 4, 'Kelurahan Pantoloan', ''),
(40, 4, 'Kelurahan Kayumalue Pajeko', ''),
(41, 4, 'Kelurahan Kayumalue Ngapa', ''),
(42, 4, 'Kelurahan Taipa', ''),
(43, 4, 'Kelurahan Mamboro', ''),
(44, 5, 'Kelurahan Balantak', ''),
(45, 5, 'Kelurahan Batusimpang', ''),
(46, 5, 'Kelurahan Boloak', ''),
(47, 5, 'Kelurahan Booy', ''),
(48, 5, 'Kelurahan Dolo m', ''),
(49, 5, 'Kelurahan Dondo', ''),
(50, 5, 'Kelurahan Giwang', ''),
(51, 5, 'Kelurahan Gorontalo', ''),
(52, 5, 'Kelurahan Kampangar', ''),
(53, 5, 'Kelurahan Kiloma', ''),
(54, 5, 'Kelurahan Luok', ''),
(55, 5, 'Kelurahan Mamping', ''),
(56, 5, 'Kelurahan Ondoliang', ''),
(57, 5, 'Kelurahan Padang', ''),
(58, 5, 'Kelurahan Pangkalaseang', ''),
(59, 5, 'Kelurahan Poyang', ''),
(60, 5, 'Kelurahan Rau', ''),
(61, 5, 'Kelurahan Resarna', ''),
(62, 5, 'Kelurahan Sepe', ''),
(63, 5, 'Kelurahan Talima A', ''),
(64, 5, 'Kelurahan Talima B', ''),
(65, 5, 'Kelurahan Tanggawas', ''),
(66, 5, 'Kelurahan Tanotu', ''),
(67, 5, 'Kelurahan Teku', ''),
(68, 5, 'Kelurahan Tintingon', ''),
(69, 5, 'Kelurahan Tombos', ''),
(70, 5, 'Kelurahan Tongke', ''),
(71, 6, 'Kelurahan Balantang', ''),
(72, 6, 'Kelurahan Batui', ''),
(73, 6, 'Kelurahan Bone Balantak', ''),
(74, 6, 'Kelurahan Bugis', ''),
(75, 6, 'Kelurahan Honbola', ''),
(76, 6, 'Kelurahan Kayowa', ''),
(77, 6, 'Kelurahan Lamo', ''),
(78, 6, 'Kelurahan Maasing', ''),
(79, 6, 'Kelurahan Maleo Jaya', ''),
(80, 6, 'Kelurahan Masungkang', ''),
(81, 6, 'Kelurahan Nonong', ''),
(82, 6, 'Kelurahan Ondo Ondolu I', ''),
(83, 6, 'Kelurahan Ondo-Ondolu II', ''),
(84, 6, 'Kelurahan Sinorang', ''),
(85, 6, 'Kelurahan Sisipan', ''),
(86, 6, 'Kelurahan Sukamaju', ''),
(87, 6, 'Kelurahan Sukamaju I', ''),
(88, 6, 'Kelurahan Tolando', ''),
(89, 6, 'Kelurahan Uso', ''),
(90, 7, 'Kelurahan Bima Karya', ''),
(91, 7, 'Kelurahan Binsil Ch', ''),
(92, 7, 'Kelurahan Binsil Padang', ''),
(93, 7, 'Kelurahan Bualemo A', ''),
(94, 7, 'Kelurahan Bualemo B', ''),
(95, 7, 'Kelurahan Lembah Tompotika', ''),
(96, 7, 'Kelurahan Longkoga Barat', ''),
(97, 7, 'Kelurahan Longkoga Timur', ''),
(98, 7, 'Kelurahan Malik', ''),
(99, 7, 'Kelurahan Mayayap', ''),
(100, 7, 'Kelurahan Nipa Kaleman', ''),
(101, 7, 'Kelurahan Sampaka', ''),
(102, 7, 'Kelurahan Taima', ''),
(103, 7, 'Kelurahan Tikupon', ''),
(104, 7, 'Kelurahan Toiba', ''),
(105, 7, 'Kelurahan Trans Mayayap', ''),
(106, 8, 'Kelurahan Beringin Jaya', ''),
(107, 8, 'Kelurahan Bohotokong', ''),
(108, 8, 'Kelurahan Bunta Dua', ''),
(109, 8, 'Kelurahan Bunta Satu', ''),
(110, 8, 'Kelurahan Doda Bunta', ''),
(111, 8, 'Kelurahan Dondo Soboli', ''),
(112, 8, 'Kelurahan Dowiwi', ''),
(113, 8, 'Kelurahan Dwipa Karya', ''),
(114, 8, 'Kelurahan Gonohop', ''),
(115, 8, 'Kelurahan Hion', ''),
(116, 8, 'Kelurahan Huhak', ''),
(117, 8, 'Kelurahan Koninis', ''),
(118, 8, 'Kelurahan Laonggo', ''),
(119, 8, 'Kelurahan Lontio', ''),
(120, 8, 'Kelurahan Mantan A', ''),
(121, 8, 'Kelurahan Matabas', ''),
(122, 8, 'Kelurahan Nanga-Nangaon', ''),
(123, 8, 'Kelurahan Pongian', ''),
(124, 8, 'Kelurahan Rantau Jaya', ''),
(125, 8, 'Kelurahan Simpang Dua', ''),
(126, 8, 'Kelurahan Simpang Satu', ''),
(127, 8, 'Kelurahan Sumber Mulya', ''),
(128, 8, 'Kelurahan Toima', ''),
(129, 8, 'Kelurahan Tombongan Ulos', ''),
(130, 8, 'Kelurahan Tuntung', ''),
(131, 9, 'Kelurahan Beringin Jaya', ''),
(132, 9, 'Kelurahan Bohotokong', ''),
(133, 9, 'Kelurahan Bunta Dua', ''),
(134, 9, 'Kelurahan Bunta Satu', ''),
(135, 9, 'Kelurahan Doda Bunta', ''),
(136, 9, 'Kelurahan Dondo Soboli', ''),
(137, 9, 'Kelurahan Dowiwi', ''),
(138, 9, 'Kelurahan Dwipa Karya', ''),
(139, 9, 'Kelurahan Gonohop', ''),
(140, 9, 'Kelurahan Hion', ''),
(141, 9, 'Kelurahan Huhak', ''),
(142, 9, 'Kelurahan Koninis', ''),
(143, 9, 'Kelurahan Laonggo', ''),
(144, 9, 'Kelurahan Lontio', ''),
(145, 9, 'Kelurahan Mantan A', ''),
(146, 9, 'Kelurahan Matabas', ''),
(147, 9, 'Kelurahan Nanga-Nangaon', ''),
(148, 9, 'Kelurahan Pongian', ''),
(149, 9, 'Kelurahan Rantau Jaya', ''),
(150, 9, 'Kelurahan Simpang Dua', ''),
(151, 9, 'Kelurahan Simpang Satu', ''),
(152, 9, 'Kelurahan Sumber Mulya', ''),
(153, 9, 'Kelurahan Toima', ''),
(154, 9, 'Kelurahan Tombongan Ulos', ''),
(155, 9, 'Kelurahan Tuntung', ''),
(156, 10, 'Kelurahan Babang Buyangge', ''),
(157, 10, 'Kelurahan Dimpalon', ''),
(158, 10, 'Kelurahan Kalolos', ''),
(159, 10, 'Kelurahan Kintom', ''),
(160, 10, 'Kelurahan Lontio', ''),
(161, 10, 'Kelurahan Manyula', ''),
(162, 10, 'Kelurahan Mendono', ''),
(163, 10, 'Kelurahan Padang', ''),
(164, 10, 'Kelurahan Padungnyo', ''),
(165, 10, 'Kelurahan Samadoya', ''),
(166, 10, 'Kelurahan Sayambongin', ''),
(167, 10, 'Kelurahan Solan', ''),
(168, 10, 'Kelurahan Tangkiang', ''),
(169, 10, 'Kelurahan Uling', ''),
(170, 11, 'Kelurahan Awu', ''),
(171, 11, 'Kelurahan Boyou', ''),
(172, 11, 'Kelurahan Bubung', ''),
(173, 11, 'Kelurahan Bumi Beringin', ''),
(174, 11, 'Kelurahan Hanga-Hanga', ''),
(175, 11, 'Kelurahan Kampung Baru', ''),
(176, 11, 'Kelurahan Kilongan', ''),
(177, 11, 'Kelurahan Koyoan', ''),
(178, 11, 'Kelurahan Lumpoknyo', ''),
(179, 11, 'Kelurahan Luwuk', ''),
(180, 11, 'Kelurahan Tontouan', ''),
(181, 11, 'Kelurahan Bungin', ''),
(182, 11, 'Kelurahan Soho', ''),
(183, 11, 'Kelurahan Simpong', ''),
(184, 11, 'Kelurahan Biak', ''),
(185, 11, 'Kelurahan Bunga', ''),
(186, 11, 'Kelurahan Kamumu', ''),
(187, 11, 'Kelurahan Maahas', ''),
(188, 11, 'Kelurahan Nambo Lempek', ''),
(189, 11, 'Kelurahan Nambo Padang', ''),
(190, 11, 'Kelurahan Salodik', ''),
(191, 12, 'Kelurahan Bantayan', ''),
(192, 12, 'Kelurahan Baya', ''),
(193, 12, 'Kelurahan Boitan', ''),
(194, 12, 'Kelurahan Hunduhon', ''),
(195, 12, 'Kelurahan Kayutanyo', ''),
(196, 12, 'Kelurahan Lauwon', ''),
(197, 12, 'Kelurahan Molino', ''),
(198, 12, 'Kelurahan Pohi', ''),
(199, 12, 'Kelurahan Uwedikan', ''),
(200, 13, 'Kelurahan Duata Karya', ''),
(201, 13, 'Kelurahan Eteng', ''),
(202, 13, 'Kelurahan Kembang Mentha (Merta/Menta)', ''),
(203, 13, 'Kelurahan Kospa Buata Karya (Dwata Karya)', ''),
(204, 13, 'Kelurahan Minangandala', ''),
(205, 13, 'Kelurahan Purwo Agung', ''),
(206, 13, 'Kelurahan Seresa', ''),
(207, 13, 'Kelurahan Simpangan', ''),
(208, 13, 'Kelurahan Tangabean (Tangeban)', ''),
(209, 13, 'Kelurahan Taugi', ''),
(210, 13, 'Kelurahan Tompotika Makmur', ''),
(211, 14, 'Kelurahan Balaang', ''),
(212, 14, 'Kelurahan Bangketa', ''),
(213, 14, 'Kelurahan Batu Hitam', ''),
(214, 14, 'Kelurahan Bella', ''),
(215, 14, 'Kelurahan Binohu', ''),
(216, 14, 'Kelurahan Bolobungkang', ''),
(217, 14, 'Kelurahan Damai Makmur', ''),
(218, 14, 'Kelurahan Jaya Makmur', ''),
(219, 14, 'Kelurahan Kabua Dua', ''),
(220, 14, 'Kelurahan Mantan B', ''),
(221, 14, 'Kelurahan Pakowa Bunta (Pokowa Bunta)', ''),
(222, 14, 'Kelurahan Petak', ''),
(223, 14, 'Kelurahan Pimbombo (Pibombo)', ''),
(224, 14, 'Kelurahan Saiti', ''),
(225, 14, 'Kelurahan Sumber Agung', ''),
(226, 14, 'Kelurahan Temeang (Tomeang)', ''),
(227, 14, 'Kelurahan Tobelombang', ''),
(228, 15, 'Kelurahan Asaan', ''),
(229, 15, 'Kelurahan Bahingin', ''),
(230, 15, 'Kelurahan Bajo Poat', ''),
(231, 15, 'Kelurahan Balai Gondi', ''),
(232, 15, 'Kelurahan Balean', ''),
(233, 15, 'Kelurahan Baloa Doda', ''),
(234, 15, 'Kelurahan Basabungan', ''),
(235, 15, 'Kelurahan Bolobungkang', ''),
(236, 15, 'Kelurahan Bondat', ''),
(237, 15, 'Kelurahan Bungawon', ''),
(238, 15, 'Kelurahan Gomuo', ''),
(239, 15, 'Kelurahan Hohudongan', ''),
(240, 15, 'Kelurahan Huhak', ''),
(241, 15, 'Kelurahan Jaya Bakti', ''),
(242, 15, 'Kelurahan Kadodi', ''),
(243, 15, 'Kelurahan Lambangan', ''),
(244, 15, 'Kelurahan Lamo', ''),
(245, 15, 'Kelurahan Lobu', ''),
(246, 15, 'Kelurahan Nain', ''),
(247, 15, 'Kelurahan Niubulan', ''),
(248, 15, 'Kelurahan Pagimana', ''),
(249, 15, 'Kelurahan Pakowa', ''),
(250, 15, 'Kelurahan Pinapuan', ''),
(251, 15, 'Kelurahan Pisou', ''),
(252, 15, 'Kelurahan Poh', ''),
(253, 15, 'Kelurahan Salipi', ''),
(254, 15, 'Kelurahan Samma Jatem', ''),
(255, 15, 'Kelurahan Sepa (Sepak)', ''),
(256, 15, 'Kelurahan Sinampangnyo', ''),
(257, 15, 'Kelurahan Siuna', ''),
(258, 15, 'Kelurahan Taloyon', ''),
(259, 15, 'Kelurahan Tampe', ''),
(260, 15, 'Kelurahan Tintingan', ''),
(261, 15, 'Kelurahan Toipan', ''),
(262, 15, 'Kelurahan Tongkonunuk', ''),
(263, 15, 'Kelurahan Uha-Uhangon', ''),
(264, 15, 'Kelurahan Uwedaka', ''),
(265, 15, 'Kelurahan Uwedaka-Daka', ''),
(266, 16, 'Kelurahan Argakencana', ''),
(267, 16, 'Kelurahan Bentang', ''),
(268, 16, 'Kelurahan Bukit Jaya', ''),
(269, 16, 'Kelurahan Bumiharjo', ''),
(270, 16, 'Kelurahan Cendanapura', ''),
(271, 16, 'Kelurahan Mansahang', ''),
(272, 16, 'Kelurahan Minahaki', ''),
(273, 16, 'Kelurahan Minakarya', ''),
(274, 16, 'Kelurahan Moilong', ''),
(275, 16, 'Kelurahan Mulyoharjo', ''),
(276, 16, 'Kelurahan Piondo', ''),
(277, 16, 'Kelurahan Rusa Kencana', ''),
(278, 16, 'Kelurahan Saluan', ''),
(279, 16, 'Kelurahan Samalore', ''),
(280, 16, 'Kelurahan Selamet Raharjo', ''),
(281, 16, 'Kelurahan Sentral Sari', ''),
(282, 16, 'Kelurahan Sidoharjo', ''),
(283, 16, 'Kelurahan Sindang Baru', ''),
(284, 16, 'Kelurahan Singkoyo', ''),
(285, 16, 'Kelurahan Tirta Kencana', ''),
(286, 16, 'Kelurahan Tirta Sari', ''),
(287, 16, 'Kelurahan Tohiti Sari', ''),
(288, 16, 'Kelurahan Toili', ''),
(289, 16, 'Kelurahan Tolisu', ''),
(290, 16, 'Kelurahan Uwe Mea', ''),
(291, 17, 'Kelurahan Bukit Makarti', ''),
(292, 17, 'Kelurahan Bumi Harapan', ''),
(293, 17, 'Kelurahan Dongin', ''),
(294, 17, 'Kelurahan Gunung Kramat', ''),
(295, 17, 'Kelurahan Kami Wangi', ''),
(296, 17, 'Kelurahan Karya Makmur', ''),
(297, 17, 'Kelurahan Lembah Kramat', ''),
(298, 17, 'Kelurahan Makapa', ''),
(299, 17, 'Kelurahan Mantawa', ''),
(300, 17, 'Kelurahan Mantawa Bonebae (Bonebai)', ''),
(301, 17, 'Kelurahan Pandan Wangi', ''),
(302, 17, 'Kelurahan Pasir Lamba', ''),
(303, 17, 'Kelurahan Rata', ''),
(304, 17, 'Kelurahan Sindang Sari', ''),
(305, 17, 'Kelurahan Uwelolu', ''),
(306, 18, 'Kelurahan Dangkalan', ''),
(307, 18, 'Kelurahan Dodung', ''),
(308, 18, 'Kelurahan Kokini', ''),
(309, 18, 'Kelurahan Lambako', ''),
(310, 18, 'Kelurahan Lampa', ''),
(311, 18, 'Kelurahan Lompio', ''),
(312, 18, 'Kelurahan Pasir Putih', ''),
(313, 18, 'Kelurahan Potil Pololoba', ''),
(314, 18, 'Kelurahan Tano Bonunungan', ''),
(315, 18, 'Kelurahan Tinakin Laut', ''),
(316, 19, 'Kelurahan Bantean', ''),
(317, 19, 'Kelurahan Kelapa Lima', ''),
(318, 19, 'Kelurahan Malino Padas', ''),
(319, 19, 'Kelurahan Matanga', ''),
(320, 19, 'Kelurahan Tolokibit', ''),
(321, 20, 'Kelurahan Adean', ''),
(322, 20, 'Kelurahan Badumpayan', ''),
(323, 20, 'Kelurahan Gong-Gong', ''),
(324, 20, 'Kelurahan Mominit', ''),
(325, 20, 'Kelurahan Monsongan', ''),
(326, 20, 'Kelurahan Timbong Mominit', ''),
(327, 21, 'Kelurahan Bone Baru', ''),
(328, 21, 'Kelurahan Kendek', ''),
(329, 21, 'Kelurahan Lokotoy', ''),
(330, 21, 'Kelurahan Paisumosoni', ''),
(331, 21, 'Kelurahan Popisi', ''),
(332, 21, 'Kelurahan Tolise Tubono', ''),
(333, 22, 'Kelurahan Bone Bone', ''),
(334, 22, 'Kelurahan Kalupapi', ''),
(335, 22, 'Kelurahan Kanari', ''),
(336, 22, 'Kelurahan Lantibung', ''),
(337, 22, 'Kelurahan Mbeleang', ''),
(338, 22, 'Kelurahan Sasabobok', ''),
(339, 22, 'Kelurahan Tabulang', ''),
(340, 22, 'Kelurahan Tadiana Bungin', ''),
(341, 22, 'Kelurahan Taduno', ''),
(342, 22, 'Kelurahan Togong Sagu', ''),
(343, 23, 'Kelurahan Bungin', ''),
(344, 23, 'Kelurahan Kasuari', ''),
(345, 23, 'Kelurahan Kaukes', ''),
(346, 23, 'Kelurahan Kokudang', ''),
(347, 23, 'Kelurahan Mbuang Mbuang', ''),
(348, 23, 'Kelurahan Ndindibung', ''),
(349, 23, 'Kelurahan Nggasuang', ''),
(350, 23, 'Kelurahan Paisubebe', ''),
(351, 23, 'Kelurahan Panapat', ''),
(352, 23, 'Kelurahan Sonit', ''),
(353, 23, 'Kelurahan Timpaus', ''),
(354, 23, 'Kelurahan Toropot', ''),
(355, 24, 'Kelurahan Batangono', ''),
(356, 24, 'Kelurahan Labasiano', ''),
(357, 24, 'Kelurahan Lalengan', ''),
(358, 24, 'Kelurahan Leme-Leme Bungin', ''),
(359, 24, 'Kelurahan Leme-Leme Darat', ''),
(360, 24, 'Kelurahan Malanggong', ''),
(361, 24, 'Kelurahan Paisubatu', ''),
(362, 24, 'Kelurahan Peling Lalomo', ''),
(363, 24, 'Kelurahan Tataba', ''),
(364, 25, 'Kelurahan Buko', ''),
(365, 25, 'Kelurahan Kambani', ''),
(366, 25, 'Kelurahan Labangun', ''),
(367, 25, 'Kelurahan Lumbi-Lumbia', ''),
(368, 25, 'Kelurahan Palapat', ''),
(369, 25, 'Kelurahan Seano', ''),
(370, 25, 'Kelurahan Tatabau', ''),
(371, 26, 'Kelurahan Alul', ''),
(372, 26, 'Kelurahan Boloi (Boloy)', ''),
(373, 26, 'Kelurahan Bulagi Dua', ''),
(374, 26, 'Kelurahan Bulagi Satu', ''),
(375, 26, 'Kelurahan Komba-Komba', ''),
(376, 26, 'Kelurahan Lalanday', ''),
(377, 26, 'Kelurahan Meselesek', ''),
(378, 26, 'Kelurahan Montomisan', ''),
(379, 26, 'Kelurahan Oluno', ''),
(380, 26, 'Kelurahan Peling Seasa', ''),
(381, 26, 'Kelurahan Sosom', ''),
(382, 26, 'Kelurahan Sumondung', ''),
(383, 26, 'Kelurahan Tolo', ''),
(384, 27, 'Kelurahan Balalon', ''),
(385, 27, 'Kelurahan Bonepuso', ''),
(386, 27, 'Kelurahan Lemelu', ''),
(387, 27, 'Kelurahan Lolantang', ''),
(388, 27, 'Kelurahan Mangais', ''),
(389, 27, 'Kelurahan Osan', ''),
(390, 27, 'Kelurahan Palabatu Dua', ''),
(391, 27, 'Kelurahan Palabatu Satu', ''),
(392, 27, 'Kelurahan Pandaluk', ''),
(393, 27, 'Kelurahan Sabelak', ''),
(394, 27, 'Kelurahan Suit', ''),
(395, 27, 'Kelurahan Tatarandang', ''),
(396, 27, 'Kelurahan Toy-Toy', ''),
(397, 27, 'Kelurahan Unu', ''),
(398, 28, 'Kelurahan Bakalinga', ''),
(399, 28, 'Kelurahan Bangunemo', ''),
(400, 28, 'Kelurahan Bolubung', ''),
(401, 28, 'Kelurahan Koyobunga', ''),
(402, 28, 'Kelurahan Luk Panenteng', ''),
(403, 28, 'Kelurahan Montop', ''),
(404, 28, 'Kelurahan Ombuli', ''),
(405, 28, 'Kelurahan Paisuluno', ''),
(406, 28, 'Kelurahan Sabang', ''),
(407, 28, 'Kelurahan Sambulangan', ''),
(408, 29, 'Kelurahan Alasan', ''),
(409, 29, 'Kelurahan Bontosi', ''),
(410, 29, 'Kelurahan Lalong', ''),
(411, 29, 'Kelurahan Lipulalongo', ''),
(412, 29, 'Kelurahan Mansalean', ''),
(413, 29, 'Kelurahan Paisulamo', ''),
(414, 30, 'Kelurahan Apal', ''),
(415, 30, 'Kelurahan Bajo', ''),
(416, 30, 'Kelurahan Balayon', ''),
(417, 30, 'Kelurahan Basosol', ''),
(418, 30, 'Kelurahan Binuntuli', ''),
(419, 30, 'Kelurahan Boyomoute', ''),
(420, 30, 'Kelurahan Kindandal', ''),
(421, 30, 'Kelurahan Liang', ''),
(422, 30, 'Kelurahan Mamulusan', ''),
(423, 30, 'Kelurahan Okumel', ''),
(424, 30, 'Kelurahan PopiDolo n', ''),
(425, 30, 'Kelurahan Saleati', ''),
(426, 30, 'Kelurahan Tangkop', ''),
(427, 30, 'Kelurahan Tomboniki', ''),
(428, 31, 'Kelurahan Alakasing', ''),
(429, 31, 'Kelurahan Balombong', ''),
(430, 31, 'Kelurahan Kolak', ''),
(431, 31, 'Kelurahan Koyobunga', ''),
(432, 31, 'Kelurahan Labibi', ''),
(433, 31, 'Kelurahan Luk', ''),
(434, 31, 'Kelurahan Patukuki', ''),
(435, 31, 'Kelurahan Popisi', ''),
(436, 31, 'Kelurahan Tolulos', ''),
(437, 31, 'Kelurahan Tombos', ''),
(438, 32, 'Kelurahan Ambelang', ''),
(439, 32, 'Kelurahan Baka', ''),
(440, 32, 'Kelurahan Bakalan', ''),
(441, 32, 'Kelurahan Bongganan', ''),
(442, 32, 'Kelurahan Bulungkobit', ''),
(443, 32, 'Kelurahan Bungin', ''),
(444, 32, 'Kelurahan Kautu', ''),
(445, 32, 'Kelurahan Manggalai', ''),
(446, 32, 'Kelurahan Saiyong', ''),
(447, 32, 'Kelurahan Salakan', ''),
(448, 32, 'Kelurahan Tompudau', ''),
(449, 33, 'Kelurahan Kampung Baru', ''),
(450, 33, 'Kelurahan Gansal', ''),
(451, 33, 'Kelurahan Mansamat A', ''),
(452, 33, 'Kelurahan Mansamat B', ''),
(453, 33, 'Kelurahan Paisumosoni', ''),
(454, 33, 'Kelurahan Tinangkung', ''),
(455, 33, 'Kelurahan Tobing', ''),
(456, 33, 'Kelurahan Tobungin', ''),
(457, 34, 'Kelurahan Lalong', ''),
(458, 34, 'Kelurahan Luk Sagu', ''),
(459, 34, 'Kelurahan Palam', ''),
(460, 34, 'Kelurahan Ponding-Ponding', ''),
(461, 34, 'Kelurahan Tatakalai', ''),
(462, 35, 'Kelurahan Abason', ''),
(463, 35, 'Kelurahan Batang Babasal', ''),
(464, 35, 'Kelurahan Bolonan', ''),
(465, 35, 'Kelurahan Kombutokan', ''),
(466, 35, 'Kelurahan Lopito', ''),
(467, 35, 'Kelurahan Palam', ''),
(468, 35, 'Kelurahan Sakay', ''),
(469, 35, 'Kelurahan Salangano', ''),
(470, 35, 'Kelurahan Sambiut', ''),
(471, 35, 'Kelurahan Sampaka', ''),
(472, 35, 'Kelurahan Sobonon', ''),
(473, 35, 'Kelurahan Tone', ''),
(474, 36, 'Kelurahan Kanali', ''),
(475, 36, 'Kelurahan Nulion', ''),
(476, 36, 'Kelurahan Peley', ''),
(477, 36, 'Kelurahan Tobungku', ''),
(478, 36, 'Kelurahan Tonuson', ''),
(479, 37, 'Kelurahan Bukaan', ''),
(480, 37, 'Kelurahan Lakea I', ''),
(481, 37, 'Kelurahan Lakea II', ''),
(482, 37, 'Kelurahan Lakuan Buol', ''),
(483, 37, 'Kelurahan Tuinan', ''),
(484, 38, 'Kelurahan Bukamog', ''),
(485, 38, 'Kelurahan Butukan', ''),
(486, 38, 'Kelurahan Duamayo (Duwamayo)', ''),
(487, 38, 'Kelurahan Kantanan', ''),
(488, 38, 'Kelurahan Langudon', ''),
(489, 38, 'Kelurahan Negeri Lama', ''),
(490, 38, 'Kelurahan Tang', ''),
(491, 38, 'Kelurahan Tikopo', ''),
(492, 38, 'Kelurahan Bokat', ''),
(493, 38, 'Kelurahan Bokat IV', ''),
(494, 38, 'Kelurahan Bongo', ''),
(495, 38, 'Kelurahan Doulan', ''),
(496, 38, 'Kelurahan Kodolagon', ''),
(497, 38, 'Kelurahan Poongan', ''),
(498, 38, 'Kelurahan Tayadun', ''),
(499, 39, 'Kelurahan Biau', ''),
(500, 39, 'Kelurahan Binuang', ''),
(501, 39, 'Kelurahan Bungkudu', ''),
(502, 39, 'Kelurahan Mooyong', ''),
(503, 39, 'Kelurahan Yugut', ''),
(504, 39, 'Kelurahan Diat', ''),
(505, 39, 'Kelurahan Modo', ''),
(506, 39, 'Kelurahan Mopu', ''),
(507, 39, 'Kelurahan Potangoan', ''),
(508, 39, 'Kelurahan Rantemaranu', ''),
(509, 39, 'Kelurahan Unone', ''),
(510, 39, 'Kelurahan Winangun', ''),
(511, 40, 'Kelurahan Botugolu', ''),
(512, 40, 'Kelurahan Bunobogu (Bonobogu)', ''),
(513, 40, 'Kelurahan Domag', ''),
(514, 40, 'Kelurahan Domag Mekar', ''),
(515, 40, 'Kelurahan Inalatan', ''),
(516, 40, 'Kelurahan Kenamukan (Konamukan)', ''),
(517, 40, 'Kelurahan Lonu', ''),
(518, 40, 'Kelurahan Ponipingan', ''),
(519, 40, 'Kelurahan Tamit', ''),
(520, 41, 'Kelurahan Bulagidun', ''),
(521, 41, 'Kelurahan Bulagidun Tanjung', ''),
(522, 41, 'Kelurahan Diapatih', ''),
(523, 41, 'Kelurahan Labuton', ''),
(524, 41, 'Kelurahan Lipubogu', ''),
(525, 41, 'Kelurahan Lokodidi', ''),
(526, 41, 'Kelurahan Lokodoka', ''),
(527, 41, 'Kelurahan Matinan', ''),
(528, 41, 'Kelurahan Nandu', ''),
(529, 41, 'Kelurahan Taat', ''),
(530, 42, 'Kelurahan Baruga', ''),
(531, 42, 'Kelurahan Busak I', ''),
(532, 42, 'Kelurahan Busak II', ''),
(533, 42, 'Kelurahan Lamakan', ''),
(534, 42, 'Kelurahan Mendaan', ''),
(535, 42, 'Kelurahan Mokupo', ''),
(536, 42, 'Kelurahan Monano', ''),
(537, 43, 'Kelurahan Buol', ''),
(538, 43, 'Kelurahan Kali', ''),
(539, 43, 'Kelurahan Kumaligon', ''),
(540, 43, 'Kelurahan Leok 1', ''),
(541, 43, 'Kelurahan Leok II', ''),
(542, 44, 'Kelurahan Bugis', ''),
(543, 44, 'Kelurahan Guamonial', ''),
(544, 44, 'Kelurahan Kulango', ''),
(545, 44, 'Kelurahan Lamadong 1', ''),
(546, 44, 'Kelurahan Lamadong 2', ''),
(547, 44, 'Kelurahan Mangubi', ''),
(548, 44, 'Kelurahan Momunu', ''),
(549, 44, 'Kelurahan Pajeko', ''),
(550, 44, 'Kelurahan Panimbul', ''),
(551, 44, 'Kelurahan Pinamula', ''),
(552, 44, 'Kelurahan Pomayagon', ''),
(553, 44, 'Kelurahan Potugu', ''),
(554, 44, 'Kelurahan Pujimulyo', ''),
(555, 44, 'Kelurahan Soraya', ''),
(556, 44, 'Kelurahan Taluan', ''),
(557, 44, 'Kelurahan Tongon', ''),
(558, 44, 'Kelurahan Wakat', ''),
(559, 45, 'Kelurahan Batu Rata', ''),
(560, 45, 'Kelurahan Dopalak', ''),
(561, 45, 'Kelurahan Dutuno', ''),
(562, 45, 'Kelurahan Kuala Besar', ''),
(563, 45, 'Kelurahan Lilito', ''),
(564, 45, 'Kelurahan Lintidu', ''),
(565, 45, 'Kelurahan Mulangato', ''),
(566, 45, 'Kelurahan Paleleh', ''),
(567, 45, 'Kelurahan Pionoto', ''),
(568, 45, 'Kelurahan Talaki', ''),
(569, 45, 'Kelurahan Tolau', ''),
(570, 45, 'Kelurahan Umu', ''),
(571, 46, 'Kelurahan Bodi', ''),
(572, 46, 'Kelurahan Harmoni', ''),
(573, 46, 'Kelurahan Hulubalang', ''),
(574, 46, 'Kelurahan Lunguto', ''),
(575, 46, 'Kelurahan Oyak', ''),
(576, 46, 'Kelurahan Tayokan', ''),
(577, 46, 'Kelurahan Timbulon', ''),
(578, 47, 'Kelurahan Airterang', ''),
(579, 47, 'Kelurahan Balau', ''),
(580, 47, 'Kelurahan Boilan', ''),
(581, 47, 'Kelurahan Jatimulyo', ''),
(582, 47, 'Kelurahan Kokobuka', ''),
(583, 47, 'Kelurahan Lomuli', ''),
(584, 47, 'Kelurahan Maniala', ''),
(585, 47, 'Kelurahan Panilan Jaya', ''),
(586, 48, 'Kelurahan Kamonji', ''),
(587, 48, 'Kelurahan Ketong', ''),
(588, 48, 'Kelurahan Labean', ''),
(589, 48, 'Kelurahan Lombonga', ''),
(590, 48, 'Kelurahan Malei', ''),
(591, 48, 'Kelurahan Malino', ''),
(592, 48, 'Kelurahan Manimbaya', ''),
(593, 48, 'Kelurahan Mapane', ''),
(594, 48, 'Kelurahan Meli', ''),
(595, 48, 'Kelurahan Palao (Palau)', ''),
(596, 48, 'Kelurahan Pomolulu', ''),
(597, 48, 'Kelurahan Rano', ''),
(598, 48, 'Kelurahan Sibayu', ''),
(599, 48, 'Kelurahan Siboalang (Sibualong)', ''),
(600, 48, 'Kelurahan Siweli', ''),
(601, 48, 'Kelurahan Tambu', ''),
(602, 48, 'Kelurahan Tovia', ''),
(603, 48, 'Kelurahan Walandano', ''),
(604, 49, 'Kelurahan Boneoge', ''),
(605, 49, 'Kelurahan Boya', ''),
(606, 49, 'Kelurahan Ganti', ''),
(607, 49, 'Kelurahan Gunung Bale', ''),
(608, 49, 'Kelurahan Kabonga Besar', ''),
(609, 49, 'Kelurahan Kabonga Kecil', ''),
(610, 49, 'Kelurahan Labuan Bajo', ''),
(611, 49, 'Kelurahan Loli Dondo', ''),
(612, 49, 'Kelurahan Loli Pesua', ''),
(613, 49, 'Kelurahan Loli Saluran', ''),
(614, 49, 'Kelurahan Loli Tasibori (Tasiburi)', ''),
(615, 49, 'Kelurahan Lolioge', ''),
(616, 49, 'Kelurahan Maleni', ''),
(617, 49, 'Kelurahan Tanjung Batu', ''),
(618, 50, 'Kelurahan Bambarimi', ''),
(619, 50, 'Kelurahan Lalombi', ''),
(620, 50, 'Kelurahan Lumbu Tarombo', ''),
(621, 50, 'Kelurahan Lumbumamara', ''),
(622, 50, 'Kelurahan Mbuwu', ''),
(623, 50, 'Kelurahan Salumpaku', ''),
(624, 50, 'Kelurahan Salungkaenu', ''),
(625, 50, 'Kelurahan Surumana', ''),
(626, 50, 'Kelurahan Tanahmea', ''),
(627, 50, 'Kelurahan Tolongano', ''),
(628, 50, 'Kelurahan Tosale', ''),
(629, 50, 'Kelurahan Watatu', ''),
(630, 51, 'Kelurahan Kola-Kola', ''),
(631, 51, 'Kelurahan Limboro', ''),
(632, 51, 'Kelurahan LumbuDolo', ''),
(633, 51, 'Kelurahan Mekar Baru', ''),
(634, 51, 'Kelurahan Powelua', ''),
(635, 51, 'Kelurahan Salubomba', ''),
(636, 51, 'Kelurahan Towale', ''),
(637, 52, 'Kelurahan Budimukti', ''),
(638, 52, 'Kelurahan Bukit Harapan', ''),
(639, 52, 'Kelurahan Karyamukti', ''),
(640, 52, 'Kelurahan Kembayang (Kambayang)', ''),
(641, 52, 'Kelurahan Lemba Mukti', ''),
(642, 52, 'Kelurahan Malonas', ''),
(643, 52, 'Kelurahan Panii', ''),
(644, 52, 'Kelurahan Parisan Agung', ''),
(645, 52, 'Kelurahan Ponggerang', ''),
(646, 52, 'Kelurahan Rerang', ''),
(647, 52, 'Kelurahan Sabang', ''),
(648, 52, 'Kelurahan Sioyong', ''),
(649, 52, 'Kelurahan Talaga', ''),
(650, 53, 'Kelurahan Labuan Kungguma', ''),
(651, 53, 'Kelurahan Labuan Lelea', ''),
(652, 53, 'Kelurahan Labuan Panimba', ''),
(653, 53, 'Kelurahan Labuan Salumbone', ''),
(654, 53, 'Kelurahan Labuan ToPoso', ''),
(655, 54, 'Kelurahan Bambakaenu', ''),
(656, 54, 'Kelurahan Bambakanini', ''),
(657, 54, 'Kelurahan Dangaraa', ''),
(658, 54, 'Kelurahan Gimpubia', ''),
(659, 54, 'Kelurahan Palintuma', ''),
(660, 54, 'Kelurahan Tamodo', ''),
(661, 55, 'Kelurahan Bonemarawa', ''),
(662, 55, 'Kelurahan Bukit Indah', ''),
(663, 55, 'Kelurahan Lalundu', ''),
(664, 55, 'Kelurahan Mbulava', ''),
(665, 55, 'Kelurahan Minti Makmur', ''),
(666, 55, 'Kelurahan Ngovi Vacava', ''),
(667, 55, 'Kelurahan Panca Mukti', ''),
(668, 55, 'Kelurahan Pantalobete', ''),
(669, 55, 'Kelurahan Polando Jaya', ''),
(670, 55, 'Kelurahan Polanto Jaya', ''),
(671, 55, 'Kelurahan Rio Mukti', ''),
(672, 55, 'Kelurahan Tinauka', ''),
(673, 55, 'Kelurahan Toviora (Towiora)', ''),
(674, 55, 'Kelurahan Vacava', ''),
(675, 56, 'Kelurahan Amal', ''),
(676, 56, 'Kelurahan Dalaka', ''),
(677, 56, 'Kelurahan Enu', ''),
(678, 56, 'Kelurahan Kumbasa', ''),
(679, 56, 'Kelurahan Lero', ''),
(680, 56, 'Kelurahan Lero Tatari', ''),
(681, 56, 'Kelurahan Marana', ''),
(682, 56, 'Kelurahan Masaingi', ''),
(683, 56, 'Kelurahan Sumari', ''),
(684, 56, 'Kelurahan Taripa', ''),
(685, 56, 'Kelurahan Toaya', ''),
(686, 56, 'Kelurahan Toaya Vunta/Funta', ''),
(687, 57, 'Kelurahan Alindau', ''),
(688, 57, 'Kelurahan Oti', ''),
(689, 57, 'Kelurahan Sikara Tobata', ''),
(690, 57, 'Kelurahan Sipeso', ''),
(691, 57, 'Kelurahan Tamarenja', ''),
(692, 58, 'Kelurahan Batusuya', ''),
(693, 58, 'Kelurahan Kaliburu', ''),
(694, 58, 'Kelurahan Saloya', ''),
(695, 58, 'Kelurahan Tibo', ''),
(696, 59, 'Kelurahan Balintuma', ''),
(697, 59, 'Kelurahan Dampal', ''),
(698, 59, 'Kelurahan Jono oge', ''),
(699, 59, 'Kelurahan Lende', ''),
(700, 59, 'Kelurahan Lende Tovea', ''),
(701, 59, 'Kelurahan Lompio', ''),
(702, 59, 'Kelurahan Ombo', ''),
(703, 59, 'Kelurahan Sibado', ''),
(704, 59, 'Kelurahan Sipi', ''),
(705, 59, 'Kelurahan Tanjung Padang', ''),
(706, 59, 'Kelurahan Tompe', ''),
(707, 59, 'Kelurahan Tondo S', ''),
(708, 60, 'Kelurahan Balukang', ''),
(709, 60, 'Kelurahan Balukang II', ''),
(710, 60, 'Kelurahan Bou', ''),
(711, 60, 'Kelurahan Bukit Harapan', ''),
(712, 60, 'Kelurahan Panggalasiang (Pangalaseang)', ''),
(713, 60, 'Kelurahan Samalili', ''),
(714, 60, 'Kelurahan Siboalong (Siboang)', ''),
(715, 60, 'Kelurahan Siwalempu', ''),
(716, 60, 'Kelurahan Tonggolobibi', ''),
(717, 61, 'Kelurahan Lenju', ''),
(718, 61, 'Kelurahan Ogoamas I', ''),
(719, 61, 'Kelurahan Ogoamas II', ''),
(720, 61, 'Kelurahan Pesik', ''),
(721, 62, 'Kelurahan Bale', ''),
(722, 62, 'Kelurahan Guntarano', ''),
(723, 62, 'Kelurahan Nupa Bomba', ''),
(724, 62, 'Kelurahan Wani I', ''),
(725, 62, 'Kelurahan Wani II', ''),
(726, 62, 'Kelurahan Wombo Kalonggo', ''),
(727, 62, 'Kelurahan Wombo Mpanau (Panau)', ''),
(728, 63, 'Kelurahan Bahodopi', ''),
(729, 63, 'Kelurahan Bahomakmur', ''),
(730, 63, 'Kelurahan Bete Bete', ''),
(731, 63, 'Kelurahan Dampala', ''),
(732, 63, 'Kelurahan Fatufia', ''),
(733, 63, 'Kelurahan Keurea', ''),
(734, 63, 'Kelurahan Labota', ''),
(735, 63, 'Kelurahan Lalampu', ''),
(736, 63, 'Kelurahan Le-Le', ''),
(737, 63, 'Kelurahan Makarti Jaya', ''),
(738, 63, 'Kelurahan Padabaho', ''),
(739, 63, 'Kelurahan Siumbatu', ''),
(740, 64, 'Kelurahan Atananga', ''),
(741, 64, 'Kelurahan Bahonsuai', ''),
(742, 64, 'Kelurahan Beringin Jaya', ''),
(743, 64, 'Kelurahan Harapan Jaya', ''),
(744, 64, 'Kelurahan Karaupa', ''),
(745, 64, 'Kelurahan Lambelu', ''),
(746, 64, 'Kelurahan Lasampi', ''),
(747, 64, 'Kelurahan Limbo Makmur', ''),
(748, 64, 'Kelurahan Parilangke', ''),
(749, 64, 'Kelurahan Pebatae', ''),
(750, 64, 'Kelurahan Pebotoa', ''),
(751, 64, 'Kelurahan Samarenda', ''),
(752, 64, 'Kelurahan Umbele', ''),
(753, 65, 'Kelurahan Ambunu', ''),
(754, 65, 'Kelurahan Bahoea Reko Reko', ''),
(755, 65, 'Kelurahan Larobenu', ''),
(756, 65, 'Kelurahan Marga Mulya', ''),
(757, 65, 'Kelurahan Tondo', ''),
(758, 65, 'Kelurahan Topogaro (Tofogaro)', ''),
(759, 65, 'Kelurahan Uedago', ''),
(760, 65, 'Kelurahan Umpanga', ''),
(761, 65, 'Kelurahan Wata', ''),
(762, 65, 'Kelurahan Wosu', ''),
(763, 66, 'Kelurahan Bakala', ''),
(764, 66, 'Kelurahan Boelimau', ''),
(765, 66, 'Kelurahan Buajangka', ''),
(766, 66, 'Kelurahan Buleleng', ''),
(767, 66, 'Kelurahan Bungingkela', ''),
(768, 66, 'Kelurahan Bungintende', ''),
(769, 66, 'Kelurahan Buton', ''),
(770, 66, 'Kelurahan Jawi Jawi', ''),
(771, 66, 'Kelurahan Kaleroang', ''),
(772, 66, 'Kelurahan Koburu', ''),
(773, 66, 'Kelurahan Lafeu', ''),
(774, 66, 'Kelurahan Lakombulo (Lokombulo)', ''),
(775, 66, 'Kelurahan Lalemo', ''),
(776, 66, 'Kelurahan Lamontoli', ''),
(777, 66, 'Kelurahan Laroenai', ''),
(778, 66, 'Kelurahan One Ete', ''),
(779, 66, 'Kelurahan Padabale', ''),
(780, 66, 'Kelurahan Pado Pado', ''),
(781, 66, 'Kelurahan Paku', ''),
(782, 66, 'Kelurahan Panimbawang', ''),
(783, 66, 'Kelurahan Po\'o', ''),
(784, 66, 'Kelurahan Polewali', ''),
(785, 66, 'Kelurahan Pulau Bapa', ''),
(786, 66, 'Kelurahan Pulau Dua', ''),
(787, 66, 'Kelurahan Puungkeu', ''),
(788, 66, 'Kelurahan Sainoa', ''),
(789, 66, 'Kelurahan Sambalagi', ''),
(790, 66, 'Kelurahan Tanda Oleo', ''),
(791, 66, 'Kelurahan Tangofa', ''),
(792, 66, 'Kelurahan Torete', ''),
(793, 66, 'Kelurahan Umbele', ''),
(794, 66, 'Kelurahan Waru Waru', ''),
(795, 66, 'Kelurahan Werea', ''),
(796, 67, 'Kelurahan Bahomante', ''),
(797, 67, 'Kelurahan Bahomoahi', ''),
(798, 67, 'Kelurahan Bahomohoni', ''),
(799, 67, 'Kelurahan Bahomoleo', ''),
(800, 67, 'Kelurahan Bahomotefe', ''),
(801, 67, 'Kelurahan Bahontobungku', ''),
(802, 67, 'Kelurahan Bahoruru', ''),
(803, 67, 'Kelurahan Bente', ''),
(804, 67, 'Kelurahan Bungi', ''),
(805, 67, 'Kelurahan Geresa', ''),
(806, 67, 'Kelurahan Ipi', ''),
(807, 67, 'Kelurahan Kolono', ''),
(808, 67, 'Kelurahan Lahuafu', ''),
(809, 67, 'Kelurahan Lamberea', ''),
(810, 67, 'Kelurahan Lanona', ''),
(811, 67, 'Kelurahan Laroue', ''),
(812, 67, 'Kelurahan Marsaoleh', ''),
(813, 67, 'Kelurahan Matano', ''),
(814, 67, 'Kelurahan Matansala', ''),
(815, 67, 'Kelurahan Mendui', ''),
(816, 67, 'Kelurahan Nambo', ''),
(817, 67, 'Kelurahan Onepute Jaya', ''),
(818, 67, 'Kelurahan Puungkoilu', ''),
(819, 67, 'Kelurahan Sakita', ''),
(820, 67, 'Kelurahan Tofoiso', ''),
(821, 67, 'Kelurahan Tofuti', ''),
(822, 67, 'Kelurahan Tudua', ''),
(823, 67, 'Kelurahan Ululere', ''),
(824, 67, 'Kelurahan Unsongi', ''),
(825, 68, 'Kelurahan Baturube', ''),
(826, 68, 'Kelurahan Boba', ''),
(827, 68, 'Kelurahan Kalombang', ''),
(828, 68, 'Kelurahan Lemo', ''),
(829, 68, 'Kelurahan Matube', ''),
(830, 68, 'Kelurahan Opo', ''),
(831, 68, 'Kelurahan Posangke', ''),
(832, 68, 'Kelurahan Salubiro', ''),
(833, 68, 'Kelurahan Siliti', ''),
(834, 68, 'Kelurahan Tambarobone', ''),
(835, 68, 'Kelurahan Tanaku Raya', ''),
(836, 68, 'Kelurahan Taronggo', ''),
(837, 68, 'Kelurahan Tirongan Atas', ''),
(838, 68, 'Kelurahan Tirongan Bawah', ''),
(839, 68, 'Kelurahan Tokala Atas', ''),
(840, 68, 'Kelurahan Tokonanaka', ''),
(841, 68, 'Kelurahan Uemasi', ''),
(842, 68, 'Kelurahan Ueruru', ''),
(843, 68, 'Kelurahan Uewajo', ''),
(844, 68, 'Kelurahan Woomparigi', ''),
(845, 69, 'Kelurahan Beteleme', ''),
(846, 69, 'Kelurahan Bintangor Mukti', ''),
(847, 69, 'Kelurahan Dolupo Karya', ''),
(848, 69, 'Kelurahan Jamor Jaya', ''),
(849, 69, 'Kelurahan Koro Walelo', ''),
(850, 69, 'Kelurahan Korobonde', ''),
(851, 69, 'Kelurahan Korompeeli', ''),
(852, 69, 'Kelurahan Korowu (Korowou)', ''),
(853, 69, 'Kelurahan Kumpi', ''),
(854, 69, 'Kelurahan Lembobaru', ''),
(855, 69, 'Kelurahan Lembobelala', ''),
(856, 69, 'Kelurahan Lemboroma', ''),
(857, 69, 'Kelurahan Mandula', ''),
(858, 69, 'Kelurahan Mora', ''),
(859, 69, 'Kelurahan Paawaru', ''),
(860, 69, 'Kelurahan Petumbea', ''),
(861, 69, 'Kelurahan Pontangoa', ''),
(862, 69, 'Kelurahan Poona', ''),
(863, 69, 'Kelurahan Ronta', ''),
(864, 69, 'Kelurahan Tingkeao', ''),
(865, 69, 'Kelurahan Tinompo', ''),
(866, 69, 'Kelurahan Uluanso', ''),
(867, 69, 'Kelurahan Waraa', ''),
(868, 69, 'Kelurahan Wawopada', ''),
(869, 70, 'Kelurahan Girimulya', ''),
(870, 70, 'Kelurahan Kolo Atas', ''),
(871, 70, 'Kelurahan Kolo Bawah', ''),
(872, 70, 'Kelurahan Lijo', ''),
(873, 70, 'Kelurahan Menyoe', ''),
(874, 70, 'Kelurahan Minangobino (Winangabino)', ''),
(875, 70, 'Kelurahan Momo', ''),
(876, 70, 'Kelurahan Pandauke', ''),
(877, 70, 'Kelurahan Parangisi', ''),
(878, 70, 'Kelurahan Sea', ''),
(879, 70, 'Kelurahan Tambale (Thambale)', ''),
(880, 70, 'Kelurahan Tana Sumpu', ''),
(881, 70, 'Kelurahan Tananagaya', ''),
(882, 70, 'Kelurahan Uepakatu', ''),
(883, 70, 'Kelurahan Winangabino', ''),
(884, 71, 'Kelurahan Buranga', ''),
(885, 71, 'Kelurahan Dongkalang', ''),
(886, 71, 'Kelurahan Kofalagadi', ''),
(887, 71, 'Kelurahan Masadian', ''),
(888, 71, 'Kelurahan Matano', ''),
(889, 71, 'Kelurahan Matarape', ''),
(890, 71, 'Kelurahan Morompaitonga', ''),
(891, 71, 'Kelurahan Ngapaea', ''),
(892, 71, 'Kelurahan Padalaa', ''),
(893, 71, 'Kelurahan Padei Darat', ''),
(894, 71, 'Kelurahan Padei Laut', ''),
(895, 71, 'Kelurahan Pulau Tiga', ''),
(896, 71, 'Kelurahan Samarengga', ''),
(897, 71, 'Kelurahan Tanjung Harapan', ''),
(898, 71, 'Kelurahan Terebino', ''),
(899, 71, 'Kelurahan Torukuno', ''),
(900, 71, 'Kelurahan Ulunambo', ''),
(901, 71, 'Kelurahan Ulunipa', ''),
(902, 71, 'Kelurahan Wawongkolono', ''),
(903, 72, 'Kelurahan Ensa', ''),
(904, 72, 'Kelurahan Era', ''),
(905, 72, 'Kelurahan Gontara', ''),
(906, 72, 'Kelurahan Kasingoli', ''),
(907, 72, 'Kelurahan Kolaka', ''),
(908, 72, 'Kelurahan Lanumor', ''),
(909, 72, 'Kelurahan Lee', ''),
(910, 72, 'Kelurahan Lembontonara', ''),
(911, 72, 'Kelurahan Londi', ''),
(912, 72, 'Kelurahan Mayumba', ''),
(913, 72, 'Kelurahan Peleru', ''),
(914, 72, 'Kelurahan Peonea', ''),
(915, 72, 'Kelurahan Saemba', ''),
(916, 72, 'Kelurahan Tabarano', ''),
(917, 72, 'Kelurahan Taende', ''),
(918, 72, 'Kelurahan Tamonjengi', ''),
(919, 72, 'Kelurahan Tiwaa', ''),
(920, 72, 'Kelurahan Tomata', ''),
(921, 72, 'Kelurahan Tomoi Karya', ''),
(922, 72, 'Kelurahan Wawondula', ''),
(923, 73, 'Kelurahan Bahontula', ''),
(924, 73, 'Kelurahan Bahoue', ''),
(925, 73, 'Kelurahan Bimor/Binor Jaya', ''),
(926, 73, 'Kelurahan Bungintimbe', ''),
(927, 73, 'Kelurahan Bunta', ''),
(928, 73, 'Kelurahan Ganda Ganda', ''),
(929, 73, 'Kelurahan Gililana', ''),
(930, 73, 'Kelurahan Kolonodale', ''),
(931, 73, 'Kelurahan Korololaki', ''),
(932, 73, 'Kelurahan Korololama', ''),
(933, 73, 'Kelurahan Koromatantu', ''),
(934, 73, 'Kelurahan Koya', ''),
(935, 73, 'Kelurahan Maralee', ''),
(936, 73, 'Kelurahan Masara', ''),
(937, 73, 'Kelurahan Mohoni', ''),
(938, 73, 'Kelurahan Moleono', ''),
(939, 73, 'Kelurahan Molino', ''),
(940, 73, 'Kelurahan Mondowe', ''),
(941, 73, 'Kelurahan Moroles (Molores)', ''),
(942, 73, 'Kelurahan Onepute', ''),
(943, 73, 'Kelurahan Polewali', ''),
(944, 73, 'Kelurahan Sampalowo', ''),
(945, 73, 'Kelurahan Tanauge', ''),
(946, 73, 'Kelurahan Tiu', ''),
(947, 73, 'Kelurahan Togomulyo', ''),
(948, 73, 'Kelurahan Tompira', ''),
(949, 73, 'Kelurahan Tontowea', ''),
(950, 73, 'Kelurahan Towara', ''),
(951, 73, 'Kelurahan Ungkea', ''),
(952, 74, 'Kelurahan Bau', ''),
(953, 74, 'Kelurahan Lembah Sumara', ''),
(954, 74, 'Kelurahan Malino', ''),
(955, 74, 'Kelurahan Malino Jaya', ''),
(956, 74, 'Kelurahan Panca Makmur', ''),
(957, 74, 'Kelurahan Sumara Jaya', ''),
(958, 74, 'Kelurahan Tamainusi', ''),
(959, 74, 'Kelurahan Tambayoli', ''),
(960, 74, 'Kelurahan Tandoyondo', ''),
(961, 75, 'Kelurahan Bumi Harapan', ''),
(962, 75, 'Kelurahan Emea', ''),
(963, 75, 'Kelurahan Karaupa', ''),
(964, 75, 'Kelurahan Lantula Jaya', ''),
(965, 75, 'Kelurahan Margajaya', ''),
(966, 75, 'Kelurahan Moahino', ''),
(967, 75, 'Kelurahan Puntari Makmur', ''),
(968, 75, 'Kelurahan Sampeantaba', ''),
(969, 75, 'Kelurahan Solonsa', ''),
(970, 75, 'Kelurahan Solonsa Jaya', ''),
(971, 75, 'Kelurahan Umbele', ''),
(972, 75, 'Kelurahan Ungkaya', ''),
(973, 76, 'Kelurahan Ampibabo', ''),
(974, 76, 'Kelurahan Ampibabo Utara', ''),
(975, 76, 'Kelurahan Buranga', ''),
(976, 76, 'Kelurahan Lemo', ''),
(977, 76, 'Kelurahan Paranggi', ''),
(978, 76, 'Kelurahan Sidole', ''),
(979, 76, 'Kelurahan Tamanpedagi', ''),
(980, 76, 'Kelurahan Toga', ''),
(981, 76, 'Kelurahan Tolole', ''),
(982, 76, 'Kelurahan Tombi', ''),
(983, 77, 'Kelurahan Balinggi', ''),
(984, 77, 'Kelurahan Balinggi Jati', ''),
(985, 77, 'Kelurahan Beraban', ''),
(986, 77, 'Kelurahan Malakosa', ''),
(987, 77, 'Kelurahan Suli', ''),
(988, 77, 'Kelurahan Suli Indah', ''),
(989, 78, 'Kelurahan Bajo', ''),
(990, 78, 'Kelurahan Beringin Jaya', ''),
(991, 78, 'Kelurahan Bolano', ''),
(992, 78, 'Kelurahan Bolano Barat', ''),
(993, 78, 'Kelurahan Karya Mandiri', ''),
(994, 78, 'Kelurahan Kotanagaya', ''),
(995, 78, 'Kelurahan Lambunu', ''),
(996, 78, 'Kelurahan Lambunu Utara', ''),
(997, 78, 'Kelurahan Malino', ''),
(998, 78, 'Kelurahan Margapura', ''),
(999, 78, 'Kelurahan Ongka', ''),
(1000, 78, 'Kelurahan Persatuan Sejati', ''),
(1001, 78, 'Kelurahan Petuna Sugi', ''),
(1002, 78, 'Kelurahan Santigi', ''),
(1003, 78, 'Kelurahan Sritabaang', ''),
(1004, 78, 'Kelurahan Tabolo-bolo', ''),
(1005, 78, 'Kelurahan Tinombala', ''),
(1006, 78, 'Kelurahan Wanagading', ''),
(1007, 78, 'Kelurahan Wanamukti', ''),
(1008, 79, 'Kelurahan Donggulu', ''),
(1009, 79, 'Kelurahan Kasimbar', ''),
(1010, 79, 'Kelurahan Kasimbar Barat', ''),
(1011, 79, 'Kelurahan Kasimbar Selatan', ''),
(1012, 79, 'Kelurahan Laemanta', ''),
(1013, 79, 'Kelurahan Poso na', ''),
(1014, 79, 'Kelurahan Silampayang', ''),
(1015, 79, 'Kelurahan Tavalo', ''),
(1016, 80, 'Kelurahan Bugis', ''),
(1017, 80, 'Kelurahan Kayu Agung', ''),
(1018, 80, 'Kelurahan Kotaraya', ''),
(1019, 80, 'Kelurahan Kotaraya Timur', ''),
(1020, 80, 'Kelurahan Mensung', ''),
(1021, 80, 'Kelurahan Mepanga', ''),
(1022, 80, 'Kelurahan Moubang', ''),
(1023, 80, 'Kelurahan Ogobayas', ''),
(1024, 80, 'Kelurahan Ogotion', ''),
(1025, 80, 'Kelurahan Sumber Agung', ''),
(1026, 81, 'Kelurahan Aedan Raya', ''),
(1027, 81, 'Kelurahan Bolaung Olonggata', ''),
(1028, 81, 'Kelurahan Gio', ''),
(1029, 81, 'Kelurahan Lobu', ''),
(1030, 81, 'Kelurahan Moutong Barat', ''),
(1031, 81, 'Kelurahan Moutong Tengah', ''),
(1032, 81, 'Kelurahan Moutong Timur', ''),
(1033, 81, 'Kelurahan Pandelalap (Lalap)', ''),
(1034, 81, 'Kelurahan Salum Pengut', ''),
(1035, 81, 'Kelurahan Sejoli', ''),
(1036, 81, 'Kelurahan Tuladenggi Pantai', ''),
(1037, 82, 'Kelurahan Bobalo', ''),
(1038, 82, 'Kelurahan Dongkalan', ''),
(1039, 82, 'Kelurahan Eeya', ''),
(1040, 82, 'Kelurahan Palasa', ''),
(1041, 82, 'Kelurahan Palasa Lambori', ''),
(1042, 82, 'Kelurahan Palasa Tangki', ''),
(1043, 82, 'Kelurahan Pebounang', ''),
(1044, 82, 'Kelurahan Ulatan', ''),
(1045, 83, 'Kelurahan Bambalemo', ''),
(1046, 83, 'Kelurahan Bantaya', ''),
(1047, 83, 'Kelurahan Kampal', ''),
(1048, 83, 'Kelurahan Lebo', ''),
(1049, 83, 'Kelurahan Loji', ''),
(1050, 83, 'Kelurahan Maesa', ''),
(1051, 83, 'Kelurahan Masigi', ''),
(1052, 83, 'Kelurahan Mertasari', ''),
(1053, 83, 'Kelurahan Olaya', ''),
(1054, 83, 'Kelurahan Parigimpu', ''),
(1055, 83, 'Kelurahan Pombalowo (Pembalowo)', ''),
(1056, 84, 'Kelurahan Air Panas', ''),
(1057, 84, 'Kelurahan Baliara', ''),
(1058, 84, 'Kelurahan Jono Kalora', ''),
(1059, 84, 'Kelurahan Kayuboko', ''),
(1060, 85, 'Kelurahan Boyantongo', ''),
(1061, 85, 'Kelurahan Dolago', ''),
(1062, 85, 'Kelurahan Lemusa', ''),
(1063, 85, 'Kelurahan Masari', ''),
(1064, 85, 'Kelurahan Nambaru', ''),
(1065, 85, 'Kelurahan Olobaru', ''),
(1066, 85, 'Kelurahan Sumber Sari', ''),
(1067, 85, 'Kelurahan Tindaki', ''),
(1068, 86, 'Kelurahan Binangga', ''),
(1069, 86, 'Kelurahan Jononunu', ''),
(1070, 86, 'Kelurahan Matolele', ''),
(1071, 86, 'Kelurahan Pelawa', ''),
(1072, 86, 'Kelurahan Pelawa Baru', ''),
(1073, 86, 'Kelurahan Petapa', ''),
(1074, 87, 'Kelurahan Avolua', ''),
(1075, 87, 'Kelurahan Pangi', ''),
(1076, 87, 'Kelurahan Sakina Jaya', ''),
(1077, 87, 'Kelurahan Toboli', ''),
(1078, 87, 'Kelurahan Toboli Barat', ''),
(1079, 88, 'Kelurahan Maleali (Maleyali)', ''),
(1080, 88, 'Kelurahan Sausu Gandasari', ''),
(1081, 88, 'Kelurahan Sausu Pakareme', ''),
(1082, 88, 'Kelurahan Sausu Piore', ''),
(1083, 88, 'Kelurahan Sausu Salubanga', ''),
(1084, 88, 'Kelurahan Sausu Taliabo', ''),
(1085, 88, 'Kelurahan Sausu Torono', ''),
(1086, 88, 'Kelurahan Sausu Trans', ''),
(1087, 89, 'Kelurahan Marantale', ''),
(1088, 89, 'Kelurahan Silanga', ''),
(1089, 89, 'Kelurahan Siniu', ''),
(1090, 89, 'Kelurahan Tandaigi', ''),
(1091, 89, 'Kelurahan Toraranga', ''),
(1092, 89, 'Kelurahan Towera', ''),
(1093, 90, 'Kelurahan Karya Agung', ''),
(1094, 90, 'Kelurahan Nunurantai', ''),
(1095, 90, 'Kelurahan Palapi', ''),
(1096, 90, 'Kelurahan Taopa', ''),
(1097, 90, 'Kelurahan Taopa Utara', ''),
(1098, 90, 'Kelurahan Tompo', ''),
(1099, 90, 'Kelurahan Tuladenggi Sibatang', ''),
(1100, 91, 'Kelurahan Bainaa', ''),
(1101, 91, 'Kelurahan Bondoyong', ''),
(1102, 91, 'Kelurahan Dongkas', ''),
(1103, 91, 'Kelurahan Dusunan', ''),
(1104, 91, 'Kelurahan Lombok', ''),
(1105, 91, 'Kelurahan Ogoalas', ''),
(1106, 91, 'Kelurahan Sidoan', ''),
(1107, 91, 'Kelurahan Sidoan Barat', ''),
(1108, 91, 'Kelurahan Sidoan Selatan', ''),
(1109, 91, 'Kelurahan Sipayo', ''),
(1110, 91, 'Kelurahan Tibu', ''),
(1111, 91, 'Kelurahan Tinombo', ''),
(1112, 92, 'Kelurahan Khatulistiwa', ''),
(1113, 92, 'Kelurahan Malanggo (Malggo)', ''),
(1114, 92, 'Kelurahan Maninili', ''),
(1115, 92, 'Kelurahan Polly', ''),
(1116, 92, 'Kelurahan Siaga', ''),
(1117, 92, 'Kelurahan Sigenti', ''),
(1118, 92, 'Kelurahan Sigenti Barat', ''),
(1119, 92, 'Kelurahan Sigenti Bersehati', ''),
(1120, 92, 'Kelurahan Sigenti Selatan', ''),
(1121, 92, 'Kelurahan Silutung', ''),
(1122, 92, 'Kelurahan Sinei', ''),
(1123, 92, 'Kelurahan Tada', ''),
(1124, 92, 'Kelurahan Tada Selatan', ''),
(1125, 92, 'Kelurahan Tada Timur', ''),
(1126, 93, 'Kelurahan Ambesia', ''),
(1127, 93, 'Kelurahan Ambesia Selatan', ''),
(1128, 93, 'Kelurahan Biga', ''),
(1129, 93, 'Kelurahan Ogotomubu (Ogotumubu)', ''),
(1130, 93, 'Kelurahan Tilung', ''),
(1131, 93, 'Kelurahan Tingkulang', ''),
(1132, 93, 'Kelurahan Tomini', ''),
(1133, 93, 'Kelurahan Tomini Barat', ''),
(1134, 93, 'Kelurahan Tomini Utara', ''),
(1135, 94, 'Kelurahan Pinotu', ''),
(1136, 94, 'Kelurahan Sibolago', ''),
(1137, 94, 'Kelurahan Sienjo', ''),
(1138, 94, 'Kelurahan Singura', ''),
(1139, 94, 'Kelurahan Tomili Selatan', ''),
(1140, 94, 'Kelurahan Tomoli', ''),
(1141, 94, 'Kelurahan Toribulu', ''),
(1142, 95, 'Kelurahan Astina', ''),
(1143, 95, 'Kelurahan Purwosari', ''),
(1144, 95, 'Kelurahan Tanah Lanto', ''),
(1145, 95, 'Kelurahan Tolai', ''),
(1146, 95, 'Kelurahan Tolai Barat', ''),
(1147, 95, 'Kelurahan Torue', ''),
(1148, 96, 'Kelurahan Bategencu', ''),
(1149, 96, 'Kelurahan Labuan', ''),
(1150, 96, 'Kelurahan Malei', ''),
(1151, 96, 'Kelurahan Maliwuko', ''),
(1152, 96, 'Kelurahan Pandiri', ''),
(1153, 96, 'Kelurahan Sepe', ''),
(1154, 96, 'Kelurahan Silanca', ''),
(1155, 96, 'Kelurahan Sintuwulemba', ''),
(1156, 96, 'Kelurahan Tagolu', ''),
(1157, 96, 'Kelurahan Tambaro', ''),
(1158, 96, 'Kelurahan Tampemadoro', ''),
(1159, 96, 'Kelurahan Tiyado (Toyado)', ''),
(1160, 96, 'Kelurahan Tongko', ''),
(1161, 96, 'Kelurahan Watuawu', ''),
(1162, 97, 'Kelurahan Kageroa', ''),
(1163, 97, 'Kelurahan Kolori', ''),
(1164, 97, 'Kelurahan Lelio', ''),
(1165, 97, 'Kelurahan Lengkeka', ''),
(1166, 97, 'Kelurahan Tomehipi', ''),
(1167, 97, 'Kelurahan Tuare', ''),
(1168, 98, 'Kelurahan Betue', ''),
(1169, 98, 'Kelurahan Kaduwaa', ''),
(1170, 98, 'Kelurahan Siliwanga', ''),
(1171, 98, 'Kelurahan Talabosa', ''),
(1172, 98, 'Kelurahan Wanga', ''),
(1173, 98, 'Kelurahan Watutau', ''),
(1174, 99, 'Kelurahan Badangkaiya (Badangkaia)', ''),
(1175, 99, 'Kelurahan Bakekau', ''),
(1176, 99, 'Kelurahan Bewa', ''),
(1177, 99, 'Kelurahan Bomba', ''),
(1178, 99, 'Kelurahan Bulili', ''),
(1179, 99, 'Kelurahan Gintu', ''),
(1180, 99, 'Kelurahan Pada', ''),
(1181, 99, 'Kelurahan Runde', ''),
(1182, 100, 'Kelurahan Baleura (Baliura)', ''),
(1183, 100, 'Kelurahan Bariri', ''),
(1184, 100, 'Kelurahan Katu', ''),
(1185, 100, 'Kelurahan Rompo', ''),
(1186, 100, 'Kelurahan Torire', ''),
(1187, 100, 'Kelurahan Doda', ''),
(1188, 100, 'Kelurahan Hanggira', ''),
(1189, 100, 'Kelurahan Lempe', ''),
(1190, 101, 'Kelurahan Maholo', ''),
(1191, 101, 'Kelurahan Mekar Sari', ''),
(1192, 101, 'Kelurahan Tamadue', ''),
(1193, 101, 'Kelurahan Winowanga', ''),
(1194, 102, 'Kelurahan Alitupu', ''),
(1195, 102, 'Kelurahan Bumi Banyubaru', ''),
(1196, 102, 'Kelurahan Bumi Banyusari', ''),
(1197, 102, 'Kelurahan DoDolo', ''),
(1198, 102, 'Kelurahan Sedoa', ''),
(1199, 102, 'Kelurahan Watumaeta', ''),
(1200, 102, 'Kelurahan Wuasa', ''),
(1201, 103, 'Kelurahan Toinasa', ''),
(1202, 103, 'Kelurahan Uranosari', ''),
(1203, 103, 'Kelurahan Meko', ''),
(1204, 103, 'Kelurahan Salukaiya (Salukaia)', ''),
(1205, 103, 'Kelurahan Taipa', ''),
(1206, 104, 'Kelurahan Bancea', ''),
(1207, 104, 'Kelurahan Bangun Jaya', ''),
(1208, 104, 'Kelurahan Boe', ''),
(1209, 104, 'Kelurahan Maya Jaya', ''),
(1210, 104, 'Kelurahan Maya Sari', ''),
(1211, 104, 'Kelurahan Mayoa', ''),
(1212, 104, 'Kelurahan Panda Jaya', ''),
(1213, 104, 'Kelurahan Pandayora', ''),
(1214, 104, 'Kelurahan Panjo', ''),
(1215, 104, 'Kelurahan Pasir Putih', ''),
(1216, 104, 'Kelurahan PenDolo', ''),
(1217, 104, 'Kelurahan Uelene', ''),
(1218, 105, 'Kelurahan Amporiwo', ''),
(1219, 105, 'Kelurahan Barati', ''),
(1220, 105, 'Kelurahan Korobono', ''),
(1221, 105, 'Kelurahan Salindu', ''),
(1222, 105, 'Kelurahan Singkona', ''),
(1223, 105, 'Kelurahan Tindoli', ''),
(1224, 105, 'Kelurahan Tokilo', ''),
(1225, 105, 'Kelurahan Tolambo', ''),
(1226, 105, 'Kelurahan Wayura', ''),
(1227, 106, 'Kelurahan Didiri (Ompo Didiri)', ''),
(1228, 106, 'Kelurahan Kancuu', ''),
(1229, 106, 'Kelurahan Kelei', ''),
(1230, 106, 'Kelurahan Matialemba', ''),
(1231, 106, 'Kelurahan Poleganyara', ''),
(1232, 106, 'Kelurahan Tiu', ''),
(1233, 106, 'Kelurahan Kamba', ''),
(1234, 106, 'Kelurahan Mesewe (Masewe)', ''),
(1235, 106, 'Kelurahan Olumokunde', ''),
(1236, 106, 'Kelurahan Pancasila', ''),
(1237, 106, 'Kelurahan Petiro', ''),
(1238, 106, 'Kelurahan Taripa', ''),
(1239, 107, 'Kelurahan Buyumpondoli', ''),
(1240, 107, 'Kelurahan Dulumai', ''),
(1241, 107, 'Kelurahan Kuku', ''),
(1242, 107, 'Kelurahan Leboni', ''),
(1243, 107, 'Kelurahan Lena', ''),
(1244, 107, 'Kelurahan Mayakeli', ''),
(1245, 107, 'Kelurahan Pamona', ''),
(1246, 107, 'Kelurahan Panjoka', ''),
(1247, 107, 'Kelurahan Petirodongi', ''),
(1248, 107, 'Kelurahan Peura', ''),
(1249, 107, 'Kelurahan Sangele', ''),
(1250, 107, 'Kelurahan Sangira (Sabgira)', ''),
(1251, 107, 'Kelurahan Saojo', ''),
(1252, 107, 'Kelurahan Sawidago', ''),
(1253, 107, 'Kelurahan Soe', ''),
(1254, 107, 'Kelurahan Sulewana', ''),
(1255, 107, 'Kelurahan Tendeadongi', ''),
(1256, 107, 'Kelurahan Tentena', ''),
(1257, 107, 'Kelurahan Tonusu', ''),
(1258, 107, 'Kelurahan Uelincu', ''),
(1259, 108, 'Kelurahan Kayamanya', ''),
(1260, 108, 'Kelurahan Moengko Baru', ''),
(1261, 108, 'Kelurahan Moengko Lama', ''),
(1262, 108, 'Kelurahan Gebang Rejo', ''),
(1263, 109, 'Kelurahan Sayo', ''),
(1264, 109, 'Kelurahan Bukit Bambu', ''),
(1265, 109, 'Kelurahan Lembamawo (Lembomawo)', ''),
(1266, 109, 'Kelurahan Kawua', ''),
(1267, 109, 'Kelurahan Ranononcu', ''),
(1268, 110, 'Kelurahan Kasintuwu', ''),
(1269, 110, 'Kelurahan Lombogia (Lombugia)', ''),
(1270, 110, 'Kelurahan Madale', ''),
(1271, 110, 'Kelurahan Lawanga', ''),
(1272, 110, 'Kelurahan Tegal Rejo', ''),
(1273, 110, 'Kelurahan Bonesompe', ''),
(1274, 111, 'Kelurahan Bega', ''),
(1275, 111, 'Kelurahan Betania', ''),
(1276, 111, 'Kelurahan Kasiguncu', ''),
(1277, 111, 'Kelurahan Lanto Jaya', ''),
(1278, 111, 'Kelurahan Lape', ''),
(1279, 111, 'Kelurahan Mapane', ''),
(1280, 111, 'Kelurahan Masamba', ''),
(1281, 111, 'Kelurahan Masani', ''),
(1282, 111, 'Kelurahan Pinedapa', ''),
(1283, 111, 'Kelurahan Saatu', ''),
(1284, 111, 'Kelurahan Toini', ''),
(1285, 111, 'Kelurahan Tokorondo', ''),
(1286, 111, 'Kelurahan Towu (Tiwaa)', ''),
(1287, 112, 'Kelurahan Betalemba', ''),
(1288, 112, 'Kelurahan Dewua', ''),
(1289, 112, 'Kelurahan Malitu', ''),
(1290, 112, 'Kelurahan Padalembara', ''),
(1291, 112, 'Kelurahan Pantangolemba', ''),
(1292, 112, 'Kelurahan Patiwunga', ''),
(1293, 112, 'Kelurahan Sangginora', ''),
(1294, 112, 'Kelurahan Tangkura', ''),
(1295, 113, 'Kelurahan Bakti Agung', ''),
(1296, 113, 'Kelurahan Kalora', ''),
(1297, 113, 'Kelurahan Kawende', ''),
(1298, 113, 'Kelurahan Kilo', ''),
(1299, 113, 'Kelurahan Mambuke', ''),
(1300, 113, 'Kelurahan Tambarana', ''),
(1301, 113, 'Kelurahan Tobe', ''),
(1302, 113, 'Kelurahan Tri Mulya', ''),
(1303, 113, 'Kelurahan Tumora', ''),
(1304, 114, 'Kelurahan Kabobona', ''),
(1305, 114, 'Kelurahan Karawana', ''),
(1306, 114, 'Kelurahan Kota Pulu', ''),
(1307, 114, 'Kelurahan Kotarindau', ''),
(1308, 114, 'Kelurahan Langleso (Langaleso)', ''),
(1309, 114, 'Kelurahan Maku', ''),
(1310, 114, 'Kelurahan Potoya', ''),
(1311, 114, 'Kelurahan Soulowe', ''),
(1312, 114, 'Kelurahan Tulo', ''),
(1313, 114, 'Kelurahan Watubula', ''),
(1314, 114, 'Kelurahan Waturalele', ''),
(1315, 115, 'Kelurahan Balamoa', ''),
(1316, 115, 'Kelurahan Balumpewa', ''),
(1317, 115, 'Kelurahan Bobo', ''),
(1318, 115, 'Kelurahan Kaleke', ''),
(1319, 115, 'Kelurahan Kalukutinggu', ''),
(1320, 115, 'Kelurahan Mantikole', ''),
(1321, 115, 'Kelurahan Pesaku', ''),
(1322, 115, 'Kelurahan Pewunu', ''),
(1323, 115, 'Kelurahan Rarampadende', ''),
(1324, 115, 'Kelurahan Sibonu', ''),
(1325, 116, 'Kelurahan Balongga', ''),
(1326, 116, 'Kelurahan Baluase', ''),
(1327, 116, 'Kelurahan Bangga', ''),
(1328, 116, 'Kelurahan Bulubete', ''),
(1329, 116, 'Kelurahan Jono', ''),
(1330, 116, 'Kelurahan Pulu', ''),
(1331, 116, 'Kelurahan Rogo', ''),
(1332, 116, 'Kelurahan Sambo', ''),
(1333, 116, 'Kelurahan Walatana', ''),
(1334, 116, 'Kelurahan Wisolo', ''),
(1335, 117, 'Kelurahan Kalawara', ''),
(1336, 117, 'Kelurahan Omu', ''),
(1337, 117, 'Kelurahan Pakuli', ''),
(1338, 117, 'Kelurahan Pandere', ''),
(1339, 117, 'Kelurahan Simoro', ''),
(1340, 117, 'Kelurahan Tuva (Tuwa)', ''),
(1341, 118, 'Kelurahan Balane', ''),
(1342, 118, 'Kelurahan Bolobia', ''),
(1343, 118, 'Kelurahan Daenggune', ''),
(1344, 118, 'Kelurahan Doda', ''),
(1345, 118, 'Kelurahan Kalora', ''),
(1346, 118, 'Kelurahan Kanuna', ''),
(1347, 118, 'Kelurahan Porame', ''),
(1348, 118, 'Kelurahan Rondingo', ''),
(1349, 118, 'Kelurahan Uwemanje', ''),
(1350, 119, 'Kelurahan Banggai ba', ''),
(1351, 119, 'Kelurahan Boladangko', ''),
(1352, 119, 'Kelurahan Bolapapu', ''),
(1353, 119, 'Kelurahan Lonca', ''),
(1354, 119, 'Kelurahan Mataue', ''),
(1355, 119, 'Kelurahan Namo', ''),
(1356, 119, 'Kelurahan Rantewulu', ''),
(1357, 119, 'Kelurahan Salua', ''),
(1358, 119, 'Kelurahan Siwongi', ''),
(1359, 119, 'Kelurahan Sungku', ''),
(1360, 119, 'Kelurahan Tangkulowi', ''),
(1361, 119, 'Kelurahan Toro', ''),
(1362, 119, 'Kelurahan Towulu', ''),
(1363, 119, 'Kelurahan Winatu', ''),
(1364, 120, 'Kelurahan Gimpu', ''),
(1365, 120, 'Kelurahan Lawua', ''),
(1366, 120, 'Kelurahan Lempelero', ''),
(1367, 120, 'Kelurahan Moa', ''),
(1368, 120, 'Kelurahan O\'o', ''),
(1369, 120, 'Kelurahan Palamaki', ''),
(1370, 120, 'Kelurahan Palimakijawa (Pilimakujawa/Makujawa)', ''),
(1371, 120, 'Kelurahan Salutome', ''),
(1372, 120, 'Kelurahan Tompi Bugis', ''),
(1373, 120, 'Kelurahan Tomua', ''),
(1374, 120, 'Kelurahan Wangka', ''),
(1375, 120, 'Kelurahan Watukilo', ''),
(1376, 121, 'Kelurahan Anca', ''),
(1377, 121, 'Kelurahan Langko', ''),
(1378, 121, 'Kelurahan Puro/Puroo', ''),
(1379, 121, 'Kelurahan Tomado', ''),
(1380, 122, 'Kelurahan Baliase', ''),
(1381, 122, 'Kelurahan Beka', ''),
(1382, 122, 'Kelurahan Binangga', ''),
(1383, 122, 'Kelurahan Bomba', ''),
(1384, 122, 'Kelurahan Boyabaliase', ''),
(1385, 122, 'Kelurahan Lebanu', ''),
(1386, 122, 'Kelurahan Padende', ''),
(1387, 122, 'Kelurahan Sibedi', ''),
(1388, 122, 'Kelurahan Sunju', ''),
(1389, 122, 'Kelurahan Tinggede', ''),
(1390, 122, 'Kelurahan Tinggede Selatan', ''),
(1391, 123, 'Kelurahan Dombu', ''),
(1392, 123, 'Kelurahan Lewara', ''),
(1393, 123, 'Kelurahan Lumbulama', ''),
(1394, 123, 'Kelurahan Malino', ''),
(1395, 123, 'Kelurahan Matantimali', ''),
(1396, 123, 'Kelurahan Ongulara', ''),
(1397, 123, 'Kelurahan Ongulero', ''),
(1398, 123, 'Kelurahan Panesibaja', ''),
(1399, 123, 'Kelurahan Pobolobia', ''),
(1400, 123, 'Kelurahan Soi', ''),
(1401, 123, 'Kelurahan Taipanggabe', ''),
(1402, 123, 'Kelurahan Wayu', '');
INSERT INTO `kelurahan` (`id`, `id_kecamatan`, `name`, `deskripsi`) VALUES
(1403, 123, 'Kelurahan Wiapore', ''),
(1404, 123, 'Kelurahan Wugaga (Wawugaga)', ''),
(1405, 124, 'Kelurahan Bulili', ''),
(1406, 124, 'Kelurahan Kadidia', ''),
(1407, 124, 'Kelurahan Kamarora A', ''),
(1408, 124, 'Kelurahan Kamarora B', ''),
(1409, 124, 'Kelurahan Sopu', ''),
(1410, 125, 'Kelurahan Ampera', ''),
(1411, 125, 'Kelurahan Bahagia', ''),
(1412, 125, 'Kelurahan Baku-Bakulu', ''),
(1413, 125, 'Kelurahan Berdikari', ''),
(1414, 125, 'Kelurahan Bobo', ''),
(1415, 125, 'Kelurahan Bunga', ''),
(1416, 125, 'Kelurahan Kapiroe', ''),
(1417, 125, 'Kelurahan Lemban Tongoa', ''),
(1418, 125, 'Kelurahan Makmur', ''),
(1419, 125, 'Kelurahan Petimbe', ''),
(1420, 125, 'Kelurahan Rahmat', ''),
(1421, 125, 'Kelurahan Rantelede (Ranteleda)', ''),
(1422, 125, 'Kelurahan Rejeki', ''),
(1423, 125, 'Kelurahan Sejahtera', ''),
(1424, 125, 'Kelurahan Sigimpu', ''),
(1425, 125, 'Kelurahan Sintuwu', ''),
(1426, 125, 'Kelurahan Tanah Harapan', ''),
(1427, 125, 'Kelurahan Tongoa', ''),
(1428, 125, 'Kelurahan Uenuni', ''),
(1429, 126, 'Kelurahan Banasu', ''),
(1430, 126, 'Kelurahan Kalamanta', ''),
(1431, 126, 'Kelurahan Kantewu', ''),
(1432, 126, 'Kelurahan Kantewu II', ''),
(1433, 126, 'Kelurahan Koja', ''),
(1434, 126, 'Kelurahan Lawe', ''),
(1435, 126, 'Kelurahan Lone Basa', ''),
(1436, 126, 'Kelurahan Mamu', ''),
(1437, 126, 'Kelurahan Mapahi', ''),
(1438, 126, 'Kelurahan Morui (Murui)', ''),
(1439, 126, 'Kelurahan Onu', ''),
(1440, 126, 'Kelurahan Parelea', ''),
(1441, 126, 'Kelurahan Peana', ''),
(1442, 127, 'Kelurahan Bora', ''),
(1443, 127, 'Kelurahan Jonooge', ''),
(1444, 127, 'Kelurahan Kalukubula', ''),
(1445, 127, 'Kelurahan Lemba Palu', ''),
(1446, 127, 'Kelurahan Lolu', ''),
(1447, 127, 'Kelurahan Loru', ''),
(1448, 127, 'Kelurahan Maranata', ''),
(1449, 127, 'Kelurahan Mpanau', ''),
(1450, 127, 'Kelurahan Ngatabaru', ''),
(1451, 127, 'Kelurahan Oloboju', ''),
(1452, 127, 'Kelurahan Pombewe', ''),
(1453, 127, 'Kelurahan Sidera', ''),
(1454, 127, 'Kelurahan Sidondo I', ''),
(1455, 127, 'Kelurahan Sidondo II', ''),
(1456, 127, 'Kelurahan Sidondo III', ''),
(1457, 127, 'Kelurahan Sidondo IV', ''),
(1458, 127, 'Kelurahan Soulowe', ''),
(1459, 127, 'Kelurahan Watunonju', ''),
(1460, 128, 'Kelurahan Lambara', ''),
(1461, 128, 'Kelurahan Sibalaya Selatan', ''),
(1462, 128, 'Kelurahan Sibalaya Utara', ''),
(1463, 128, 'Kelurahan Sibowi', ''),
(1464, 129, 'Kelurahan Ampana', ''),
(1465, 129, 'Kelurahan Bailo', ''),
(1466, 129, 'Kelurahan Dondo', ''),
(1467, 129, 'Kelurahan Labuan', ''),
(1468, 129, 'Kelurahan Malotong', ''),
(1469, 129, 'Kelurahan Padang Tumbuo', ''),
(1470, 129, 'Kelurahan Sabulira Toba', ''),
(1471, 129, 'Kelurahan Sansarino', ''),
(1472, 129, 'Kelurahan Sumoli', ''),
(1473, 129, 'Kelurahan Uentanaga Atas', ''),
(1474, 129, 'Kelurahan Uentanaga Bawah', ''),
(1475, 130, 'Kelurahan Balanggala', ''),
(1476, 130, 'Kelurahan Balingara', ''),
(1477, 130, 'Kelurahan Bantuga', ''),
(1478, 130, 'Kelurahan Borone', ''),
(1479, 130, 'Kelurahan Bulan Jaya', ''),
(1480, 130, 'Kelurahan Giri Mulyo', ''),
(1481, 130, 'Kelurahan Kajulangko', ''),
(1482, 130, 'Kelurahan Longge', ''),
(1483, 130, 'Kelurahan Mantangisi', ''),
(1484, 130, 'Kelurahan Pusungi', ''),
(1485, 130, 'Kelurahan Sabo', ''),
(1486, 130, 'Kelurahan Tampabatu', ''),
(1487, 130, 'Kelurahan Tete Atas', ''),
(1488, 130, 'Kelurahan Tete Bawah', ''),
(1489, 130, 'Kelurahan Uebone', ''),
(1490, 130, 'Kelurahan Urundaka', ''),
(1491, 130, 'Kelurahan Wanasari', ''),
(1492, 131, 'Kelurahan Awo', ''),
(1493, 131, 'Kelurahan Bangkagi', ''),
(1494, 131, 'Kelurahan Baulu', ''),
(1495, 131, 'Kelurahan Benteng', ''),
(1496, 131, 'Kelurahan Bungayo', ''),
(1497, 131, 'Kelurahan Katupat', ''),
(1498, 131, 'Kelurahan Kololio', ''),
(1499, 131, 'Kelurahan Lebiti', ''),
(1500, 131, 'Kelurahan Lembanato', ''),
(1501, 131, 'Kelurahan Matobiai (Motobisi)', ''),
(1502, 131, 'Kelurahan Pulau Enam', ''),
(1503, 131, 'Kelurahan Tobil', ''),
(1504, 131, 'Kelurahan Tongkabo', ''),
(1505, 131, 'Kelurahan Urulepe', ''),
(1506, 132, 'Kelurahan Bahari', ''),
(1507, 132, 'Kelurahan Banano', ''),
(1508, 132, 'Kelurahan Betaua', ''),
(1509, 132, 'Kelurahan Dataran Bugi', ''),
(1510, 132, 'Kelurahan Kalemba I', ''),
(1511, 132, 'Kelurahan Kalemba II', ''),
(1512, 132, 'Kelurahan Korondoda', ''),
(1513, 132, 'Kelurahan Lemoro', ''),
(1514, 132, 'Kelurahan Pancuma', ''),
(1515, 132, 'Kelurahan Podi', ''),
(1516, 132, 'Kelurahan Sandada', ''),
(1517, 132, 'Kelurahan Tayawa', ''),
(1518, 132, 'Kelurahan Tojo', ''),
(1519, 132, 'Kelurahan Tongku', ''),
(1520, 132, 'Kelurahan Uedele', ''),
(1521, 132, 'Kelurahan Uekuli', ''),
(1522, 133, 'Kelurahan Bambalo', ''),
(1523, 133, 'Kelurahan Galuga', ''),
(1524, 133, 'Kelurahan Kabalo', ''),
(1525, 133, 'Kelurahan Malei Tojo', ''),
(1526, 133, 'Kelurahan Malewa', ''),
(1527, 133, 'Kelurahan Matako', ''),
(1528, 133, 'Kelurahan Mawomba', ''),
(1529, 133, 'Kelurahan Nggawia', ''),
(1530, 133, 'Kelurahan Tanamawau', ''),
(1531, 133, 'Kelurahan Tatari', ''),
(1532, 133, 'Kelurahan Toliba', ''),
(1533, 133, 'Kelurahan Tombiano', ''),
(1534, 133, 'Kelurahan Ujung Tibu', ''),
(1535, 134, 'Kelurahan Bonebae II', ''),
(1536, 134, 'Kelurahan Bonebae III', ''),
(1537, 134, 'Kelurahan Boneveto (Bonevoto)I', ''),
(1538, 134, 'Kelurahan Bongka Koy (Koi)I', ''),
(1539, 134, 'Kelurahan Bongka MakmurI', ''),
(1540, 134, 'Kelurahan BorneangI', ''),
(1541, 134, 'Kelurahan CempaI', ''),
(1542, 134, 'Kelurahan MarowoI', ''),
(1543, 134, 'Kelurahan MireI', ''),
(1544, 134, 'Kelurahan ParanongeI', ''),
(1545, 134, 'Kelurahan RompiI', ''),
(1546, 134, 'Kelurahan TakibangkeI', ''),
(1547, 134, 'Kelurahan TampanomboI', ''),
(1548, 134, 'Kelurahan TobamawuI', ''),
(1549, 134, 'Kelurahan UekambunoI', ''),
(1550, 134, 'Kelurahan UematopaI', ''),
(1551, 134, 'Kelurahan WatusonguI', ''),
(1552, 135, 'Kelurahan BambuI', ''),
(1553, 135, 'Kelurahan BombaI', ''),
(1554, 135, 'Kelurahan KambutuI', ''),
(1555, 135, 'Kelurahan KulingkinariI', ''),
(1556, 135, 'Kelurahan LembanyaI', ''),
(1557, 135, 'Kelurahan MalinoI', ''),
(1558, 135, 'Kelurahan MolowaguI', ''),
(1559, 135, 'Kelurahan SiatuI', ''),
(1560, 135, 'Kelurahan TaningkolaI', ''),
(1561, 135, 'Kelurahan Tanjung PudeI', ''),
(1562, 135, 'Kelurahan TumbulawaI', ''),
(1563, 135, 'Kelurahan Una UnaI', ''),
(1564, 135, 'Kelurahan WakaiI', ''),
(1565, 136, 'Kelurahan Biga', ''),
(1566, 136, 'Kelurahan Katogop (Kotogop)', ''),
(1567, 136, 'Kelurahan Kondongan', ''),
(1568, 136, 'Kelurahan Malapo', ''),
(1569, 136, 'Kelurahan Pasokan', ''),
(1570, 136, 'Kelurahan Salinggoha', ''),
(1571, 136, 'Kelurahan Tingki', ''),
(1572, 137, 'Kelurahan Dolo ng A (Dolo nga)', ''),
(1573, 137, 'Kelurahan Dolo ng B', ''),
(1574, 137, 'Kelurahan Kabalutan', ''),
(1575, 137, 'Kelurahan Kalia', ''),
(1576, 137, 'Kelurahan Kolami (Kolampi)', ''),
(1577, 137, 'Kelurahan Luok', ''),
(1578, 137, 'Kelurahan Malenge', ''),
(1579, 137, 'Kelurahan Olilan', ''),
(1580, 137, 'Kelurahan Pautu', ''),
(1581, 137, 'Kelurahan Popolii (PopolIi)', ''),
(1582, 137, 'Kelurahan Tiga Pulau', ''),
(1583, 137, 'Kelurahan Tiloan', ''),
(1584, 137, 'Kelurahan Tumotok', ''),
(1585, 137, 'Kelurahan Tutung', ''),
(1586, 138, 'Kelurahan Panasakan', ''),
(1587, 138, 'Kelurahan Sidoarjo', ''),
(1588, 138, 'Kelurahan Nalu', ''),
(1589, 138, 'Kelurahan Baru', ''),
(1590, 138, 'Kelurahan Tuweley', ''),
(1591, 138, 'Kelurahan Tambun', ''),
(1592, 138, 'Kelurahan Buntuna', ''),
(1593, 138, 'Kelurahan Dadakitan', ''),
(1594, 139, 'Kelurahan Kayu Lompa', ''),
(1595, 139, 'Kelurahan Kinapasan', ''),
(1596, 139, 'Kelurahan Kongkomos', ''),
(1597, 139, 'Kelurahan Labonu', ''),
(1598, 139, 'Kelurahan Ogosipat', ''),
(1599, 139, 'Kelurahan Sibaluton', ''),
(1600, 139, 'Kelurahan Silondou', ''),
(1601, 140, 'Kelurahan Dungingis', ''),
(1602, 140, 'Kelurahan Galumpang', ''),
(1603, 140, 'Kelurahan Kapas', ''),
(1604, 140, 'Kelurahan Lingadan', ''),
(1605, 141, 'Kelurahan Bangkir', ''),
(1606, 141, 'Kelurahan Dongko', ''),
(1607, 141, 'Kelurahan Kombo', ''),
(1608, 141, 'Kelurahan Mimbala', ''),
(1609, 141, 'Kelurahan Soni', ''),
(1610, 141, 'Kelurahan Tampiala', ''),
(1611, 142, 'Kelurahan Bambapula', ''),
(1612, 142, 'Kelurahan Banagan', ''),
(1613, 142, 'Kelurahan Kabinuang', ''),
(1614, 142, 'Kelurahan Malambigu', ''),
(1615, 142, 'Kelurahan Ogotua', ''),
(1616, 142, 'Kelurahan Sese', ''),
(1617, 142, 'Kelurahan Simatang Tanjung', ''),
(1618, 142, 'Kelurahan Simatang Utara', ''),
(1619, 142, 'Kelurahan Tompoh', ''),
(1620, 143, 'Kelurahan Anggasan', ''),
(1621, 143, 'Kelurahan Bambapun', ''),
(1622, 143, 'Kelurahan Lais', ''),
(1623, 143, 'Kelurahan Luok Manipi', ''),
(1624, 143, 'Kelurahan Malala', ''),
(1625, 143, 'Kelurahan Malomba', ''),
(1626, 143, 'Kelurahan Malulu', ''),
(1627, 143, 'Kelurahan Ogogasang', ''),
(1628, 143, 'Kelurahan Ogogili', ''),
(1629, 143, 'Kelurahan Ogowele', ''),
(1630, 143, 'Kelurahan Pmst Sinungkut', ''),
(1631, 143, 'Kelurahan Salumbia', ''),
(1632, 143, 'Kelurahan Tinabogan', ''),
(1633, 144, 'Kelurahan Bajugan', ''),
(1634, 144, 'Kelurahan Ginunggung', ''),
(1635, 144, 'Kelurahan Kalangkangan', ''),
(1636, 144, 'Kelurahan Lakatan', ''),
(1637, 144, 'Kelurahan Lalos', ''),
(1638, 144, 'Kelurahan Lantapan', ''),
(1639, 144, 'Kelurahan Ogomuli (Ogomoli)', ''),
(1640, 144, 'Kelurahan Sabang', ''),
(1641, 144, 'Kelurahan Sandana', ''),
(1642, 144, 'Kelurahan Tende', ''),
(1643, 144, 'Kelurahan Tinigi', ''),
(1644, 145, 'Kelurahan Lampasio', ''),
(1645, 145, 'Kelurahan Tinading', ''),
(1646, 145, 'Kelurahan Janja', ''),
(1647, 145, 'Kelurahan Maibua', ''),
(1648, 145, 'Kelurahan Mulyasari (Mulia Sari)', ''),
(1649, 145, 'Kelurahan Oyom', ''),
(1650, 145, 'Kelurahan Salugan', ''),
(1651, 145, 'Kelurahan Sibea', ''),
(1652, 146, 'Kelurahan Bambalaga', ''),
(1653, 146, 'Kelurahan Batuilo', ''),
(1654, 146, 'Kelurahan Bilo', ''),
(1655, 146, 'Kelurahan Buga', ''),
(1656, 146, 'Kelurahan Kabetan', ''),
(1657, 146, 'Kelurahan Kamalu', ''),
(1658, 146, 'Kelurahan Labuan Lobo', ''),
(1659, 146, 'Kelurahan Muara Besar', ''),
(1660, 146, 'Kelurahan Pagaitan', ''),
(1661, 146, 'Kelurahan Pulias', ''),
(1662, 146, 'Kelurahan Sambujan', ''),
(1663, 147, 'Kelurahan/Desa Binontoan', ''),
(1664, 147, 'Kelurahan Diule', ''),
(1665, 147, 'Kelurahan Lakuan Toli Toli', ''),
(1666, 147, 'Kelurahan Laulalang', ''),
(1667, 147, 'Kelurahan Pinjan', ''),
(1668, 147, 'Kelurahan Salumpaga', ''),
(1669, 147, 'Kelurahan Santigi', '');

-- --------------------------------------------------------

--
-- Table structure for table `kotadesa`
--

CREATE TABLE `kotadesa` (
  `id_kota` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `kotadesa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kotadesa`
--

INSERT INTO `kotadesa` (`id_kota`, `id_provinsi`, `kotadesa`) VALUES
(1, 27, 'Palu'),
(2, 27, 'Banggai Pusat'),
(3, 27, 'Banggai Kepulauan'),
(4, 27, 'Buol'),
(5, 27, 'Donggala'),
(6, 27, 'Morowali'),
(7, 27, 'Parigi Moutung'),
(8, 27, 'Poso'),
(9, 27, 'Sigi'),
(10, 27, 'Tojo UnaUna'),
(11, 27, 'Toli-Toli');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL,
  `provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `provinsi`) VALUES
(25, 'Sulawesi Barat'),
(26, 'Sulawesi Selatan'),
(27, 'Sulawesi Tengah'),
(28, 'Sulawesi Tenggara'),
(29, 'Sulawesi Utara');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_amandemen`
--

CREATE TABLE `tbl_amandemen` (
  `id_amd` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `nomor` varchar(250) NOT NULL,
  `nilai` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asistensi`
--

CREATE TABLE `tbl_asistensi` (
  `id_asistensi` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_konsultan` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `asistensi` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_asistensi`
--

INSERT INTO `tbl_asistensi` (`id_asistensi`, `id_kegiatan`, `id_konsultan`, `tanggal`, `asistensi`, `desc`, `status`) VALUES
(1, 1, 2, '0000-00-00 00:00:00', 'Revisi defiasi anggaran', '', ''),
(2, 1, 2, '0000-00-00 00:00:00', 'revisi nilai pagu menjadi 200juta', '', 'open'),
(3, 2, 0, '2019-08-20 03:30:04', 'Salah kaprah', 'ini adalah salah kaprah', 'open'),
(4, 3, 0, '2019-08-20 08:54:42', 'Lambang Negara', 'Indonesia Bersatu', 'open'),
(5, 3, 0, '2019-08-20 08:57:28', 'fatan ganteng', 'terbaik memang', 'open'),
(6, 4, 0, '2019-08-20 09:15:07', 'Laporan Pendahuluan', '-', 'open'),
(7, 4, 0, '2019-08-20 09:19:20', 'Laporan Antara', '-', 'open'),
(8, 5, 0, '2019-08-20 09:45:52', 'mmm', 'mmmm', 'open'),
(9, 8, 0, '2019-08-21 14:17:47', 'coba', 'coba', 'open'),
(10, 8, 0, '2019-08-21 14:21:04', 'coba  lagi', 'cobaaaaaaaaaaa', 'open'),
(11, 11, 0, '2019-08-21 14:48:13', 'hhhhhhhhhhhhhhhhhhh', 'ggggggggggggggggggg', 'open'),
(12, 12, 0, '2019-08-21 14:50:34', 'aaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaa', 'open'),
(13, 12, 0, '2019-08-21 15:00:06', 'cccccccccccccccccc', 'c', 'open'),
(14, 13, 0, '2019-08-21 15:05:13', 'ddddddddddddddddddddd', 'dd', 'open'),
(15, 13, 0, '2019-08-21 15:08:17', 'aaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaa', 'open'),
(16, 13, 0, '2019-08-21 15:48:55', 'vvvvvvvvvvvvvv', 'vvvvvvvvvvvvvvvv', 'open'),
(17, 15, 0, '2019-08-21 16:15:13', 'beli thai tea', 'ayo bel', 'open'),
(18, 15, 0, '2019-08-21 16:15:55', 'beli pisang goreng', 'contoh', 'open'),
(19, 18, 0, '2019-08-21 16:21:34', 'Bank BNI', 'tes', 'open'),
(20, 18, 0, '2019-08-21 16:22:09', 'Bank BRI', 'test', 'open'),
(21, 18, 0, '2019-08-21 16:24:06', 'a', 'a', 'open'),
(22, 18, 0, '2019-08-21 16:26:08', 'b', 'b', 'open'),
(23, 18, 0, '2019-08-21 16:28:13', 'b', 'b', 'open'),
(24, 18, 0, '2019-08-21 16:28:34', 'b', 'b', 'open'),
(25, 18, 0, '2019-08-21 16:29:14', 'b', 'b', 'open'),
(26, 18, 0, '2019-08-21 16:29:41', 'b', 'b', 'open'),
(27, 18, 0, '2019-08-21 16:31:16', 'b', 'b', 'open'),
(28, 18, 0, '2019-08-21 16:31:56', 'b', 'b', 'open'),
(29, 18, 0, '2019-08-21 16:32:17', 'b', 'b', 'open'),
(30, 18, 0, '2019-08-21 16:33:07', 'b', 'b', 'open'),
(31, 18, 0, '2019-08-21 16:33:36', 'b', 'b', 'open'),
(32, 18, 0, '2019-08-21 16:33:58', 'b', 'b', 'open'),
(33, 22, 0, '2019-08-21 17:14:41', 'coba asistensi', 'ini', 'open'),
(34, 22, 0, '2019-08-21 17:15:20', 'coba asistensi2', 'tes', 'open'),
(35, 22, 0, '2019-08-21 17:15:50', 'hari', 'hari', 'open'),
(36, 22, 0, '2019-08-21 17:24:12', 'untuk nana', 'aaaaaaaaaaaaaaaa', 'open'),
(37, 25, 0, '2019-08-21 22:46:08', 'asistensi pertama', 'tolong', 'open'),
(38, 26, 0, '2019-08-22 08:39:19', 'coba', 'iin adalah coba', 'open'),
(39, 31, 0, '2019-08-22 09:11:49', 'ddd', 'ddd', 'open'),
(40, 32, 0, '2019-08-22 10:17:18', 'Ini adalah topik', '-', 'close'),
(41, 33, 0, '2019-08-22 12:15:11', 'Bab 1', '-', 'open'),
(42, 34, 0, '2019-08-22 12:53:12', 'bab1', '-', 'open'),
(43, 35, 0, '2019-08-22 13:07:54', 'coba', '1', 'close'),
(44, 35, 0, '2019-08-22 13:37:22', 'osa', 'osa', 'open'),
(45, 36, 0, '2019-08-22 15:02:38', 'Bab I', '-', 'open'),
(46, 36, 0, '2019-08-22 16:20:06', 'bab 2', 'coba', 'open'),
(47, 37, 0, '2019-08-23 09:54:12', 'Bab I', '-', 'close'),
(48, 40, 0, '2019-09-03 10:19:36', 'Bab I', 'Banyak kesalahan', 'open'),
(49, 40, 0, '2019-09-03 10:24:58', 'kok tdk bisa kirim email', 'coba', 'open'),
(50, 42, 0, '2019-09-03 10:53:34', 'Bab 1', '-', 'open'),
(51, 42, 0, '2019-09-03 10:55:26', 'Bab 1', '-', 'open'),
(52, 42, 0, '2019-09-03 10:55:41', 'bab 1', 'ini adalah bab 1', 'open'),
(53, 43, 0, '2019-09-04 11:27:46', 'Laporan Antara', 'Perbaiki Redaksional\nTambahkan Peta', 'open'),
(55, 46, 0, '2019-09-05 14:29:17', 'Laporan RMK', 'Laporan', 'open'),
(59, 46, 0, '2019-09-05 14:37:32', 'Laporan Pendahuluan', 'Laporan RMK', 'open'),
(60, 49, 0, '2019-09-05 15:02:17', 'Laporan Pendahuluan', 'Pendahuluan OK', 'open'),
(61, 48, 0, '2019-09-05 15:07:16', 'Laporan triwulan 1', 'tes', 'open'),
(62, 48, 0, '2019-09-05 15:21:02', 'coba', '', 'open'),
(63, 54, 0, '2019-09-06 06:25:03', 'kkkkkkkkk', '', 'open'),
(64, 54, 0, '2019-09-06 07:29:50', 'zikra', '', 'open'),
(65, 54, 0, '2019-09-06 07:37:10', 'Laporan Antara', '', 'open'),
(66, 56, 0, '2019-09-06 09:19:37', 'hai bang', '', 'open'),
(67, 57, 0, '2019-09-06 09:27:39', 'Laporan RMK', '', 'close');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asistensi_lbr`
--

CREATE TABLE `tbl_asistensi_lbr` (
  `id_asistensi_lbr` int(11) NOT NULL,
  `id_asistensi` int(11) NOT NULL,
  `id_user_dari` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `isi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_asistensi_lbr`
--

INSERT INTO `tbl_asistensi_lbr` (`id_asistensi_lbr`, `id_asistensi`, `id_user_dari`, `tanggal`, `isi`) VALUES
(1, 3, 15, '2019-08-20 03:30:37', 'coba'),
(2, 3, 20, '2019-08-20 03:31:20', 'mantap'),
(3, 4, 15, '2019-08-20 08:55:01', 'Jangan rusuh please'),
(4, 4, 20, '2019-08-20 08:55:11', 'ok bro'),
(5, 4, 15, '2019-08-20 08:55:27', 'jangan liat2 mas awin'),
(6, 5, 15, '2019-08-20 08:57:42', 'nikah sm sappo'),
(7, 5, 15, '2019-08-20 08:58:13', 'perbaiki halaman 30'),
(8, 5, 20, '2019-08-20 08:58:24', 'minta maaf ini sy masih buang air'),
(9, 6, 15, '2019-08-20 09:15:56', '- Perbaiki Halaman 3'),
(10, 6, 21, '2019-08-20 09:16:15', 'apanya ka?'),
(11, 6, 15, '2019-08-20 09:18:35', 'Halaman 3 Patok, Behel masih kurang lengkap masih kurang lengkap ,  tolong dilengkapi'),
(12, 7, 15, '2019-08-20 09:21:24', 'tololong  kak'),
(13, 7, 15, '2019-08-20 09:21:31', 'minta maaf ini sy masih buang air'),
(14, 7, 21, '2019-08-20 09:23:13', 'maksud???'),
(15, 8, 24, '2019-08-20 09:45:59', 'mmmmmmmmmmmmmmmmmmm'),
(16, 8, 25, '2019-08-20 09:46:15', 'hhhhhhhhhhhhhhhhhhhhhh'),
(17, 8, 24, '2019-08-20 09:49:48', 'oooooo'),
(18, 8, 24, '2019-08-20 09:50:01', 'hack'),
(19, 8, 24, '2019-08-20 09:50:07', 'im going to hack'),
(20, 8, 25, '2019-08-20 09:50:10', 'ribut'),
(21, 8, 24, '2019-08-20 09:50:35', 'we'),
(22, 8, 25, '2019-08-20 10:02:46', 'tes'),
(23, 8, 25, '2019-08-20 10:03:02', 'aaaaaaaaaaaaaaaaaaa'),
(24, 8, 24, '2019-08-20 15:04:09', 'nambongo'),
(25, 9, 24, '2019-08-21 14:17:59', 'coba dulu gais'),
(26, 9, 24, '2019-08-21 14:20:51', 'tes'),
(27, 10, 24, '2019-08-21 14:21:22', 'yoiii'),
(28, 10, 24, '2019-08-21 14:23:00', 'minta maaf ini sy masih buang air'),
(29, 10, 24, '2019-08-21 14:23:14', 'mantap'),
(30, 10, 24, '2019-08-21 14:27:03', 'tes'),
(31, 11, 24, '2019-08-21 14:48:39', '- Perbaiki Halaman 3'),
(32, 12, 24, '2019-08-21 14:50:55', 'aaaaaaaaaaaaaaaaaaaaaaaaa'),
(33, 12, 24, '2019-08-21 14:54:22', 'minta maaf ini sy masih buang air'),
(34, 12, 24, '2019-08-21 15:00:21', 'coba dulu gais'),
(35, 13, 24, '2019-08-21 15:00:51', 'aaaaaaaaaaaaaaaaaaaaaaaaa'),
(36, 13, 24, '2019-08-21 15:04:35', 'coba dulu gais'),
(37, 14, 24, '2019-08-21 15:05:27', 'aaaaaaaaaaaaaaaaaaaaaaaaa'),
(38, 14, 24, '2019-08-21 15:07:56', '- Perbaiki Halaman 3'),
(39, 15, 24, '2019-08-21 15:08:34', 'mantap'),
(40, 15, 24, '2019-08-21 15:12:56', '- Perbaiki Halaman 3'),
(41, 15, 24, '2019-08-21 15:13:02', 'aaaaaaaaaaaaaaaaaaaaaaaaa'),
(42, 16, 24, '2019-08-21 15:49:15', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),
(43, 16, 24, '2019-08-21 16:12:40', 'coba'),
(44, 18, 24, '2019-08-21 16:16:26', '- Perbaiki Halaman 3'),
(45, 18, 24, '2019-08-21 16:16:31', 'coba dulu gais'),
(46, 35, 24, '2019-08-21 17:16:16', '- Perbaiki Halaman 3'),
(47, 37, 24, '2019-08-21 22:46:42', 'bab1'),
(48, 37, 24, '2019-08-21 22:47:21', 'coba dulu gais'),
(49, 39, 24, '2019-08-22 09:12:18', '- Perbaiki Halaman 3'),
(50, 39, 24, '2019-08-22 09:12:47', 'ini'),
(51, 39, 24, '2019-08-22 09:16:47', 'tas'),
(52, 39, 20, '2019-08-22 10:16:25', 'aku harus apa??'),
(53, 40, 24, '2019-08-22 10:18:02', 'Assalamualaikum'),
(54, 40, 24, '2019-08-22 11:39:41', 'waalaikumsalam'),
(55, 41, 24, '2019-08-22 12:16:24', 'cek'),
(56, 42, 25, '2019-08-22 13:07:16', 'coba'),
(57, 43, 27, '2019-08-22 13:08:20', 'tes'),
(58, 43, 25, '2019-08-22 13:08:57', 'balas'),
(59, 44, 24, '2019-08-22 13:37:49', 'fgbcb'),
(60, 44, 24, '2019-08-22 13:37:51', 'ddd'),
(61, 44, 24, '2019-08-22 13:37:52', 'd'),
(62, 44, 24, '2019-08-22 13:37:55', 'fffff'),
(63, 45, 24, '2019-08-22 15:03:21', 'Rung Lingkup, tukar dengann lingkup di bab2'),
(64, 45, 24, '2019-08-22 15:05:01', 'Cek Kembali bagian 2.1 Gambaran penjelasan item'),
(65, 45, 24, '2019-08-22 15:06:54', 'selesaikan bab 2 dahulu'),
(66, 45, 24, '2019-08-22 15:22:47', 'a'),
(67, 45, 24, '2019-08-22 15:28:49', 'a'),
(68, 45, 24, '2019-08-22 15:29:55', 'b'),
(69, 45, 24, '2019-08-22 15:30:42', 'c'),
(70, 46, 24, '2019-08-22 16:20:20', '- Perbaiki Halaman 3'),
(71, 46, 24, '2019-08-22 16:50:27', '- Perbaiki Halaman 2'),
(72, 45, 24, '2019-08-22 17:07:43', '- Perbaiki Halaman 3222'),
(73, 47, 24, '2019-08-23 09:54:27', 'Tolong Perbaiki metode kerja'),
(74, 47, 24, '2019-08-23 09:55:31', 'Sesuaikan lingkup pekerjaan dengan KAK'),
(75, 48, 24, '2019-09-03 10:21:14', 'ini adalah pesan untuk konsulta ZIKRA'),
(76, 49, 24, '2019-09-03 10:26:04', 'ini adalah pesan untuk konsultab'),
(77, 49, 20, '2019-09-03 10:28:21', 'ini adalah balasan'),
(78, 49, 20, '2019-09-03 10:29:08', '- Perbaiki Halaman 3'),
(79, 49, 20, '2019-09-03 10:30:12', 'ini adalah pesan untuk konsultab'),
(80, 49, 24, '2019-09-03 10:30:57', '- Perbaiki Halaman 3'),
(81, 49, 24, '2019-09-03 10:32:52', 'ini adalah pesan untuk konsultab'),
(82, 49, 24, '2019-09-03 10:33:21', 'mantap'),
(83, 49, 24, '2019-09-03 10:34:00', 'coba dulu gais'),
(84, 49, 24, '2019-09-03 10:34:42', 'ini adalah pesan untuk konsultab'),
(85, 49, 24, '2019-09-03 10:35:20', 'ini adalah pesan untuk konsultab'),
(86, 49, 24, '2019-09-03 10:42:03', 'mantap'),
(87, 49, 24, '2019-09-03 10:42:26', 'jkjkjkj'),
(88, 49, 20, '2019-09-03 10:43:39', 'ini adalah pesan untuk konsultab'),
(89, 49, 20, '2019-09-03 10:44:12', 'aaaaaaaaaaaaaaaaaaaaaaaaa'),
(90, 52, 24, '2019-09-03 10:57:41', 'ini adalah pesan'),
(91, 54, 24, '2019-09-05 14:23:04', 'perbaiki kata pengantar'),
(92, 55, 24, '2019-09-05 14:33:49', 'perbaiki ruang lingkup'),
(93, 55, 20, '2019-09-05 14:36:15', 'sudah ditukar pak'),
(94, 55, 27, '2019-09-05 14:41:49', 'ini adalah pesan untuk konsultab'),
(95, 55, 24, '2019-09-05 14:42:20', 'iya betul @iflapor'),
(96, 60, 33, '2019-09-05 15:03:14', 'Pendahuluan ACC'),
(97, 61, 24, '2019-09-05 15:07:30', 'ini adalah pesan untuk konsultab'),
(98, 59, 24, '2019-09-05 15:20:06', 'tes'),
(99, 66, 25, '2019-09-06 09:22:47', 'Luv'),
(100, 66, 25, '2019-09-06 09:24:21', 'tolong koreksi codingan semalam'),
(101, 66, 15, '2019-09-06 09:25:03', 'maaf pak lagi ngantuk'),
(102, 67, 24, '2019-09-06 09:28:34', 'Perbaiki Bagan Alir');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bidang`
--

CREATE TABLE `tbl_bidang` (
  `id_bidang` int(11) NOT NULL,
  `nama_bidang` varchar(255) NOT NULL,
  `des_bidang` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tbl_kegiatan';

--
-- Dumping data for table `tbl_bidang`
--

INSERT INTO `tbl_bidang` (`id_bidang`, `nama_bidang`, `des_bidang`) VALUES
(24, 'Dukungan Manajemen BBWS/BWS', 'Dukungan Manajemen 2'),
(25, 'Pengembangan dan Rehabilitasi Jaringan Irigasi Permukaan, Rawa dan Tambak', ''),
(26, 'Pengendalian Banjir, Lahar, Pengelolaan Drainase Utama Perkotaan, dan Pengaman Pantai', ''),
(27, 'Peningkatan Tatakelola Pengelolaan SDA Terpadu', ''),
(28, 'Pengelolaan Bendungan, Danau, dan Bangunan Penampung Air Lainnya', ''),
(29, 'Penyediaan dan Pengelolaan Air Tanah dan Air Baku', ''),
(30, 'Operasi dan Pemeliharaan Sarana Prasarana SDA', 'Operasi dan Pemeliharaan '),
(31, 'Kanal Banjir Yang Dibangun/Ditingkatkan', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_das`
--

CREATE TABLE `tbl_das` (
  `id_das` int(11) NOT NULL,
  `id_wlsungai` int(11) NOT NULL,
  `nama_das` varchar(255) NOT NULL,
  `tipe_das` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_das`
--

INSERT INTO `tbl_das` (`id_das`, `id_wlsungai`, `nama_das`, `tipe_das`) VALUES
(1, 1, 'DAS Lariang', 'Kipas (Fan Shaped Watershed)'),
(2, 1, 'DAS Minti', 'Melebar (Broad Watershed)'),
(3, 1, 'DAS Sulung', 'Kipas (Fan Shaped Watershed)'),
(4, 1, 'DAS Pangian', 'Melebar (Broad Watershed)'),
(5, 1, 'DAS Sawi', 'Melebar (Broad Watershed)'),
(6, 1, 'DAS Randomayang', 'Melebar (Broad Watershed)'),
(7, 1, 'DAS Kasuloang', 'Kipas (Fan Shaped Watershed)'),
(8, 1, 'DAS Bambaira', 'Melebar (Broad Watershed)'),
(9, 1, 'DAS Tampaore', 'Melebar (Broad Watershed)'),
(10, 1, 'DAS Kumboki', 'Melebar (Broad Watershed)'),
(11, 1, 'DAS Surumana', 'Kipas (Fan Shaped Watershed)'),
(12, 1, 'DAS Bambalalombi', 'Melebar (Broad Watershed)'),
(13, 1, 'DAS Mamara', 'Kipas (Fan Shaped Watershed)'),
(14, 1, 'DAS Tolongano', 'Melebar (Broad Watershed)'),
(15, 1, 'DAS Kangando', 'Melebar (Broad Watershed)'),
(16, 1, 'DAS Towale', 'Kipas (Fan Shaped Watershed)'),
(17, 1, 'DAS Tomaku', 'Melebar (Broad Watershed)'),
(18, 1, 'DAS Donggala', 'Melebar (Broad Watershed)'),
(19, 1, 'DAS Uwemole', 'Memanjang (Long Watershed)'),
(20, 1, 'DAS Lottu', 'Melebar (Broad Watershed)'),
(21, 1, 'DAS Nggoji', 'Melebar (Broad Watershed)'),
(22, 1, 'DAS Watusampu', 'Memanjang (Long Watershed)'),
(23, 1, 'DAS Buluri', 'Melebar (Broad Watershed)'),
(24, 1, 'DAS Owenumpu', 'Memanjang (Long Watershed)'),
(25, 1, 'DAS Palu', 'Melebar (Broad Watershed)'),
(26, 1, 'DAS Lepata', 'Memanjang (Long Watershed)'),
(27, 1, 'DAS Watutela', 'Memanjang (Long Watershed)'),
(28, 1, 'DAS Watutailo', 'Melebar (Broad Watershed)'),
(29, 1, 'DAS Biuluniongga', 'Memanjang (Long Watershed)'),
(30, 1, 'DAS Taipa', 'Kipas (Fan Shaped Watershed)'),
(31, 1, 'DAS Tabeo', 'Melebar (Broad Watershed)'),
(32, 1, 'DAS Tavaili', 'Kipas (Fan Shaped Watershed)'),
(33, 1, 'DAS Lambagu', 'Melebar (Broad Watershed)'),
(34, 1, 'DAS Labuan', 'Kipas (Fan Shaped Watershed)'),
(35, 1, 'DAS Toaya', 'Melebar (Broad Watershed)'),
(36, 1, 'DAS Masaengi', 'Melebar (Broad Watershed)'),
(37, 1, 'DAS Tibo', 'Kipas (Fan Shaped Watershed)'),
(38, 1, 'DAS Batusuya', 'Melebar (Broad Watershed)'),
(39, 1, 'DAS Aliandu', 'Kipas (Fan Shaped Watershed)'),
(40, 1, 'DAS Loro', 'Memanjang (Long Watershed)'),
(41, 1, 'DAS Sinapa', 'Memanjang (Long Watershed)'),
(42, 1, 'DAS Sikara', 'Memanjang (Long Watershed)'),
(43, 1, 'DAS Omba', 'Memanjang (Long Watershed)'),
(44, 1, 'DAS Tondo', 'Memanjang (Long Watershed)'),
(45, 1, 'DAS Lente', 'Kipas (Fan Shaped Watershed)'),
(46, 1, 'DAS Tompe', 'Kipas (Fan Shaped Watershed)'),
(47, 1, 'DAS Lende', 'Melebar (Broad Watershed)'),
(48, 1, 'DAS Airmakuni', 'Melebar (Broad Watershed)'),
(49, 1, 'DAS Kusu', 'Melebar (Broad Watershed)'),
(50, 1, 'DAS Kamonji', 'Kipas (Fan Shaped Watershed)'),
(51, 1, 'DAS Tompo', 'Melebar (Broad Watershed)'),
(52, 1, 'DAS Maruri', 'Melebar (Broad Watershed)'),
(53, 2, 'DAS Tompis', ''),
(54, 2, 'DAS Kasimbar', ''),
(55, 2, 'DAS Toribulu', ''),
(56, 2, 'DAS Topoya', ''),
(57, 2, 'DAS Silanga', ''),
(58, 2, 'DAS Marantale', ''),
(59, 2, 'DAS Salumbia', ''),
(60, 2, 'DAS Toboli', ''),
(61, 2, 'DAS Pelawa', ''),
(62, 2, 'DAS Baliara', ''),
(63, 2, 'DAS Olaya', ''),
(64, 2, 'DAS Korontua', ''),
(65, 2, 'DAS Dolago', ''),
(66, 2, 'DAS Tindaki', ''),
(67, 2, 'DAS Sampaloe', ''),
(68, 2, 'DAS Torue', ''),
(69, 2, 'DAS Tolai', ''),
(70, 2, 'DAS Topeau', ''),
(71, 2, 'DAS Suli', ''),
(72, 2, 'DAS Sausu', ''),
(73, 2, 'DAS Tambarana', ''),
(74, 2, 'DAS Kalora', ''),
(75, 2, 'DAS Samalera', ''),
(76, 2, 'DAS Kilo', ''),
(77, 2, 'DAS Kameasi', ''),
(78, 2, 'DAS Tiwáa', ''),
(79, 2, 'DAS Masani', ''),
(80, 2, 'DAS Lape', ''),
(81, 2, 'DAS Puna', ''),
(82, 2, 'DAS Mapane', ''),
(83, 2, 'DAS Poso', ''),
(84, 2, 'DAS Tongko', ''),
(85, 2, 'DAS Malei', ''),
(86, 2, 'DAS Bambalo', ''),
(87, 2, 'DAS Toliba', ''),
(88, 2, 'DAS Tambiano', ''),
(89, 2, 'DAS Mawomba', ''),
(90, 2, 'DAS Kabalo', ''),
(91, 2, 'DAS Tayawa', ''),
(92, 2, 'DAS Ue Kuli', ''),
(93, 2, 'DAS Betaue', ''),
(94, 2, 'DAS Ue Dele', ''),
(95, 2, 'DAS Sandada', ''),
(96, 2, 'DAS Tojo', ''),
(97, 2, 'DAS Masalongi', ''),
(98, 2, 'DAS Pancuma', ''),
(99, 2, 'DAS Tongku', ''),
(100, 2, 'DAS Ue Podi', ''),
(101, 2, 'DAS Padapu', ''),
(102, 2, 'DAS Kayunyole', ''),
(103, 3, 'DAS Majene', 'Memanjang (Long Watershed)'),
(104, 3, 'DAS Kaili', 'Melebar (Broad Watershed)'),
(105, 3, 'DAS Tuwiuni', 'Melebar (Broad Watershed)'),
(106, 3, 'DAS Kuma', 'Memanjang (Long Watershed)'),
(107, 3, 'DAS Karossa', 'Kipas (Fan Shaped Watershed)'),
(108, 3, 'DAS Benggaulu', 'Melebar (Broad Watershed)'),
(109, 3, 'DAS Budongbudong', 'Kipas (Fan Shaped Watershed)'),
(110, 3, 'DAS Panggajoan', 'Melebar (Broad Watershed)'),
(111, 3, 'DAS Kamansi', 'Melebar (Broad Watershed)'),
(112, 3, 'DAS Lumu', 'Kipas (Fan Shaped Watershed)'),
(113, 3, 'DAS Karama', 'Kipas (Fan Shaped Watershed)'),
(114, 3, 'DAS Paniki', 'Melebar (Broad Watershed)'),
(115, 3, 'DAS Ponalana', 'Kipas (Fan Shaped Watershed)'),
(116, 3, 'DAS Loli', 'Melebar (Broad Watershed)'),
(117, 3, 'DAS Guliling', 'Melebar (Broad Watershed)'),
(118, 3, 'DAS Kaluku', 'Kipas (Fan Shaped Watershed)'),
(119, 3, 'DAS Pure', 'Melebar (Broad Watershed)'),
(120, 3, 'DAS Gintungan', 'Memanjang (Long Watershed)'),
(121, 3, 'DAS Ahuni', 'Memanjang (Long Watershed)'),
(122, 3, 'DAS Ampelas', 'Memanjang (Long Watershed)'),
(123, 3, 'DAS Bonebone', 'Memanjang (Long Watershed)'),
(124, 3, 'DAS Tumuki', 'Memanjang (Long Watershed)'),
(125, 3, 'DAS Mamuju', 'Memanjang (Long Watershed)'),
(126, 3, 'DAS Karema', 'Kipas (Fan Shaped Watershed)'),
(127, 3, 'DAS Simboro', 'Kipas (Fan Shaped Watershed)'),
(128, 3, 'DAS Gimbang', 'Memanjang (Long Watershed)'),
(129, 3, 'DAS Kulasi', 'Memanjang (Long Watershed)'),
(130, 3, 'DAS Lumandang', 'Melebar (Broad Watershed)'),
(131, 3, 'DAS Malawa', 'Melebar (Broad Watershed)'),
(132, 3, 'DAS Losa', 'Memanjang (Long Watershed)'),
(133, 3, 'DAS Padala', 'Memanjang (Long Watershed)'),
(134, 3, 'DAS Takke', 'Memanjang (Long Watershed)'),
(135, 3, 'DAS Tamala', 'Melebar (Broad Watershed)'),
(136, 3, 'DAS Sulako', 'Memanjang (Long Watershed)'),
(137, 3, 'DAS Panantai', 'Melebar (Broad Watershed)'),
(138, 3, 'DAS Pindang', 'Kipas (Fan Shaped Watershed)'),
(139, 3, 'DAS Petakean', 'Melebar (Broad Watershed)'),
(140, 3, 'DAS Air Panas', 'Melebar (Broad Watershed)'),
(141, 3, 'DAS Ahu', 'Memanjang (Long Watershed)'),
(142, 3, 'DAS Tamao', 'Kipas (Fan Shaped Watershed)'),
(143, 3, 'DAS Taosa', 'Memanjang (Long Watershed)'),
(144, 3, 'DAS Anusu', 'Kipas (Fan Shaped Watershed)'),
(145, 3, 'DAS Karang Matti', 'Memanjang (Long Watershed)'),
(146, 3, 'DAS Taan', 'Kipas (Fan Shaped Watershed)'),
(147, 3, 'DAS Maliaya', 'Memanjang (Long Watershed)'),
(148, 3, 'DAS Babappu', 'Memanjang (Long Watershed)'),
(149, 3, 'DAS Samalio', 'Memanjang (Long Watershed)'),
(150, 3, 'DAS Mataurang', 'Melebar (Broad Watershed)'),
(151, 3, 'DAS Malunda', 'Kipas (Fan Shaped Watershed)'),
(152, 3, 'DAS Talalere', 'Melebar (Broad Watershed)'),
(153, 3, 'DAS Asaasaang', 'Melebar (Broad Watershed)'),
(154, 3, 'DAS Tubo', 'Kipas (Fan Shaped Watershed)'),
(155, 3, 'DAS Takombe', 'Melebar (Broad Watershed)'),
(156, 3, 'DAS Batururu', 'Melebar (Broad Watershed)'),
(157, 3, 'DAS Rawang Rawang', 'Kipas (Fan Shaped Watershed)'),
(158, 3, 'DAS Pumbiu', 'Melebar (Broad Watershed)'),
(159, 3, 'DAS Labuang', 'Memanjang (Long Watershed)'),
(160, 3, 'DAS Waisering', 'Memanjang (Long Watershed)'),
(161, 3, 'DAS Sumakuyu', 'Melebar (Broad Watershed)'),
(162, 3, 'DAS Tammerodo', 'Melebar (Broad Watershed)'),
(163, 3, 'DAS Lombangan', 'Melebar (Broad Watershed)'),
(164, 3, 'DAS Palipi', 'Melebar (Broad Watershed)'),
(165, 3, 'DAS Binangatanga', 'Memanjang (Long Watershed)'),
(166, 3, 'DAS Lembang', 'Melebar (Broad Watershed)'),
(167, 3, 'DAS Apoleang', 'Memanjang (Long Watershed)'),
(168, 3, 'DAS Mosso', 'Melebar (Broad Watershed)'),
(169, 3, 'DAS Pamboang', 'Memanjang (Long Watershed)'),
(170, 3, 'DAS Camba', 'Melebar (Broad Watershed)'),
(171, 3, 'DAS Mandar', 'Memanjang (Long Watershed)'),
(172, 3, 'DAS Tombo', 'Melebar (Broad Watershed)'),
(173, 3, 'DAS Maloso', 'Kipas (Fan Shaped Watershed)'),
(174, 3, 'DAS Matakali', 'Kipas (Fan Shaped Watershed)'),
(175, 3, 'DAS Binuang', 'Melebar (Broad Watershed)'),
(176, 3, 'DAS Silopo', 'Melebar (Broad Watershed)'),
(177, 4, 'DAS Siwali', ''),
(178, 4, 'DAS Siboalo', ''),
(179, 4, 'DAS Sibayu', ''),
(180, 4, 'DAS Sabang', ''),
(181, 4, 'DAS Sioyong', ''),
(182, 4, 'DAS Malonas', ''),
(183, 4, 'DAS Siraurang', ''),
(184, 4, 'DAS Long', ''),
(185, 4, 'DAS Binamo', ''),
(186, 4, 'DAS Bayang', ''),
(187, 4, 'DAS Siraru', ''),
(188, 4, 'DAS Ou', ''),
(189, 4, 'DAS Taipa', ''),
(190, 4, 'DAS Babatona', ''),
(191, 4, 'DAS Siboang', ''),
(192, 4, 'DAS Silempu', ''),
(193, 4, 'DAS Silamboo', ''),
(194, 4, 'DAS Balukang', ''),
(195, 4, 'DAS Baloni', ''),
(196, 4, 'DAS Sampaga', ''),
(197, 4, 'DAS Bantayang', ''),
(198, 4, 'DAS Resi', ''),
(199, 4, 'DAS Tandaiyo', ''),
(200, 4, 'DAS Malukang', ''),
(201, 4, 'DAS Ogoamas', ''),
(202, 4, 'DAS Cendrana', ''),
(203, 4, 'DAS Angudangeng', ''),
(204, 4, 'DAS Soni', ''),
(205, 4, 'DAS Bangkir', ''),
(206, 4, 'DAS Silumba', ''),
(207, 4, 'DAS Mimbala', ''),
(208, 4, 'DAS Telanja', ''),
(209, 4, 'DAS Kabiunang', ''),
(210, 4, 'DAS Ogotua', ''),
(211, 4, 'DAS Koni', ''),
(212, 4, 'DAS Manuawa', ''),
(213, 4, 'DAS Bantoli', ''),
(214, 4, 'DAS Banagan', ''),
(215, 4, 'DAS Luok', ''),
(216, 4, 'DAS Kulasi', ''),
(217, 4, 'DAS Maloma', ''),
(218, 4, 'DAS Bailo', ''),
(219, 4, 'DAS Bambapun', ''),
(220, 4, 'DAS Lais', ''),
(221, 4, 'DAS Ogogasang', ''),
(222, 4, 'DAS Ogogili', ''),
(223, 4, 'DAS Ogolalo', ''),
(224, 4, 'DAS Maraja', ''),
(225, 4, 'DAS Salugan', ''),
(226, 4, 'DAS Janja', ''),
(227, 4, 'DAS Talaut', ''),
(228, 4, 'DAS Dadakitan', ''),
(229, 4, 'DAS Tuwelei', ''),
(230, 4, 'DAS Kalangkangan', ''),
(231, 4, 'DAS Bajugan', ''),
(232, 4, 'DAS Dongingis', ''),
(233, 4, 'DAS Lingadan', ''),
(234, 4, 'DAS Salumpaga', ''),
(235, 4, 'DAS Diule', ''),
(236, 4, 'DAS Pinjan', ''),
(237, 4, 'DAS Binontoan', ''),
(238, 4, 'DAS Lakuan', ''),
(239, 4, 'DAS Busak', ''),
(240, 4, 'DAS Botakna Busak', ''),
(241, 4, 'DAS Buol', ''),
(242, 4, 'DAS Bokat', ''),
(243, 4, 'DAS Ponagoan', ''),
(244, 4, 'DAS Lomu', ''),
(245, 4, 'DAS Bunobogu', ''),
(246, 4, 'DAS Motinunu', ''),
(247, 4, 'DAS Bulongidun', ''),
(248, 4, 'DAS Bodi', ''),
(249, 4, 'DAS ButakioTotanggelodoka', ''),
(250, 4, 'DAS Butakiodata', ''),
(251, 4, 'DAS Lobu', ''),
(252, 4, 'DAS Tuladengi', ''),
(253, 4, 'DAS Lambunu', ''),
(254, 4, 'DAS Tampo', ''),
(255, 4, 'DAS Bataonyo Malino', ''),
(256, 4, 'DAS Ongka Malino', ''),
(257, 4, 'DAS Mepanga', ''),
(258, 4, 'DAS Moubang/Mepanga', ''),
(259, 4, 'DAS Tilung', ''),
(260, 4, 'DAS Ogotomubu', ''),
(261, 4, 'DAS Bangkalang Ogomojolo', ''),
(262, 4, 'DAS Palasa', ''),
(263, 4, 'DAS Bangkalang Bobalo', ''),
(264, 4, 'DAS Tinombo', ''),
(265, 4, 'DAS Bangkalan Dongkas', ''),
(266, 4, 'DAS Bainaa', ''),
(267, 4, 'DAS Punasela', ''),
(268, 4, 'DAS Sidoan', ''),
(269, 4, 'DAS Malanggo', ''),
(270, 4, 'DAS Sigenti', ''),
(271, 4, 'DAS Maninili', ''),
(272, 4, 'DAS Tada', ''),
(273, 4, 'DAS Koala Posona', ''),
(274, 4, 'DAS Simatang', ''),
(275, 4, 'DAS Kabetan', ''),
(276, 5, 'DAS Boba', ''),
(277, 5, 'DAS Tirangan', ''),
(278, 5, 'DAS Salato', ''),
(279, 5, 'DAS Peo', ''),
(280, 5, 'DAS Tiworo', ''),
(281, 5, 'DAS Tufu', ''),
(282, 5, 'DAS Bungku Utara', ''),
(283, 5, 'DAS Kanipi', ''),
(284, 5, 'DAS Morowali', ''),
(285, 5, 'DAS Waerangu', ''),
(286, 5, 'DAS Sumara', ''),
(287, 5, 'DAS Petasia', ''),
(288, 5, 'DAS Tapohulu', ''),
(289, 5, 'DAS Lambolo', ''),
(290, 5, 'DAS Bahoue', ''),
(291, 5, 'DAS Koya', ''),
(292, 5, 'DAS Gililana', ''),
(293, 5, 'DAS Tanauge', ''),
(294, 5, 'DAS Laa', ''),
(295, 5, 'DAS Tambalako', ''),
(296, 5, 'DAS Solonsa', ''),
(297, 5, 'DAS Ungkaya', ''),
(298, 5, 'DAS Emea', ''),
(299, 5, 'DAS Karaopa', ''),
(300, 5, 'DAS Dendeo', ''),
(301, 5, 'DAS Baho Belu', ''),
(302, 5, 'DAS Baho Suai', ''),
(303, 5, 'DAS Parilangke', ''),
(304, 5, 'DAS Baho Monsambu', ''),
(305, 5, 'DAS Boho Ambunu', ''),
(306, 5, 'DAS Boho Maburu', ''),
(307, 5, 'DAS Umpanga', ''),
(308, 5, 'DAS Boho Mangoni', ''),
(309, 5, 'DAS Boho Wosu', ''),
(310, 5, 'DAS Boho Mooluno', ''),
(311, 5, 'DAS Boho Larakorako', ''),
(312, 5, 'DAS Lanona', ''),
(313, 5, 'DAS Boho Lanona', ''),
(314, 5, 'DAS Boho Kantobantalangu', ''),
(315, 5, 'DAS Banomohoni', ''),
(316, 5, 'DAS Boho Paororoa', ''),
(317, 5, 'DAS Torukuno Totoko', ''),
(318, 5, 'DAS Baho Lanona', ''),
(319, 5, 'DAS Baho Tofu', ''),
(320, 5, 'DAS Baho Bohontue', ''),
(321, 5, 'DAS Baho Lahuafu', ''),
(322, 5, 'DAS Baho Unsongi', ''),
(323, 5, 'DAS Baho Larongsangi', ''),
(324, 5, 'DAS Baho Mofefe', ''),
(325, 5, 'DAS Baho Petula', ''),
(326, 5, 'DAS Siumbahu', ''),
(327, 5, 'DAS Baho Niula', ''),
(328, 5, 'DAS Baho Nkolango', ''),
(329, 5, 'DAS Baho Dopi', ''),
(330, 5, 'DAS Kumpi', ''),
(331, 5, 'DAS Morafu', ''),
(332, 5, 'DAS Padabahu', ''),
(333, 5, 'DAS Huwu', ''),
(334, 5, 'DAS Watubobotol', ''),
(335, 5, 'DAS Mente', ''),
(336, 5, 'DAS Lafeu', ''),
(337, 5, 'DAS Tinala', ''),
(338, 5, 'DAS Laroenai', ''),
(339, 5, 'DAS Sambalagi', ''),
(340, 5, 'DAS Torukuno Aea', ''),
(341, 5, 'DAS Warea', ''),
(342, 5, 'DAS Lamontoli', ''),
(343, 5, 'DAS Latamo', ''),
(344, 5, 'DAS Sambuenga', ''),
(345, 5, 'DAS Matano', ''),
(346, 5, 'DAS Rano', ''),
(347, 5, 'DAS Menui', ''),
(348, 5, 'DAS Matarase', ''),
(349, 5, 'DAS Kayangan', ''),
(350, 5, 'DAS Ambokita', ''),
(351, 5, 'DAS Kokoh', ''),
(352, 5, 'DAS Harapan', ''),
(353, 5, 'DAS Pandat', ''),
(354, 5, 'DAS Sombori', ''),
(355, 5, 'DAS Marege', ''),
(356, 5, 'DAS Bapa', ''),
(357, 5, 'DAS Pado-pado', ''),
(358, 5, 'DAS Padabale', ''),
(359, 5, 'DAS Tadingan', ''),
(360, 5, 'DAS Waru-waru', ''),
(361, 5, 'DAS Karantu', ''),
(362, 5, 'DAS Kaleroang', ''),
(363, 5, 'DAS Paku', ''),
(364, 5, 'DAS Umbele', ''),
(365, 6, 'DAS Bongka', ''),
(366, 6, 'DAS Podimati', ''),
(367, 6, 'DAS Bailo', ''),
(368, 6, 'DAS Ampana', ''),
(369, 6, 'DAS Toba', ''),
(370, 6, 'DAS Dondo', ''),
(371, 6, 'DAS Sumoli', ''),
(372, 6, 'DAS Siba', ''),
(373, 6, 'DAS Masapi', ''),
(374, 6, 'DAS Borone', ''),
(375, 6, 'DAS Balanggala', ''),
(376, 6, 'DAS Padauleyo', ''),
(377, 6, 'DAS Sabo', ''),
(378, 6, 'DAS Balingara', ''),
(379, 6, 'DAS Kauhangkang', ''),
(380, 6, 'DAS Bangketa', ''),
(381, 6, 'DAS Bolaang', ''),
(382, 6, 'DAS Auk/Hek', ''),
(383, 6, 'DAS Tomeang', ''),
(384, 6, 'DAS Lialiatongoa', ''),
(385, 6, 'DAS Petak', ''),
(386, 6, 'DAS Bela', ''),
(387, 6, 'DAS Kalumbangan', ''),
(388, 6, 'DAS Kalaka', ''),
(389, 6, 'DAS Bunta', ''),
(390, 6, 'DAS Toima', ''),
(391, 6, 'DAS Lobu', ''),
(392, 6, 'DAS Pakowa', ''),
(393, 6, 'DAS Lambangan', ''),
(394, 6, 'DAS Poh', ''),
(395, 6, 'DAS Kaunyo Siuna', ''),
(396, 6, 'DAS Pagimana', ''),
(397, 6, 'DAS Salipi', ''),
(398, 6, 'DAS Sambuang', ''),
(399, 6, 'DAS Mayayap', ''),
(400, 6, 'DAS Toiba', ''),
(401, 6, 'DAS Patipati', ''),
(402, 6, 'DAS Samaku', ''),
(403, 6, 'DAS Oma', ''),
(404, 6, 'DAS Longkonga', ''),
(405, 6, 'DAS Boalemo', ''),
(406, 6, 'DAS Nipa', ''),
(407, 6, 'DAS Malik', ''),
(408, 6, 'DAS Toku', ''),
(409, 6, 'DAS Luok', ''),
(410, 6, 'DAS Balantak', ''),
(411, 6, 'DAS Dolian', ''),
(412, 6, 'DAS Owan', ''),
(413, 6, 'DAS Sukon', ''),
(414, 6, 'DAS Lomba', ''),
(415, 6, 'DAS Waru', ''),
(416, 6, 'DAS Montu', ''),
(417, 6, 'DAS Bantayan', ''),
(418, 6, 'DAS Hunduhon', ''),
(419, 6, 'DAS Sandimak', ''),
(420, 6, 'DAS Mansandak', ''),
(421, 6, 'DAS Biak', ''),
(422, 6, 'DAS Soho', ''),
(423, 6, 'DAS Simpong', ''),
(424, 6, 'DAS Maahas', ''),
(425, 6, 'DAS Nombo', ''),
(426, 6, 'DAS Mendono', ''),
(427, 6, 'DAS Kintom', ''),
(428, 6, 'DAS Tangkiang', ''),
(429, 6, 'DAS Omolu', ''),
(430, 6, 'DAS Uso', ''),
(431, 6, 'DAS Luk', ''),
(432, 6, 'DAS Batui', ''),
(433, 6, 'DAS Bakung', ''),
(434, 6, 'DAS Kayowa', ''),
(435, 6, 'DAS Matinduk', ''),
(436, 6, 'DAS Sinorang', ''),
(437, 6, 'DAS Toili', ''),
(438, 6, 'DAS Mansahang', ''),
(439, 6, 'DAS Topo', ''),
(440, 6, 'DAS Dongin', ''),
(441, 6, 'DAS Mentawa', ''),
(442, 6, 'DAS Rata', ''),
(443, 6, 'DAS Pareoti', ''),
(444, 6, 'DAS Odolia', ''),
(445, 6, 'DAS Tanasumpu', ''),
(446, 6, 'DAS Damar', ''),
(447, 6, 'DAS Wine', ''),
(448, 6, 'DAS Bonebone', ''),
(449, 6, 'DAS Taningkola', ''),
(450, 6, 'DAS Tanimpu', ''),
(451, 6, 'DAS Tanjungpude', ''),
(452, 6, 'DAS Lengo', ''),
(453, 6, 'DAS Pomangana', ''),
(454, 6, 'DAS Ompotan', ''),
(455, 6, 'DAS Baulu', ''),
(456, 6, 'DAS Talaiakoh', ''),
(457, 6, 'DAS Malengke', ''),
(458, 6, 'DAS Tiga Pulau', ''),
(459, 6, 'DAS Waleakodi', ''),
(460, 6, 'DAS Kaunpo Minanga', ''),
(461, 6, 'DAS Poat', ''),
(462, 6, 'DAS Tampo', ''),
(463, 6, 'DAS Urulepe', ''),
(464, 6, 'DAS Pemantingan', ''),
(465, 6, 'DAS Bajangan', ''),
(466, 6, 'DAS Urundaka', ''),
(467, 6, 'DAS Unauna', ''),
(468, 6, 'DAS Lemba', ''),
(469, 6, 'DAS Awo', ''),
(470, 6, 'DAS Kololio', ''),
(471, 6, 'DAS Bambacolo', ''),
(472, 6, 'DAS Tampabatu', ''),
(473, 6, 'DAS Maduna', ''),
(474, 7, 'DAS Lukpanateng', ''),
(475, 7, 'DAS Ombuli', ''),
(476, 7, 'DAS Batuampas', ''),
(477, 7, 'DAS Sabang', ''),
(478, 7, 'DAS Sambulangan', ''),
(479, 7, 'DAS Paisu Lamo', ''),
(480, 7, 'DAS Bangkalan', ''),
(481, 7, 'DAS Montop', ''),
(482, 7, 'DAS Paisu Luno', ''),
(483, 7, 'DAS Langga', ''),
(484, 7, 'DAS Palumang', ''),
(485, 7, 'DAS Bungkuko Meusean', ''),
(486, 7, 'DAS Tinangkung', ''),
(487, 7, 'DAS Kwakon', ''),
(488, 7, 'DAS Paisu Telen', ''),
(489, 7, 'DAS Paisu Sagu', ''),
(490, 7, 'DAS Bungkuko Tatandak', ''),
(491, 7, 'DAS Bungkuko Tilean', ''),
(492, 7, 'DAS Lembah Bakatua', ''),
(493, 7, 'DAS Paisu Batu', ''),
(494, 7, 'DAS Lolang', ''),
(495, 7, 'DAS Paisu Kalana', ''),
(496, 7, 'DAS Paisu Tatakalay', ''),
(497, 7, 'DAs Paisu Lomou', ''),
(498, 7, 'DAS Luksagu', ''),
(499, 7, 'DAS Paisu Puso', ''),
(500, 7, 'DAS Tompulaan', ''),
(501, 7, 'DAS Poisu Babasal', ''),
(502, 7, 'DAS Lopito', ''),
(503, 7, 'DAS Obuluson', ''),
(504, 7, 'DAS Poisu Moti', ''),
(505, 7, 'DAS Sambiut', ''),
(506, 7, 'DAS Sakay', ''),
(507, 7, 'DAS Sobonon', ''),
(508, 7, 'DAS Paisu Muntano', ''),
(509, 7, 'DAS Cura', ''),
(510, 7, 'DAS Paisu Tobing', ''),
(511, 7, 'DAS Paisu Musoni', ''),
(512, 7, 'DAS Bobu', ''),
(513, 7, 'DAS Paisu Tobungin', ''),
(514, 7, 'DAS Paisu Nipa', ''),
(515, 7, 'DAS Paisu Songoloyo', ''),
(516, 7, 'DAS Gasal', ''),
(517, 7, 'DAS Liang', ''),
(518, 7, 'DAS Apal', ''),
(519, 7, 'DAS Bonetandunono', ''),
(520, 7, 'DAS Balayon', ''),
(521, 7, 'DAS Pal', ''),
(522, 7, 'DAS Popidolon', ''),
(523, 7, 'DAS Binuntuli', ''),
(524, 7, 'DAS Tompulenan', ''),
(525, 7, 'DAS Tangkop', ''),
(526, 7, 'DAS Tomboniki', ''),
(527, 7, 'DAS Kindandal', ''),
(528, 7, 'DAS Mamulusan', ''),
(529, 7, 'DAS Buko', ''),
(530, 7, 'DAS Popisi', ''),
(531, 7, 'DAS Luk', ''),
(532, 7, 'DAS Tombos', ''),
(533, 7, 'DAS Lempek', ''),
(534, 7, 'DAS Paisu Saa', ''),
(535, 7, 'DAS Patukuki', ''),
(536, 7, 'DAS Koyobunga', ''),
(537, 7, 'DAS Kolak', ''),
(538, 7, 'DAS Peling', ''),
(539, 7, 'DAS Meselesek', ''),
(540, 7, 'DAS Bulagi', ''),
(541, 7, 'DAS Sosom', ''),
(542, 7, 'DAS Lalon', ''),
(543, 7, 'DAS Tolo', ''),
(544, 7, 'DAS Mangais', ''),
(545, 7, 'DAS Lalean', ''),
(546, 7, 'DAS Potung', ''),
(547, 7, 'DAS Osam', ''),
(548, 7, 'DAS Alani', ''),
(549, 7, 'DAS Lumbilumbia', ''),
(550, 7, 'DAS Balantindakan', ''),
(551, 7, 'DAS Labangun', ''),
(552, 7, 'DAS Seano', ''),
(553, 7, 'DAS Talastalas', ''),
(554, 7, 'DAS Batangono', ''),
(555, 7, 'DAS Mata', ''),
(556, 7, 'DAS Lalengan', ''),
(557, 7, 'DAS Pelinglalomo', ''),
(558, 7, 'DAS Tataba', ''),
(559, 7, 'DAS Malanggong', ''),
(560, 7, 'DAS Lamelamebungin', ''),
(561, 7, 'DAS Ngiokngiok', ''),
(562, 7, 'DAS Tatendeng', ''),
(563, 7, 'DAS Pelak', ''),
(564, 7, 'DAS Potil', ''),
(565, 7, 'DAS Bakalanpauno', ''),
(566, 7, 'DAS Bakalan', ''),
(567, 7, 'DAS Mbumbu', ''),
(568, 7, 'DAS Patolang', ''),
(569, 7, 'DAS Tibarat', ''),
(570, 7, 'DAS Tamailoboang', ''),
(571, 7, 'DAS Batupanapi', ''),
(572, 7, 'DAS Boniton', ''),
(573, 7, 'DAS Bolanan', ''),
(574, 7, 'DAS Nggilas', ''),
(575, 7, 'DAS Sangean', ''),
(576, 7, 'DAS Lipulalongo', ''),
(577, 7, 'DAS Bolokimolomos', ''),
(578, 7, 'DAS Tonggobubu', ''),
(579, 7, 'DAS Tongkulun', ''),
(580, 7, 'DAS Saleng', ''),
(581, 7, 'DAS Tatapon', ''),
(582, 7, 'DAS Sabian', ''),
(583, 7, 'DAS Lokotoy', ''),
(584, 7, 'DAS Mutiara', ''),
(585, 7, 'DAS Kaumbombol', ''),
(586, 7, 'DAS Paisu Tune', ''),
(587, 7, 'DAS Bolitan', ''),
(588, 7, 'DAS Paisu Tadunoakato', ''),
(589, 7, 'DAS Kombobol', ''),
(590, 7, 'DAS Lantolanto', ''),
(591, 7, 'DAS Kelapalima', ''),
(592, 7, 'DAS Malino', ''),
(593, 7, 'DAS Belek', ''),
(594, 7, 'DAS Balas', ''),
(595, 7, 'DAS Tolokibit', ''),
(596, 7, 'DAS Badumpayan', ''),
(597, 7, 'DAS Tokubei', ''),
(598, 7, 'DAS Kampung Mutiara', ''),
(599, 7, 'DAS Pososlalongo', ''),
(600, 7, 'DAS Monsongan', ''),
(601, 7, 'DAS Talambatu', ''),
(602, 7, 'DAS Paisu Malino', ''),
(603, 7, 'DAS Paisu Puruso', ''),
(604, 7, 'DAS Dodung', ''),
(605, 7, 'DAS Paisu Tango', ''),
(606, 7, 'DAS Tolisetubono', ''),
(607, 7, 'DAS Asasal', ''),
(608, 7, 'DAS Togongpauno', ''),
(609, 7, 'DAS Togongpotil', ''),
(610, 7, 'DAS Toulan', ''),
(611, 7, 'DAS Togong', ''),
(612, 7, 'DAS Babakan', ''),
(613, 7, 'DAS Bandang', ''),
(614, 7, 'DAS Kaukanao', ''),
(615, 7, 'DAS Tengkel', ''),
(616, 7, 'DAS Paisu Tengkel', ''),
(617, 7, 'DAS Mansalean', ''),
(618, 7, 'DAS Lalong', ''),
(619, 7, 'DAS Lalango', ''),
(620, 7, 'DAS Talas', ''),
(621, 7, 'DAS Lobangkurung', ''),
(622, 7, 'DAS Padengkian', ''),
(623, 7, 'DAS Lintoluk', ''),
(624, 7, 'DAS Labobo Pauno', ''),
(625, 7, 'DAS Buau', ''),
(626, 7, 'DAS Bongko', ''),
(627, 7, 'DAS Dungkean', ''),
(628, 7, 'DAS Bangi', ''),
(629, 7, 'DAS Bunibuni', ''),
(630, 7, 'DAS Bonebone', ''),
(631, 7, 'DAS Bundu', ''),
(632, 7, 'DAS Sidula', ''),
(633, 7, 'DAS Panteh', ''),
(634, 7, 'DAS Saloka', ''),
(635, 7, 'DAS Bangko', ''),
(636, 7, 'DAS Telopo', ''),
(637, 7, 'DAS Tumbak', ''),
(638, 7, 'DAS Loiloi', ''),
(639, 7, 'DAS Toropot', ''),
(640, 7, 'DAS Kokudang', ''),
(641, 7, 'DAS Kombongan', ''),
(642, 7, 'DAS Poaboloki', ''),
(643, 7, 'DAS Minanga', ''),
(644, 7, 'DAS Basapa', ''),
(645, 7, 'DAS Malangan', ''),
(646, 7, 'DAS Pongonpiti', ''),
(647, 7, 'DAS Posuposu', ''),
(648, 7, 'DAS Bokan', ''),
(649, 7, 'DAS Namut', ''),
(650, 7, 'DAS Batutunape', ''),
(651, 7, 'DAS Kawalu', ''),
(652, 7, 'DAS Tolobe', ''),
(653, 7, 'DAS Keak', ''),
(654, 7, 'DAS Bungin', ''),
(655, 7, 'DAS Leelang', ''),
(656, 7, 'DAS Togongbojoko', ''),
(657, 7, 'DAS Pobongkoh', ''),
(658, 7, 'DAS Sago', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gambar`
--

CREATE TABLE `tbl_gambar` (
  `id_gambar` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `des` text NOT NULL,
  `proses` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_kegiatan`
--

CREATE TABLE `tbl_jenis_kegiatan` (
  `id_jenis_kegiatan` int(11) NOT NULL,
  `jenis_kegiatan` varchar(200) NOT NULL,
  `des_jenis_kegaitan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenis_kegiatan`
--

INSERT INTO `tbl_jenis_kegiatan` (`id_jenis_kegiatan`, `jenis_kegiatan`, `des_jenis_kegaitan`) VALUES
(1, 'Konsultansi', ''),
(2, 'Konstruksi', 'konstruksi1'),
(3, 'Supervisi', ''),
(4, 'Swakelola', ''),
(6, 'Operasi dan Pemeliharaan', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kajian_lingkungan`
--

CREATE TABLE `tbl_kajian_lingkungan` (
  `id_kajian_lingkungan` int(11) NOT NULL,
  `kajian_lingkungan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kajian_lingkungan`
--

INSERT INTO `tbl_kajian_lingkungan` (`id_kajian_lingkungan`, `kajian_lingkungan`) VALUES
(1, 'Ada'),
(2, 'Belum ada'),
(3, 'On Going');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan`
--

CREATE TABLE `tbl_kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `id_satker` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `konsultan` int(11) NOT NULL COMMENT 'id user konsultan',
  `nm_kegiatan` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_jenis_kegiatan` int(30) NOT NULL,
  `tahun` int(11) NOT NULL,
  `tahun_anggaran_2` int(11) DEFAULT NULL,
  `multi_years` varchar(100) NOT NULL,
  `id_ppk` varchar(100) NOT NULL,
  `id_bidang` int(30) NOT NULL,
  `id_output` int(9) NOT NULL,
  `jenis_konstruksi` varchar(40) NOT NULL,
  `id_satuan_output` int(11) NOT NULL,
  `id_satuan_outcome` int(11) NOT NULL,
  `provinsi` varchar(25) NOT NULL,
  `kabupaten` varchar(25) NOT NULL,
  `kecamatan` varchar(25) NOT NULL,
  `desa` varchar(25) NOT NULL,
  `id_wlsungai` int(11) NOT NULL,
  `das` varchar(40) NOT NULL,
  `tujuan` text NOT NULL,
  `manfaat` text NOT NULL,
  `id_kajian_lingkungan` int(11) NOT NULL,
  `output` text NOT NULL,
  `outcome` text NOT NULL,
  `data_teknis` text NOT NULL,
  `data_kegiatan` varchar(255) NOT NULL,
  `th_kegiatanlanjutan` varchar(255) NOT NULL,
  `kegiatan_terkait` text DEFAULT NULL,
  `kegiatan_terkait_tahun` varchar(10) DEFAULT NULL,
  `ks_konsultasi` text DEFAULT NULL,
  `ks_konsultasi_tahun` varchar(10) DEFAULT NULL,
  `ks_konstruksi` text DEFAULT NULL,
  `ks_konstruksi_tahun` varchar(10) DEFAULT NULL,
  `ks_supervisi` text DEFAULT NULL,
  `ks_supervisi_tahun` varchar(10) DEFAULT NULL,
  `sumber_dana` varchar(255) NOT NULL,
  `status_kegiatan` varchar(255) NOT NULL,
  `dasar_kegiatan` varchar(255) NOT NULL,
  `nmp_jasa` varchar(100) NOT NULL,
  `nmp_jasa2` varchar(255) DEFAULT NULL,
  `kso_jo` varchar(250) NOT NULL,
  `perencana` varchar(255) NOT NULL,
  `supervisi` varchar(255) NOT NULL,
  `tgl_kontrak_awal` varchar(255) NOT NULL,
  `no_kontrak_awal` varchar(255) NOT NULL,
  `nilai_pagu` varchar(255) NOT NULL,
  `nilai_kontrak` varchar(255) NOT NULL,
  `jenis_kontrak` varchar(50) NOT NULL,
  `tgl_spmk` date NOT NULL,
  `no_spmk` varchar(255) NOT NULL,
  `tgl_penyerapan` date NOT NULL,
  `nilai_penyerapan` varchar(255) NOT NULL,
  `keuangan_persen` varchar(255) NOT NULL,
  `progres_fisik_rencana` varchar(20) DEFAULT NULL,
  `progres_fisik_realisasi` varchar(20) DEFAULT NULL,
  `progres_fisik_deviasi` varchar(20) DEFAULT NULL,
  `progres_fisik_tanggal` date DEFAULT NULL,
  `keuangan_tanggal` date DEFAULT NULL,
  `fisik_persen` varchar(255) NOT NULL,
  `jkwkt_pelaksanaan` varchar(30) NOT NULL,
  `jml_hari` int(9) NOT NULL,
  `youtube_url` varchar(150) NOT NULL,
  `waktu_pelaksanaan_mulai` date DEFAULT NULL,
  `waktu_pelaksanaan_akhir` date DEFAULT NULL,
  `waktu_pelaksanaan_bulan` int(11) DEFAULT NULL,
  `waktu_pelaksanaan_hari` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `last_update_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kegiatan`
--

INSERT INTO `tbl_kegiatan` (`id_kegiatan`, `id_satker`, `user_id`, `konsultan`, `nm_kegiatan`, `deskripsi`, `id_jenis_kegiatan`, `tahun`, `tahun_anggaran_2`, `multi_years`, `id_ppk`, `id_bidang`, `id_output`, `jenis_konstruksi`, `id_satuan_output`, `id_satuan_outcome`, `provinsi`, `kabupaten`, `kecamatan`, `desa`, `id_wlsungai`, `das`, `tujuan`, `manfaat`, `id_kajian_lingkungan`, `output`, `outcome`, `data_teknis`, `data_kegiatan`, `th_kegiatanlanjutan`, `kegiatan_terkait`, `kegiatan_terkait_tahun`, `ks_konsultasi`, `ks_konsultasi_tahun`, `ks_konstruksi`, `ks_konstruksi_tahun`, `ks_supervisi`, `ks_supervisi_tahun`, `sumber_dana`, `status_kegiatan`, `dasar_kegiatan`, `nmp_jasa`, `nmp_jasa2`, `kso_jo`, `perencana`, `supervisi`, `tgl_kontrak_awal`, `no_kontrak_awal`, `nilai_pagu`, `nilai_kontrak`, `jenis_kontrak`, `tgl_spmk`, `no_spmk`, `tgl_penyerapan`, `nilai_penyerapan`, `keuangan_persen`, `progres_fisik_rencana`, `progres_fisik_realisasi`, `progres_fisik_deviasi`, `progres_fisik_tanggal`, `keuangan_tanggal`, `fisik_persen`, `jkwkt_pelaksanaan`, `jml_hari`, `youtube_url`, `waktu_pelaksanaan_mulai`, `waktu_pelaksanaan_akhir`, `waktu_pelaksanaan_bulan`, `waktu_pelaksanaan_hari`, `last_update`, `last_update_user`) VALUES
(52, 0, 24, 20, 'SID sungai lumu', '', 0, 2030, 0, 'N', '', 0, 0, '', 0, 0, '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, '', '', 0, '', '0000-00-00', '0000-00-00', 0, 0, '2019-09-06 05:51:18', 15),
(53, 0, 15, 20, 'Pembangunan  Teluk Palu', '', 0, 2024, NULL, '', '', 0, 0, '', 0, 0, '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, '', '', 0, '', NULL, NULL, NULL, NULL, '2019-09-06 05:51:32', 15),
(54, 0, 15, 20, 'Pembangunan  Teluk Palu', '', 0, 2029, 0, 'N', '', 0, 0, '', 0, 0, '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, '', '', 0, '', '0000-00-00', '0000-00-00', 0, 0, '2019-09-06 08:04:02', 24),
(55, 0, 24, 20, 'SID sungai lumu', '', 0, 2029, NULL, '', '', 0, 0, '', 0, 0, '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, '', '', 0, '', NULL, NULL, NULL, NULL, '2019-09-06 09:15:55', 24),
(56, 0, 15, 25, 'untuk hari', '', 0, 2026, 0, 'N', '', 0, 0, '', 0, 0, '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, '', '', 0, '', '0000-00-00', '0000-00-00', 0, 0, '2019-09-06 09:17:39', 25),
(57, 0, 24, 25, 'SID Tambak Rakyat Kab. Morowali Utara', '', 0, 2019, 0, 'N', '', 0, 0, '', 0, 0, '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, '', '', 0, '', '0000-00-00', '0000-00-00', 0, 0, '2019-09-06 10:19:35', 24);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_das`
--

CREATE TABLE `tbl_kegiatan_das` (
  `id_kegiatan_das` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_wlsungai` int(11) NOT NULL,
  `id_das` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_detail_konstruksi`
--

CREATE TABLE `tbl_kegiatan_detail_konstruksi` (
  `id_detail_konstruksi` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `nama_bangunan` varchar(100) NOT NULL,
  `volume` varchar(100) NOT NULL,
  `satuan` int(11) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_files`
--

CREATE TABLE `tbl_kegiatan_files` (
  `id_file_kegiatan` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_kajian_lingkungan` int(11) NOT NULL,
  `id_progres_laporan` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `file_name` text NOT NULL,
  `nama` text NOT NULL,
  `volume_laporan` varchar(100) DEFAULT NULL,
  `tahun` varchar(10) DEFAULT NULL,
  `des` text DEFAULT NULL,
  `ekstensi` varchar(10) DEFAULT NULL,
  `ukuran` varchar(10) DEFAULT NULL,
  `dibuat_oleh` varchar(50) NOT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kegiatan_files`
--

INSERT INTO `tbl_kegiatan_files` (`id_file_kegiatan`, `id_kegiatan`, `id_kajian_lingkungan`, `id_progres_laporan`, `jenis`, `file_name`, `nama`, `volume_laporan`, `tahun`, `des`, `ekstensi`, `ukuran`, `dibuat_oleh`, `tanggal`) VALUES
(269, 73, 0, 0, 'doc_kontrak', '3841801054fb7dc3023b79bb77c02d92.doc', 'kontrak 1', NULL, NULL, NULL, 'doc', '5.3 MB', 'SISDA', '2019-07-16 10:34:24'),
(268, 73, 0, 3, 'doc_pelaporan', '', 'Produk 1', '1', NULL, NULL, '', '', 'SISDA', '2019-07-16 10:34:09'),
(267, 72, 0, 3, 'doc_pelaporan', '', 'produk 2', '2', NULL, NULL, '', '', 'SISDA', '2019-07-10 14:37:33'),
(266, 72, 0, 2, 'doc_pelaporan', 'a:1:{i:0;a:3:{s:9:\"file_name\";s:30:\"Notulen_Progres_2_Surumana.doc\";s:8:\"file_ext\";s:4:\".doc\";s:9:\"file_size\";d:710;}}', 'produk 1', '1', NULL, NULL, 'eny', '710.0 KB', 'SISDA', '2019-07-10 14:37:19'),
(270, 3, 0, 1, 'doc_pelaporan', 'a:1:{i:0;a:3:{s:9:\"file_name\";s:60:\"dc-Cover-652ovhkibhg82kh6on274ihkn1-20180130023251_Medi.jpeg\";s:8:\"file_ext\";s:5:\".jpeg\";s:9:\"file_size\";d:37.310000000000002;}}', '1', '2', NULL, NULL, 'eny', '37.3 KB', 'fatan', '2019-08-20 09:01:55'),
(271, 4, 0, 2, 'doc_pelaporan', '', 'Laporan Pendahuluan', '3', NULL, NULL, '', '', 'awin', '2019-08-20 09:24:38'),
(272, 46, 0, 2, 'doc_pelaporan', '', 'Laporan RMK', '3', NULL, NULL, '', '', 'oping', '2019-09-05 14:31:39'),
(273, 46, 0, 2, 'doc_pelaporan', '', 'Laporan pendahuluan', '5', NULL, NULL, '', '', 'oping', '2019-09-05 14:31:52'),
(274, 46, 0, 1, 'doc_pelaporan', '', 'Laporan Antara', '5', NULL, NULL, '', '', 'oping', '2019-09-05 14:32:08'),
(275, 46, 0, 0, 'doc_kontrak', 'e3b40fa1cc71513457a6234a0aa59b09.pdf', 'kontrak', NULL, NULL, NULL, 'pdf', '442.0 KB', 'oping', '2019-09-05 14:58:15'),
(276, 52, 0, 1, 'doc_pelaporan', '', 'ssssssssssssssss', 'ssssssssssssssss', NULL, NULL, '', '', 'zikra', '2019-09-05 15:59:39'),
(277, 54, 0, 3, 'doc_pelaporan', '', 'tes', '1', NULL, NULL, '', '', 'admin', '2019-09-06 06:45:00'),
(278, 54, 0, 0, 'doc_kontrak', 'd0893849f57e77ca7482e1df5d82c614.docx', 'aaaaaaaaaaaa', NULL, NULL, NULL, 'docx', '161.4 KB', 'admin', '2019-09-06 06:57:55'),
(279, 54, 0, 1, 'doc_pelaporan', 'a:1:{i:0;a:3:{s:9:\"file_name\";s:49:\"FORMAT-LAPORAN-KEMAJUAN-AWAL-PPBT-PT-2019_(1).doc\";s:8:\"file_ext\";s:4:\".doc\";s:9:\"file_size\";d:181.5;}}', 'coba lagi', '1', NULL, NULL, 'eny', '181.5 KB', 'oping', '2019-09-06 07:06:46'),
(280, 54, 0, 0, 'doc_kontrak', 'e794cba4cd9edf6245f6730dccf5b361.doc', 'coba dlu', NULL, NULL, NULL, 'doc', '181.5 KB', 'oping', '2019-09-06 07:07:01'),
(281, 54, 0, 1, 'doc_pelaporan', 'a:1:{i:0;a:3:{s:9:\"file_name\";s:50:\"FORMAT-LAPORAN-KEMAJUAN-AWAL-PPBT-PT-2019_(1)1.doc\";s:8:\"file_ext\";s:4:\".doc\";s:9:\"file_size\";d:181.5;}}', 'coba', '1', NULL, NULL, 'eny', '181.5 KB', 'oping', '2019-09-06 07:36:52'),
(282, 54, 0, 0, 'doc_kontrak', 'aaa278c2f6f0bf520b61f61a20f7748e.doc', 'terakhir', NULL, NULL, NULL, 'doc', '181.5 KB', 'oping', '2019-09-06 07:59:33'),
(283, 54, 0, 0, 'doc_kontrak', '5f223e6f880cad524164868f1082e107.docx', 'kedua', NULL, NULL, NULL, 'docx', '86.3 KB', 'oping', '2019-09-06 08:00:06'),
(284, 54, 0, 0, 'doc_kontrak', '41217a83828a1ca7680873ccca8c388f.xlsx', 'tes', NULL, NULL, NULL, 'xlsx', '2.0 MB', 'oping', '2019-09-06 08:01:47'),
(285, 54, 0, 0, 'doc_kontrak', 'e8a005ac39e8c73947507253c78f32ce.docx', 'rveisi cop', NULL, NULL, NULL, 'docx', '161.4 KB', 'oping', '2019-09-06 08:03:09'),
(286, 57, 0, 0, 'doc_pelaporan', 'a:2:{i:0;a:3:{s:9:\"file_name\";s:30:\"8_Laporan_Bulanan_Agustus.docx\";s:8:\"file_ext\";s:5:\".docx\";s:9:\"file_size\";d:1084.26;}i:1;a:3:{s:9:\"file_name\";s:30:\"8_Permohonan_Gaji_Agustus.docx\";s:8:\"file_ext\";s:5:\".docx\";s:9:\"file_size\";d:770.96000000000004;}}', '', '', NULL, NULL, 'eny', '1.8 MB', 'oping', '2019-09-06 10:19:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_lokasi`
--

CREATE TABLE `tbl_kegiatan_lokasi` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `kecamatan` text NOT NULL,
  `desa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_realisasi_pelaksanaan`
--

CREATE TABLE `tbl_kegiatan_realisasi_pelaksanaan` (
  `id_realisasi_pelaksanaan` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `progres_fisik_tanggal` date DEFAULT NULL,
  `progres_fisik_rencana` varchar(20) DEFAULT NULL,
  `progres_fisik_realisasi` varchar(20) DEFAULT NULL,
  `progres_fisik_deviasi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kegiatan_realisasi_pelaksanaan`
--

INSERT INTO `tbl_kegiatan_realisasi_pelaksanaan` (`id_realisasi_pelaksanaan`, `id_kegiatan`, `progres_fisik_tanggal`, `progres_fisik_rencana`, `progres_fisik_realisasi`, `progres_fisik_deviasi`) VALUES
(9, 1, '2019-07-18', '67%', '6%', '-61.00%'),
(10, 36, '2019-08-07', '1%', '2%', '1.00%'),
(12, 39, '2019-09-17', '2.22%', '2%', '-0.22%'),
(13, 39, '2019-09-27', '50%', '2%', '-48.00%'),
(14, 40, '2019-09-14', '100.00%', '30%', '-70.00%'),
(15, 40, '2019-09-27', '50%', '2%', '-48.00%'),
(16, 50, '2019-09-25', '1%', '2%', '1.00%'),
(17, 50, '2019-09-27', '50%', '2%', '-48.00%'),
(18, 54, '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_relasi`
--

CREATE TABLE `tbl_kegiatan_relasi` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `tahun` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan_ws`
--

CREATE TABLE `tbl_kegiatan_ws` (
  `id_kegiatan_ws` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `id_wlsungai` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_koordinat`
--

CREATE TABLE `tbl_koordinat` (
  `id_koordinat` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `koordinat_x` varchar(255) NOT NULL,
  `koordinat_y` varchar(255) NOT NULL,
  `koordinat_x1` varchar(255) NOT NULL,
  `koordinat_y1` varchar(255) NOT NULL,
  `ket_lokasi` varchar(255) NOT NULL,
  `tipe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_output`
--

CREATE TABLE `tbl_output` (
  `id_output` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `nama_output` varchar(255) NOT NULL,
  `des_output` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_output`
--

INSERT INTO `tbl_output` (`id_output`, `id_bidang`, `nama_output`, `des_output`) VALUES
(1, 24, 'Perencanaan Program dan Perancangan BWS/BBWS', ''),
(3, 24, 'Rencana dan program kerja BBWS/BWS', ''),
(4, 24, 'Layanan ketatalaksanaan BBWS/BWS', ''),
(5, 24, 'Layanan Internal (Overhead)', ''),
(6, 24, 'Layanan Perkantoran', ''),
(7, 25, 'Rencana teknis dan dokumen lingkungan hidup untuk konstruksi irigasi dan rawa', ''),
(8, 25, 'Jaringan irigasi permukaan kewenangan Pusat yang dibangun', ''),
(9, 25, 'Bendung irigasi kewenangan Pusat yang dibangun', ''),
(10, 25, 'Jaringan irigasi permukaan kewenangan Daerah yang dibangun', ''),
(11, 25, 'Bendung irigasi kewenangan Daerah yang dibangun', ''),
(12, 25, 'Jaringan irigasi rawa yang dibangun', ''),
(13, 25, 'Jaringan irigasi tambak yang dibangun', ''),
(14, 25, 'Jaringan irigasi permukaan kewenangan Pusat yang direhabilitasi / ditingkatkan', ''),
(16, 25, 'Jaringan irigasi permukaan kewenangan Daerah yang direhabilitasi / ditingkatkan', ''),
(17, 25, 'Bendung irigasi kewenangan Daerah yang direhabilitasi / ditingkatkan', ''),
(18, 25, 'Jaringan irigasi rawa yang direhabilitasi / ditingkatkan', ''),
(19, 25, 'Jaringan irigasi tambak yang direhabilitasi / ditingkatkan', ''),
(20, 25, 'Kawasan rawa yang dikonservasi', ''),
(21, 25, 'Layanan Internal (Overhead)', ''),
(22, 26, 'Rencana teknis dan dokumen lingkungan hidup untuk konstruksi pengendali banjir, lahar, drainase utama perkotaan, dan pengaman pantai', ''),
(23, 26, 'Sungai yang dinormalisasi dan tanggul yang dibangun / ditingkatkan', ''),
(24, 26, 'Bangunan perkuatan tebing yang dibangun / ditingkatkan', ''),
(25, 26, 'Pintu air / bendung pengendali banjir yang dibangun / ditingkatkan', ''),
(26, 26, 'Kanal banjir yang dibangun / ditingkatkan', ''),
(27, 26, 'Stasiun pompa banjir yang dibangun / ditingkatkan', ''),
(28, 26, 'Polder / kolam retensi yang dibangun / ditingkatkan', ''),
(29, 26, 'Saluran drainase yang dibangun / ditingkatkan', ''),
(30, 26, 'Bangunan sabo yang dibangun / ditingkatkan', ''),
(31, 26, 'Checkdam yang dibangun / ditingkatkan', ''),
(32, 26, 'Breakwater yang dibangun / ditingkatkan', ''),
(33, 26, 'Seawall dan bangunan pengamanan pantai lainnya yang dibangun / ditingkatkan', ''),
(34, 26, 'Flood Forecasting & Warning System &#40;FFWS&#41; yang dibangun / ditingkatkan', ''),
(35, 26, 'Tanggul yang direhabilitasi', ''),
(36, 26, 'Bangunan perkuatan tebing yang direhabilitasi', ''),
(37, 26, 'Pintu air / bendung pengendali banjir yang direhabilitasi', ''),
(38, 26, 'Kanal banjir yang direhabilitasi', ''),
(39, 26, 'Stasiun pompa banjir yang direhabilitasi', ''),
(40, 26, 'Polder / kolam retensi yang direhabilitasi', ''),
(41, 26, 'Saluran drainase yang direhabilitasi', ''),
(42, 26, 'Bangunan sabo yang direhabilitasi', ''),
(43, 26, 'Checkdam yang direhabilitasi', ''),
(44, 26, 'Breakwater yang direhabilitasi', ''),
(45, 26, 'Seawall dan bangunan pengamanan pantai lainnya yang direhabilitasi', ''),
(46, 26, 'Sungai yang direstorasi', ''),
(47, 26, 'Mata air yang dilindungi', ''),
(48, 26, 'Layanan Internal (Overhead)', ''),
(49, 27, 'Data  informasi dan komunikasi publik BBWS/BWS', ''),
(50, 27, 'Pola dan rencana pengelolaan SDA WS kewenangan Pusat yang disusun/direview', ''),
(51, 27, 'Hidrologi dan kualitas air yang dikelola', ''),
(52, 27, 'Kelembagaan pengelolaan SDA yang ditingkatkan kapasitasnya', ''),
(53, 27, 'Rekomtek pemanfaatan SDA yang disusun', ''),
(54, 27, 'Layanan Internal (Overhead)', ''),
(55, 28, 'Rencana teknis dan dokumen lingkungan hidup untuk konstruksi bendungan dan bangunan penampung air lainnya', ''),
(56, 28, 'Bendungan baru yang dibangun', ''),
(57, 28, 'Bendungan dalam tahap pelaksanaan (on-going)', ''),
(58, 28, 'Embung dan bangunan penampung air lainnya yang dibangun', ''),
(59, 28, 'Bendungan yang direhabilitasi', ''),
(60, 28, 'Embung dan bangunan penampung air lainnya yang direhabilitasi', ''),
(61, 28, 'Danau yang direvitalisasi', ''),
(62, 28, 'Layanan Internal (Overhead)', ''),
(63, 29, 'Rencana teknis dan dokumen lingkungan hidup untuk konstruksi air tanah dan air baku', ''),
(64, 29, 'Sumur air tanah untuk air baku yang dibangun', ''),
(65, 29, 'Embung air baku yang dibangun', ''),
(66, 29, 'Unit air baku yang dibangun', ''),
(67, 29, 'Sumur JIAT yang dibangun / ditingkatkan', ''),
(68, 29, 'Jaringan irigasi air tanah (JIAT) yang dibangun / ditingkatkan', ''),
(69, 29, 'Sumur air tanah untuk air baku yang direhabilitasi', ''),
(70, 29, 'Embung air baku yang direhabilitasi', ''),
(71, 29, 'Unit air baku yang direhabilitasi', ''),
(72, 29, 'Sumur JIAT yang direhabilitasi', ''),
(73, 29, 'Jaringan irigasi air tanah (JIAT) yang direhabilitasi', ''),
(74, 29, 'Layanan Internal (Overhead)', ''),
(75, 30, 'Jaringan irigasi permukaan kewenangan Pusat yang dioperasikan dan dipelihara', ''),
(76, 30, 'Bendung irigasi permukaan kewenangan Pusat yang dioperasikan dan dipelihara', ''),
(77, 30, 'Jaringan irigasi rawa yang dioperasikan dan dipelihara', ''),
(78, 30, 'Jaringan irigasi tambak yang dioperasikan dan dipelihara', ''),
(79, 30, 'Unit pengelola irigasi', ''),
(80, 30, 'Tanggul dan tebing yang dioperasikan dan dipelihara', ''),
(81, 30, 'Kanal banjir yang dioperasikan dan dipelihara', ''),
(82, 30, 'Stasiun pompa banjir yang dioperasikan dan dipelihara', ''),
(83, 30, 'Polder / kolam retensi yang dioperasikan dan dipelihara', ''),
(84, 30, 'Bangunan sabo yang dipelihara', ''),
(85, 30, 'Checkdam yang dioperasikan dan dipelihara', ''),
(86, 30, 'Breakwater / seawall dan bangunan pengamanan pantai lainnya yang dipelihara', ''),
(87, 30, 'Sungai yang dipelihara', ''),
(88, 30, 'Mata air yang dipelihara', ''),
(89, 30, 'Bendungan yang dioperasikan dan dipelihara', ''),
(90, 30, 'Unit pengelola bendungan', ''),
(91, 30, 'Embung dan bangunan penampung air lainnya yang dioperasikan dan dipelihara', ''),
(92, 30, 'Danau yang dipelihara', ''),
(93, 30, 'Sumur air tanah untuk air baku yang dioperasikan dan dipelihara', ''),
(94, 30, 'Embung air baku yang dioperasikan dan dipelihara', ''),
(95, 30, 'Sumur JIAT yang dioperasikan dan dipelihara', ''),
(96, 30, 'Jaringan irigasi air tanah (JIAT)  yang dioperasikan dan dipelihara', ''),
(97, 30, 'Unit air baku yang dioperasikan dan dipelihara', ''),
(98, 30, 'Alokasi air yang disusun', 'tes Evaluasi (berhasil)'),
(99, 30, 'Kegiatan tanggap darurat akibat bencana', ''),
(100, 30, 'Peralatan dan fasilitas pendukung O&P yang dioperasikan dan dipelihara', ''),
(101, 30, 'Kawasan rawa yang dipelihara', ''),
(102, 30, 'Layanan Internal (Overhead)', ''),
(103, 24, 'Output Percobaan', 'Hanya untuk evaluasi sistem'),
(104, 31, 'Panjang Kanal Banjir Yang Dibangun/Ditingkatkan', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppk`
--

CREATE TABLE `tbl_ppk` (
  `id_ppk` int(11) NOT NULL,
  `id_satker` int(11) NOT NULL,
  `nama_ppk` varchar(255) NOT NULL,
  `tahun_ppk` varchar(255) NOT NULL,
  `des_ppk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ppk`
--

INSERT INTO `tbl_ppk` (`id_ppk`, `id_satker`, `nama_ppk`, `tahun_ppk`, `des_ppk`) VALUES
(1, 1, 'KETATALAKSANAAN', '', ''),
(2, 1, 'PERENCANAAN DAN PROGRAM', '', ''),
(5, 5, 'IRIGASI DAN RAWA I', '', ''),
(8, 4, 'IRIGASI DAN RAWA I', '', ''),
(10, 6, 'SUNGAI DAN PANTAI I', '', ''),
(12, 4, 'IRIGASI DAN RAWA II', '', ''),
(14, 3, 'OPERASI DAN PEMELIHARAAN SDA I', '', ''),
(15, 3, 'OPERASI DAN PEMELIHARAAN SDA II', '', ''),
(16, 3, 'OPERASI DAN PEMELIHARAAN SDA III', '', ''),
(18, 6, 'SUNGAI DAN PANTAI II', '', ''),
(19, 2, 'SUNGAI DAN PANTAI I', '', ''),
(20, 2, 'SUNGAI DAN PANTAI II', '', ''),
(21, 2, 'SUNGAI DAN PANTAI III', '', ''),
(23, 2, 'KONSERVASI DANAU SITU DAN EMBUNG', '', ''),
(24, 4, 'AIR TANAH DAN AIR BAKU', '', ''),
(25, 5, 'IRIGASI DAN RAWA II', '', ''),
(26, 5, 'AIR TANAH DAN AIR BAKU', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_progres_laporan`
--

CREATE TABLE `tbl_progres_laporan` (
  `id_progres_laporan` int(11) NOT NULL,
  `progres_laporan` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_progres_laporan`
--

INSERT INTO `tbl_progres_laporan` (`id_progres_laporan`, `progres_laporan`) VALUES
(1, 'Not Started'),
(2, 'On Progres'),
(3, 'Finished');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_satker`
--

CREATE TABLE `tbl_satker` (
  `id_satker` int(11) NOT NULL,
  `nama_satker` varchar(255) NOT NULL,
  `tahun_satker` varchar(255) NOT NULL,
  `des_satker` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_satker`
--

INSERT INTO `tbl_satker` (`id_satker`, `nama_satker`, `tahun_satker`, `des_satker`) VALUES
(1, 'SATUAN KERJA BALAI WILAYAH SUNGAI SULAWESI III', '', ''),
(2, 'SATUAN KERJA OPERASI DAN PEMELIHARAAN SUMBER DAYA AIR SULAWESI III', '', ''),
(3, 'SNVT PELAKSANAAN JARINGAN SUMBER AIR WS. PALU-LARIANG, WS. PARIGI-POSO, WS. KALUKU-KARAMA PROVINSI SULAWESI TENGAH', '', ''),
(4, 'SNVT PELAKSANAAN JARINGAN SUMBER AIR WS. KALUKU-KARAMA, WS. PALU-LARIANG PROVINSI SULAWESI BARAT', '', ''),
(5, 'SNVT PELAKSANAAN JARINGAN PEMANFAATAN AIR WS. PALU-LARIANG, WS. PARIGI-POSO, WS. KALUKU-KARAMA PROVINSI SULAWESI TENGAH', '', ''),
(6, 'SNVT PELAKSANAAN JARINGAN PEMANFAATAN AIR WS. KALUKU-KARAMA, WS. PALU-LARIANG PROVINSI SULAWESI BARAT', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_satuan`
--

CREATE TABLE `tbl_satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(250) NOT NULL,
  `des_satuan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_satuan`
--

INSERT INTO `tbl_satuan` (`id_satuan`, `nama_satuan`, `des_satuan`) VALUES
(1, 'Layanan', ''),
(2, 'Km', ''),
(3, 'Hektar', ''),
(4, 'm³/detik', ''),
(5, 'Juta m³', ''),
(6, 'Laporan', ''),
(7, 'Dokumen', ''),
(8, 'Buah', ''),
(9, 'Bendung', ''),
(10, 'Bendungan', ''),
(11, 'Titik', ''),
(12, 'Sungai', ''),
(13, 'Unit', ''),
(14, 'Danau', ''),
(28, '%', ''),
(29, 'N/S/P/K', ''),
(30, 'Instansi', ''),
(31, 'Mata air', ''),
(32, 'Lembaga', ''),
(33, 'Kegiatan', ''),
(34, 'BBWS/BWS', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_satuan_outcome`
--

CREATE TABLE `tbl_satuan_outcome` (
  `id_satuan_outcome` int(11) NOT NULL,
  `nama_satuan_outcome` varchar(250) NOT NULL,
  `des_satuan_outcome` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_satuan_outcome`
--

INSERT INTO `tbl_satuan_outcome` (`id_satuan_outcome`, `nama_satuan_outcome`, `des_satuan_outcome`) VALUES
(1, 'Layanan', ''),
(2, 'Km', ''),
(3, 'Hektar', ''),
(4, 'm³/detik', ''),
(5, 'Juta m³', ''),
(6, 'Laporan', ''),
(7, 'Dokumen', ''),
(8, 'Buah', ''),
(9, 'Bendung', ''),
(10, 'Bendungan', ''),
(11, 'Titik', ''),
(12, 'Sungai', ''),
(13, 'Unit', ''),
(14, 'Danau', ''),
(15, '%', ''),
(16, 'N/S/P/K', ''),
(17, 'Instansi', ''),
(18, 'Mata air', ''),
(19, 'Lembaga', ''),
(20, 'Kegiatan', ''),
(21, 'BBWS/BWS', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_satuan_output`
--

CREATE TABLE `tbl_satuan_output` (
  `id_satuan_output` int(11) NOT NULL,
  `nama_satuan_output` varchar(250) NOT NULL,
  `des_satuan_output` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_satuan_output`
--

INSERT INTO `tbl_satuan_output` (`id_satuan_output`, `nama_satuan_output`, `des_satuan_output`) VALUES
(1, 'Layanan', ''),
(2, 'Km', ''),
(3, 'Hektar', ''),
(4, 'm³/detik', ''),
(5, 'Juta m³', ''),
(6, 'Laporan', ''),
(7, 'Dokumen', ''),
(8, 'Buah', ''),
(9, 'Bendung', ''),
(10, 'Bendungan', ''),
(11, 'Titik', ''),
(12, 'Sungai', ''),
(13, 'Unit', ''),
(14, 'Danau', ''),
(15, '%', ''),
(16, 'N/S/P/K', ''),
(17, 'Instansi', ''),
(18, 'Mata air', ''),
(19, 'Lembaga', ''),
(20, 'Kegiatan', ''),
(21, 'BBWS/BWS', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tahun`
--

CREATE TABLE `tbl_tahun` (
  `id_thn` int(11) NOT NULL,
  `nama_tahun` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tahun`
--

INSERT INTO `tbl_tahun` (`id_thn`, `nama_tahun`) VALUES
(1, '2001'),
(2, '2002'),
(3, '2003'),
(4, '2004'),
(5, '2005'),
(6, '2006'),
(7, '2007'),
(8, '2008'),
(9, '2009'),
(10, '2010'),
(11, '2011'),
(12, '2012'),
(13, '2013'),
(14, '2014'),
(15, '2015'),
(16, '2016'),
(17, '2017'),
(18, '2018'),
(19, '2019'),
(20, '2020'),
(21, '2021'),
(22, '2022'),
(23, '2023'),
(24, '2024'),
(25, '2025'),
(26, '2026'),
(27, '2027'),
(28, '2028'),
(29, '2029'),
(30, '2030');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL,
  `active_since` date NOT NULL,
  `id_satker` int(11) DEFAULT NULL,
  `id_ppk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `nama`, `password`, `email`, `level`, `active_since`, `id_satker`, `id_ppk`) VALUES
(15, 'admin', 'Andi Anzanul Zikra', '$2a$12$JjEOe5FyXQOSuj7QlUrIy.rNXyTgwnl0lVxMFH2GFAYWyxdFfmg5W', 'iflapor@gmail.com', 'superadmin', '2018-11-19', NULL, 0),
(20, 'zikra', 'CV informasi kita bersama', '$2a$12$JjEOe5FyXQOSuj7QlUrIy.rNXyTgwnl0lVxMFH2GFAYWyxdFfmg5W', 'andianzanul@gmail.com', 'konsultan', '2019-06-24', NULL, 2),
(24, 'oping', 'PT Potomu net', '$2a$12$JjEOe5FyXQOSuj7QlUrIy.rNXyTgwnl0lVxMFH2GFAYWyxdFfmg5W', 'zikra.zigzag1@gmail.com', 'direksi', '2019-08-20', NULL, 0),
(25, 'hari', 'PT Transcosmos', '$2a$12$tZApu777uh8ZfAiQX8b8OuokwavLwisE9S2ayLyJ14jRsFJ57tPLa', 'mhjhariel@gmail.com', 'konsultan', '2019-08-20', NULL, 0),
(26, 'Dian', 'CV Dian Utami', '$2a$12$43iH8nPAnbMllSRsiKpg8.3UOwHjYHVWb8M/x2UT1hwON.D6xjTGa', 'fdianutami@gmail.com', 'konsultan', '2019-08-22', NULL, 0),
(27, 'iflapor', 'iflapor', '$2a$12$31hXTgr8kwgLNHAXVGv1m.eQw033B2Hds3CEeSAqg/By.o31FW/LK', 'iflapor@gmail.com', 'direksi', '2019-08-22', NULL, 0),
(31, 'tiffanimantong', 'Tiffani Mantong', '$2a$12$14flqN.fIemtSOlIJnKmyerkFn.ZPi1eukqHPUarFZm8pUEd9Cbm6', 'tiffanimantong77@gmail.com', 'direksi', '2019-09-04', NULL, 0),
(32, 'awind', 'Muhamad Munawir', '$2a$12$Ybz5CQkyGywYgnJnuOg6aenNXLrthElBof74xeE92ImAkWKv7Wpem', 'mmunawir22@gmail.com', 'superadmin', '2019-09-05', NULL, 0),
(34, 'Satyakarsa', 'PT. Satyakarsa Mudatama', '$2a$12$2jBqisyA4LD8Lh0dd35XS.sgz/ha/lPj2PVhXxgCjNAePifgM7VAC', 'fikar.donk2@gmail.com', 'konsultan', '2019-09-05', NULL, 0),
(36, 'aristantha', 'Fatan', '$2a$12$7pHrDktTokILWO.eKriFDO1NnAipg1CagpZWOJLvpAmgA/EGS0wYa', 'aristantha@gmail.com', 'direksi', '2019-09-05', NULL, 0),
(37, 'ariesto', 'Ariesto Krestiadi, ST., M.Eng', '$2a$12$.RslNKkDrNy5mTty/SfEV.yMLjUwYOUQxUMQ6a8wYmAtPXc5ZnFB.', 'ariestok@gmail.com', 'superadmin', '2019-09-06', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wlsungai`
--

CREATE TABLE `tbl_wlsungai` (
  `id_wlsungai` int(11) NOT NULL,
  `nama_wlsungai` varchar(255) NOT NULL,
  `des_wlsungai` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wlsungai`
--

INSERT INTO `tbl_wlsungai` (`id_wlsungai`, `nama_wlsungai`, `des_wlsungai`) VALUES
(1, 'WS. PALU-LARIANG', 'ini adalah deskripsi palu lariang'),
(2, 'WS. PARIGI-POSO', 'ini adalah deskripsi palu poso'),
(3, 'WS. KALUKKU-KARAMA', 'WS. KALUKKU-KARAMA'),
(4, 'WS. LAMBUNU-BUOL', 'Wilayah Sungai Lambunu Buol'),
(5, 'WS. LAA-TAMBALAKO', 'Wilayah Sungai Laa-Tambalako'),
(6, 'WS. BONGKA-MENTAWA', 'Wilayah Sungai Bongka Mentawa'),
(7, 'WS. BANGGAI KEPULAUAN', 'Wilayah Sungai Banggai Kepulauan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regencies_province_id_index` (`id_provinsi`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kota` (`id_kabupaten`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kec_id` (`id_kecamatan`);

--
-- Indexes for table `kotadesa`
--
ALTER TABLE `kotadesa`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `tbl_amandemen`
--
ALTER TABLE `tbl_amandemen`
  ADD PRIMARY KEY (`id_amd`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tbl_asistensi`
--
ALTER TABLE `tbl_asistensi`
  ADD PRIMARY KEY (`id_asistensi`);

--
-- Indexes for table `tbl_asistensi_lbr`
--
ALTER TABLE `tbl_asistensi_lbr`
  ADD PRIMARY KEY (`id_asistensi_lbr`);

--
-- Indexes for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `tbl_das`
--
ALTER TABLE `tbl_das`
  ADD PRIMARY KEY (`id_das`);

--
-- Indexes for table `tbl_gambar`
--
ALTER TABLE `tbl_gambar`
  ADD PRIMARY KEY (`id_gambar`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tbl_jenis_kegiatan`
--
ALTER TABLE `tbl_jenis_kegiatan`
  ADD PRIMARY KEY (`id_jenis_kegiatan`);

--
-- Indexes for table `tbl_kajian_lingkungan`
--
ALTER TABLE `tbl_kajian_lingkungan`
  ADD PRIMARY KEY (`id_kajian_lingkungan`);

--
-- Indexes for table `tbl_kegiatan`
--
ALTER TABLE `tbl_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `tbl_kegiatan_das`
--
ALTER TABLE `tbl_kegiatan_das`
  ADD PRIMARY KEY (`id_kegiatan_das`),
  ADD KEY `id_kegiatan` (`id_kegiatan`),
  ADD KEY `id_das` (`id_das`),
  ADD KEY `id_wlsungai` (`id_wlsungai`);

--
-- Indexes for table `tbl_kegiatan_detail_konstruksi`
--
ALTER TABLE `tbl_kegiatan_detail_konstruksi`
  ADD PRIMARY KEY (`id_detail_konstruksi`),
  ADD KEY `idkegiatan_dk` (`id_kegiatan`);

--
-- Indexes for table `tbl_kegiatan_files`
--
ALTER TABLE `tbl_kegiatan_files`
  ADD PRIMARY KEY (`id_file_kegiatan`);

--
-- Indexes for table `tbl_kegiatan_lokasi`
--
ALTER TABLE `tbl_kegiatan_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kegiatan_realisasi_pelaksanaan`
--
ALTER TABLE `tbl_kegiatan_realisasi_pelaksanaan`
  ADD PRIMARY KEY (`id_realisasi_pelaksanaan`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tbl_kegiatan_relasi`
--
ALTER TABLE `tbl_kegiatan_relasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indexes for table `tbl_kegiatan_ws`
--
ALTER TABLE `tbl_kegiatan_ws`
  ADD PRIMARY KEY (`id_kegiatan_ws`),
  ADD KEY `id_kegiatan` (`id_kegiatan`),
  ADD KEY `id_wlsungai` (`id_wlsungai`);

--
-- Indexes for table `tbl_koordinat`
--
ALTER TABLE `tbl_koordinat`
  ADD PRIMARY KEY (`id_koordinat`);

--
-- Indexes for table `tbl_output`
--
ALTER TABLE `tbl_output`
  ADD PRIMARY KEY (`id_output`);

--
-- Indexes for table `tbl_ppk`
--
ALTER TABLE `tbl_ppk`
  ADD PRIMARY KEY (`id_ppk`);

--
-- Indexes for table `tbl_progres_laporan`
--
ALTER TABLE `tbl_progres_laporan`
  ADD PRIMARY KEY (`id_progres_laporan`);

--
-- Indexes for table `tbl_satker`
--
ALTER TABLE `tbl_satker`
  ADD PRIMARY KEY (`id_satker`);

--
-- Indexes for table `tbl_satuan`
--
ALTER TABLE `tbl_satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `tbl_satuan_outcome`
--
ALTER TABLE `tbl_satuan_outcome`
  ADD PRIMARY KEY (`id_satuan_outcome`);

--
-- Indexes for table `tbl_satuan_output`
--
ALTER TABLE `tbl_satuan_output`
  ADD PRIMARY KEY (`id_satuan_output`);

--
-- Indexes for table `tbl_tahun`
--
ALTER TABLE `tbl_tahun`
  ADD PRIMARY KEY (`id_thn`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `id_satker` (`id_satker`);

--
-- Indexes for table `tbl_wlsungai`
--
ALTER TABLE `tbl_wlsungai`
  ADD PRIMARY KEY (`id_wlsungai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1670;

--
-- AUTO_INCREMENT for table `kotadesa`
--
ALTER TABLE `kotadesa`
  MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbl_amandemen`
--
ALTER TABLE `tbl_amandemen`
  MODIFY `id_amd` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_asistensi`
--
ALTER TABLE `tbl_asistensi`
  MODIFY `id_asistensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `tbl_asistensi_lbr`
--
ALTER TABLE `tbl_asistensi_lbr`
  MODIFY `id_asistensi_lbr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_das`
--
ALTER TABLE `tbl_das`
  MODIFY `id_das` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=659;

--
-- AUTO_INCREMENT for table `tbl_gambar`
--
ALTER TABLE `tbl_gambar`
  MODIFY `id_gambar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_jenis_kegiatan`
--
ALTER TABLE `tbl_jenis_kegiatan`
  MODIFY `id_jenis_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_kajian_lingkungan`
--
ALTER TABLE `tbl_kajian_lingkungan`
  MODIFY `id_kajian_lingkungan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_kegiatan`
--
ALTER TABLE `tbl_kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_das`
--
ALTER TABLE `tbl_kegiatan_das`
  MODIFY `id_kegiatan_das` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=488;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_detail_konstruksi`
--
ALTER TABLE `tbl_kegiatan_detail_konstruksi`
  MODIFY `id_detail_konstruksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_files`
--
ALTER TABLE `tbl_kegiatan_files`
  MODIFY `id_file_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=287;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_lokasi`
--
ALTER TABLE `tbl_kegiatan_lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_realisasi_pelaksanaan`
--
ALTER TABLE `tbl_kegiatan_realisasi_pelaksanaan`
  MODIFY `id_realisasi_pelaksanaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_relasi`
--
ALTER TABLE `tbl_kegiatan_relasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_kegiatan_ws`
--
ALTER TABLE `tbl_kegiatan_ws`
  MODIFY `id_kegiatan_ws` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tbl_koordinat`
--
ALTER TABLE `tbl_koordinat`
  MODIFY `id_koordinat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_output`
--
ALTER TABLE `tbl_output`
  MODIFY `id_output` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `tbl_ppk`
--
ALTER TABLE `tbl_ppk`
  MODIFY `id_ppk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_progres_laporan`
--
ALTER TABLE `tbl_progres_laporan`
  MODIFY `id_progres_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_satker`
--
ALTER TABLE `tbl_satker`
  MODIFY `id_satker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_satuan`
--
ALTER TABLE `tbl_satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tbl_satuan_outcome`
--
ALTER TABLE `tbl_satuan_outcome`
  MODIFY `id_satuan_outcome` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_satuan_output`
--
ALTER TABLE `tbl_satuan_output`
  MODIFY `id_satuan_output` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_tahun`
--
ALTER TABLE `tbl_tahun`
  MODIFY `id_thn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_wlsungai`
--
ALTER TABLE `tbl_wlsungai`
  MODIFY `id_wlsungai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`id_satker`) REFERENCES `tbl_satker` (`id_satker`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
