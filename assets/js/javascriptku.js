$(document).ready( function(){

	$( ".help-popup" ).on({
		mouseover: function() {
			$(this).popover('show');
		}, 
		mouseout: function() {
			$(this).popover('hide');
		}
	});

	$(".select2").select2({
	  tags: true,
	  tokenSeparators: [',', ' ']
	});


	/* Hapus gambar kegiatan */
	$('body').on('click','.btn-hapus',function(e){

		e.preventDefault();
		e.stopPropagation();

		var tombol = $(this);
		
		if(confirm("Anda yakin mengahpus?")){
			$.ajax({
				type: 'GET',
				url: tombol.attr('href'),
				dataType: 'html',
				beforeSend: function() { 
					
				},
				success: function(response) {
					tombol.parents('.box-btn-gambar').slideUp();
				}
			});
		} 
	});

	
	/* Hapus laporan */
	$('.btn-hapus-laporan').on('click',function(e){

		e.preventDefault();
		e.stopPropagation();

		var tombol = $(this);
		
		 swal({
			title: 'Hapus Laporan?',
			text: "Data yang sudah dihapus, tidak bisa di kembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal' 
			}).then(function () {
			  

				$.ajax({
					type: 'get', 
					url: appSettings.base_url + 'admin/kegiatan/delete_laporan/' + tombol.data('id'),
					dataType: 'json',
					beforeSend: function() { 
						
					},
					success: function(response) {

						if(response.status == 1){
							tombol.parents('tr').slideUp();
						}

						swal({
						  	title: response.title,
						  	text: response.message,
						  	type: response.type,
						  	timer: 2000,
							showConfirmButton:false
						});
					}
				});
 
			}, function (dismiss) {

			  // dismiss can be 'cancel', 'overlay',
			  // 'close', and 'timer'
			  if (dismiss === 'cancel') {
			  /*  swal(
			      'Cancelled',
			      'Your imaginary file is safe :)',
			      'error'
			    )*/
			  }
			}) 
	});



	/* Hapus asistensi */
	$('.btn-hapus-asistensi').on('click',function(e){

		e.preventDefault();
		e.stopPropagation();

		var tombol = $(this);
		
		 swal({
			title: 'Hapus Asistensi?',
			text: "Data yang sudah dihapus, tidak bisa di kembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal' 
			}).then(function () {
			  

				$.ajax({
					type: 'get', 
					url: appSettings.base_url + 'admin/kegiatan/delete_asistensi/' + tombol.data('id'),
					dataType: 'json',
					beforeSend: function() { 
						
					},
					success: function(response) {
						if(response.status == 1){
							tombol.parents('tr').slideUp();
						}

						swal({
						  title: response.title,
						  text: response.message,
						  type: response.type,
						  timer: 2000,
										    showConfirmButton:false
						} 
							  )
					}
				});
 
			}, function (dismiss) {
			  // dismiss can be 'cancel', 'overlay',
			  // 'close', and 'timer'
			  if (dismiss === 'cancel') {
			  /*  swal(
			      'Cancelled',
			      'Your imaginary file is safe :)',
			      'error'
			    )*/
			  }
			}) 
	});

	/* Hapus kegiatan */
	$('.btn-hapus-kegiatan').on('click',function(e){

		//e.preventDefault();
		//e.stopPropagation();

		var tombol = $(this);
		
		 swal({
	 title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Hapus',
  cancelButtonText: 'Batal',
  //confirmButtonClass: 'btn btn-success',
  //cancelButtonClass: 'btn btn-danger',
  //buttonsStyling: false
			}).then(function () {
			  

				$.ajax({
					type: 'get', 
					url: appSettings.base_url + 'admin/kegiatan/delete/' + tombol.data('id'),
					dataType: 'json',
					beforeSend: function() { 
						
					},
					success: function(response) {
						if(response.status == 1){
							tombol.parents('tr').slideUp();
						}

						swal({
			  title: response.title,
			  text: response.message,
			  type: response.type,
			  timer: 2000,
							    showConfirmButton:false
			} 
							  )
					}
				});

			  



			}, function (dismiss) {
			  // dismiss can be 'cancel', 'overlay',
			  // 'close', and 'timer'
			  if (dismiss === 'cancel') {
			  /*  swal(
			      'Cancelled',
			      'Your imaginary file is safe :)',
			      'error'
			    )*/
			  }
			}) 
	});


	$('#cekAllIdKegiatan').on('change',function(e){

		var tombol = $(this);
		if( tombol.prop( "checked" ) ){
			$('.id-kegiatan-main').prop( "checked",true );

		}else{
			$('.id-kegiatan-main').prop( "checked",false );
		}

	});

	/* WILAYAH SUNGAI */
	 $('.cek-ws').on('change',function(e){

		var tombol = $(this);

		if( tombol.prop( "checked" ) ){

			$.ajax({
				type: 'post',
				data:{
					id : tombol.val(),
					nama : tombol.data('nama')
				},
				url: appSettings.base_url + 'admin/kegiatan/get_form_wilayah_sungai',
				dataType: 'html',
				beforeSend: function() { 
					$('.cek-ws').prop( "disabled", true );
				},
				success: function(response) {
					 $("#box-ws").append(response);
					 $('.cek-ws').prop( "disabled", false );

					 $(".select2").select2({
					 	 placeholder: 'Pilih DAS', 
					});
				}
			});


		}else{

			swal({
				title: 'Hapus '+ tombol.data('nama') +'?',
				text: "Tidak bisa dibatalkan",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Hapus',
				cancelButtonText: 'Batal',
				//confirmButtonClass: 'btn btn-success',
				//cancelButtonClass: 'btn btn-danger',
				//buttonsStyling: false
			}).then(function () { 

				$.ajax({
					type: 'post',
					data:{
						id : tombol.val(), // provinsi id
						id_kegiatan : tombol.data('kegiatan'), // id kegiatan
						nama : tombol.data('nama')
					},
					url: appSettings.base_url + 'admin/kegiatan/delete_form_ws',
					dataType: 'html',
					beforeSend: function() { 
						$('.cek-ws').prop( "disabled", true );
					},
					success: function(response) {
						 $("#box-ws").append(response);
						 $('.cek-ws').prop( "disabled", false );
					}
				});

			  	$('#box-ws').find('.ws-'+ tombol.val()).remove()



			}, function (dismiss) {
			   tombol.prop( "checked",true );
			}); 
		}
	});

	 /* PROVINSI 2*/
	 /*$(".combo_kabupaten, .combo-kecamatan").select2({
	  tags: true,
	  tokenSeparators: [',', ' ']
	});*/


	/* PROVINSI */
	var hapus_combo_options = function(combo){
		combo.find('option').each(function(i, item){ 
				 	if(i != 0){
				 		item.remove();
				 	}
				 });
	}


	/* handle checkbox provinsi */
	$('.cek-provinsi').on('change',function(e){

		var tombol = $(this);

		if( tombol.prop( "checked" ) ){

			$.ajax({
				type: 'post',
				data:{
					id : tombol.val(),
					nama : tombol.data('nama')
				},
				url: appSettings.base_url + 'admin/kegiatan/get_form_lokasi',
				dataType: 'html',
				beforeSend: function() { 
					$('.cek-provinsi').prop( "disabled", true );
				},
				success: function(response) {
					 $("#box-lokasi").append(response);
					 $('.cek-provinsi').prop( "disabled", false );
				}
			});
		}else{

			swal({
				title: 'Hapus '+ tombol.data('nama') +' ?',
				text: "Data yang telah dihapus tidak dapat dikembalikan!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Hapus',
				cancelButtonText: 'Batal'
			}).then(function () { 

				$.ajax({
					type: 'post',
					data:{
						id : tombol.val(), // provinsi id
						id_kegiatan : tombol.data('kegiatan'), // id kegiatan
						nama : tombol.data('nama')
					},
					url: appSettings.base_url + 'admin/kegiatan/delete_form_lokasi',
					dataType: 'html',
					beforeSend: function() { 
						$('.cek-provinsi').prop( "disabled", true );
					},
					success: function(response) {
						$("#box-lokasi").append(response);
						$('.cek-provinsi').prop( "disabled", false );
					}
				});

			  	$('#box-lokasi').find('.lokasi-'+ tombol.val()).remove()

			}, function (dismiss) {
			   tombol.prop( "checked",true );
			}); 
		}
	});

	/* button tambah lokasi (kabupaten) */
	$('body').on('click','.btn-add-lokasi', function(e){ 
		$(this).parents('.item-lokasi').find('.box-lokasi').append( $(this).parents('.item-lokasi').find('.lokasi-tambah-daya').html() );
	});


	/* hapus lokasi temp */
	/*$('body').on('click','.btn-hapus-lokasi-123', function(e){
		var tombol = $(this);
		tombol.parents('.row').remove(); 
	});*/

	/* hapus lokasi ke server */
	/*$('body').on('click','.btn-hapus-lokasi-saat-edit',function(e){

		var tombol = $(this);

		swal({
				title: 'Apakah anda yakin?',
				text: "Tindakan ini akan menghapus langsung ke database",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Hapus',
				cancelButtonText: 'Batal'
				 
			}).then(function () { 

				$.ajax({
					type: 'post',
					data:{
						id : tombol.data('id')
					},
					url: appSettings.base_url + 'admin/kegiatan/delete_lokasi',
					dataType: 'html',
					beforeSend: function() { 
						 
					},
					success: function(response) {
						 
						tombol.parents('.row-aja').remove(); 
					}
				}); 
			}, function (dismiss) {
			    
			});
	});*/

	//
	var hapus_lokasi_kegiatan = function(prov, kab,kec,kel){
		$.ajax({
			type: 'post',
			data:{
				id : $("input[name='id']").val(), 
				prov : prov, 
				kab : kab, 
				kec : kec, 
				kel : kel
			},
			url: appSettings.base_url + 'admin/kegiatan/delete_lokasi_kegiatan',
			dataType: 'json',
			beforeSend: function() { 
				console.log("hapus lokasi ...")
			} 
		});
	}

	/* load kecamatan saat form kabupaten di pilih / berubah */ 
	//input[name='cekKab']
	$('body').on('change', ".cekKab", function(e){

		var tombol = $(this);
		var box = tombol.parents('.row-lokasi');
		var kecamatan_cek = tombol.parents('.row-lokasi').find('.kecamatan-cek');

		if ( !tombol.is(':checked')) {
			$('#box-list-kec-kab-' + $(this).val() ).remove(); 
			hapus_lokasi_kegiatan(box.data('prov'), $(this).val(),'','');
		}

		var values = new Array();
		var selected_kab = box.find("input.cekKab:checked");
		$.each(selected_kab, function() {
			console.log($(this).val())
		  	values.push($(this).val()); 
		});


		$.each(selected_kab, function() { 
			if(!kecamatan_cek.find('#box-list-kec-kab-' + $(this).val() ).size() ){
				kecamatan_cek.append('<div id="box-list-kec-kab-'+ $(this).val()+'"><b>'+ $(this).parents('.checkbox-inline').find('span').html() +'</b><br></div>');
			}
		});


		 
		$.ajax({
			type: 'post',
			data:{
				id : values, 
			},
			url: appSettings.base_url + 'admin/kegiatan/get_kec_by_kab',
			dataType: 'json',
			beforeSend: function() { 
				// hapus semua option combo kecamatan
				//hapus_combo_options(kec); 
				//hapus_combo_options(kelurahan); 
				//kec.prop( "disabled",true );
				//kelurahan.prop( "disabled",true );
			},
			success: function(response) {
				  
				  
				 $.each(response,function(i,item) { 
				 	if(  !kecamatan_cek.find('#item-kec-'+  item.id ).size() ){
				 		$('#box-list-kec-kab-'+ item.id_kabupaten).append('<label class="checkbox-inline" id="item-kec-'+  item.id +'">' +
                                '<input data-kab="'+ item.id_kabupaten +'" class="cekKec" type="checkbox" name="cekkec['+ item.id_kabupaten +'][]" value="'+  item.id +'"><span>'+  item.name +'</span></label><br>');
				 	}
				 	
				 });

				// kec.prop( "disabled",false );
				 //kelurahan.prop( "disabled",false );
			}
		});
	});


	$('body').on('change', ".cekKec", function(e){

		var tombol = $(this);
		var box = tombol.parents('.row-lokasi'); 
		var kelurahan_cek = tombol.parents('.row-lokasi').find('.kelurahan-cek');

		if ( !tombol.is(':checked')) {
			$('#box-list-kelurahan-kec-' + $(this).val() ).remove();

			hapus_lokasi_kegiatan(box.data('prov'), $(this).data('kab'),$(this).val(),'');
		}

		var values = new Array();
		var selected_kab = box.find("input.cekKec:checked");
		$.each(selected_kab, function() {
			console.log($(this).val())
		  	values.push($(this).val()); 
		});


		$.each(selected_kab, function() { 
			if(!kelurahan_cek.find('#box-list-kelurahan-kec-' + $(this).val() ).size() ){
				kelurahan_cek.append('<div id="box-list-kelurahan-kec-'+ $(this).val()+'"><b>'+ $(this).parents('.checkbox-inline').find('span').html() +'</b><br></div>');
			}
		});


		 
		$.ajax({
			type: 'post',
			data:{
				id : values, 
			},
			url: appSettings.base_url + 'admin/kegiatan/get_kelurahan_by_kec',
			dataType: 'json',
			beforeSend: function() { 
				// hapus semua option combo kecamatan
				//hapus_combo_options(kec); 
				//hapus_combo_options(kelurahan); 
				//kec.prop( "disabled",true );
				//kelurahan.prop( "disabled",true );
			},
			success: function(response) {
				  
				  
				 $.each(response,function(i,item) { 
				 	if(  !kelurahan_cek.find('#item-kel-'+  item.id ).size() ){
				 		$('#box-list-kelurahan-kec-'+ item.id_kecamatan).append('<label class="checkbox-inline" id="item-kel-'+  item.id +'">' +
                                '<input  type="checkbox" name="cekkel['+item.id_kecamatan+'][]" value="'+  item.id +'">'+  item.name +'</label><br>');
				 	}
				 	
				 });

				// kec.prop( "disabled",false );
				 //kelurahan.prop( "disabled",false );
			}
		});
	});


	$('body').on('change', ".cekKel", function(e){

		var tombol = $(this);
		var box = tombol.parents('.row-lokasi');  

		if ( !tombol.is(':checked')) { 
			hapus_lokasi_kegiatan('none', 'none','none',$(this).val());
		}

		 


		 


		 
		 
	});

	/* load kelurahan saat combo kec berubah */
	/*$('body').on('change', '.combo-kecamatan', function(e){

		var tombol = $(this);
		var kelurahan = tombol.parents('.row-lokasi').find('.combo-kelurahan');

		$.ajax({
			type: 'post',
			data:{
				id : tombol.val(), 
			},
			url: appSettings.base_url + 'admin/kegiatan/get_kelurahan_by_kec',
			dataType: 'json',
			beforeSend: function() {  
				 hapus_combo_options(kelurahan); 
				 kelurahan.prop( "disabled",true );
			},
			success: function(response) {
				 
				 $.each(response,function(i,item) {
				 	console.log(item)
				 	kelurahan.append( '<option value="'+ item.id +'">'+  item.name +'</option>' );
				 });
				 kelurahan.prop( "disabled",false );
			}
		});
	});*/

	/* end provinsi */


// start koordinat Zikra
$('#btn-add-koordinat').on('click',function(e){
		if(jumlah_koordinat < 50){
			var dd = $('#new-item-koordinat').html();

			$('#area-koordinat').append(dd	);
			jumlah_koordinat++;
		}else{
			alert("sudah maksimal!")
		}
		
	});

$('#btn-add-koordinat-garis').on('click',function(e){
		if(jumlah_koordinat < 50){
			var dd = $('#new-item-koordinat-garis').html();

			$('#area-koordinat-garis').append(dd	);
			jumlah_koordinat++;
		}else{
			alert("sudah maksimal!")
		}
		
	});
$('#btn-add-koordinat-luasan').on('click',function(e){
		if(jumlah_koordinat < 50){
			var dd = $('#new-item-koordinat-luasan').html();

			$('#area-koordinat-luasan').append(dd	);
			jumlah_koordinat++;
		}else{
			alert("sudah maksimal!")
		}
		
	});
	
	$('body').on('click','.btn-hapus-koordinat',function(e){

		var tombol = $(this);
		jumlah_koordinat--;
		tombol.parents('.item-koordinat').remove(); 

	});

	$('body').on('click','.btn-hapus-koordinat2',function(e){

		var tombol = $(this);

		swal({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				buttonsStyling: false
			}).then(function () { 

				$.ajax({
					type: 'post',
					data:{
						id_koordinat : tombol.data('id'),
						id_kegiatan : tombol.data('k')
					},
					url: appSettings.base_url + 'admin/kegiatan/delete_koordinat',
					dataType: 'html',
					beforeSend: function() { 
						 
					},
					success: function(response) {
						jumlah_koordinat--;
						tombol.parents('.item-koordinat').remove(); 
					}
				}); 
			}, function (dismiss) {
			    
			});
		

	});	$('body').on('click','.btn-hapus-koordinat3',function(e){

		var tombol = $(this);

		swal({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				buttonsStyling: false
			}).then(function () { 

				$.ajax({
					type: 'post',
					data:{
						id_koordinat : tombol.data('id'),
						id_kegiatan : tombol.data('k')
					},
					url: appSettings.base_url + 'admin/kegiatan/delete_koordinat',
					dataType: 'html',
					beforeSend: function() { 
						 
					},
					success: function(response) {
						jumlah_koordinat--;
						tombol.parents('.item-koordinat').remove(); 
					}
				}); 
			}, function (dismiss) {
			    
			});
		

	}); // end koordinat Zikra



	$('.btn-delete-amd').on('click',function(e){

		var tombol = $(this);

		if(confirm("Anda yakin mengahpus?")){
			$.ajax({
				type: 'post',
				data:{
					id : tombol.data('id')
				},
				url: appSettings.base_url + 'admin/kegiatan/delete_amd',
				dataType: 'html',
				beforeSend: function() { 
					
				},
				success: function(response) {
					tombol.parents('.item-amd').remove();

					$('.item-amd').each(function(a,b){
						$(this).find('.numberAMD').html(a + 1);
					});
				}
			});
		} 

	});


	/* add amd kegaitan */
	$('.btn-add-amd').on('click',function(e){

		var nomor_amb = parseInt($('.item-amd').size()) + 1;

		if(jumlah_amd < 20){
		var dd = '<div class="item-amd">'+
                '<div class="row">'+
                    '<div class="col-md-11">'+
                       ' <div class="row">'+
                       		'<div class="col-md-4">'+
                                '<div class="form-group">'+
                                   ' <label>Nomor Amandemen (<span class="numberAMD">'+ nomor_amb +'</span>)</label>'+
                                    '<input type="text" class="form-control" name="amd_nomor_add[]"  value="" />'+
                                '</div>'+
                            '</div>'+

                           '<div class="col-md-4">'+
                                '<div class="form-group">'+
                                    '<label>Tanggal Amandemen (<span class="numberAMD">'+ nomor_amb +'</span>)</label>'+
                                    '<input type="date" class="form-control" name="amd_tanggal_add[]"  value="" />'+
                                '</div>'+
                            '</div>   '+ 
                            '<div class="col-md-3">'+
                                '<div class="form-group">'+
                                    '<label>Nilai Amandemen (Rp) (<span class="numberAMD">'+ nomor_amb +'</span>)</label>'+
                                    '<input type="text" class="form-control input-rupiah2" name="amd_nilai_add[]"  value="" />'+
                                '</div>'+
                            '</div>   '+
                            
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1">'+
                        '<label> &nbsp;</label><br />'+
                        '<button type="button" class="btn btn-warning btn-del-amd"> - </button>'+
                    '</div>'+
                '</div>  '+
            '</div>';

		$('.box-amd').append(dd	);

		$('.input-rupiah2').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

		jumlah_amd++;
		}
		else{

		}
		
	});

	$('body').on('click','.btn-del-amd',function(e){

		 jumlah_amd--;

		$(this).parents('.item-amd').remove();

		$('.item-amd').each(function(a,b){
			$(this).find('.numberAMD').html(a + 1);
		});

		
	});


});