<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testmail extends CI_Controller{

    /* tampung var untuk di kirim ke view */
	public $data;

  /* apakah kirim email pemberitahuan ke konsultan saat asistensi */
  private $kirim_email_asistensi = true;

 	function __construct() {
        parent::__construct();

        
        $this->load->model('kegiatan_model');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
	    
        $this->load->helper('url');
        $this->load->helper('main');
        $this->load->helper('file');
        $this->load->helper('date');
        $this->load->helper('waktu');
    } 
    
    function zikra(){

        

      $this->load->config("email");
      $this->load->library('parser'); 
      $this->load->library('email'); 

      $pesan = 'Yth. {nama}<br /><br />Anda telah dipilih sebagai pihak penyedia jasa dalam pekerjaan {nama_kegiatan} Tahun Anggaran {tahun} di aplikasi CO-P. cek link di : {link}<br />';

       

      
      $data = array(
        'pesan'           => $pesan, 
        'tahun'            => 2019,
        'nama'            => "CV Buana jaya",
        'nama_kegiatan'   => "SID Gunung Emas",
        'link'   => site_url("admin/kegiatan"),
      );

      $message  = $this->parser->parse("default_template",$data,true); 
      $subject = "Pemberitahuan lembar asistensi online, Pekerjaan ";

      $this->email->from($this->config->item("email_sender"), $this->config->item("email_name"));
      $this->email->to("andianzanul@gmail.com"); 
      
      echo $message;

      $this->email->subject($subject);
      $this->email->message($message);  

     if ($this->email->send() == TRUE)
		{
			echo 'Terkirim!';
		}else{
			echo $this->email->print_debugger();
		}


    }
    
}
    
    
    
    ?>