<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller{

	
 	function __construct()
    {
        parent::__construct();

        $this->load->model('m_kabupaten');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
		$this->mainlib->logged_in();

		$this->data = array();

    }

	public function index(){

		$this->data['kabupaten'] = $this->m_kabupaten->get()->result_object(); 
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kabupaten/main');
        $this->load->view('admin/footer');

	}

    public function create() 
    {
     	$this->mainlib->batasi_akses_user(['superadmin']);

        $this->create_action();
        $this->set_provinsi();
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kabupaten/add');
        $this->load->view('admin/footer');
    }
    

    function set_provinsi(){

        $this->mainlib->batasi_akses_user(['superadmin']);

        $this->load->model('m_provinsi');
      
        $this->data['provinsi2'] = $this->m_provinsi->get()->result_object();
        $this->data['provinsi'] = array('--Pilih --');
       foreach( $this->data['provinsi2']  as $j){
            $this->data['provinsi'][$j->id_provinsi] = $j->provinsi;
        }
        
    }


    private function create_action() {


      	$this->form_validation->set_rules('provinsi', 'Provinsi', 'required|trim');
       $this->form_validation->set_rules('kab', 'Kabupaten', 'required|trim'); 

        if ($this->form_validation->run() == true) {


            $data = array( 
                'id_provinsi'    => $this->input->post('provinsi',TRUE),
        		'name'    => $this->input->post('kab',TRUE), 
         );

           if($this->m_kabupaten->insert($data)){
              $this->session->set_flashdata('message', 'Create Record Success');
              redirect(site_url('admin/kabupaten'));
           } else{
              show_error("aduh!");
           }
        }
    }



     function edit($id) {

    	$this->mainlib->batasi_akses_user(['superadmin']);

     	$this->form_validation->set_rules('provinsi', 'Provinsi', 'required|trim');
        $this->form_validation->set_rules('kab', 'Kabupaten', 'required|trim'); 

         if ($this->form_validation->run() == true) {


             $data = array(
            	'id_provinsi'    => $this->input->post('provinsi',TRUE),
        		'name'    => $this->input->post('kab',TRUE), 
 	   		 );

            if($this->m_kabupaten->update($data,$this->input->post('id',TRUE))){

            		$this->session->set_flashdata('message', 'Update Record Success');
            		redirect(site_url('admin/kabupaten'));

            } else{
            		show_error("aduh!");
            }

         }
      	$this->set_provinsi();
     	 $this->data['kab'] = $this->m_kabupaten->getById($id)->row();

     	$this->load->view('admin/header', $this->data);
         $this->load->view('admin/kabupaten/edit');
         $this->load->view('admin/footer');
     }


     function delete($id){
    	
        $this->mainlib->batasi_akses_user(['superadmin']);
        
     	if($this->m_kabupaten->delete($id)){
        		$this->session->set_flashdata('message', 'Delete Record Success');
        		redirect(site_url('admin/kabupaten'));
        } else{
        		show_error("aduh!");
        }

     }





 }