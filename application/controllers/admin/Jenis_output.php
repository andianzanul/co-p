<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_output extends CI_Controller{

	
 	function __construct()
    {
        parent::__construct();

        $this->load->model('m_jenis_output');
        $this->load->model('m_jenis_bidang');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
		$this->mainlib->logged_in();

		$this->data = array();

    }


	public function index(){

		$this->data['jenis'] = $this->m_jenis_output->get()->result_object(); 
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/jenis_output/main');
        $this->load->view('admin/footer');

	}

     public function create() 
     {
        $this->mainlib->batasi_akses_user(['superadmin']);
        
     	$this->create_action();
        $this->set_jenis_bidang();
         $this->load->view('admin/header', $this->data);
         $this->load->view('admin/jenis_output/add');
         $this->load->view('admin/footer');

     }
     private function set_jenis_bidang(){
        $this->load->model('m_jenis_bidang');
        $this->data['jenis_bidang_data'] = array('--Pilih --');
        $jenis = $this->m_jenis_bidang->get()->result_object();
        foreach($jenis as $j){
            $this->data['jenis_bidang_data'][ $j->id_bidang ] = $j->nama_bidang;
            
        }
    }
    
    private function create_action() {


      $this->form_validation->set_rules('nama_output', 'Nama_output', 'required|trim'); 

        if ($this->form_validation->run() == true) {


            $data = array( 
                'id_bidang'    => $this->input->post('nama_bidang',TRUE),
        'nama_output'    => $this->input->post('nama_output',TRUE),
        'des_output'  => $this->input->post('des_output',TRUE),
         );

           if($this->m_jenis_output->insert($data)){
              $this->session->set_flashdata('message', 'Create Record Success');
              redirect(site_url('admin/jenis_output'));
           } else{
              show_error("aduh!");
           }
        }
    }



     function edit($id){
    	$this->mainlib->batasi_akses_user(['superadmin']);
     	$this->form_validation->set_rules('nama_output', 'Nama_output', 'required|trim'); 

         if ($this->form_validation->run() == true) {


            $data = array('id_bidang' => $this->input->post('nama_bidang',TRUE), 
 				'nama_output' => $this->input->post('nama_output',TRUE),
 				'des_output'  => $this->input->post('des_output',TRUE),
 	   		 );

            if($this->m_jenis_output->update($data,$this->input->post('id',TRUE))){

            		$this->session->set_flashdata('message', 'Update Record Success');
            		redirect(site_url('admin/jenis_output'));

            } else{
            		show_error("aduh!");
            }

         }
        $this->set_jenis_bidang();
     	$this->data['jenis'] = $this->m_jenis_output->get_by_id($id)->row_object();


        
     	$this->load->view('admin/header', $this->data);
         $this->load->view('admin/jenis_output/edit');
         $this->load->view('admin/footer');
     }


     function delete($id){
    	$this->mainlib->batasi_akses_user(['superadmin']);
     	if($this->m_jenis_output->delete($id)){
        		$this->session->set_flashdata('message', 'Delete Record Success');
        		redirect(site_url('admin/jenis_output'));
        } else{
        		show_error("aduh!");
        }

     }





 }