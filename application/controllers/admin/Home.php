<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

	public $data;

 	function __construct()
    {
        parent::__construct();
        $this->load->model('kegiatan_model');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
		$this->mainlib->logged_in();

    }

	public function index(){

		/*  $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->kegiatan_model->total_rows($q);
        $tbl_kegiatan = $this->kegiatan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_kegiatan_data' => $tbl_kegiatan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );*/

        // $u=$this->session->userdata('username');
        // $l=$this->session->userdata('level');

        // if ($this->session->set_userdata('level')=='superadmin') {

            $this->load->view('admin/header', $this->data);
            $this->load->view('admin/main');
            $this->load->view('admin/footer');

        // } else {

            // $this->load->view('admin/header_asatker', $this->data);
            // $this->load->view('admin/main');
            // $this->load->view('admin/footer');
        // }
	}	
}