<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_ppk extends CI_Controller{

	
 	function __construct()
    {
        parent::__construct();

        $this->load->model('m_jenis_ppk');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
		$this->mainlib->logged_in();

		$this->data = array();

    }


	public function index(){

		$this->data['jenis'] = $this->m_jenis_ppk->get()->result_object();     
        

      //  print_r($this->data['jenis']); 
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/jenis_ppk/main');
        $this->load->view('admin/footer');

	}

     public function create() 
     {
        $this->mainlib->batasi_akses_user(['superadmin']);
     	$this->create_action();
        $this->set_jenis_satker();
         $this->load->view('admin/header', $this->data);
         $this->load->view('admin/jenis_ppk/add');
         $this->load->view('admin/footer');

     }
     private function set_jenis_satker(){
        $this->load->model('m_jenis_satker');
        $this->data['jenis_satker_data'] = array('--Pilih --');
        $jenis = $this->m_jenis_satker->get()->result_object();
        foreach($jenis as $j){
            $this->data['jenis_satker_data'][ $j->id_satker ] = $j->nama_satker;
            
        }
    }
    
    private function create_action() {


      $this->form_validation->set_rules('nama_ppk', 'Nama_ppk', 'required|trim'); 

        if ($this->form_validation->run() == true) {


            $data = array( 
                'id_satker'    => $this->input->post('nama_satker',TRUE),
        'nama_ppk'    => $this->input->post('nama_ppk',TRUE),
        'tahun_ppk'    => $this->input->post('tahun',TRUE),
        'des_ppk'  => $this->input->post('des_ppk',TRUE),
         );

           if($this->m_jenis_ppk->insert($data)){
              $this->session->set_flashdata('message', 'Create Record Success');
              redirect(site_url('admin/jenis_ppk'));
           } else{
              show_error("aduh!");
           }
        }
    }



     function edit($id){
    	
        $this->mainlib->batasi_akses_user(['superadmin']);

     	$this->form_validation->set_rules('nama_ppk', 'Nama_ppk', 'required|trim'); 

         if ($this->form_validation->run() == true) {


             $data = array(
             'id_satker'    => $this->input->post('nama_satker',TRUE), 
 				'nama_ppk' 		=> $this->input->post('nama_ppk',TRUE),
                'tahun_ppk'      => $this->input->post('tahun',TRUE),
 				'des_ppk' 	=> $this->input->post('des_ppk',TRUE),
 	   		 );

            if($this->m_jenis_ppk->update($data,$this->input->post('id',TRUE))){

            		$this->session->set_flashdata('message', 'Update Record Success');
            		redirect(site_url('admin/jenis_ppk'));

            } else{
            		show_error("aduh!");
            }

         }
        $this->set_jenis_satker();

     	$this->data['jenis'] = $this->m_jenis_ppk->get_by_id($id)->row_object();

      //  print_r($this->data['jenis']); 
     	$this->load->view('admin/header', $this->data);
         $this->load->view('admin/jenis_ppk/edit');
         $this->load->view('admin/footer');
     }


     function delete($id){
    	
        $this->mainlib->batasi_akses_user(['superadmin']);
        
     	if($this->m_jenis_ppk->delete($id)){
        		$this->session->set_flashdata('message', 'Delete Record Success');
        		redirect(site_url('admin/jenis_ppk'));
        } else{
        		show_error("aduh!");
        }

     }





 }