<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller{

    /* tampung var untuk di kirim ke view */
	public $data;

  /* apakah kirim email pemberitahuan ke konsultan saat asistensi */
  private $kirim_email_asistensi = false;

 	function __construct() {
        parent::__construct();

        $this->init_var();
        $this->load->model('kegiatan_model');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
	    $this->mainlib->logged_in();
        $this->load->helper('url');
        $this->load->helper('main');
        $this->load->helper('file');
        $this->load->helper('date');
        $this->load->helper('waktu');
    } 

    private function init_var(){
        /* batas maksimum besar file upload berkas dokument */ 
        $this->max_size_upload_berkas = 120 * 1024 ; //120 * 1024;  // 120 MB
        $this->allowed_types_upload = 'gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx';
    }

  	public function index(){
      $par = array();
      $this->data['the_kegiatan']         = '';
      $this->data['the_tahun']            = array();
      $this->data['the_nama_kegiatan']    = '';
      $this->data['the_satker']           = array();
      $this->data['the_provinsi']         = array();
      $this->data['the_kab']              = array();
      $this->data['the_jenis_kegiatan']   = array();

      $tahun = $this->input->get('thn',true);
      if($tahun){
        $this->data['the_tahun'] = $tahun;
        $par['tahun'] = $tahun;
      }

      $kegiatan = $this->input->get('kegiatan',true);
      if($kegiatan){
        $this->data['the_kegiatan'] = $kegiatan;
        $par['kegiatan'] = $kegiatan;
      }

      $jenis_kegiatan = $this->input->get('jenis_kegiatan',true);
      if($jenis_kegiatan){
        $this->data['the_jenis_kegiatan'] = $jenis_kegiatan;
         $par['jenis_kegiatan'] = $jenis_kegiatan;
      }

      $satker = $this->input->get('satker',true);
      if($satker){
        $this->data['the_satker'] = $satker;
        $par['satker'] = $satker;
      }

      $prov = $this->input->get('prov',true);
      if($prov){
        $this->data['the_provinsi'] = $prov;
         $par['prov'] = $prov;
      }

      $kab = $this->input->get('kab',true);
      if($kab){
        $this->data['the_kab'] = $kab;
         $par['kab'] = $kab;
      }

      $nama_kegiatan = $this->input->get('nama_kegiatan',true);
      if($nama_kegiatan){
        $this->data['the_nama_kegiatan']  = $nama_kegiatan;
        $par['nama_kegiatan']             = $nama_kegiatan;
      }

  	  $q      = urldecode($this->input->get('q', TRUE));
      $start  = intval($this->input->get('start'));
      
      $config['per_page']           = 20;
      $config['page_query_string']  = TRUE;
      $config['reuse_query_string'] = true;

      $par['limit'] = array($config['per_page'], $start);

     
      if($this->mainlib->cek_level(['superadmin','direksi','ppk'])) {
        $tbl_kegiatan = $this->kegiatan_model->get_limit_data($config['per_page'], $start, $par)->result();

        unset($par['limit']);
        $config['total_rows']= $this->kegiatan_model->get_limit_data('','',$par)->num_rows();

        /*$download_excel = $this->input->get('download_excel',true);
        if($download_excel){
            $this->excel( $tbl_kegiatan ); 
            exit;
        }*/
      
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->data['tbl_kegiatan_data']  = $tbl_kegiatan;
        $this->data['q']                  = $q; 
        $this->data['total_rows']         = $config['total_rows'];
        $this->data['start']              = $start;
      
        $this->set_jenis_kegiatan(); 
        $this->set_jenis_satker();
        $this->set_jenis_bidang();
        $this->set_jenis_output();
        $this->set_jenis_tahun(); 
        $this->set_provinsi();
        $this->set_kabupaten();
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kegiatan/main');
        $this->load->view('admin/footer');
      }
     /* else if($this->mainlib->cek_level(['satker'])) {
        $tbl_kegiatan = $this->kegiatan_model->get_limit_data_bysatker($config['per_page'], $start, $par)->result();

        unset($par['limit']);
        $config['total_rows']= $this->kegiatan_model->get_limit_data('','',$par)->num_rows(); 
      
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->data['tbl_kegiatan_data']  = $tbl_kegiatan;
        $this->data['q']                  = $q; 
        $this->data['total_rows']         = $config['total_rows'];
        $this->data['start']              = $start;
      
        $this->set_jenis_kegiatan(); 
        $this->set_jenis_satker();
        $this->set_jenis_bidang();
        $this->set_jenis_output();
        $this->set_jenis_tahun(); 
        $this->set_provinsi();
        $this->set_kabupaten();
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kegiatan/main');
        $this->load->view('admin/footer');
      } */

    else if($this->mainlib->cek_level(['konsultan'])) {
        $tbl_kegiatan = $this->kegiatan_model->get_limit_data_by_ppk($config['per_page'], $start, $par)->result();

        unset($par['limit']);
        $config['total_rows']= $this->kegiatan_model->get_limit_data('','',$par)->num_rows(); 
      
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->data['tbl_kegiatan_data']  = $tbl_kegiatan;
        $this->data['q']                  = $q; 
        $this->data['total_rows']         = $config['total_rows'];
        $this->data['start']              = $start;
      
        $this->set_jenis_kegiatan(); 
        $this->set_jenis_satker();
        $this->set_jenis_bidang();
        $this->set_jenis_output();
        $this->set_jenis_tahun(); 
        $this->set_provinsi();
        $this->set_kabupaten();
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kegiatan/main');
        $this->load->view('admin/footer');
      } 
      else{
        show_error("Data tidak ditemukan!");
      }
  	}

    /*function get_das_by_wilayah_sungai(){
        $id_wlsungai = $this->input->post('id');
        echo combo_das_multi(array(),$id_wlsungai);
    }*/

    public function read($id){
        $this->load->model('m_amandemen');
        $this->data['tbl_kegiatan'] = $this->kegiatan_model->read_by_id($id);
        if($this->data['tbl_kegiatan']){
            $this->data['amandemen'] = $this->m_amandemen->get_by_kegiatan($id)->result_object();
            // get doc  
            $this->data['doc_kontrak'] = $this->kegiatan_model->get_files($id,'doc_kontrak')->result_object();
            $this->data['doc_kangkung'] = $this->kegiatan_model->get_files2($id,'doc_kangkung')->result_object();
            $this->data['doc_pelaporan'] = $this->kegiatan_model->get_files($id,'doc_pelaporan')->result_object();
            $this->data['doc_lainnya'] = $this->kegiatan_model->get_files($id,'doc_lainnya')->result_object();

            $this->data['dasar_kegiatan'] = $this->kegiatan_model->get_files($id,'dasar_kegiatan')->result_object();
            $this->data['koordinats'] = $this->kegiatan_model->get_koordinat($id)->result_object();

            $this->data['koordinats'] = $this->kegiatan_model->get_koordinat($id)->result_object();       
           $this->data['koordinats_garis'] = $this->kegiatan_model->get_koordinat_garis($id)->result_object();   
           $this->data['koordinats_luasan'] = $this->kegiatan_model->get_koordinat_luasan($id)->result_object();  

            $this->data['images'] = $this->kegiatan_model->get_image($id)->result_object();
            $gambar=array();
            foreach ($this->data['images'] as $key=>$val) {
                if (''!=$val->proses) {
                    $gambar [$val->proses][] = $val;
                }
            }
            $this->data['images'] = $gambar;
           // $this->data['lokasi']  = $this->kegiatan_model->get_locations($id)->result_object();
            
            $this->data['data_ws'] = $this->kegiatan_model->get_ws_das($id);
            $this->data['kegiatan_terkait'] = $this->kegiatan_model->get_relasi($id,'kegiatan_terkait')->result_object();
            $this->data['kegiatan_lanjutan'] = $this->kegiatan_model->get_relasi($id,'kegiatan_lanjutan')->result_object();
            $this->data['kegiatan_supervisi'] = $this->kegiatan_model->get_relasi($id,'kegiatan_supervisi')->result_object();
            $this->data['kegiatan_konstruksi'] = $this->kegiatan_model->get_relasi($id,'kegiatan_konstruksi')->result_object();
            $this->data['kegiatan_konsultansi'] = $this->kegiatan_model->get_relasi($id,'kegiatan_konsultansi')->result_object();
            $this->data['detail_konstruksi'] = $this->kegiatan_model->get_detail_konstruksi($id)->result_object();
            $this->data['realisasi_pelaksanaan'] = $this->kegiatan_model->get_realisasi_pelaksanaan($id)->result_object();

            //print_r($this->data['tbl_kegiatan']); die();

            /* set lokasi */
            $this->load->model('m_kabupaten');
         // $this->data['provinsinya'] = array();
         // $this->data['lokasi'] = '';
          $lokasi = $this->kegiatan_model->get_locations($id)->result_object();
          $data_per_provinsi = array(); 
          //$data_kabupaten = array(); 

          // loop untuk data provinsi
          foreach($lokasi as $lok){
            if(0 != $lok->id_provinsi){
                $data_per_provinsi[$lok->id_provinsi] = array(
                    'prov'    => $lok->id_provinsi,
                    'nama'    => $lok->provinsi,  
                  ); 
            }
          }

          // loop untuk kab data
          foreach($lokasi as $lok){ 
            if(0 != $lok->id_provinsi && 0 != $lok->id_kabupaten){ 
                $data_per_provinsi[$lok->id_provinsi]['kab'][$lok->id_kabupaten] = array(
                    'id_kabupaten'  => $lok->id_kabupaten,
                    'nama'          => $lok->kabupaten, 
                  ); 
            }
          }

          // loop untuk kab data
          foreach($lokasi as $lok){ 
            if(0 != $lok->id_provinsi && 0 != $lok->id_kabupaten && 0 != $lok->id_kecamatan){
                //$data_per_provinsi[$lok->id_provinsi]['kecArray'][] = $lok->id_kecamatan;
                $data_per_provinsi[$lok->id_provinsi]['kab'][$lok->id_kabupaten]['kec'][$lok->id_kecamatan] = array(
                    'id_kecamatan'  => $lok->id_kecamatan,
                    'nama'          => $lok->kecamatan, 
                  );  
            }
          }

          // loop untuk kelurahan data
          foreach($lokasi as $lok){ 
            if(0 != $lok->id_provinsi && 0 != $lok->id_kabupaten && 0 != $lok->id_kecamatan && 0 != $lok->id_kelurahan){
                 $data_per_provinsi[$lok->id_provinsi]['kab'][$lok->id_kabupaten]['kec'][$lok->id_kecamatan]['kel'][$lok->id_kelurahan] = array(
                    'id_kelurahan'  => $lok->id_kelurahan,
                    'nama'          => $lok->kelurahan, 
                  ); 
            }
          }

        //
          foreach ($data_per_provinsi as $key => $prov) { 
              $data_per_provinsi[$key]['rowspan'] = 0;

              foreach ($data_per_provinsi[$key]['kab'] as $k2 => $kab) {  
                $data_per_provinsi[$key]['kab'][$k2]['rowspan'] = 0;

                if(isset( $data_per_provinsi[$key]['kab'][$k2]['kec'] )){
                  foreach ($data_per_provinsi[$key]['kab'][$k2]['kec'] as $k3 => $kec) { 
                      $data_per_provinsi[$key]['kab'][$k2]['kec'][$k3]['rowspan'] = 0;

                      foreach ($data_per_provinsi[$key]['kab'][$k2]['kec'][$k3]['kel'] as $k4 => $kel) {  
                          $data_per_provinsi[$key]['rowspan']++;
                          $data_per_provinsi[$key]['kab'][$k2]['rowspan']++;
                          $data_per_provinsi[$key]['kab'][$k2]['kec'][$k3]['rowspan']++;
                      }
                  }
                }
                
              } 
          }

        // header('Content-Type: application/json'); echo   json_encode($data_per_provinsi); die();
          
          $this->data['data_per_provinsi'] = $data_per_provinsi;
          $this->data['koordinats'] = $this->kegiatan_model->get_koordinat($id)->result_object();       
          $this->data['koordinats_garis'] = $this->kegiatan_model->get_koordinat_garis($id)->result_object();   
           $this->data['koordinats_luasan'] = $this->kegiatan_model->get_koordinat_luasan($id)->result_object();  
  
            // print_r($this->data['koordinats']);
            //  print_r($this->data['koordinats_garis']);
            //  print_r($this->data['koordinats_luasan']);  // end koordinat Zikra
          unset($data_per_provinsi);
          //print_r($this->data);
            $this->load->view('admin/header', $this->data);
            $this->load->view('admin/kegiatan/read');
            $this->load->view('admin/footer');
        } else{
            show_404();
        }
  }

  public function create() {    
    
    $this->mainlib->batasi_akses_user(["superadmin",'direksi']);

    $this->form_validation->set_rules('nm_kegiatan', 'Nama Kegiatan', 'required');
    $this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', 'required');
    $this->form_validation->set_rules('konsultan', 'Konsultan', 'required');
      if ($this->form_validation->run() == true) {
            $data = array(
                'user_id' => $this->session->userdata("id_user"),
                'nm_kegiatan'   => $this->input->post('nm_kegiatan',TRUE),
                'tahun'         => $this->input->post('tahun_anggaran',TRUE),
				'konsultan'     => $this->input->post('konsultan',TRUE),
				'last_update'=> date("Y-m-d H:i:s",now()),
                'last_update_user' => $this->session->userdata("id_user")
			 );

            if($this->mainlib->cek_level(['ppk'])){
              $data['id_ppk'] = $this->session->userdata("id_ppk");
              $data['id_satker'] = $this->session->userdata("id_satker");
            }



            $id = $this->kegiatan_model->insert($data);

            $this->kirim_email_pemberitahuan_kepada_konsultan_dipilih_sebagai_penyedia_jasa($id);

            $this->session->set_flashdata('message', 'Create Record Success');
            redirect('admin/kegiatan/update/'.$id); 
        }

        $this->data = array(
			    'id_kegiatan' => set_value('id_kegiatan'),
			    'id_satker' => set_value('id_satker'),
			    'nm_satker' => set_value('nm_satker'),
			    'nm_kegiatan' => set_value('nm_kegiatan'),
			    'jenis_kegiatan' => set_value('jenis_kegiatan'),
			    'thn_anggaran' => set_value('thn_anggaran'),
			);

        $this->set_jenis_kegiatan(); 
        $this->set_jenis_satker();
        $this->set_jenis_bidang();
        $this->set_jenis_ppk();
      
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kegiatan/tbl_kegiatan_form');
        $this->load->view('admin/footer');
    }

    

    

    function update($id){

        $this->mainlib->batasi_akses_user(['superadmin','direksi','konsultan','ppk']);
        
        $this->load->model('m_amandemen');
        $this->form_validation->set_rules('nm_kegiatan', 'Nama Kegiatan', 'required|trim'); 
        // $this->form_validation->set_rules('provinsi', 'Nama provinsi', 'required|trim'); 
        $this->form_validation->set_message('required','%s masih kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        if ($this->form_validation->run() == true) {
          
        $data = array(
            'nm_kegiatan' => $this->input->post('nm_kegiatan',TRUE),
           // 'deskripsi' => trim($this->input->post('deskripsi',TRUE)),
            'id_jenis_kegiatan' => $this->input->post('jenis_kegiatan',TRUE),
            //'id_satker' =>$this->input->post('nama_satker',TRUE), // batasi di bawah
            'tahun' =>$this->input->post('tahun_anggaran',TRUE),
           // 'id_ppk' => $this->input->post('nama_ppk',TRUE), // batasi dibawah
            // 'id_wlsungai' => $this->input->post('nama_wlsungai',TRUE),// hapus
            'id_bidang' => $this->input->post('nama_bidang',TRUE),
            'id_output' => $this->input->post('nama_output',TRUE),
            // 'id_kajian_lingkungan' => $this->input->post('kajian_lingkungan',TRUE),
            'jenis_konstruksi' => $this->input->post('jenis_konstruksi',TRUE),
            'id_satuan_output' => $this->input->post('nama_satuan_output', TRUE),
            'id_satuan_outcome' => $this->input->post('nama_satuan_outcome', TRUE),
            // 'provinsi' => $this->input->post('provinsi',TRUE),
            // 'kabupaten' => $this->input->post('kabupaten',TRUE),
            // 'kecamatan' => $this->input->post('kecamatan',TRUE),
            // 'desa' => $this->input->post('desa',TRUE),
            // 'wl_sungai' => $this->input->post('wl_sungai',TRUE),
            //'das' => $this->input->post('das',TRUE), //hapus
            'tujuan' => $this->input->post('tujuan',TRUE),
            'manfaat' => $this->input->post('manfaat',TRUE),
            'output' => $this->input->post('output',TRUE),
            'outcome' => $this->input->post('outcome',TRUE),
            'data_teknis' => $this->input->post('data_teknis',TRUE),

            // Kegiatan Sebelumnya:
            'data_kegiatan' => $this->input->post('data_kegiatan',TRUE),
            'th_kegiatanlanjutan' => $this->input->post('th_kegiatanlanjutan',TRUE),
            //'kegiatan_terkait' => $this->input->post('kegiatan_terkait',TRUE),
           // 'kegiatan_terkait_tahun' => $this->input->post('kegiatan_terkait_tahun',TRUE),
            //'ks_konsultasi' => $this->input->post('ks_konsultasi',TRUE),
            //'ks_konsultasi_tahun' => $this->input->post('ks_konsultasi_tahun',TRUE),
            //'ks_konstruksi' => $this->input->post('ks_konstruksi',TRUE),
           // 'ks_konstruksi_tahun' => $this->input->post('ks_konstruksi_tahun',TRUE),
           // 'ks_supervisi' => $this->input->post('ks_supervisi',TRUE),
           // 'ks_supervisi_tahun' => $this->input->post('ks_supervisi_tahun',TRUE),

            // waktu pelaksanaan:
            'waktu_pelaksanaan_mulai' => $this->input->post('waktu_pelaksanaan_mulai',TRUE),
            'waktu_pelaksanaan_akhir' => $this->input->post('waktu_pelaksanaan_akhir',TRUE),
            'waktu_pelaksanaan_bulan' => $this->input->post('waktu_pelaksanaan_bulan',TRUE),
            'waktu_pelaksanaan_hari' => $this->input->post('waktu_pelaksanaan_hari',TRUE),

            // Progres Fisik:
            'progres_fisik_rencana' => $this->input->post('progres_fisik_rencana',TRUE),
            'progres_fisik_realisasi' => $this->input->post('progres_fisik_realisasi',TRUE),
            'progres_fisik_deviasi' => $this->input->post('progres_fisik_deviasi',TRUE),
            'progres_fisik_tanggal' => $this->input->post('progres_fisik_tanggal',TRUE),

            // Progres (Keuangan):
            'nilai_penyerapan' => $this->input->post('nilai_penyerapan',TRUE),
            'keuangan_persen' => $this->input->post('keuangan_persen',TRUE),
            'keuangan_tanggal' => $this->input->post('keuangan_tanggal',TRUE),

            'sumber_dana' => $this->input->post('sumber_dana',TRUE),
            'status_kegiatan' => $this->input->post('status_kegiatan',TRUE),
            'dasar_kegiatan' => $this->input->post('dasar_kegiatan',TRUE),
            'nmp_jasa' => $this->input->post('nmp_jasa',TRUE),
            'kso_jo' => $this->input->post('kso_jo', TRUE),
            'perencana' => $this->input->post('perencana',TRUE),
           // 'supervisi' => $this->input->post('supervisi',TRUE),
            'tgl_kontrak_awal' => $this->input->post('tgl_kontrak_awal',TRUE),
            'no_kontrak_awal' => $this->input->post('no_kontrak_awal',TRUE),
            'nilai_pagu' => $this->input->post('nilai_pagu',TRUE),
            'nilai_kontrak' => $this->input->post('nilai_kontrak',TRUE),
            'jenis_kontrak' => $this->input->post('jenis_kontrak',TRUE),
            'tgl_spmk' => $this->input->post('tgl_spmk',TRUE),
            'no_spmk' => $this->input->post('no_spmk',TRUE),
            'tgl_penyerapan' => $this->input->post('tgl_penyerapan',TRUE),
            
            'fisik_persen' => $this->input->post('fisik_persen',TRUE),
            'jkwkt_pelaksanaan' => $this->input->post('jkwkt_pelaksanaan',TRUE),
            // 'jml_hari' => $this->input->post('jml_hari',TRUE),
            //'koordinat_x' => $this->input->post('koordinat_x',TRUE),
            //'koordinat_y' => $this->input->post('koordinat_y',TRUE),
           // 'koordinat_x1' => $this->input->post('koordinat_x1',TRUE),
           // 'koordinat_y1' => $this->input->post('koordinat_y1',TRUE),
            'youtube_url' => $this->input->post('youtube_url',TRUE),
            'last_update'=> date("Y-m-d H:i:s",now()),
            'last_update_user' => $this->session->userdata("id_user")
            );
            
            /* hanya direksi yang boleh edit satker dan ppk */
            if($this->mainlib->cek_level(['direksi'])) {
              $data["id_satker"] = $this->input->post('nama_satker',TRUE);
              $data["id_ppk"] = $this->input->post('nama_ppk',TRUE); 
            }

            if($this->input->post('kso_jo', TRUE) == 'kso/jo'){
                $data['nmp_jasa2'] = serialize($this->input->post('nmp_jasa2',TRUE));
            }else{
                $data['nmp_jasa2'] = "";
            }

            if($this->input->post('multi_years', TRUE) == '1'){
                $data['multi_years'] = 'Y';
                $data['tahun_anggaran_2'] = $this->input->post('tahun_anggaran_2',TRUE);
            }else{
                $data['multi_years'] = 'N';
                $data['tahun_anggaran_2'] = 0;
            }

            $id_kegiatan = $this->input->post('id',TRUE);

            if($this->kegiatan_model->update($data, $id_kegiatan )){

                // kegiatan konsultansi
                $this->save_kegiatan_relasi_baru($id_kegiatan,'konsultansi');
                $this->save_kegiatan_relasi('kegiatan_konsultansi_edit');

                // kegiatan konstruksi
                $this->save_kegiatan_relasi_baru($id_kegiatan,'konstruksi');
                $this->save_kegiatan_relasi('kegiatan_konstruksi_edit');

                // kegiatan supervisi
                $this->save_kegiatan_relasi_baru($id_kegiatan,'supervisi');
                $this->save_kegiatan_relasi('kegiatan_supervisi_edit');

                // kegiatan lanjutan
                $this->save_kegiatan_relasi_baru($id_kegiatan,'lanjutan');
                $this->save_kegiatan_relasi('kegiatan_lanjutan_edit');

                // kegiatan terkait
                $this->save_kegiatan_relasi_baru($id_kegiatan,'terkait');
                $this->save_kegiatan_relasi('kegiatan_terkait_edit'); 

                // detail konstruksi
                $this->save_detail_konstruksi();
                $this->save_detail_konstruksi_baru($id_kegiatan);

                // realisasi pelaksanaan
                $this->save_realisasi_pelaksanaan();
                $this->save_realisasi_pelaksanaan_baru($id_kegiatan);

                // Wilayah Sungai (WS)
                $ws = $this->input->post('ws'); 
                if($ws){
                    $this->kegiatan_model->add_ws($id_kegiatan,$ws);
                }

                // DAS
                /*$das = $this->input->post('das',true);
                foreach ($das as $das_key => $v_das) {
                     $this->kegiatan_model->set_das($id_kegiatan,$das_key, $v_das); // das_key = id_wlsungai, v_das = id_das array
                }*/

                // Amandemen
                $amds = $this->input->post('amd_nomor_add');
                $nilaiamd = $this->input->post('amd_nilai_add');
                $tanggalamd = $this->input->post('amd_tanggal_add');
                if($amds){
                    foreach ($amds as $key_amd => $amd) {
                        $this->m_amandemen->add(array(
                            'nomor' => $amd,
                            'nilai' => $nilaiamd[$key_amd],
                            'tanggal' => $tanggalamd[$key_amd],
                            'id_kegiatan' => $id_kegiatan
                            ));
                    }
                }
                $amd_e = $this->input->post('amd_e');
                foreach ($amd_e as $key_e => $val_amd_edit) {
                    $new_update_amd = array(
                            'nomor' => $val_amd_edit['nomor'],
                            'nilai' => $val_amd_edit['nilai'],
                            'tanggal' => $val_amd_edit['tanggal'],
                            );
                     $this->m_amandemen->update($new_update_amd,$key_e);
                }

                // Povinsi
                $lokasi = array();
                
                $prov   = $this->input->post('cekkab');
                $kec    = $this->input->post('cekkec');
                $kel    = $this->input->post('cekkel');
 
                foreach($prov as $id_prov => $kabupaten_array){ // prov

                    foreach($kabupaten_array as $kabupaten){
                    
                    if(array_key_exists($kabupaten, $kec)){

                        foreach($kec[$kabupaten] as $kecamatan){ // kec 
                             
                            if(array_key_exists($kecamatan, $kel)){

                                foreach($kel[$kecamatan] as $kelurahan){
                                    $lokasi[] = ['id_kegiatan' => $id_kegiatan,'id_provinsi' => $id_prov,  'id_kabupaten' => $kabupaten, 'id_kecamatan' => $kecamatan, 'id_kelurahan' => $kelurahan];
                                }
                            }else{
                                $lokasi[] = ['id_kegiatan' => $id_kegiatan,'id_provinsi' => $id_prov, 'id_kabupaten' => $kabupaten, 'id_kecamatan' => $kecamatan];
                            }
                        }
                    }else{
                        $lokasi[] = ['id_kegiatan' => $id_kegiatan, 'id_provinsi' => $id_prov, 'id_kabupaten' => $kabupaten];
                    }
                    }
                }
                foreach($lokasi as $new_lokasi){
                   if(count($new_lokasi) == 5){ // simpan data yang lengkap saja
                        $this->kegiatan_model->set_lokasi($new_lokasi);
                   } 
                }

                /*print_r2($prov);
                print_r2($kec);
                print_r2($kel);
                print_r2($lokasi);
                die();*/

                // add lokasi
                /*$prov       = $this->input->post('prov');
                $kabupaten  = $this->input->post('kabupaten');
                $kecamatan  = $this->input->post('kecamatan');
                $desa       = $this->input->post('desa');
                if(!is_null($prov)){
                    foreach ($prov as $key_prov => $pro) {
                        if(!is_null($kabupaten)){
                            foreach ($kabupaten[$key_prov] as $key_pro => $pr) {
                                $new_lokasi = array(
                                    'id_kegiatan'   => $id_kegiatan,
                                    'id_provinsi'   => $pro,
                                    'id_kabupaten'  => $pr,
                                    'id_kecamatan'  => $kecamatan[$key_prov][$key_pro],
                                    'id_kelurahan'  => $desa[$key_prov][$key_pro],
                                ); 
                                $this->kegiatan_model->set_lokasi($new_lokasi,$id_kegiatan);
                            }
                      }
                    }
                }*/

                // edit lokasi
                /*$lok       = $this->input->post('lok'); 
                foreach ($lok as $susu) {
                    $id_lokasi = $susu['id']; 
                    unset($susu['id']); 
                    $this->kegiatan_model->update_lokasi($susu,$id_lokasi);
                }*/
               // start koordinat add Zikra
                $koordinat_x        = $this->input->post('koordinat_x_titik');
                $koordinat_y        = $this->input->post('koordinat_y_titik');
                $ket_lokasi        = $this->input->post('ket_lokasi_titik');
                if($koordinat_x ){ 
                foreach ($koordinat_x as $key_koor => $k) {
                    $new_koordinat_titik = array(
                          'id_kegiatan'   => $id_kegiatan,
                          'koordinat_x'   => $koordinat_x[$key_koor], 
                          'koordinat_y'   => $koordinat_y[$key_koor],  
                          'ket_lokasi'  => $ket_lokasi[$key_koor], 
                          'tipe' => 'titik',
                          ); 
                     $this->kegiatan_model->set_koordinat($new_koordinat_titik);
                }
              }
              // koordinat garis add
                $koordinat_x        = $this->input->post('koordinat_x_garis');
                $koordinat_y        = $this->input->post('koordinat_y_garis');
                $ket_lokasi        = $this->input->post('ket_lokasi_garis');
                if($koordinat_x ){ 
                foreach ($koordinat_x as $key_koor => $k) {
                    $new_koordinat_garis = array(
                          'id_kegiatan'   => $id_kegiatan,
                          'koordinat_x'   => $koordinat_x[$key_koor], 
                          'koordinat_y'   => $koordinat_y[$key_koor],  
                          'ket_lokasi'  => $ket_lokasi[$key_koor], 
                          'tipe' => 'garis',
                          ); 
                     $this->kegiatan_model->set_koordinat_garis($new_koordinat_garis);
                }
              }

              // koordinat luasan add
              $koordinat_x        = $this->input->post('koordinat_x_luasan');
                $koordinat_y        = $this->input->post('koordinat_y_luasan');
                $ket_lokasi        = $this->input->post('ket_lokasi_luasan');
                if($koordinat_x ){ 
                foreach ($koordinat_x as $key_koor => $k) {
                    $new_koordinat_luasan = array(
                          'id_kegiatan'   => $id_kegiatan,
                          'koordinat_x'   => $koordinat_x[$key_koor], 
                          'koordinat_y'   => $koordinat_y[$key_koor],  
                          'ket_lokasi'  => $ket_lokasi[$key_koor], 
                          'tipe' => 'luasan',
                          ); 
                     $this->kegiatan_model->set_koordinat_luasan($new_koordinat_luasan);
                }
              }
               



                // koordinat edit titik
                $koor               = $this->input->post('koor');
                $koordinat_x        = $this->input->post('k_x');
                $koordinat_y        = $this->input->post('k_y');
                $ket_lokasi        = $this->input->post('k_l');
                $koordinat_x1       = $this->input->post('k_x1');
                $koordinat_y1       = $this->input->post('k_y1');
                foreach ($koordinat_x as $key_koor => $k) {
                     $new_koordinat = array(
                          'id_kegiatan'   => $id_kegiatan,
                          'koordinat_x'   => $koordinat_x[$key_koor], 
                          'koordinat_y'   => $koordinat_y[$key_koor],  
                          'ket_lokasi'  => $ket_lokasi[$key_koor], 
                          'tipe' => 'titik',
                          ); 
                     $this->kegiatan_model->update_koordinat($new_koordinat,$koor[$key_koor]);
                }
// koordinat edit garis
                $koor               = $this->input->post('koor_garis');
                $koordinat_x_garis  = $this->input->post('k_x_garis');
                $koordinat_y_garis  = $this->input->post('k_y_garis');
                $ket_lokasi_garis   = $this->input->post('ket_lokasi_garis');
                $koordinat_x1       = $this->input->post('k_x1');
                $koordinat_y1       = $this->input->post('k_y1');
                foreach ($koordinat_x_garis as $key_koor_garis => $k) {
                     $new_koordinat_garis = array(
                          'id_kegiatan'   => $id_kegiatan,
                          'koordinat_x'   => $koordinat_x_garis[$key_koor_garis], 
                          'koordinat_y'   => $koordinat_y_garis[$key_koor_garis],  
                          'ket_lokasi'  => $ket_lokasi_garis[$key_koor_garis], 
                          'tipe' => 'garis',
                          ); 
                     $this->kegiatan_model->update_koordinat_garis($new_koordinat_garis,$koor[$key_koor_garis]);
                }
                // koordinat edit luasan
                $koor               = $this->input->post('koor_luasan');
                $koordinat_x_luasan        = $this->input->post('k_x_luasan');
                $koordinat_y_luasan        = $this->input->post('k_y_luasan');
                $ket_lokasi_luasan        = $this->input->post('ket_lokasi_luasan');
                $koordinat_x1       = $this->input->post('k_x1');
                $koordinat_y1       = $this->input->post('k_y1');
                foreach ($koordinat_x_luasan as $key_koor_luasan => $k) {
                     $new_koordinat_luasan = array(
                          'id_kegiatan'   => $id_kegiatan,
                          'koordinat_x'   => $koordinat_x_luasan[$key_koor_luasan], 
                          'koordinat_y'   => $koordinat_y_luasan[$key_koor_luasan],  
                          'ket_lokasi'  => $ket_lokasi_luasan[$key_koor_luasan], 
                          'tipe' => 'luasan',
                          ); 
                     $this->kegiatan_model->update_koordinat_luasan($new_koordinat_luasan,$koor[$key_koor_luasan]);
                }
                $this->session->set_flashdata('message', 'Update Record Success');
                // Custom redirect setelah disimpan
                if($this->input->post('back')){
                    if('detail' == $this->input->post('back')){
                        redirect(site_url('admin/kegiatan/read/' . $id)); // redirect ke halaman detail
                    }
                }
                redirect(site_url('admin/kegiatan')); // redirect ke halaman list kegiatan
           } else{
              show_error("Terjadi Kesalahan!");
           }
        } // end koordinat add Zikra


        $this->data['kegiatan'] = $this->kegiatan_model->get_by_id($id);
        if($this->data['kegiatan']){ 


          $this->data['amandemen'] = $this->m_amandemen->get_by_kegiatan($id)->result_object();
          $this->set_jenis_kegiatan(); 
          $this->set_jenis_satker(); 
          $this->set_jenis_bidang(); 
          $this->set_jenis_output();
          $this->set_provinsi();
          $this->set_kabupaten();//baru 
          $this->set_jenis_ppk(); 
          $this->set_jenis_tahun(); 
          $this->set_jenis_wlsungai();
          // $this->set_jenis_kajian_lingkungan();
          $this->set_jenis_satuan();
          $this->set_jenis_satuan_output();
          $this->set_jenis_satuan_outcome();

          // get doc 
          $this->data['doc_kontrak'] = $this->kegiatan_model->get_files($id,'doc_kontrak')->result_object();
          $this->data['doc_kangkung'] = $this->kegiatan_model->get_files2($id,'doc_kangkung')->result_object();
          $this->data['doc_pelaporan'] = $this->kegiatan_model->get_files($id,'doc_pelaporan')->result_object();
          $this->data['doc_lainnya'] = $this->kegiatan_model->get_files($id,'doc_lainnya')->result_object();
          $this->data['dasar_kegiatan'] = $this->kegiatan_model->get_files($id,'dasar_kegiatan')->result_object();
          $this->data['kegiatan_terkait'] = $this->kegiatan_model->get_relasi($id,'kegiatan_terkait')->result_object();
          $this->data['kegiatan_lanjutan'] = $this->kegiatan_model->get_relasi($id,'kegiatan_lanjutan')->result_object();
          $this->data['kegiatan_supervisi'] = $this->kegiatan_model->get_relasi($id,'kegiatan_supervisi')->result_object();
          $this->data['kegiatan_konstruksi'] = $this->kegiatan_model->get_relasi($id,'kegiatan_konstruksi')->result_object();
          $this->data['kegiatan_konsultansi'] = $this->kegiatan_model->get_relasi($id,'kegiatan_konsultansi')->result_object();
          $this->data['kegiatan_detail_konstruksi'] = $this->kegiatan_model->get_detail_konstruksi($id)->result_object();
          $this->data['kegiatan_realisasi_pelaksanaan'] = $this->kegiatan_model->get_realisasi_pelaksanaan($id)->result_object();

          #$this->data['list_asistensi'] = $this->kegiatan_model->get_asistensi($id)->result_object();
          $this->data['list_laporan'] = $this->kegiatan_model->get_laporan($id)->result_object();


            $tahunAnggaran = (int) $this->data['kegiatan']->tahun;
            $this->data['maxTahun'] = ($tahunAnggaran ? $tahunAnggaran : 2031);
          

          $this->load->model('m_kabupaten');
          $this->data['provinsinya'] = array();
          $this->data['lokasi'] = '';
          $lokasi = $this->kegiatan_model->get_locations($id)->result_object();

          $data_per_provinsi = array(); 
          $data_kabupaten = array(); 

          // loop untuk data provinsi
          foreach($lokasi as $lok){
            
            if(0 != $lok->id_provinsi){
                
                $data_per_provinsi[$lok->id_provinsi] = array(
                    'prov'          => $lok->id_provinsi,
                    'nama'          => $lok->provinsi, 
                  ); 

                $data_per_provinsi[$lok->id_provinsi]['kabArray'] = array();
                $data_per_provinsi[$lok->id_provinsi]['kecArray'] = array();
                $data_per_provinsi[$lok->id_provinsi]['kelArray'] = array();
                $data_per_provinsi[$lok->id_provinsi]['kab'] = array();
                $data_per_provinsi[$lok->id_provinsi]['kec'] = array();
                $data_per_provinsi[$lok->id_provinsi]['kel'] = array();

                $data_kabupaten[$lok->id_provinsi] = $this->m_kabupaten->getByProvinsi( $lok->id_provinsi )->result_object();

                $this->data['provinsinya'][] = $lok->id_provinsi;

            }
          }

          // loop untuk kab data
          foreach($lokasi as $lok){ 
            if(0 != $lok->id_provinsi && 0 != $lok->id_kabupaten){
                $data_per_provinsi[$lok->id_provinsi]['kabArray'][] = $lok->id_kabupaten;
                $data_per_provinsi[$lok->id_provinsi]['kab'][$lok->id_kabupaten] = array(
                    'id_kabupaten'  => $lok->id_kabupaten,
                    'nama'          => $lok->kabupaten, 
                  ); 
            }
          }

          // loop untuk kab data
          foreach($lokasi as $lok){ 
            if(0 != $lok->id_provinsi && 0 != $lok->id_kabupaten && 0 != $lok->id_kecamatan){
                $data_per_provinsi[$lok->id_provinsi]['kecArray'][] = $lok->id_kecamatan;
                $data_per_provinsi[$lok->id_provinsi]['kab'][$lok->id_kabupaten]['kec'][$lok->id_kecamatan] = array(
                    'id_kecamatan'          => $lok->id_kecamatan,
                    'nama'          => $lok->kecamatan, 
                  ); 
            }
          }

          // loop untuk kelurahan data
          foreach($lokasi as $lok){ 
            if(0 != $lok->id_provinsi && 0 != $lok->id_kabupaten && 0 != $lok->id_kecamatan && 0 != $lok->id_kelurahan){
                $data_per_provinsi[$lok->id_provinsi]['kelArray'][] = $lok->id_kelurahan; 
            }
          }

         // print_r2($data_per_provinsi);
          

             $this->data['lokasi'] .= $this->load->view('admin/kegiatan/form_lokasi_edit2',array(
              'data' => $data_per_provinsi,
              'kabupaten' => $data_kabupaten
              ),true);

        // WILAYAH SUNGAI
        $this->data['wsnya'] = array();   
        $this->data['dasnya'] = '';   
        $ws_temp = $this->kegiatan_model->get_ws($id)->result_object();

        foreach($ws_temp as $wst){
            $das_selected = $this->kegiatan_model->get_das_array($id, $wst->id_wlsungai );
            $this->data['dasnya'] .= $this->load->view('admin/kegiatan/form_das',array(
                'das_selected' => $das_selected,
                'ws' => $wst->id_wlsungai,
                'nama' => $wst->nama_wlsungai
              ),true);
              $this->data['wsnya'][] = $wst->id_wlsungai;
          }

        // print_r( $data_kabupaten); die();

          //$this->data['provinsi']=$this->kegiatan_model->provinsi();

         /* $modul=$this->input->post('modul');
         // $id=$this->input->post('id');

          if($modul=="kabupaten"){
            echo $this->kegiatan_model->kabupaten($id);
          }
          else if($modul=="kecamatan"){
            echo $this->kegiatan_model->kecamatan($id);

          }
          else if($modul=="kelurahan"){
          echo $this->kegiatan_model->kelurahan($id);
          }*/ 

         $this->data['images'] = $this->kegiatan_model->get_image($id)->result_object();
         $gambar=array();

          foreach ($this->data['images'] as $key=>$val) {
              if (''!=$val->proses) {
                   $gambar [$val->proses][] = $val;
              }
          }

          $this->data['images'] = $gambar;
            //print_r($this->data['images']);exit;

          // start koordinat ZIkra
          $this->data['koordinats'] = $this->kegiatan_model->get_koordinat($id)->result_object();       
          $this->data['koordinats_garis'] = $this->kegiatan_model->get_koordinat_garis($id)->result_object();   
           $this->data['koordinats_luasan'] = $this->kegiatan_model->get_koordinat_luasan($id)->result_object();  
  
            //print_r($this->data['koordinats']);
           // print_r($this->data['koordinats_garis']);
           // print_r($this->data['doc_kangkung']);  // end koordinat Zikra
          $this->load->view('admin/header', $this->data);
          $this->load->view('admin/kegiatan/edit');
          $this->load->view('admin/footer');

        }else{
          show_error("Data tidak ditemukan!");
        }
      }



    


    function hitung_hari(){
        $result = array(
                'status' => '0'
            ); 
        $this->form_validation->set_rules('start', 'Tanggal mulai', 'required|trim'); 
        $this->form_validation->set_rules('end', 'Tanggal berakhir', 'required|trim'); 

        if ($this->form_validation->run() == true) {
            $start_date = new DateTime($this->input->post('start'));
            $end_date = new DateTime($this->input->post('end'));
            $interval = $start_date->diff($end_date);

            $result['status'] = '1';
            $result['d'] = $interval->days + 1;

            $bulan = 0;
            if($interval->y != 0){
                $bulan = 12 * $interval->y;
            }
            $result['m'] = $interval->m + $bulan + 1;
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }



    function delete_koordinat(){
      $this->kegiatan_model->delete_koordinat(
        $this->input->post('id_kegiatan'),
        $this->input->post('id_koordinat')
        );
    }

    function delete_file_di_baris(){
        $result = array( 
            'status'    => '0',
            'message'   => 'gagal',
            'title'     => 'gagal',
            'type'      => 'error' 
        );

        $id_file_kegiatan = $this->input->post('id',true);
        $file_name = $this->input->post('file_name',true);
        $file_kegiatan = $this->kegiatan_model->get_file($id_file_kegiatan)->row_object();

        if($file_kegiatan){

            if(is_serialized($file_kegiatan->file_name)){
                $files = unserialize($file_kegiatan->file_name);

                foreach ($files as $key => $file) {
                    if($file['file_name'] == $file_name){

                        $path = 'uploads/kegiatan/' . $file['file_name'];
                        if(file_exists($path)){
                            unlink($path);
                        }

                        unset($files[$key]);

                    }
                    
                } 

                $data = array(
                    'file_name' => empty($files) ? '' : serialize($files)
                );

                
                if($this->kegiatan_model->update_file($id_file_kegiatan, $data)){
                     $result = array(
                        'status' => '1',
                        'message' => 'Berhasil dihapus',
                        'title' => 'terima kasih',
                        'type' => 'success'
                    );    
                }
                
            }
        }
       
        echo json_encode($result);
        
        /*if($this->kegiatan_model->delete_file($id)){
             $result = array(
                'status' => '1',
                'message' => 'Berhasil dihapus',
                'title' => 'terima kasih',
                'type' => 'success'
            );    
        } 
        header('Content-Type: application/json');
        */
    }

    function delete_file(){
        $result = array( 
            'status'    => '0',
            'message'   => 'gagal',
            'title'     => 'gagal',
            'type'      => 'error' 
        );

        $id = $this->input->post('id',true);
        if($this->kegiatan_model->delete_file($id)){
             $result = array(
                'status' => '1',
                'message' => 'Berhasil dihapus',
                'title' => 'terima kasih',
                'type' => 'success'
            );    
        } 
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function edit_file(){
        $result = array( 
            'status'    => '0',
            'message'   => 'gagal',
            'title'     => 'gagal',
            'type'      => 'error' 
        );

        $id = $this->input->post('id',true);
        if($this->kegiatan_model->update_file($id, ['nama' => $this->input->post('nama',true), 'tahun' => $this->input->post('tahun',true)])){
             $result = array(
                'status' => '1',
                'message' => 'Berhasil di edit',
                'title' => 'terima kasih',
                'type' => 'success'
            );    
        } 
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function edit_kajian_lingkungan(){
        $result = array( 
            'status'    => '0',
            'message'   => 'gagal',
            'title'     => 'gagal',
            'type'      => 'error' 
        );

        $id = $this->input->post('id',true);
        if($this->kegiatan_model->update_file($id, [
            'nama' => $this->input->post('nama',true), 
            'volume_laporan' => $this->input->post('volume',true),
            'id_kajian_lingkungan' => $this->input->post('kajian',true)
        ])){ 
             $result = array(
                'status' => '1',
                'message' => 'Berhasil di edit',
                'title' => 'terima kasih',
                'type' => 'success'
            );   

            $this->load->model('m_jenis_kajian_lingkungan');
            $result['label_progress']    =  $this->m_jenis_kajian_lingkungan->get_name_by_id_kajian($this->input->post('kajian',true)); 
        } 
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function edit_produk_laporan_gambar(){
        $result = array( 
            'status'    => '0',
            'message'   => 'gagal',
            'title'     => 'gagal',
            'type'      => 'error' 
        );

        $id = $this->input->post('id',true);
        if($this->kegiatan_model->update_file($id, [
            'nama' => $this->input->post('nama',true), 
            'volume_laporan' => $this->input->post('volume',true),
            'id_progres_laporan' => $this->input->post('progres',true)
        ])){ 
             $result = array(
                'status' => '1',
                'message' => 'Berhasil di edit',
                'title' => 'terima kasih',
                'type' => 'success'
            );   

            $this->load->model('m_progres_laporan');
            $result['label_progress']    =  $this->m_progres_laporan->get_name_by_id($this->input->post('progres',true)); 
        } 
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    
    /* hapus asistensi */
    function delete_asistensi($id=""){
        $result = array(
            'status' => '0',
            'message' => 'gagal',
            'title' => 'gagal',
            'type' => 'error'
            );

        if($this->mainlib->cek_level(['direksi','superadmin'])) {  

            if($this->kegiatan_model->delete_asistensi($id)){
                 $result = array(
                    'status' => '1',
                    'message' => 'Asistensi telah dihapus',
                    'title' => 'Berhasil',
                    'type' => 'success'
                );  
            }

        }  
       header('Content-Type: application/json');
       echo json_encode($result);
    }


    /* hapus kegiatan */
    function delete($id){
        $result = array(
            'status' => '0',
            'message' => 'gagal',
            'title' => 'gagal',
            'type' => 'error'
            );

        if(!$this->mainlib->cek_level(['direksi'])) {

          $kegiatan = $this->kegiatan_model->get_by_id($id); 
          if($kegiatan){
            if($kegiatan->id_satker != $this->session->userdata("id_satker") && $kegiatan->id_ppk != $this->session->userdata("id_ppk")){
              header('Content-Type: application/json');
              echo json_encode($result);
            }
          } 
        }


        if($this->kegiatan_model->delete($id)){
             $result = array(
                'status' => '1',
                'message' => 'Kegiatan telah dihapus',
                'title' => 'Berhasil',
                'type' => 'success'
            );  
        }  
           header('Content-Type: application/json');
           echo json_encode($result);
    }


    /* Hapus amandement */
    function delete_amd(){
          $this->load->model('m_amandemen');
          $this->form_validation->set_rules('id', 'ID', 'required|trim'); 
          if ($this->form_validation->run() == true) {
              $this->m_amandemen->delete( $this->input->post('id') );
          }   
    }

  public function word($id,$return=false) {
    $q = urldecode($this->input->get('q', TRUE));
    $data = array(  );
    $this->load->model('m_amandemen');
    $this->data['tbl_kegiatan'] = $this->kegiatan_model->read_by_id($id);
    if($this->data['tbl_kegiatan']){
        $this->data['amandemen']  = $this->m_amandemen->get_by_kegiatan($id)->result_object();
        $this->data['lokasi']  = $this->kegiatan_model->get_locations($id)->result_object();
        $this->data['images'] = $this->kegiatan_model->get_image($id)->result_object();
        $this->data['koordinats'] = $this->kegiatan_model->get_koordinat($id)->result_object();

      $gambar=array();
      foreach ($this->data['images'] as $key=>$val) {
        if (''!=$val->proses) {
          $gambar [$val->proses] = $val;
        }
      }

      $this->data['images'] = $gambar;
      if($return === false){
        header("Content-Type: application/vnd.ms-word");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header("Content-disposition: attachment; filename=". $this->data['tbl_kegiatan']->nama_satker .".doc");
        $this->load->view('admin/kegiatan/doc_single',$this->data);
      }else{
        return $this->load->view('admin/kegiatan/doc_single',$this->data,true);
      }
    }else{
      //die("data tidak ada");
    }   
  }
   
    function download(){
      if($this->input->post('word')){
          $id_kegiatan = $this->input->post('id_kegiatan',true);
          if($id_kegiatan ){
            $word = '';
            foreach ($id_kegiatan as $id) {
              $word .= $this->word($id,true); 
            }
            header("Content-Type: application/vnd.ms-word");
            header("Expires: 0");
            header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
            header("Content-disposition: attachment; filename=Kegiatan.doc");
            echo $word;
          }
      }
      else if($this->input->post('pdf')){
        $this->pdf_download();
      }
      else{
        redirect('admin/kegiatan');
      }
    }

private function pdf_download(){
        $id_kegiatan = $this->input->post('id_kegiatan',true);
        if($id_kegiatan ){
       // $config['per_page'] = 10; //000000;
       // $config['page_query_string'] = TRUE;
       // $config['total_rows'] = $this->kegiatan_model->total_rows($q);
        $tbl_kegiatan_data = $this->kegiatan_model->get_limit_data(0, 0, array(
          'id_kegiatan' => $id_kegiatan
          ))->result_object();
        if( $tbl_kegiatan_data ){
       // print_r($tbl_kegiatan_data); die();
         $this->load->model('m_amandemen');

         foreach ($tbl_kegiatan_data as $key => $value) {
             $tbl_kegiatan_data[$key]->amandemen = $this->m_amandemen->get_by_kegiatan($value->id_kegiatan)->result_object();
             $this->data['images'] = $this->kegiatan_model->get_image($value->id_kegiatan)->result_object();
                $gambar=array();
                foreach ($this->data['images'] as $val) {
                    if (''!=$val->proses) {
                         $gambar [$val->proses] = $val;
                    }
                }
            $tbl_kegiatan_data[$key]->images = $gambar;
            $tbl_kegiatan_data[$key]->lokasi = $this->kegiatan_model->get_locations( $value->id_kegiatan )->result_object();
            $tbl_kegiatan_data[$key]->koordinats = $this->kegiatan_model->get_koordinat( $value->id_kegiatan )->result_object();
         } 
        $this->load->library('Pupdf');
        $this->pupdf->set_data_gegiatan($tbl_kegiatan_data);
        // print_r($tbl_kegiatan_data);
        $this->pupdf->buat_pdf();
        }
        else{
          echo "Data tidak ditemukan!";
        }
       } else{
          echo "Tidak ada data yang dipilih.";
        }
    } 

public function pdf(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        $config['per_page'] = 10; //000000;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->kegiatan_model->total_rows($q);
        $tbl_kegiatan_data = $this->kegiatan_model->get_limit_data($config['per_page'], $start, array())->result_object();

       // print_r($tbl_kegiatan_data); die();
         $this->load->model('m_amandemen');
         foreach ($tbl_kegiatan_data as $key => $value) {
             $tbl_kegiatan_data[$key]->amandemen = $this->m_amandemen->get_by_kegiatan($value->id_kegiatan)->result_object();
             $this->data['images'] = $this->kegiatan_model->get_image($value->id_kegiatan)->result_object();
                $gambar=array();
                foreach ($this->data['images'] as $val) {
                    if (''!=$val->proses) {
                         $gambar [$val->proses] = $val;
                    }
                }
            $tbl_kegiatan_data[$key]->images = $gambar;
            $tbl_kegiatan_data[$key]->lokasi = $this->kegiatan_model->get_locations( $value->id_kegiatan )->result_object();
         } 
        $this->load->library('Pupdf');
        $this->pupdf->set_data_gegiatan($tbl_kegiatan_data);
        // print_r($tbl_kegiatan_data);
        $this->pupdf->buat_pdf();
    }

public function word2($id) {
        $q = urldecode($this->input->get('q', TRUE));
        $data = array(
                );
        $this->load->model('m_amandemen');
        $this->data['amandemen'] = $this->m_amandemen->get_by_kegiatan($id)->result_object();
        $this->data['tbl_kegiatan'] = $this->kegiatan_model->read_by_id($id);
        $this->data['images'] = $this->kegiatan_model->get_image($id)->result_object();
        $gambar=array();
        foreach ($this->data['images'] as $key=>$val) {
            if (''!=$val->proses) {
                 $gambar [$val->proses] = $val;
            }
        }
        $this->data['images'] = $gambar;
        $this->load->view('admin/kegiatan/doc_single2',$this->data);
    }

    function hapus_gbr($id){
        // select dulu data gambar dari db untuk mendapatkan data2nya (name dan letak folder)
        $this->db->select('*');
        $this->db->from('tbl_gambar');
        $this->db->where('id_gambar', $id);
        $gambar=$this->db->get()->row_object();
        if ($gambar){
        // hapus file di directory mengunakan data di atas
        $this->load->helper("file");
        unlink('uploads/kegiatan/'. $gambar->nama_file);
        // hapus data di database
        $this->db->query("delete from tbl_gambar where id_gambar='$id'");
        echo "ok"; 
        }
          print_r($gambar);
    }



    /* upload berkas dasar kegiatan */
    function upload_dasar_kegiatan(){
        $this->upload_doc_multi('dasar_kegiatan');
    } 

    /* add hanya nama berkas */
    function upload_dasar_kegiatan_name_only(){
        $this->tambah_dokumen_nama_saja('dasar_kegiatan');
    }

    /* upload berkas document kontrak */
    function upload_doc_kontrak(){
        $this->upload_doc('doc_kontrak');
    }

    /* upload berkas Kajian Lingkungan */
    function upload_doc_kajian_lingkungan(){
        $this->upload_doc_multi('doc_kangkung');
    }

      /* add hanya nama berkas */
    function upload_doc_kajian_lingkungan_name_only(){
        $this->tambah_dokumen_nama_saja('doc_kangkung');
    }

    /* upload berkas Produk (Laporan/Gambar) */
    function upload_doc_pelaporan(){
        $this->upload_doc_multi('doc_pelaporan');
    }

    /* add hanya nama berkas */
    function upload_doc_pelaporan_name_only(){
        $this->tambah_dokumen_nama_saja('doc_pelaporan');
    }


    /* upload berkas Dokumentasi Lainnya*/
    function upload_doc_lainnya(){
        $this->upload_doc('doc_lainnya');
    }

    private function upload_doc_multi($jenis){
        $this->load->helper('number');

        $result = array(
            'status'    => '0', 
            'message'   => 'gagal mengupload'
        );
        $fullPath = 'uploads/kegiatan/';
        $config['upload_path']      = $fullPath;
        $config['allowed_types']    = $this->allowed_types_upload;
        $config['max_size']         = $this->max_size_upload_berkas;
        $config['max_width']        = '6000'; // 6000px
        $config['max_height']       = '6000'; // 6000px 
        //$config['encrypt_name']     = true;
        $config['file_ext_tolower'] = true;

        $this->load->library('upload', $config);

        $userfile = $_FILES['userfile'];
        $files = array();
        $total_file_size = 0;
        foreach($_FILES['userfile']['name'] as $key => $val)  {

            $_FILES['uploadedimage']['name'] = $userfile['name'][$key];
            $_FILES['uploadedimage']['type'] = $userfile['type'][$key];
            $_FILES['uploadedimage']['tmp_name'] = $userfile['tmp_name'][$key];
            $_FILES['uploadedimage']['error'] = $userfile['error'][$key];
            $_FILES['uploadedimage']['size'] = $userfile['size'][$key];

            if ($this->upload->do_upload('uploadedimage'))  {

                $data = $this->upload->data();  

                $files[] = array(
                    'file_name' => $data['file_name'],
                    'file_ext' => $data['file_ext'],
                    'file_size' => $data['file_size']  
                );

                $total_file_size = $total_file_size + $data['file_size'];
                
            }

        }

        $NewInsert = array(
            'id_kegiatan'   => $this->input->post('id',true),
            'nama'          => $this->input->post('name',true),
            'file_name'     => serialize($files),
            'ekstensi'      => 'eny',
            'ukuran'        => byte_format($total_file_size * 1024),
            'jenis'         => $jenis,
            'dibuat_oleh'   => $this->session->userdata('username'),
            'tanggal'       => date("Y-m-d H:i:s",now())
        );

        if($jenis == 'dasar_kegiatan'){
            $NewInsert['tahun'] = $this->input->post('tahun',true);
        }

        if($jenis == 'doc_kangkung'){
            $NewInsert['volume_laporan'] = $this->input->post('volume',true);
            $NewInsert['id_kajian_lingkungan'] = $this->input->post('kajian',true);
        }

        if($jenis == 'doc_pelaporan'){
            $NewInsert['volume_laporan'] = $this->input->post('volume',true);
            $NewInsert['id_progres_laporan'] = $this->input->post('progres',true);
        }


        

        $result['id'] = $this->kegiatan_model->add_file( $NewInsert );  

        $result['status'] = '1';
        $result['message'] = 'Berhasil';
        $result['data'] =  $NewInsert; 
        $result['files'] =  $files; 
        $result['icon_ext'] =  base_url('assets/img/file_icon/'. str_replace('.','',$NewInsert['ekstensi']) .'.gif');

        if($jenis == 'doc_kangkung'){
            $this->load->model('m_jenis_kajian_lingkungan');
            $result['label_progress']    =  $this->m_jenis_kajian_lingkungan->get_name_by_id_kajian($this->input->post('kajian',true)); 
        }

        if($jenis == 'doc_pelaporan'){
            $this->load->model('m_progres_laporan');
            $result['label_progress']    =  $this->m_progres_laporan->get_name_by_id($this->input->post('progres',true)); 
        }

       echo json_encode($result);
    }

    /* Upload file dan keterangan berkas Database */
    private function upload_doc($jenis){

        $this->load->helper('number');

        $result = array(
            'status'    => '0', 
            'message'   => 'gagal mengupload'
        );
        $fullPath = 'uploads/kegiatan/';
        $config['upload_path']      = $fullPath; 
        $config['allowed_types']    = $this->allowed_types_upload;
        $config['max_size']         = $this->max_size_upload_berkas;
        $config['max_width']        = '6000'; // 6000px
        $config['max_height']       = '6000'; // 6000px 
        $config['encrypt_name']     = true;
        $config['file_ext_tolower'] = true;

        

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) 
        {
            $er = $this->upload->display_errors();
            $result['message'] = strip_tags($er);
        } 
        else 
        {
            $data = $this->upload->data();  

            $NewInsert = array(
                'id_kegiatan'   => $this->input->post('id',true),
                'nama'          => $this->input->post('name',true),
                'file_name'     => $data['file_name'],
                'ekstensi'      => str_replace('.','',$data['file_ext']),
                'ukuran'        => byte_format($data['file_size'] * 1024),
                'jenis'         => $jenis,
                'dibuat_oleh'   => $this->session->userdata('username'),
                'tanggal'       => date("Y-m-d H:i:s",now())
        );
            $result['id'] = $this->kegiatan_model->add_file( $NewInsert); 
            $result['status'] = '1';
            $result['message'] = 'Berhasil';
            $result['data'] =  $NewInsert; 
            $result['icon_ext'] =  base_url('assets/img/file_icon/'. str_replace('.','',$data['file_ext']) .'.gif'); 
        }
       echo json_encode($result);
    }


    /* Upload file dan keterangan berkas Database */

    function upload_doc_saja(){

        $this->load->helper('number');

        $result = array(
            'status'    => '0', 
            'message'   => 'gagal mengupload'
        );
        $fullPath = 'uploads/kegiatan/';
        $config['upload_path']      = $fullPath;
        $config['allowed_types']    = $this->allowed_types_upload;
        $config['max_size']         = $this->max_size_upload_berkas;
        $config['max_width']        = '6000'; // 6000px
        $config['max_height']       = '6000'; // 6000px 
        //$config['encrypt_name']     = true;
        $config['file_ext_tolower']     = true;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) 
        {
            $er = $this->upload->display_errors();
            $result['message'] = strip_tags($er);
        } 
        else 
        {
            $files = array();

            $id_file_kegiatan = $this->input->post('id',true);

            $file = $this->kegiatan_model->get_file($id_file_kegiatan)->row_object();

            if($file){

                $files = unserialize($file->file_name);
            }



            $data = $this->upload->data();  

            

            $files[] = array(
                    'file_name' => $data['file_name'],
                    'file_ext' => $data['file_ext'],
                    'file_size' => $data['file_size']  
                );

            $update = array( 
                'file_name'     => serialize($files) 
            );

            $result['id'] = $this->kegiatan_model->update_file($id_file_kegiatan, $update); 
            $result['status'] = '1';
            $result['message'] = 'Berhasil';
            $result['data'] =  $update; 
            $result['files'] =  $files; 
            $result['icon_ext'] =  base_url('assets/img/file_icon/'. str_replace('.','',$data['file_ext']) .'.gif'); 
        }
       echo json_encode($result);
    }






    private function tambah_dokumen_nama_saja($jenis){
        $result = array(
            'status'    => '0', 
            'message'   => 'gagal mengupload'
        );
        

        $NewInsert = array(
            'id_kegiatan'   => $this->input->post('id',true),
            'nama'          => $this->input->post('name',true),
            'file_name'     => '',
            'ekstensi'      => '',
            'ukuran'        => '',
            'jenis'         => $jenis,
            'dibuat_oleh'   => $this->session->userdata('username'),
            'tanggal'       => date("Y-m-d H:i:s",now())
        );

        if($jenis == 'dasar_kegiatan'){
            $NewInsert['tahun'] = $this->input->post('tahun',true);
        }

        if($jenis == 'doc_kangkung'){
            $NewInsert['volume_laporan'] = $this->input->post('volume',true);
            $NewInsert['id_kajian_lingkungan'] = $this->input->post('kajian',true);
        }

        if($jenis == 'doc_pelaporan'){
            $NewInsert['volume_laporan'] = $this->input->post('volume',true);
            $NewInsert['id_progres_laporan'] = $this->input->post('progres',true);
        }

        $result['id']       = $this->kegiatan_model->add_file( $NewInsert); 
        $result['status']   = '1';
        $result['message']  = 'Berhasil';
        $result['data']     =  $NewInsert; 
        $result['label']    =  label_status_file(''); 
        $result['icon_ext'] =  ''; 

        if($jenis == 'doc_kangkung'){
            $this->load->model('m_jenis_kajian_lingkungan');
            $result['label_progress']    =  $this->m_jenis_kajian_lingkungan->get_name_by_id_kajian($this->input->post('kajian',true)); 
        }

        if($jenis == 'doc_pelaporan'){
            $this->load->model('m_progres_laporan');
            $result['label_progress']    =  $this->m_progres_laporan->get_name_by_id($this->input->post('progres',true)); 
        }
        
       echo json_encode($result);
    }

    function do_upload() {
        $result = array(
            'status' => 'gagal',
            'file_name' => '',
            'id' => '0',
            'message' => ''
        );
        $fullPath = 'uploads/kegiatan/';
        $config['upload_path'] = $fullPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 40 * 1024;  // 4MB
        $config['max_width'] = '6000'; // 6000px
        $config['max_height'] = '6000'; // 6000px
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) 
        {
            $er = $this->upload->display_errors();
            $result['message'] = strip_tags($er);
        } 
        else 
        {
            $data = $this->upload->data();
            $new_data = array(
                'id_kegiatan'  => $this->input->post('id'),
                'nama_file'     => $data['file_name'],
                'des'       => '',
                'proses'  => $this->input->post('p'),
            );
            $result['id'] = $this->kegiatan_model->add_image($new_data);
            $result['link_hapus'] = site_url('admin/kegiatan/hapus_gbr/' .  $result['id']);
            $result['status'] = 'ok';
            $result['new'] =  $fullPath . $data['file_name'];
            $result['full_url'] = base_url('thumb?w=150&h=150&src='. base_url() . $fullPath . $data['file_name']);
            $original = $data["file_name"];
            $configs[] = array('source_image' => $original, 'new_image' => $original, 'width' => 800, 'height' => 99999);
            // $this->load->library('image_lib');
            // $this->load->library('wy_image_lib');
            //$this->wy_image_lib->create_thumb($configs, $fullPath);
            $result['file_name'] = $data['file_name'];
        }
       echo json_encode($result);
    }

function excel( $tbl_kegiatan_data = array() ){
  $this->load->model('m_amandemen');
        $tbl_kegiatan_data = $this->kegiatan_model->get_limit_data(1999999, 0, array())->result();
        foreach($tbl_kegiatan_data as $key =>$val){
            $tbl_kegiatan_data[$key]->lokasi = $this->kegiatan_model->get_locations($val->id_kegiatan)->result_object();
            $tbl_kegiatan_data[$key]->amandemen  = $this->m_amandemen->get_by_kegiatan( $val->id_kegiatan )->result_object();
            $tbl_kegiatan_data[$key]->koordinats = $this->kegiatan_model->get_koordinat( $val->id_kegiatan )->result_object();
        }
  //    print_r($tbl_kegiatan_data); die;

  $this->load->library('PHPExcel');

$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Balai Wilayah Sungai III")
                            ->setLastModifiedBy("Balai Wilayah Sungai III")
                            ->setTitle("e-Sisda")
                            ->setSubject("Database")
                            ->setDescription("Data Kegiatan Balai Wilayah Sungai III")
                            ->setKeywords("novriansyah")
                            ->setCategory("Umum");
// mulai dari baris ke 2
$row = 2;

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B'.$row, 'DATABASE KEGIATAN');
$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getFont()->setBold(true);
$row++;
$row++;
// Tulis judul tabel
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B'.$row, 'Kode Kegiatan')
            ->setCellValue('C'.$row, 'Nama Kegiatan')
            ->setCellValue('D'.$row, 'Jenis Kegiatan')
            ->setCellValue('E'.$row, 'Tahun Anggaran')
            ->setCellValue('F'.$row, 'satker')
            ->setCellValue('G'.$row, 'PPK')
            ->setCellValue('H'.$row, 'Biadang/Kegiatan')
            ->setCellValue('I'.$row, 'Output/Sub bidang')
            ->setCellValue('J'.$row, 'Jenis Kontruksi')
            ->setCellValue('K'.$row, 'Provinsi')
            ->setCellValue('L'.$row, 'Kecamatan')
            ->setCellValue('M'.$row, 'kabupaten')
            ->setCellValue('N'.$row, 'Desa')
            ->setCellValue('O'.$row, 'Wilayah Sungai')
            ->setCellValue('P'.$row, 'DAS')
            ->setCellValue('Q'.$row, 'Tujuan')
            ->setCellValue('R'.$row, 'Manfaat')
            ->setCellValue('S'.$row, 'Output')
            ->setCellValue('T'.$row, 'Outcome')
            ->setCellValue('U'.$row, 'Data Teknis')
            ->setCellValue('V'.$row, 'Kegiatan Lanjutan')
            ->setCellValue('W'.$row, 'Tahun Keg.  Lanjutan')
            ->setCellValue('X'.$row, 'Sumber Dana')
            ->setCellValue('Y'.$row, 'Status Kegiatan')
            ->setCellValue('Z'.$row, 'Dasar Kegiatan')
            ->setCellValue('AA'.$row, 'Penyedia Jasa')
            ->setCellValue('AB'.$row, 'Perencana')
            ->setCellValue('AC'.$row, 'Supervisi')
            ->setCellValue('AD'.$row, 'Tgl. Kontrak (Awal)')
            ->setCellValue('AE'.$row, 'No. Kontrak (Awal)')
            ->setCellValue('AF'.$row, 'Nilai Kontrak Awal (Rp)')
            ->setCellValue('AG'.$row, 'Tgl. Kontrak (Akhir)')
            ->setCellValue('AH'.$row, 'No. Kontrak (Akhir)')
            ->setCellValue('AI'.$row, 'Nilai Kontrak Akhir (Rp')
            ->setCellValue('AJ'.$row, 'No Amandemen')     
            ->setCellValue('AK'.$row, 'Tgl Amandemen')
            ->setCellValue('AL'.$row, 'Nilai Amandemen')
            ->setCellValue('AM'.$row, 'Nilai Pagu Rp')
           ->setCellValue('AN'.$row, 'tgl.SPMK')
            ->setCellValue('AO'.$row, 'No. SPMK')
            ->setCellValue('AP'.$row, 'Penyerapan RP')
            ->setCellValue('AQ'.$row, 'Penyerapan %')
            ->setCellValue('AR'.$row, 'Waktu Pelaksanaan Bulan')
            ->setCellValue('AS'.$row, 'Jumlah Hari')
            ->setCellValue('AT'.$row, 'Koordinat Awal')
            ->setCellValue('AU'.$row, 'Koordinat Akhir')
            ->setCellValue('AV'.$row, 'Foto')
            ->setCellValue('AW'.$row, 'Video');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('1');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('11.71');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('45.19');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('14.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('12.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('32.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('39.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('26.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('39.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('29.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('32.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('43.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('17.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('10.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('10.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('10.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('10.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('10.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('15.71');
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('15.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('25.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth('11.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('13.71');
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('11.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('12.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth('26.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth('29.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth('15.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth('15.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth('15.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth('15.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth('15.29');
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth('32.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth('31');
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth('31');
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth('31');
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth('26.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth('26.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth('26.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth('26.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth('40.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth('12.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth('12.57');
$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth('19.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth('19.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth('13.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth('13.86');
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth('11.43');
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth('13');

$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':AW'.$row)->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(42);

 $tengah2 = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ),
     'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffe699')
        ),
      'borders' => array(
          'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
          )
      )
    );

 $tengah_isi = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ) 
    );

$objPHPExcel->getActiveSheet()->setAutoFilter('B'.$row.':XFD'.$row);
$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':XFD'.$row)->applyFromArray($tengah2);
$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('M'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('O'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('U'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('V'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('W'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('Y'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('AE'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('AQ'.$row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$row)->getAlignment()->setWrapText(true);

$nomor  = 1; // set nomor urut = 1;

$row++; // pindah ke row bawahnya. (ke row 2)

// lakukan perulangan untuk menuliskan data
 foreach ($tbl_kegiatan_data as $tbl_kegiatan)  {
    $Provinsi = $kabupaten = $kecamatan = $desa = $amandemen_tgl = $amandemen_nomor = $amandemen_nilai =   array();
    foreach($tbl_kegiatan->lokasi as $lok){
        $Provinsi[$lok->provinsi] = $lok->provinsi;
        $kabupaten[$lok->name] = $lok->name;
        $kecamatan[$lok->kecamatan] = $lok->kecamatan;
        $desa[$lok->desa] = $lok->desa;
    }

    foreach($tbl_kegiatan->amandemen as $amd){
        $amandemen_tgl[]    = tgl_indo($amd->tanggal);
        $amandemen_nomor[]  = $amd->nomor;
        $amandemen_nilai[]  = $amd->nilai; 
    }

    $awal = $akhir = array();
    foreach ( $tbl_kegiatan->koordinats as $kor) {
      $awal[] = "(X:$kor->koordinat_x,Y:$kor->koordinat_y)";
      $akhir[] = "(X:$kor->koordinat_x1,Y:$kor->koordinat_y1)";
    }

    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A'.$row )
      ->setCellValue('C'.$row,$tbl_kegiatan->nm_kegiatan )
      ->setCellValue('D'.$row,$tbl_kegiatan->jenis_kegiatan )
      ->setCellValue('E'.$row,$tbl_kegiatan->tahun )
      ->setCellValue('F'.$row,$tbl_kegiatan->nama_satker )
      ->setCellValue('G'.$row,$tbl_kegiatan->nama_ppk )
      ->setCellValue('H'.$row,$tbl_kegiatan->nama_bidang )
      ->setCellValue('I'.$row,$tbl_kegiatan->nama_output )
      ->setCellValue('J'.$row,$tbl_kegiatan->jenis_konstruksi )
      ->setCellValue('K'.$row, implode(', ',$Provinsi ) )
      ->setCellValue('L'.$row, implode(', ',$kecamatan ) )
      ->setCellValue('M'.$row, implode(', ',$kabupaten ) )
      ->setCellValue('N'.$row, implode(', ',$desa ) )
      ->setCellValue('O'.$row,$tbl_kegiatan->nama_wlsungai )
      ->setCellValue('P'.$row,$tbl_kegiatan->das )
      ->setCellValue('Q'.$row,$tbl_kegiatan->tujuan )
      ->setCellValue('R'.$row,$tbl_kegiatan->manfaat )
      ->setCellValue('S'.$row,$tbl_kegiatan->output )
      ->setCellValue('T'.$row,$tbl_kegiatan->outcome )
      ->setCellValue('U'.$row,$tbl_kegiatan->data_teknis )
      ->setCellValue('V'.$row,$tbl_kegiatan->data_kegiatan )
      ->setCellValue('W'.$row,$tbl_kegiatan->th_kegiatanlanjutan )
      ->setCellValue('X'.$row,$tbl_kegiatan->sumber_dana )
      ->setCellValue('Y'.$row,$tbl_kegiatan->status_kegiatan )
      ->setCellValue('Z'.$row,$tbl_kegiatan->dasar_kegiatan )
      ->setCellValue('AA'.$row,$tbl_kegiatan->nmp_jasa )
      ->setCellValue('AB'.$row,$tbl_kegiatan->perencana )
      ->setCellValue('AC'.$row,$tbl_kegiatan->supervisi )
      ->setCellValue('AD'.$row,$tbl_kegiatan->tgl_kontrak_awal )
      ->setCellValue('AE'.$row,$tbl_kegiatan->no_kontrak_awal )
      //AF Nilai Kontrak Awal
      //AG Tanggal Kontrak Akhir
      ->setCellValue('AF'.$row,$tbl_kegiatan->nilai_kontrak )
      //
      //   ->setCellValue('AG'.$row,$tbl_kegiatan->tgl_spmk )
      //   ->setCellValue('AH'.$row,$tbl_kegiatan->nomnor kontrak akhir )
      // >setCellValue('AI'.$row,$tbl_kegiatan->nilai kontrak akhir rP )

      //belum ada nilai kontrak akhir        //belum bisa spmk

      //->setCellValue('AI'.$row,$tbl_kegiatan->nilai kontrak akhir )
       ->setCellValue('AJ'.$row, implode(', ', $amandemen_tgl) )
       ->setCellValue('AK'.$row, implode(', ', $amandemen_nomor) )
       ->setCellValue('AL'.$row, implode(', ', $amandemen_nilai) )

      ->setCellValue('AM'.$row,$tbl_kegiatan->nilai_pagu )
      ->setCellValue('AN'.$row,$tbl_kegiatan->tgl_spmk )
      ->setCellValue('AO'.$row,$tbl_kegiatan->no_spmk )
      ->setCellValue('AP'.$row,$tbl_kegiatan->nilai_penyerapan )
      ->setCellValue('AQ'.$row,$tbl_kegiatan->keuangan_persen )
      // ->setCellValue('AR'.$row,$tbl_kegiatan->jkwkt_pelaksanaan )
      ->setCellValue('AS'.$row,$tbl_kegiatan->jkwkt_pelaksanaan )

      ->setCellValue('AT'.$row, implode(', ',$awal) )
      ->setCellValue('AU'.$row, implode(', ',$akhir) )

      ->setCellValue('AW'.$row,$tbl_kegiatan->youtube_url );
      //  ->setCellValue('AV'.$row,$tbl_kegiatan->gambar )
         
      $objPHPExcel->getActiveSheet()->getStyle('G'.$row )->applyFromArray($tengah_isi);

   $row++; // pindah ke row bawahnya ($row + 1)
   $nomor++;
 }

// Rename worksheet
//$objPHPExcel->getActiveSheet()->setTitle('');

// Set sheet yang aktif adalah index pertama, jadi saat dibuka akan langsung fokus ke sheet pertama
$objPHPExcel->setActiveSheetIndex(0);

// Simpan ke Excel 2007
/* $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('data.xlsx'); */

// Simpan ke Excel 2003
/* $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('data.xls'); */

//Download (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="database balai sungai III.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 

$objWriter->save('php://output');
exit;

/* 
// Download (Excel2003)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="data.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 

$objWriter->save('php://output');
exit;
 */

}

private function set_jenis_kegiatan(){
      $this->load->model('m_jenis_kegiatan');
        $this->data['jenis_kegiatan_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_kegiatan->get()->result_object();
        foreach($jenis as $j){
          $this->data['jenis_kegiatan_data'][$j->id_jenis_kegiatan] = $j->jenis_kegiatan;
        }
    }

/*private function set_jenis_kajian_lingkungan(){
  $this->load->model('m_jenis_kajian_lingkungan');
  $this->data['jenis_kajian_lingkungan_data'] = array('-- Pilih --');
  $jenis = $this->m_jenis_kajian_lingkungan->get()->result_object();
  foreach ($jenis as $j) {
    # code...
    $this->data['jenis_kajian_lingkungan_data'][$j->id_kajian_lingkungan] = $j->kajian_lingkungan;
  }
}*/

private function set_jenis_satuan(){
    $this->load->model('m_jenis_satuan');
    $this->data['jenis_satuan_data'] = array('');
    $jenis = $this->m_jenis_satuan->get()->result_object();
    foreach($jenis as $j){
      $this->data['jenis_satuan_data'][$j->id_satuan] = $j->nama_satuan;
    }
}

private function set_jenis_satuan_output(){
    $this->load->model('m_jenis_satuan_output');
    $this->data['jenis_satuan_output_data'] = array('');
    $jenis = $this->m_jenis_satuan_output->get()->result_object();
    foreach($jenis as $j){
      $this->data['jenis_satuan_output_data'][$j->id_satuan_output] = $j->nama_satuan_output;
    }
}

private function set_jenis_satuan_outcome(){
        $this->load->model('m_jenis_satuan_outcome');
        $this->data['jenis_satuan_outcome_data'] = array('');
        $jenis = $this->m_jenis_satuan_outcome->get()->result_object();
        foreach($jenis as $j){
          $this->data['jenis_satuan_outcome_data'][$j->id_satuan_outcome] = $j->nama_satuan_outcome;
        }
    }    

 private function set_jenis_satker(){
      $this->load->model('m_jenis_satker');
        $this->data['jenis_satker_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_satker->get()->result_object();
        foreach($jenis as $j){
          $this->data['jenis_satker_data'][ $j->id_satker ] = $j->nama_satker;
          
        }
    }

 private function set_jenis_bidang(){
        $this->load->model('m_jenis_bidang');
        $this->data['jenis_bidang_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_bidang->get()->result_object();
        foreach($jenis as $j){
            $this->data['jenis_bidang_data'][$j->id_bidang] = $j->nama_bidang;
        }
    }

  private function set_jenis_output(){
        $this->load->model('m_jenis_output');
        $this->data['jenis_output_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_output->get()->result_object();
        $this->data['jenis_output_data2'] = $jenis;
        foreach($jenis as $j){
            $this->data['jenis_output_data'][$j->id_output] = $j->nama_output;
        }
    }

   function set_provinsi(){
       $this->load->model('m_provinsi');
       $this->data['provinsi2'] = $this->m_provinsi->get()->result_object();
       $this->data['provinsi'] = array('-- Pilih --');
       foreach( $this->data['provinsi2']  as $j){
            $this->data['provinsi'][$j->id_provinsi] = $j->provinsi;
        }
    }

    function set_kabupaten(){
       $this->load->model('m_kabupaten');
       $this->data['kab2'] = $this->m_kabupaten->get()->result_object();
       $this->data['kab'] = array('-- Pilih --');
       foreach( $this->data['kab2']  as $j){
            $this->data['kab'][$j->id] = $j->name;
        }
    }

private function set_jenis_ppk(){
        $this->load->model('m_jenis_ppk');
        $this->data['jenis_ppk_data'] = array('-- Pilih --');
        // $this->data['jenis_ppk_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_ppk->get()->result_object();
        $this->data['jenis_ppk_data2'] = $jenis;
        foreach($jenis as $j){
            $this->data['jenis_ppk_data'][$j->id_ppk] = $j->nama_ppk;
        }
    }

 private function set_jenis_tahun(){
      $this->load->model('m_jenis_tahun');
        $this->data['jenis_tahun_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_tahun->get()->result_object();
        foreach($jenis as $j){
          $this->data['jenis_tahun_data'][$j->id_thn] = $j->nama_tahun;
        }
    }

 private function set_jenis_wlsungai(){
        $this->load->model('m_jenis_wlsungai');
        $this->data['jenis_wlsungai_data'] = array('-- Pilih --');
        $jenis = $this->m_jenis_wlsungai->get()->result_object();
        $this->data['wilayah_sungai'] = $jenis;
        foreach($jenis as $j){
            $this->data['jenis_wlsungai_data'][$j->id_wlsungai] = $j->nama_wlsungai;
        }
    }

    function get_form_wilayah_sungai(){
        $this->data['ws'] = $this->input->post('id');
        $this->data['nama'] = $this->input->post('nama');
        $this->load->model('m_das');
        $this->data['das'] = $this->m_das->getByWs($this->data['ws'])->result_object();
        $this->data['das_selected'] = array();
        $this->load->view('admin/kegiatan/form_das',$this->data);
    }

    function delete_form_ws(){
      $ws           = $this->input->post('id');
      $id_kegiatan  = $this->input->post('id_kegiatan');
      $this->kegiatan_model->delete_ws($id_kegiatan,$ws);
      $this->kegiatan_model->delete_das($id_kegiatan,$ws);
    }

    function get_form_lokasi_111(){
      $this->data['prov'] = $this->input->post('id');
      $this->data['nama'] = $this->input->post('nama');
      $this->data['id_kecamatan'] = '';
      $this->data['id_kelurahan'] = '';
      $this->data['id_kabupaten'] = '';
      $this->load->model('m_kabupaten');
      $this->data['kabupaten'] = $this->m_kabupaten->getByProvinsi($this->data['prov'])->result_object();
      $this->load->view('admin/kegiatan/form_lokasi',$this->data);
    }

    function get_form_lokasi(){
      $this->data['prov'] = $this->input->post('id');
      $this->data['nama'] = $this->input->post('nama');
      $this->data['id_kecamatan'] = '';
      $this->data['id_kelurahan'] = '';
      $this->data['id_kabupaten'] = '';
      $this->load->model('m_kabupaten');
      $this->data['kabupaten'] = $this->m_kabupaten->getByProvinsi($this->data['prov'])->result_object();
      $this->load->view('admin/kegiatan/form_lokasi2',$this->data);
    }

    function delete_form_lokasi(){
      $prov = $this->input->post('id');
      $id_kegiatan = $this->input->post('id_kegiatan');
      $this->kegiatan_model->delete_lokasi($id_kegiatan,$prov);
    }

    function delete_lokasi(){
      $id_lokasi = $this->input->post('id'); 
      $this->kegiatan_model->delete_lokasi_by_id($id_lokasi);
    }

    function delete_lokasi_kegiatan(){ 

        $id_kegiatan= (int) $this->input->post('id');
        $prov       = (int) $this->input->post('prov');
        $kab        = (int) $this->input->post('kab');
        $kec        = $this->input->post('kec');
        $kel        = $this->input->post('kel');

        
        // hapus by kelurahan
        if(!empty($kel)){
            $this->db->where('id_kelurahan',(int) $kel);
            $this->db->where('id_kegiatan',$id_kegiatan);
            $this->db->delete('tbl_kegiatan_lokasi');
        }

        // hapus by kec
        if(!empty($kec)){
            $this->db->where('id_kecamatan',$kec);
            $this->db->where('id_kegiatan',$id_kegiatan);
            $this->db->delete('tbl_kegiatan_lokasi');
        }

        
        $this->db->where('id_kegiatan',$id_kegiatan)->where('id_provinsi',$prov)->where('id_kabupaten',$kab);
        $this->db->delete('tbl_kegiatan_lokasi');
    }

    

    function get_kec_by_kab(){
        $this->load->model('m_kecamatan');
        $id = $this->input->post('id');
        $result = $this->m_kecamatan->getByKota($id)->result_array();
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function get_kelurahan_by_kec(){
        $this->load->model('m_kelurahan');
        $id = $this->input->post('id');
        $result = $this->m_kelurahan->getByKec($id)->result_array();
        header('Content-Type: application/json');
        echo json_encode($result);
    }




    /* kegiatan relasi */ 
    private function save_kegiatan_relasi($name){
        $kegiatan = $this->input->post($name);
        if($kegiatan ){ 
            foreach($kegiatan as $key => $kg){
                $this->kegiatan_model->update_relasi($key,$kg);
            }
        }
    }

    private function save_kegiatan_relasi_baru($id_kegiatan, $name){
        $isi    = $this->input->post('kegiatan_'. $name .'_isi_new');
        $tahun  = $this->input->post('kegiatan_'. $name .'_tahun_new');
        if($isi ){ 
            foreach ($isi as $key => $isinya) {
                $this->kegiatan_model->add_relasi(array(
                    'id_kegiatan'   => $id_kegiatan,
                    'name'          => 'kegiatan_' . $name,
                    'isi'           => $isinya,
                    'tahun'         => $tahun[$key],
                ));
            }
        }
    }

    /* detail konstruksi */
    private function save_detail_konstruksi_baru($id_kegiatan){
        $nama_bangunan    = $this->input->post('new_detail_konstruksi_nama_bangunan'); 
        $volume    = $this->input->post('new_detail_konstruksi_volume'); 
        $satuan    = $this->input->post('new_detail_konstruksi_satuan'); 
        $tahun    = $this->input->post('new_detail_konstruksi_tahun'); 
        if($nama_bangunan ){ 
            foreach ($nama_bangunan as $key => $nama) {
                $this->kegiatan_model->add_detail_konstruksi(array(
                    'id_kegiatan'   => $id_kegiatan,
                    'nama_bangunan' => $nama,
                    'volume'           => $volume[$key],
                    'satuan'         => $satuan[$key],
                    'tahun'         => $tahun[$key],
                ));
            }
        }
    }

     private function save_detail_konstruksi(){
        $detail_konstruksi = $this->input->post('detail_konstruksi');
        if($detail_konstruksi ){ 
            foreach($detail_konstruksi as $key => $dk){
                $this->kegiatan_model->update_detail_konstruksi($key,$dk);
            }
        }
    }

/* realisasi pelaksanaan */
    private function save_realisasi_pelaksanaan_baru($id_kegiatan){
        $progres_fisik_tanggal    = $this->input->post('new_realisasi_pelaksanaan_progres_fisik_tanggal'); 
        $progres_fisik_rencana    = $this->input->post('new_realisasi_pelaksanaan_progres_fisik_rencana'); 
        $progres_fisik_realisasi  = $this->input->post('new_realisasi_pelaksanaan_progres_fisik_realisasi'); 
        $progres_fisik_deviasi    = $this->input->post('new_realisasi_pelaksanaan_progres_fisik_deviasi'); 
        if($progres_fisik_tanggal ){ 
            foreach ($progres_fisik_tanggal as $key => $pftang) {
                $this->kegiatan_model->add_realisasi_pelaksanaan(array(
                    'id_kegiatan'   => $id_kegiatan,
                    'progres_fisik_tanggal'   => $pftang,
                    'progres_fisik_rencana'   => $progres_fisik_rencana[$key],
                    'progres_fisik_realisasi' => $progres_fisik_realisasi[$key],
                    'progres_fisik_deviasi'   => $progres_fisik_deviasi[$key],
                ));
            }
        }
    }

    private function save_realisasi_pelaksanaan(){
            $realisasi_pelaksanaan = $this->input->post('realisasi_pelaksanaan');
            if($realisasi_pelaksanaan){ 
                foreach($realisasi_pelaksanaan as $key => $rp){
                    $this->kegiatan_model->update_realisasi_pelaksanaan($key,$rp);
                }
            }
        }
     

    function delete_kegiatan_relasi($id){
        $result = array(
            'status' => '0',
            'message' => 'gagal',
            'title' => 'gagal',
            'type' => 'error' );
        if($this->kegiatan_model->delete_relasi($id)){
             $result = array(
                'status' => '1',
                'message' => 'Berhasil dihapus',
                'title' => 'terima kasih',
                'type' => 'success'
            );    
        }  
       header('Content-Type: application/json');
       echo json_encode($result);
    }


    function delete_detail_konstruksi($id){
        $result = array(
            'status' => '0',
            'message' => 'gagal',
            'title' => 'gagal',
            'type' => 'error' );
        if($this->kegiatan_model->delete_detail_konstruksi($id)){
             $result = array(
                'status' => '1',
                'message' => 'Data 1 detail konstruksi terhapus',
                'title' => 'Berhasil',
                'type' => 'success'
            );    
        }  
       header('Content-Type: application/json');
       echo json_encode($result);
    }

    function delete_realisasi_pelaksanaan($id){
        $result = array(
            'status' => '0',
            'message' => 'gagal',
            'title' => 'gagal',
            'type' => 'error' );
        if($this->kegiatan_model->delete_realisasi_pelaksanaan($id)){
             $result = array(
                'status' => '1',
                'message' => 'Data 1 realisasi pelaksanaan terhapus',
                'title' => 'Berhasil',
                'type' => 'success'
            );    
        }  
       header('Content-Type: application/json');
       echo json_encode($result);
    }



    function laporan($id_laporan=""){

      $laporan = $this->db->where("id_laporan",$id_laporan)->get("tbl_laporan")->row_object();

      if($laporan){
        $this->data['laporan'] = $laporan;
        #$this->data['kegiatan'] = $this->db->where("id_kegiatan",$laporan->id_kegiatan)->get("tbl_kegiatan")->row_object();
        $this->data['list_asistensi'] = $this->kegiatan_model->get_asistensi_by_laporan($id_laporan)->result_object();
        
        /*$this->data['lbr'] = $this->db->select("lbr.*, u.nama as user_nama")
        ->join("tbl_user u","lbr.id_user_dari=u.user_id")
        ->order_by("id_asistensi_lbr","ASC")
        ->where("id_asistensi",$id_asistensi)
        ->get("tbl_asistensi_lbr lbr")
        ->result_object();*/

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kegiatan/laporan');
        $this->load->view('admin/footer');

      }else{
        show_404();
      }
    }

    function asistensi($id_asistensi=""){

      $asistensi = $this->db->where("id_asistensi", $id_asistensi  )->get("tbl_asistensi")->row_object();

      if($asistensi){
        $this->data['asistensi'] = $asistensi; 
        
        $this->data['lbr'] = $this->db->select("lbr.*, u.nama as user_nama")
            ->join("tbl_user u","lbr.id_user_dari=u.user_id")
            ->order_by("id_asistensi_lbr","ASC")
            ->where("id_asistensi",$id_asistensi)
            ->get("tbl_asistensi_lbr lbr")
            ->result_object();

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/kegiatan/asistensi');
        $this->load->view('admin/footer');

      }else{
        echo '<p style="color:red">Asistensi tidak ditemukan!</p>';
      }
    }


    


    function set_status_asistensi(){
      $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('id', 'id_asistensi', 'required|trim');  
        $this->form_validation->set_rules('status', 'status', 'required|trim');  

        if ($this->form_validation->run() == true) {

          $id_asistensi = $this->input->post("id",true);
          $data = [ 
            'status' => $this->input->post("status",true)
          ];

          if($this->kegiatan_model->update_asistensi($id_asistensi ,$data)){

            

            redirect("admin/kegiatan/asistensi/" . $this->input->post("id",true));
          }
             
        } 
        
        echo validation_errors();
    }

    function set_status_laporan(){
      $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('id', 'id laporan', 'required|trim');  
        $this->form_validation->set_rules('status', 'status', 'required|trim');  

        if ($this->form_validation->run() == true) {

          $id_laporan = $this->input->post("id",true);
          $data = [ 
            'status' => $this->input->post("status",true)
          ];

          if($this->kegiatan_model->update_laporan($id_laporan ,$data)){
            redirect("admin/kegiatan/update/" . $this->input->post("kegiatan",true));
          }
             
        } 
        
        echo validation_errors();
    }


    /* LAPORAN */
    function add_laporan(){
      $result = array(
                'status' => 'gagal'
            ); 

        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('id', 'ID Kegiatan', 'required|trim'); 
        $this->form_validation->set_rules('judul', 'Judul', 'required|trim'); 
        // $this->form_validation->set_rules('des', 'Deskripsi', 'required|trim'); 

        if ($this->form_validation->run() == true) {

          $id_kegiatan = $this->input->post("id",true);
          $data = [
            'id_kegiatan'   => $id_kegiatan,
            'judul'         => $this->input->post("judul",true),
            'tanggal'       => date("Y-m-d H:i:s",now()),
            'status'        => "open"
          ];

          if($this->kegiatan_model->add_laporan($data)){

            /*$this->kirim_email_pemberitahuan_ada_topik_asistensi_baru([
              'id_kegiatan' => $id_kegiatan, 
              'judul' => $this->input->post("judul",true)
            ]);*/

            $result = array(
                'status' => 'ok', 
            ); 
          }
             
        }else{
          $result['error'] = validation_errors();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    function delete_laporan($id = ''){
        $result = array(
            'status' => '0',
            'message' => 'gagal',
            'title' => 'gagal',
            'type' => 'error'
            );

        if($this->mainlib->cek_level(['direksi','superadmin'])) {  

            if($this->kegiatan_model->delete_laporan($id)){
                 $result = array(
                    'status' => '1',
                    'message' => 'Laporan telah dihapus',
                    'title' => 'Berhasil',
                    'type' => 'success'
                );  
            }

        }  
       header('Content-Type: application/json');
       echo json_encode($result);
    }




    function add_asistensi(){
      $result = array(
            'status' => 'gagal'
        ); 

        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('id_laporan', 'ID Laporan', 'required|trim'); 
        $this->form_validation->set_rules('judul', 'Judul', 'required|trim');  

        if ($this->form_validation->run() == true) {

          $id_laporan = $this->input->post("id_laporan",true);
          $data = [
            'id_laporan' => $id_laporan,
            'asistensi' => $this->input->post("judul",true), 
            'tanggal'       => date("Y-m-d H:i:s",now()),
            'status' => "open"
          ];

          if($this->kegiatan_model->add_asistensi($data)){

           /* $this->kirim_email_pemberitahuan_ada_topik_asistensi_baru([
              'id_kegiatan' => $id_kegiatan, 
              'judul' => $this->input->post("judul",true)
            ]);*/

            $result = array(
                'status' => 'ok', 
            ); 
          }
             
        }else{
          $result['error'] = validation_errors();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }




    function add_asistensi_lbr(){
      $result = array(
                'status' => 'gagal'
            ); 

        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('message', 'Pesan', 'required|trim'); 
        $this->form_validation->set_rules('id', 'ID', 'required|trim');  

        if ($this->form_validation->run() == true) {

          $id_asistensi = $this->input->post("id",true);
          $data = [
            'id_asistensi'  => $id_asistensi,
            'isi'           => $this->input->post("message",true),
            'id_user_dari'  => $this->session->userdata("id_user"),
            'tanggal'       => date("Y-m-d H:i:s",now()), 
          ];


          # aksi kalau mengirim file
          if($this->input->post("file",true)){
            $data["file"] = $this->input->post("file",true);

            $path = "uploads/asistensi_lbr_temp/" . $data["file"]; 
            $dest = "uploads/lembar_asistensi/"; 
            $dest_path_file = $dest . $data["file"]; 

            # periksa apakah folder destination ada
            if(!file_exists($dest)){

                # buatkan kalau tidak ada
                mkdir($dest);
            }

            # cek file temp ada
            if( file_exists($path)){

                # copy file temp ke file penyimpanan
                if(copy($path, $dest_path_file)){

                    # hapus file temp kalau sudah di copy
                    unlink($path);
                }
            }

          }

          if($this->kegiatan_model->add_asistensi_lbr($data)){

            $asistensi = $this->db->where("id_asistensi",$id_asistensi)->get("tbl_asistensi")->row_object();

            $kirim_ke = "konsultan";
            if($this->mainlib->cek_level(['konsultan'])) {
              $kirim_ke = "direksi";
            }

            /*$this->kirim_email_pemberitahuan_ada_lbr_asistensi_baru([
              'id_laporan' => $asistensi->id_laporan, 
               'id_asistensi' => $id_asistensi,
               'kirim_ke' => $kirim_ke,
              'judul' => $this->input->post("judul",true)
            ]);*/

            $data_row = [
              'isi' => $this->input->post("message",true),
              'tanggal'       => date("Y-m-d H:i:s",now()), 
              'user_nama' => "Anda",
              'file' => $this->input->post("file",true),
            ];

            $result = array(
                'status' => 'ok',
                'html' => $this->load->view("admin/kegiatan/row-lbr-asistensi",$data_row,true)
            ); 
          }
             
        }else{
          $result['error'] = validation_errors();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    function upload_img_temp_lembar_asistensi(){
        $this->load->helper('number');

        $result = array(
            'status'    => '0', 
            'message'   => 'gagal mengupload'
        );
        $fullPath = 'uploads/asistensi_lbr_temp/';

        if(!file_exists($fullPath)){
            mkdir($fullPath);
        }

        $config['upload_path']      = $fullPath;
        $config['allowed_types']    = $this->allowed_types_upload;
        $config['max_size']         = $this->max_size_upload_berkas;
        $config['max_width']        = '6000'; // 6000px
        $config['max_height']       = '6000'; // 6000px 
        //$config['encrypt_name']     = true;
        $config['file_ext_tolower']     = true;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) 
        {
            $er = $this->upload->display_errors();
            $result['message'] = strip_tags($er);
        } 
        else 
        {
             
            $data = $this->upload->data();  

            $result['status'] = '1';
            $result['message'] = 'Berhasil';
            $result['message2'] =  $data;
            $result['data'] = array(
                    'file_name' => $data['file_name'],
                    'file_ext' => $data['file_ext'],
                    'file_size' => $data['file_size']  
                ); 
        }
       echo json_encode($result);
    }

    function delete_img_temp_lembar_asistensi(){

        
        $this->form_validation->set_rules('file', 'File', 'required|trim');   

        if ($this->form_validation->run() == true) {

            $file = $this->input->post("file",true);
            $path = "uploads/asistensi_lbr_temp/" . $file;  

            # cek file temp ada
            if( file_exists($path)){

                # hapus file temp kalau ada
                unlink($path);
                echo "file dihapus"; 
                exit;
            }

        }

        echo "tidak ada dihapus";
    }



    private function kirim_email_pemberitahuan_kepada_konsultan_dipilih_sebagai_penyedia_jasa($id_kegiatan = ''){

      if($this->kirim_email_asistensi != true){
        return false;
      }

      $this->load->config("email");
      $this->load->library('parser'); 
      $this->load->library('email'); 

      $pesan = 'Yth. {nama}<br /><br />Anda telah dipilih sebagai pihak penyedia jasa dalam pekerjaan {nama_kegiatan} Tahun Anggaran {tahun} di aplikasi CO-P.<br /> Lihat detail di link berikut <a href="{link}">{link}</a>';

      $id = $this->input->post('konsultan',TRUE);
      $user = $this->db->where("user_id", $id )->get("tbl_user")->row_object();

      
      $data = array(
        'pesan'           => $pesan, 
        'tahun'            => $this->input->post('tahun_anggaran',TRUE),
        'nama'            => $user->nama,
        'nama_kegiatan'   => $this->input->post('nm_kegiatan',TRUE),
        'link'            => site_url("admin/kegiatan/update/" . $id_kegiatan )
      );

      $message  = $this->parser->parse("default_template",$data,true); 
      $subject = "Pemberitahuan lembar asistensi online, Pekerjaan " . $this->input->post('nm_kegiatan',TRUE);

      $this->email->from($this->config->item("email_sender"), $this->config->item("email_name"));
      $this->email->to($user->email); 

      $this->email->subject($subject);
      $this->email->message($message);  

      $this->email->send();


    }

    private function kirim_email_pemberitahuan_ada_topik_asistensi_baru($data = array()){


      if($this->kirim_email_asistensi != true){
        return false;
      } 

      $kegiatan = $this->db->where("id_kegiatan", $data["id_kegiatan"] )->get("tbl_kegiatan")->row_object();

      $this->load->config("email");
      $this->load->library('parser'); 
      $this->load->library('email'); 

      $user_id = $kegiatan->konsultan;
      
      $pesan = 'Yth. {nama}<br /><br />Sampaikan Tanggapan Anda tentang Topik Asistensi secara online dengan mengisi lembar asistensi di link berikut <a href="{link}">{link}</a>'; 

      $user = $this->db->where("user_id", $user_id )->get("tbl_user")->row_object();

      if($user){ 

          $data_parse = array(
            'pesan'           => $pesan,  
            'nama'            => $user->nama,
            'link'            => site_url("admin/kegiatan/update/" . $data["id_kegiatan"] ),
          );

          

          $message  = $this->parser->parse("default_template",$data_parse,true); 
          $subject = "Pemberitahuan Penambahan Topik Asistensi ". $data['judul'] . " Pekerjaan " . $kegiatan->nm_kegiatan;

          //echo $subject;
          //echo $message; 
          //die();
           
          $this->email->from($this->config->item("email_sender"), $this->config->item("email_name"));
          $this->email->to($user->email); 

          $this->email->subject($subject);
          $this->email->message($message);  

          $this->email->send();
      }


    }

     private function kirim_email_pemberitahuan_ada_lbr_asistensi_baru($data = array()){


      if($this->kirim_email_asistensi != true){
        return false;
      } 

      $kegiatan = $this->db->where("id_kegiatan", $data["id_kegiatan"] )->get("tbl_kegiatan")->row_object();

      $this->load->config("email");
      $this->load->library('parser'); 
      $this->load->library('email'); 

      if($data['kirim_ke'] == "konsultan"){
        $user_id = $kegiatan->konsultan;
      }else{
        $user_id = $kegiatan->user_id;
      }
      
      $pesan = 'Yth. {nama}<br /><br />Sampaikan Tanggapan Anda tentang Topik Asistensi secara online dengan mengisi lembar asistensi di link berikut <a href="{link}">{link}</a>'; 

      $user = $this->db->where("user_id", $user_id )->get("tbl_user")->row_object();

      if($user){ 

          $data_parse = array(
            'pesan'           => $pesan,  
            'nama'            => $user->nama,
            'link'            => site_url("admin/kegiatan/asistensi/" . $data["id_asistensi"] ),
          );

          

          $message  = $this->parser->parse("default_template",$data_parse,true); 
          $subject = "Pemberitahuan Penambahan Topik Asistensi ". $data['judul'] . " Pekerjaan " . $kegiatan->nm_kegiatan;

          //echo $subject;
         // echo $message; 
          //die();
           
          $this->email->from($this->config->item("email_sender"), $this->config->item("email_name"));
          $this->email->to($user->email); 

          $this->email->subject($subject);
          $this->email->message($message);  

          $this->email->send();
          
         /* if ($this->email->send() == TRUE)
		{
			echo 'Terkirim!';
		}else{
			echo $this->email->print_debugger();
		}*/
		
      }


    }

    
}