<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_wlsungai extends CI_Controller{

	
 	function __construct()
    {
        parent::__construct();

        $this->load->model('m_jenis_wlsungai');
        $this->load->library('form_validation');
        $this->load->library(array('session', 'mainlib'));
		$this->mainlib->logged_in();

		$this->data = array();

    }


	public function index(){

		 $this->data['jenis'] = $this->m_jenis_wlsungai->get()->result_object();

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/jenis_wlsungai/main');
        $this->load->view('admin/footer');

	}

     public function create() 
     {
        $this->mainlib->batasi_akses_user(['superadmin']);
     	$this->create_action();

         $this->load->view('admin/header', $this->data);
         $this->load->view('admin/jenis_wlsungai/add');
         $this->load->view('admin/footer');

     }
    
    private function create_action() {


        $this->form_validation->set_rules('nama_wlsungai', 'Nama_wlsungai', 'required|trim'); 

        if ($this->form_validation->run() == true) {


            $data = array( 
        'nama_wlsungai'    => $this->input->post('nama_wlsungai',TRUE),
        'des_wlsungai'  => $this->input->post('des_wlsungai',TRUE),
         );

           if($this->m_jenis_wlsungai->insert($data)){
              $this->session->set_flashdata('message', 'Create Record Success');
              redirect(site_url('admin/jenis_wlsungai'));
           } else{
              show_error("aduh!");
           }
        }
    }



     function edit($id){
    	
        $this->mainlib->batasi_akses_user(['superadmin']);

     	$this->form_validation->set_rules('nama_wlsungai', 'Nama_wlsungai', 'required|trim'); 

         if ($this->form_validation->run() == true) {

            $data = array( 
 				'nama_wlsungai' => $this->input->post('nama_wlsungai',TRUE),
 				'des_wlsungai' 	=> $this->input->post('des_wlsungai',TRUE),
 	   		);

            if($this->m_jenis_wlsungai->update($data,$this->input->post('id',TRUE))){

            		$this->session->set_flashdata('message', 'Update Record Success');
            		redirect(site_url('admin/jenis_wlsungai'));

            } else{
            		show_error("aduh!");
            }

         }


     	$this->data['jenis'] = $this->m_jenis_wlsungai->get_by_id($id)->row_object();

     	$this->load->view('admin/header', $this->data);
        $this->load->view('admin/jenis_wlsungai/edit');
        $this->load->view('admin/footer');
     }


     function delete($id){
    	
        $this->mainlib->batasi_akses_user(['superadmin']);
        
     	if($this->m_jenis_wlsungai->delete($id)){
        		$this->session->set_flashdata('message', 'Delete Record Success');
        		redirect(site_url('admin/jenis_wlsungai'));
        } else{
        		show_error("aduh!");
        }

     }



 }