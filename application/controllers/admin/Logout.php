<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Logout extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
      	$this->load->library('form_validation'); //baris ini harus ditambahkan agar fungsi 'form_validation' dapat digunakan pada controller ini.

    	$this->load->library(array('session', 'mainlib'));
 	   	$this->mainlib->logged_in();
 	   	$this->load->helper("main");

		$this->data = array();
	}

	function index()  {
		 $this->load->library('session');
		$this->session->sess_destroy();
		redirect('');
	}

	 
}

 ?>