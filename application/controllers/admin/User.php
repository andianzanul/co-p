<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
	// tambahan
	function __construct()
    {
       	parent::__construct();

       	$this->load->model('user_model');
      	$this->load->library('form_validation'); //baris ini harus ditambahkan agar fungsi 'form_validation' dapat digunakan pada controller ini.

    	$this->load->library(array('session', 'mainlib'));
 	   	$this->mainlib->logged_in();
 	   	$this->load->helper("main");

		$this->data = array();
    }
    // tambahan 


	protected $username_temp;

	 
	public function index() {
		 
		$this->data['user'] = $this->user_model->get()->result_object();
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/user/list_user');
		$this->load->view('admin/footer');
	}
	 

	public function add(){ 

		$this->mainlib->batasi_akses_user(['superadmin','direksi']);

		$this->set_jenis_satker(); 

		$this->load->library(array('session', 'mainlib'));
		$this->mainlib->logged_in();
		$this->load->library('form_validation');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('valid_email', 'silahkan ketikkan format email yang benar');
		$this->form_validation->set_message('min_length', 'password kurang dari 5 digit');
		$this->form_validation->set_error_delimiters('<p class="alert">','</p>');

		if($this->form_validation->run('register') == FALSE){
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/user/add_user');
			$this->load->view('admin/footer');
			//$this->load->view('form_register',$this->data);
		} 
		else{
			$this->load->model('User_model');
			$this->User_model->user_register($this->input->post(NULL,TRUE));
			//$this->form_validation->set_message('password_check','Passwordnya itu salah');
			//$this->load->view('form_login');
			$this->session->set_flashdata('message', 'Tambah User berhasil');
            	redirect('admin/user');

		}
	}


	// tambahan
	function edit($user_id) {

		$this->mainlib->batasi_akses_user(['superadmin']);

		$this->load->helper('site_helper');
		// $encrypt_password = bCrypt('password',12);  
    	$this->form_validation->set_rules('username', 'Username', 'required|trim'); 
    	$this->form_validation->set_rules('level', 'level', 'required|trim'); 
    	$this->form_validation->set_rules('email', 'email', 'required|valid_email');  

      	if ($this->form_validation->run() == true) { 
          	$data = array(
          		'user_id' 	=> $this->input->post('user_id',TRUE),
				'username' 	=> $this->input->post('username',TRUE),
				//'id_satker' => $this->input->post('id_satker',TRUE),
				// 'nama_satker'=> $this->input->post('nama_satker',TRUE),
				'email' => $this->input->post('email',TRUE),
				'level' => $this->input->post('level',TRUE), 
	   		);

	   		if(!empty($this->input->post("password"))){
	   			$data['password'] = bcrypt($this->input->post('password'),12);
	   		}

	   		if(isset($_POST['id_satker'])){
				$data['id_satker'] = $this->input->post('id_satker',TRUE);
			}

			if(isset($_POST['ppk'])){

				$ppk = $this->db->where("id_ppk", $this->input->post('ppk',TRUE) )->get('tbl_ppk')->row_object();

				$data['id_satker'] = $ppk->id_satker;
				$data['id_ppk'] = $this->input->post('ppk',TRUE);
			}

	   		if($this->user_model->update($data,$this->input->post('user_id',TRUE))){

            	$this->session->set_flashdata('message', 'Update Record Success');
            	redirect('admin/user');

	        } else {
	           		show_error("Data tidak ditemukan!");
	        }
    	}

    	$this->set_jenis_satker();
    	// $this->data['jenis'] = $this->user_model->tampil_user($id)->row_object();        

	    $this->data['jenis'] = $this->user_model->get_by_id($user_id)->row_object();

	    $this->load->view('admin/header', $this->data);
	    $this->load->view('admin/user/edit_user');
	    $this->load->view('admin/footer');
	}

	public function delete($id){

		$this->mainlib->batasi_akses_user(['superadmin']);
    	
     	if($this->user_model->delete($id)){
        		$this->session->set_flashdata('message', 'Delete Record Success');
        		redirect(site_url('admin/user'));
        } else{
        		show_error("Terjadi Kesalahan!");
        }

    }  

	/* helper function start here */


	public function email_check($str){
		$this->load->model('User_model');
		if($this->User_model->exist_row_check('email', $str) > 0){
			$this->form_validation->set_message('email_check', 'Email sudah digunakan, mohon diganti yang lain');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	public function username_check($str){
		$this->load->model('User_model');
		if($this->User_model->exist_row_check('username', $str) > 0){
			$this->form_validation->set_message('username_check', 'Username sudah digunakan, mohon diganti yang lain');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}	

	// tambahan
	private function set_jenis_satker(){
        $this->load->model('user_model');
        $this->data['jenis_satker_data'] = array('-- Pilih --');
        
        $jenis = $this->user_model->get()->result_object();
        foreach($jenis as $j) {
            $this->data['jenis_satker_data'][ $j->id_satker ] = $j->nama_satker;   
        }
    }
    

   /* private function set_ppk(){
        $this->load->model('M_jenis_ppk');
        $this->data['ppk'] = array('-- Pilih --');
        
         $jenis = $this->M_jenis_ppk->get_for_combo()->result_object();
        foreach($jenis as $j) {
            $this->data['ppk'][ $j->id_ppk ] = $j->nama_ppk;   
        }
    }*/


    
	// akhir tambahan
	/*public function login(){
		$this->load->view('favicon');
		$this->load->library('form_validation');
		$input = $this->input->post(NULL,TRUE);
		$this->username_temp = @$input['username'];

		if($this->form_validation->run('login') == FALSE){
			$this->load->view('form_login');
			// redirect(base_url(''));
		}
		else {

// print_r($this->user);die();

			$this->load->library('session');
			$login_data = array(
					'username' => $input['username'],
					'id_user' => $this->user->user_id,
					'active_since' => $this->user->active_since,
					'level' => $this->user->level,
					'id_satker' => $this->user->id_satker,
					// id_ppk
 					'login_status' => TRUE
				);

			$this->session->set_userdata($login_data);

			if($this->session->userdata('level')=='superadmin') {

			}

			redirect(base_url('admin'));
		}
	}*/

	// public function login()
	// {
	// 	$this->load->view('favicon');
	// 	$this->load->library('form_validation');
	// 	$this->load->helper('site_helper');

	// 	if(isset($_POST['submit']))
	// 	{
	// 		$username = $this->input->post('username');
	// 		$password = bCrypt($this->input->post('password'));

	// 		$user = $this->user_model->cekadmin($username,$password);
	// 		$row  = $user->row_array();
	// 		$total_user = $user->num_rows();

	// 		if($total_user > 0) {
	// 			$this->session->set_userdata(array(
	// 				'id' => $row['username'], 
	// 				'level' => 'superadmin' ));
	// 		}

	// 		redirect (base_url('admin'));
	// 	} else {
	// 		$this->load->view('form_login');
	// 	}
	// }

	public function password_check($str){
		$this->load->model('User_model');
		$user_detail = $this->User_model->get_user_detail($this->username_temp);
		$this->user=$user_detail;
		if($user_detail){
			if($user_detail->password == crypt($str,$user_detail->password)){
				return TRUE;
			}
			else{
				$this->form_validation->set_message('password_check','Passwordnya itu salah');
				return FALSE;
			}			
		}
		else{
			$this->form_validation->set_message('password_check','Username tidak ada');
				return FALSE;
		}
	}

	
}