<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		//$this->load->m_login();

		$this->load->model('user_model');
      	$this->load->library('form_validation'); //baris ini harus ditambahkan agar fungsi 'form_validation' dapat digunakan pada controller ini.

    	$this->load->library(array('session', 'mainlib'));
 	   
 	   	$this->load->helper("main");

		$this->data = array();
	}

	function index() 
	{
		//$this->load->view('form_login');
		$this->load->view('favicon');
		$this->load->library('form_validation');
		$input = $this->input->post(NULL,TRUE);
		$this->username_temp = @$input['username'];

		if($this->form_validation->run('login') == FALSE){
			$this->load->view('form_login'); 
		}
		else {

 			//print_r($this->user);die();

			$this->load->library('session');
			$login_data = array(
					'username' => $input['username'],
					'id_user' => $this->user->user_id,
					'active_since' => $this->user->active_since,
					'level' => $this->user->level,
					'id_satker' => $this->user->id_satker,
					'id_ppk' => $this->user->id_ppk,
					// id_ppk
 					'login_status' => TRUE
				);


			// ubah satker ppk, ke satker induknya
			$this->load->model("m_jenis_ppk");
			$ppk = $this->m_jenis_ppk->get_by_id($this->user->id_ppk)->row_object();
			if($ppk){
				$login_data['id_satker'] = $ppk->id_satker;
			}

			$this->session->set_userdata($login_data);

			if($this->session->userdata('level')=='superadmin') {

			}

			redirect('admin');
		}
	}


	public function password_check($str){
		$this->load->model('User_model');
		$user_detail = $this->User_model->get_user_detail($this->username_temp);
		$this->user=$user_detail;
		if($user_detail){
			if($user_detail->password == crypt($str,$user_detail->password)){
				return TRUE;
			}
			else{
				$this->form_validation->set_message('password_check','Passwordnya itu salah');
				return FALSE;
			}			
		}
		else{
			$this->form_validation->set_message('password_check','Username tidak ada');
				return FALSE;
		}
	}

	/*function aksi_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password')

		$where = array(
			'username' => $username;
			'password' => bCrypt('$password');
		);

		$this->load->m_login('cek_akun');
	}*/
}

 