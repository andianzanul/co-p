<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
  <html>
    <head>
      <title></title>
      <!--Import Google Icon Font-->
      <link href="assets/css/materialicons.css" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href=base_url"assets/css/materialize.min.css"  media="screen,projection"/>
      <!--Import style.css -->
      <link type="text/css" rel="stylesheet" href=base_url"assets/css/style.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="assets/js/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="assets/js/materialize.min.js"></script>
      <script type="text/javascript" src="assets/js/style.js"></script>
    </body>
  </html>
