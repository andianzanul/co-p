<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Tambah PPK </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah PPK</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                 <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body"> 

<?php echo form_open(); ?>


        <div class="form-group">
            <label for="varchar">Satuan Kerja <?php echo form_error('nama_satker') ?></label>
            <?php echo form_dropdown('nama_satker',$jenis_satker_data,"",'class="form-control" name="nama_satker" id="nama_satker" placeholder="Satker"'); ?>
        </div>
       
        <div class="form-group">
            <label for="varchar">Nama PPK<?php echo form_error('nama_ppk') ?></label>
            <input type="text" class="form-control" name="nama_ppk" id="nama_ppk"   value="<?php echo set_value('jenis_ppk'); ?>" />
        </div>  <div class="form-group">
            <label for="varchar">Tahun Nomenklatur<?php echo form_error('tahun') ?></label>
            <input type="text" class="form-control" name="tahun" id="tahun"   value="<?php echo set_value('tahun'); ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Deskripsi <?php echo form_error('des_ppk') ?></label>
            <textarea class="form-control" name="des_ppk" id="des_ppk"><?php echo set_value('des_ppk'); ?></textarea>
            
        </div>
         
         
        <button type="submit" class="btn btn-primary">Simpan</button> 
         
    </form>





















            </div>
        </div>

    </section>
    <!-- /.content -->

         
        
   