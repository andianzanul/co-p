
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Master Kabupaten </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php 
    $message = $this->session->userdata('message');
    if($message){
      echo '<div class="alert alert-success">'. $message .'</div>';
    }

    ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">



            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body"> 
           <?php if($this->mainlib->cek_level(['superadmin'])) {  ?>
            <?php echo anchor(site_url('admin/kabupaten/create'),'Tambah', 'class="btn btn-primary btn-sm"'); }
             ?>
       <table id="example1" class="table table-bordered" style="margin-bottom: 10px">
            <thead><tr> 
           

       <th class="hiiden-sm-down hidden-xs hidden-xs-down">Provinsi</th>
        <th>Kabupaten</th> 
    <?php if($this->mainlib->cek_level(['superadmin'])) {  ?><th>Action</th><?php } ?>
            </tr>
  </thead>
            </tbody><?php
            foreach ($kabupaten as $data)
            {
                ?>
                <tr>
      <td><?php echo $data->provinsi ?></td>
   <td><?php echo $data->name ?></td>
        <?php if($this->mainlib->cek_level(['superadmin'])) {  ?>
      <td>


        <?php 
        
        echo '   '; 
        echo anchor(site_url('admin/kabupaten/edit/'.$data->id),'Edit', 'class="btn btn-warning btn-sm"'); 
        echo '  '; 
        echo anchor(site_url('admin/kabupaten/delete/'.$data->id),'Hapus','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
        ?>
      </td><?php } ?>
    </tr>
                <?php
            }
            ?></tbody>
        </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
        <script>
  $(function () {
    $("#example2").DataTable();

    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
