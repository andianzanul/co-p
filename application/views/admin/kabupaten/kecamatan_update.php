<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <br>
    <br>

    <div class="container">
        <?php if (isset($_SESSION['success'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i>
                <?php echo $_SESSION['success']; ?>
            </div>
        <?php } ?>

        <div class="card">

            <div class="card-header">
                Update data kecamatan
            </div>

            <div class="card-body">
                <?php echo validation_errors('<div class="alert alert-danger" role="alert">', '</div>'); ?>

                <p><?php $id = $this->uri->segment('3'); ?> </p>
                <?php echo form_open_multipart('kecamatan/update/' . $id); ?>
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <select class="custom-select" name="kab" id="kab">
                            <option value="<?= $kecamatan->id_kabupaten; ?>"><?= $kecamatan->id_kabupaten; ?></option>
                            <?php foreach ($kabupaten as $kab) : ?>
                                <option value="<?= $kab['id']; ?>"><?= $kab['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-auto">
                        <input type="text" class="form-control mb-2" id="name" name="name" value="<?= $kecamatan->name; ?>">
                    </div>
                    <div class="col-auto">
                        <input type="text" class="form-control mb-2" id="deskripsi" name="deskripsi" value="<?= $kecamatan->deskripsi; ?>">
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- end of inputtan -->
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>