<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Desa</title>
</head>

<body>
    <br>
    <br>
    <div class="card">
        <div class="card-body">
            <a href="<?= base_url('admin/provinsi/provinsi'); ?>" class="btn btn-info"> Provinsi </a>
            <a href="<?= base_url('admin/provinsi/kabupaten'); ?>" class="btn btn-info"> Kabupaten </a>
            <a href="<?= base_url('admin/provinsi/kecamatan'); ?>" class="btn btn-info"> Kecamatan </a>
            <a href="<?= base_url('admin/provinsi/kelurahan'); ?>" class="btn btn-info"> Kelurahan </a>
            <a href="<?= base_url('admin/provinsi/desa'); ?>" class="btn btn-info"> Desa </a>
        </div>
    </div>
    <br><br>

    <div class="container">
        <?php if (isset($_SESSION['success'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i>
                <?php echo $_SESSION['success']; ?>
            </div>
        <?php } ?>

        <div class="card">

            <div class="card-header">
                Input data Desa
            </div>

            <div class="card-body">
                <?php echo validation_errors('<div class="alert alert-danger" role="alert">', '</div>'); ?>


                <?php echo form_open_multipart('/admin/provinsi/desa/addDesa'); ?>
                <div class="form-row align-items-center">
                    <div class="row">
                        <div class="col-12">
                            <select class="custom-select" name="pro" id="pro">
                                <option selected>Select Provinsi</option>
                                <?php foreach ($provinsi as $prov) : ?>
                                    <option value="<?= $prov['id_provinsi']; ?>"><?= $prov['provinsi']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="row">
                        <div class="col-auto">
                            <input type="text" class="form-control mb-2" id="name[]" name="name[]" placeholder="kotadesa 1">
                        </div>
                        <div class="col-auto">
                            <input type="text" class="form-control mb-2" id="name[]" name="name[]" placeholder="kotadesa 2">
                        </div>
                        <div class="col-auto">
                            <input type="text" class="form-control mb-2" id="name[]" name="name[]" placeholder="kotadesa 3">
                        </div>
                        <div class="col-auto">
                            <input type="text" class="form-control mb-2" id="name[]" name="name[]" placeholder="kotadesa 4">
                        </div>
                        <div class="col-auto">
                            <input type="text" class="form-control mb-2" id="name[]" name="name[]" placeholder="kotadesa 5">
                        </div>

                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- end of inputtan -->
        <br><br><br>
        <h2>Data Kelurahan</h2>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID Kota</th>
                    <th scope="col">ID_Provinsi</th>
                    <th scope="col">Name</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
                <?php foreach ($desa as $d) : ?>
                    <tr>
                        <th scope="row"><?= $no++; ?></th>
                        <td><?= $d['id_kota']; ?></td>
                        <td><?= $d['id_provinsi']; ?></td>
                        <td><?= $d['kotadesa']; ?></td>
                        <td><a href="<?= base_url('admin/provinsi/desa/update/') . $d['id_kota']; ?>" class="btn btn-primary">Update</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>