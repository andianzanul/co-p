<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Tambah output </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah output</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                 <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body"> 

<?php echo form_open(); ?>


        <div class="form-group">
            <label for="varchar">Kegiatan <?php echo form_error('nama_bidang') ?></label>
            <?php echo form_dropdown('nama_bidang',$jenis_bidang_data,"",'class="form-control" name="nama_bidang" id="nama_bidang" placeholder="Satker"'); ?>
        </div>
       
        <div class="form-group">
            <label for="varchar">output<?php echo form_error('nama_output') ?></label>
            <input type="text" class="form-control" name="nama_output" id="nama_output"   value="<?php echo set_value('jenis_output'); ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Deskripsi <?php echo form_error('des_output') ?></label>
            <textarea class="form-control" name="des_output" id="des_output"><?php echo set_value('des_output'); ?></textarea>
            
        </div>
         
         
        <button type="submit" class="btn btn-primary">Simpan</button> 
         
    </form>





















            </div>
        </div>

    </section>
    <!-- /.content -->

         
        
   