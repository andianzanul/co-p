
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Master Wilayah Sungai</h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php 
    $message = $this->session->userdata('message');
    if($message){
      echo '<div class="alert alert-success">'. $message .'</div>';
    }

    ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">

         


            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body"> 
          <?php if($this->mainlib->cek_level(['superadmin'])) {  ?>
              <?php echo anchor(site_url('admin/jenis_wlsungai/create'),'Tambah', 'class="btn btn-primary btn-sm"'); }
             ?>
       <table id="example1" class="table table-bordered" style="margin-bottom: 10px">
            <thead><tr> 
           

    <th>Wilayah Sungai</th> 
    <th class="hiiden-sm-down hidden-xs hidden-xs-down">Deskripsi</th>
    <?php if($this->mainlib->cek_level(['superadmin'])) {  ?><th>Action</th><?php } ?>
            </tr>
  </thead>
            </tbody><?php
            foreach ($jenis as $data)
            {
                ?>
                <tr>
      <td><?php echo $data->nama_wlsungai ?></td>

       <td><?php echo $data->des_wlsungai ?></td>
       <?php if($this->mainlib->cek_level(['superadmin'])) {  ?>
      <td>


        <?php 
        
        echo '   '; 
        echo anchor(site_url('admin/jenis_wlsungai/edit/'.$data->id_wlsungai),'Edit', 'class="btn btn-warning btn-sm"'); 
        echo '  '; 
        echo anchor(site_url('admin/jenis_wlsungai/delete/'.$data->id_wlsungai),'Hapus','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
        ?>
      </td><?php } ?>
    </tr>
                <?php
            }
            ?></tbody>
        </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
        <script>
  $(function () {
    $("#example2").DataTable();

    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
