    
    <section class="content-header">
        <h1> Edit Data User </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
            <!-- <h1 class="box-title"> Master User </h1> -->

            <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
            </div> -->
        </div>
    
        <div class="box-body"> 
          <?php 
            echo form_open(current_url(),'',array('user_id'=> $jenis->user_id));
            //sampai disini dulu sore ini
          ?>
 
          <div class="form-group">
              <label for="varchar"> Username <?php echo form_error('username') ?></label>
              <input type="text" class="form-control" name="username" id="username" value="<?php echo $jenis->username; ?>" />
          </div>

          <div class="form-group">
              <label  > Level <?php echo form_error('level') ?></label>
              <?php echo combo_level_user($jenis->level);  ?>
             <?php /* <input type="text" class="form-control" name="level" id="level"   value="<?php echo $jenis->level; ?>" />*/ ?>
          </div>

          <div class="form-group">
              <label for="varchar"> Satuan Kerja <?php echo form_error('id_satker') ?></label>
              <?php 
                  echo form_dropdown('id_satker',$jenis_satker_data,$jenis->id_satker,'class="form-control" name="nama_satker" id="id_satker" placeholder="Satker"'); 
              ?>
          </div>

          <div class="form-group has-feedback">
        <strong>PPK</strong>
        <?php echo combo_ppk(set_value('ppk',$jenis->id_ppk)); ?> 
      </div>

          <div class="form-group">
              <label for="varchar"> Email <?php echo form_error('email') ?></label>
              <input type="text" class="form-control" name="email" id="email"   value="<?php echo $jenis->email; ?>" />
          </div>

          

          <div class="form-group">
              <label for="varchar"> Password <?php echo form_error('password') ?></label>
              <input type="password" class="form-control" name="password" id="password" />
              <span class="text-muted">kalau tidak diganti, kosongkan saja</span>
          </div>

          <!-- <div class="form-group">
              <label for="varchar">  Ulangi Password <?php //echo form_error('password2') ?></label>
              <input type="password" class="form-control" name="password2" id="password2" />
          </div>
 -->
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->

    <script>
  $(function() { 

    var cek_level = function(){
        var level = $('#level_user').val();

        //console.log(level)

        if( level == 'satker') {
          $('#ppk').prop( "disabled", true );
          $('#id_satker').prop( "disabled", false );
        } 

        else if( level == 'ppk') {
          $('#ppk').prop( "disabled", false );
          $('#id_satker').prop( "disabled", true );
        }
        
        else{
          $('#ppk').prop( "disabled", true );
          $('#id_satker').prop( "disabled", true );
        }
    }


    cek_level();


    $('#level_user').on('change', function(){
      cek_level(); 
    });

  });
</script>