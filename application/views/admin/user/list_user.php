    
    <section class="content-header">
        <h1> Master User </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php 
        $message = $this->session->userdata('message');
        if($message) {
          echo '<div class="alert alert-success">'. $message .'</div>';
        }
      ?>
    
    <!-- Default box -->
    <div class="box">
  
        <div class="box-header with-border">
          <?php if($this->mainlib->cek_level(['superadmin','direksi'])) {  ?>
          <?php 
            echo anchor(site_url('admin/user/add'),'Tambah', 'class="btn btn-primary btn-sm"'); }
          ?>
            <!-- <h1 class="box-title"> Master User </h1> -->

            <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
            </div> -->
        </div>
    
        <div class="box-body"> 
          <!-- <?php 
              //echo anchor(site_url('user/register'),'Tambah', 'class="btn btn-primary btn-sm"');
          ?> -->

          <table id="example1" class="table table-bordered" style="margin-bottom: 10px">
            <thead> 
              <tr>
                <th>Nama</th> 
                <th>Username</th> 
                <th>Satker</th>
                <th>Email</th>
                <th>Level</th>
                <th class="hiiden-sm-down hidden-xs hidden-xs-down">Aktif Sejak</th>
                <?php if($this->mainlib->cek_level(['superadmin'])) {  ?>
                <th>Action</th>
              <?php } ?>
              </tr>
            </thead>

            <tbody>
              <?php
                foreach ($user as $data)
                {
                  ?>
                    <tr>
                      <td><?php echo $data->nama; ?></td>
                      <td><?php echo $data->username; ?></td>
                      <td><?php echo $data->nama_satker ?></td>
                      <td><?php echo $data->email ?></td>
                      <td><?php echo $data->level ?></td>
                      <td><?php echo $data->active_since ?></td>
                      <?php if($this->mainlib->cek_level(['superadmin'])) {  ?>
                      <td>
                        <?php 
                          echo ''; 
                          echo anchor(site_url('admin/user/edit/'.$data->user_id),'Edit', 'class="btn btn-warning btn-sm"'); 
                          echo '  '; 
                          echo anchor(site_url('admin/user/delete/'.$data->user_id),'Hapus','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Anda Yakin ?\')"'); 
                        ?>
                      </td>
                    <?php } ?>
                    </tr>
                  <?php
                }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->

<script>
  $(function () {
      $("#example2").DataTable();

      $('#example1').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true, //saya setting menjadi 'false' agar tidak perlu ditampilkan untuk sementara
          "ordering": true,
          "info": true,
          "autoWidth": false
      });
  });
</script>