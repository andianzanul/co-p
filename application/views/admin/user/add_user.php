<!-- <section class="content-header">
    <h1>Tambah User Baru </h1>
</section> -->
 
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah User Baru</h3> 
        </div>
        <div class="box-body"> 
            <?php echo validation_errors(); ?>
    <form action="<?php echo base_url('admin/user/add'); ?>" method="POST">
        

      <div class="form-group has-feedback">
        <strong>Nama</strong>
        <input type="text" name="nama" value="<?php echo set_value('nama'); ?>" class="form-control" placeholder="nama">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <strong>Username</strong>
        <input type="text" name="username" value="<?php echo set_value('username'); ?>" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <strong>Level</strong>
        <?php echo combo_level_user(set_value('level'), 'name="level" id="level" class="form-control" required'); ?> 
      </div>


      

      <div class="form-group has-feedback">
        <strong>Satker</strong>
        <?php 
        echo form_dropdown('satker',$jenis_satker_data,set_value('satker'),'class="form-control" name="satker" id="satker" placeholder="Satker"'); 
              ?> 
      </div>


      <div class="form-group has-feedback">
        <strong>PPK</strong>
        <?php echo combo_ppk(set_value('ppk')); ?> 
      </div>

      <div class="form-group has-feedback">
        <strong>Alamat Email</strong>
        <input type="email" value="<?php echo set_value('email'); ?>" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      <div class="form-group has-feedback">
        <strong>Kata Sandi</strong>
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      

      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="submit" value="Login!" class="btn btn-primary btn-block btn-flat">Registrer</button>
        </div>
      </div>
    </form> 
</section>

<script>
  $(function() { 

    var cek_level = function(){
        var level = $('#level').val();

        //console.log(level)

        if( level == 'satker') {
          $('#ppk').prop( "disabled", true );
          $('#satker').prop( "disabled", false );
        } 

        else if( level == 'ppk') {
          $('#ppk').prop( "disabled", false );
          $('#satker').prop( "disabled", true );
        }
        
        else{
          $('#ppk').prop( "disabled", true );
          $('#satker').prop( "disabled", true );
        }
    }


    cek_level();


    $('#level').on('change', function(){
      cek_level(); 
    });

  });
</script>