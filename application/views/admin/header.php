<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Dashboard | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/styleadmin.css'); ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/sweetalert2/dist/sweetalert2.min.css'); ?>">
  <script src="<?php echo base_url('assets/sweetalert2/dist/sweetalert2.min.js'); ?>"></script>

  <!-- Font Awesome -->
 
  <!-- Font Awesome 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
   Ionicons 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  -->
  <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.gif" type="image/gif">

  <script src="<?php echo base_url();?>assets/bootstrap/js/jquery-2.2.3.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/ajaxupload.js"></script>
  <!-- 
  <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script> -->


  <script src="<?php echo base_url("assets/plugins/jquery.maskMoney/jquery.maskMoney.min.js"); ?>"></script>

  <script src="<?php echo base_url("assets/plugins/input-mask/jquery.inputmask.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/input-mask/jquery.inputmask.extensions.js"); ?>"></script>
  <script src="<?php echo base_url("assets/plugins/input-mask/jquery.inputmask.numeric.extensions.js"); ?>"></script>

  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">
  <script src="<?php echo base_url("assets/plugins/select2/select2.full.min.js"); ?>"></script>


  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

  <script src="<?php echo base_url('assets/jquery-mask/jquery.mask.js'); ?>"></script>
  
  <?php 
    $settings = array(
      'base_url' => base_url(),
      'site_url' => site_url()
    );
  ?>

  <script type="text/javascript">
    var appSettings = <?php echo json_encode($settings); ?>
  </script>

</head>
<body class="sidebar-mini skin-black-light">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo site_url("admin"); ?>" class="logo">
     <!--  <img class="logo-mini" src=<?php //echo base_url('assets/img/logo.jpg'); ?> width="50" height="50">  -->
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b> CO-P </b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li><!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <img src="<?php echo base_url(); ?>assets/img/user.png" class="img-rounded" alt="User Image">
                        </div>
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <!-- end message -->
                  </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
              </ul>
            </li>
            <!-- Notifications: style can be found in dropdown.less -->
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>
            <!-- Tasks: style can be found in dropdown.less -->
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li><!-- Task item -->
                      <a href="#">
                        <h3>
                          Design some buttons
                          <small class="pull-right">20%</small>
                        </h3>
                        <div class="progress xs">
                          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">20% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                  </ul>
                </li>
                <li class="footer">
                  <a href="#">View all tasks</a>
                </li>
              </ul>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>assets/img/user.png" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata("username");  ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>assets/img/user.png" class="img-rounded" alt="User Image">

                  <p>
                    <?php echo $this->session->userdata("username"); ?>
                    <small>Aktif sejak <?php echo $this->session->userdata("active_since"); ?></small>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?=base_url('admin/logout');?>" class="btn btn-default btn-flat">log out</a>
                  </div>
                </li>
        </div>
      </nav>
    </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url("assets/img/user.png"); ?>" class="img-rounded" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata("username");?> </p>
            <i class="fa fa-circle text-success"></i> <small style="color: #999;"><?php echo $this->session->userdata("level");?></small>
        </div>
      </div>
      <!-- search form -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
       <?php  if($this->mainlib->cek_level(['superadmin',])) { 
           ?> 
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
       
            <li>
              <a href="<?php echo site_url("admin/kegiatan"); ?>">
                <i class="fa fa-file-text"></i> 
                <span>Kegiatan</span>
              </a>
            </li>
            <li class="header">Master Data</li>
            <li>
              <a href="<?php echo site_url("admin/jenis_bidang"); ?>">
                <i class="fa  fa-book"></i> 
                <span>Kegiatan (Bidang)</span>
              </a>
            </li>
            <li>
              <a href="<?php echo site_url("admin/jenis_output"); ?>">
                <i class="fa  fa-book"></i> 
                <span>Output (Sub bidang)</span>
              </a>
            </li>
            <li>
              <a href="<?php echo site_url("admin/jenis_kegiatan"); ?>">
                <i class="fa fa-files-o"></i> 
                <span>Jenis Kegiatan</span>
                <li class="header"></li>             
              </a>
            </li>
            <li>
              <a href="<?php echo site_url("admin/jenis_satker"); ?>">
                <i class="fa fa-users"></i> 
                <span>Satuan Kerja</span>
              </a>
            </li>
            <!-- </li>
            </li> -->
            <li>
              <a href="<?php echo site_url("admin/jenis_ppk"); ?>">
                <i class="fa  fa-user-md"></i> 
                <span>PPK</span>
              </a>
            </li>
            <li>
              <a href="<?php echo site_url("admin/jenis_wlsungai"); ?>">
                <i class="fa fa-tint"></i> 
                <span>Wilayah Sungai</span>
                <li class="header"></li> 
              </a>
            </li><li>
              <a href="<?php echo site_url("admin/kabupaten"); ?>">
                <i class="fa fa-tint"></i> 
                <span>Kabupaten</span>
                <li class="header"></li> 
              </a>
            </li>
            <li>
              <a href="<?php echo site_url("admin/jenis_satuan"); ?>">
                <i class="fa fa-files-o"></i> 
                <span>Satuan Output/Outcome</span>
                <?php if($this->mainlib->cek_level(['balai'])) { ?>
                <li class="header"></li>
                <?php } ?>
              </a>
            </li>  
            <!-- <li class="header"> </li> -->
            
    <!-- disable dulu -->
            <!-- <li>
              <a href="<?php echo site_url("admin/kabupaten"); ?>">
                <i class="fa  fa-user-md"></i> 
                <span>Kabupaten</span>
              </a>
            </li> -->
    <!-- akhir disable dulu -->
            <?php // if($this->mainlib->cek_level(['ppk'])) { ?>
            <li class="header">Setting</li>
            <li>
              <a href="<?php echo site_url("admin/user"); ?>">
                <i class="fa fa-user text-blue"></i> 
                <span>User</span>
              </a>
            </li>
            
            <li>
              <a href="<?php echo site_url("admin/user/add"); ?>">
                <i class="fa fa-user-plus text-blue"></i> 
                <span>Tambah User</span>
              </a>
            </li>
            <?php // } ?>

            <li>
              <a href="<?php echo site_url("admin/logout"); ?>"><i class="fa fa-power-off text-red"></i> 
                <span>Logout</span>
              </a>
            </li>



          <?php } else { ?>
            
          <?php } ?>
          
          <?php  if($this->mainlib->cek_level(['direksi',])) { 
           ?> 
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
       
            <li>
              <a href="<?php echo site_url("admin/kegiatan"); ?>">
                <i class="fa fa-file-text"></i> 
                <span>Kegiatan</span>
              </a>
            </li>
       
            <li>
              <a href="<?php echo site_url("admin/userumum/addumum"); ?>">
                <i class="fa fa-user-plus text-blue"></i> 
                <span>Tambah User</span>
              </a>
            </li>
            <?php // } ?>

            <li>
              <a href="<?php echo site_url("admin/logout"); ?>"><i class="fa fa-power-off text-red"></i> 
                <span>Logout</span>
              </a>
            </li>



          <?php } else { ?>
            
          <?php } ?>

          <?php  if($this->mainlib->cek_level(['konsultan',])) { 
           ?> 
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
       
            <li>
              <a href="<?php echo site_url("admin/kegiatan"); ?>">
                <i class="fa fa-file-text"></i> 
                <span>Kegiatan</span>
              </a>
            </li>
       
          

            <li>
              <a href="<?php echo site_url("admin/logout"); ?>"><i class="fa fa-power-off text-red"></i> 
                <span>Logout</span>
              </a>
            </li>



          <?php } else { ?>
            
          <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

