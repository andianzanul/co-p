<!doctype html>
<html>
    <head>
        <title>lihat</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    </head>
    <body>
        <h2 style="margin-top:0px">Tbl_kegiatan List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('kegiatan/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('tbl_kegiatan/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('tbl_kegiatan'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Satker</th>
		<th>Nm Kegiatan</th>
		<th>Jenis Kegiatan</th>
		<th>Thn Anggaran</th>
		<th>Ppk</th>
		<th>Bidang</th>
		<th>Sub Bidang</th>
		<th>Jenis Konstruksi</th>
		<th>Provinsi</th>
		<th>Kabupaten</th>
		<th>Kecamatan</th>
		<th>Desa</th>
		<th>Wl Sungai</th>
		<th>Das</th>
		<th>Tujuan</th>
		<th>Manfaat</th>
		<th>Output</th>
		<th>Outcome</th>
		<th>Data Teknis</th>
		<th>Data Lanjutan</th>
		<th>Th Kegiatanlanjutan</th>
		<th>Sumber Dana</th>
		<th>Status Kegiatan</th>
		<th>Dasar Kegiatan</th>
		<th>Nmp Jasa</th>
		<th>Perencana</th>
		<th>Supervisi</th>
		<th>Tgl Kontrak Awal</th>
		<th>No Kontrak Awal</th>
		<th>Nilai Pagu</th>
		<th>Tgl Spmk</th>
		<th>No Spmk</th>
		<th>Nilai Penyerapan</th>
		<th>Keuangan Persen</th>
		<th>Jkwkt Pelaksanaan</th>
		<th>Jml Hari</th>
		<th>Koordinat X</th>
		<th>Koordinat Y</th>
		<th>Youtube Url</th>
		<th>Action</th>
            </tr><?php
            foreach ($tbl_kegiatan_data as $tbl_kegiatan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $tbl_kegiatan->id_satker ?></td>
			<td><?php echo $tbl_kegiatan->nm_kegiatan ?></td>
			<td><?php echo $tbl_kegiatan->jenis_kegiatan ?></td>
			<td><?php echo $tbl_kegiatan->thn_anggaran ?></td>
			<td><?php echo $tbl_kegiatan->ppk ?></td>
			<td><?php echo $tbl_kegiatan->bidang ?></td>
			<td><?php echo $tbl_kegiatan->sub_bidang ?></td>
			<td><?php echo $tbl_kegiatan->jenis_konstruksi ?></td>
			<td><?php echo $tbl_kegiatan->provinsi ?></td>
			<td><?php echo $tbl_kegiatan->kabupaten ?></td>
			<td><?php echo $tbl_kegiatan->kecamatan ?></td>
			<td><?php echo $tbl_kegiatan->desa ?></td>
			<td><?php echo $tbl_kegiatan->wl_sungai ?></td>
			<td><?php echo $tbl_kegiatan->das ?></td>
			<td><?php echo $tbl_kegiatan->tujuan ?></td>
			<td><?php echo $tbl_kegiatan->manfaat ?></td>
			<td><?php echo $tbl_kegiatan->output ?></td>
			<td><?php echo $tbl_kegiatan->outcome ?></td>
			<td><?php echo $tbl_kegiatan->data_teknis ?></td>
			<td><?php echo $tbl_kegiatan->data_kegiatan ?></td>
			<td><?php echo $tbl_kegiatan->th_kegiatanlanjutan ?></td>
			<td><?php echo $tbl_kegiatan->sumber_dana ?></td>
			<td><?php echo $tbl_kegiatan->status_kegiatan ?></td>
			<td><?php echo $tbl_kegiatan->dasar_kegiatan ?></td>
			<td><?php echo $tbl_kegiatan->nmp_jasa ?></td>
			<td><?php echo $tbl_kegiatan->perencana ?></td>
			<td><?php echo $tbl_kegiatan->supervisi ?></td>
			<td><?php echo $tbl_kegiatan->tgl_kontrak_awal ?></td>
			<td><?php echo $tbl_kegiatan->no_kontrak_awal ?></td>
			<td><?php echo $tbl_kegiatan->nilai_kontrak ?></td>
			<td><?php echo $tbl_kegiatan->tgl_spmk ?></td>
			<td><?php echo $tbl_kegiatan->no_spmk ?></td>
			<td><?php echo $tbl_kegiatan->nilai_penyerapan ?></td>
			<td><?php echo $tbl_kegiatan->keuangan_persen ?></td>
			<td><?php echo $tbl_kegiatan->jkwkt_pelaksanaan ?></td>
			<td><?php echo $tbl_kegiatan->jml_hari ?></td>
			<td><?php echo $tbl_kegiatan->koordinat_x ?></td>
			<td><?php echo $tbl_kegiatan->koordinat_y ?></td>
			<td><?php echo $tbl_kegiatan->youtube_url ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('tbl_kegiatan/read/'.$tbl_kegiatan->id_kegiatan),'Read'); 
				echo ' | '; 
				echo anchor(site_url('tbl_kegiatan/update/'.$tbl_kegiatan->id_kegiatan),'Update'); 
				echo ' | '; 
				echo anchor(site_url('tbl_kegiatan/delete/'.$tbl_kegiatan->id_kegiatan),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('tbl_kegiatan/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('tbl_kegiatan/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </body>
</html>