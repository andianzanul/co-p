
<div class="row"> 
    <div class="col-md-12">
        <label for="varchar">Laporan</label>
        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai pelaporan yang ada" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>  

        <?php if($this->mainlib->cek_level(['superadmin','direksi'])) { ?>
        <button class="btn btn-primary btn-xs" id="btn-tambah-laporan" type="button">
            <i class="fa fa-plus"></i> Tambah Laporan
        </button>   
        <?php } ?>

        <table class="table table-striped table-bordered" style="border: 1px solid #ddd;">
            <thead>
                <tr>
                    <th colspan="1">Judul Laporan</th>
                    <!-- <th width="300">Deskripsi</th> -->
                    <th width="90">Tanggal</th> 
                    <th width="100">Status</th> 
                    <th width="100">Tindak Lanjut</th> 
                </tr>
            </thead>
          <tbody> 
            <?php foreach ($list_laporan as $dk) { ?>
                <tr>
                     
                    <td><?php echo $dk->judul; ?></td> 
                    <td><?php echo $dk->tanggal; ?></td>  
                    <td>
                        <?php echo label_status_asistensi($dk->status); ?>
                    </td>
                    <td>
                       <a href="<?php echo site_url("admin/kegiatan/laporan/" . $dk->id_laporan ); ?>" class="btn btn-xs btn-primary">lihat</a> 
                        <?php if($this->mainlib->cek_level(['superadmin','direksi'])) { ?>
                            <a href="#" data-id="<?php echo $dk->id_laporan; ?>" class="btn-hapus-laporan btn btn-xs btn-danger">hapus</a> 
                        <?php } ?>
                       
                    </td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
        
    </div>
</div>


<script>

$(function() {

	/* tambah baru Laporan  */
    $('body').on('click','#btn-tambah-laporan',function(e){
        $('#modalAddLaporan').modal('show')
        $('#judul_asistensi').focus()
    });

     /* simpan data dari modal Add asistensi */
    $('body').on('click','#btn-add-laporan', function(){
        var judul_asistensi = $("#judul_asistensi").val(); 
        $.ajax({
            type: 'post',
            url:'<?php echo site_url('admin/kegiatan/add_laporan'); ?>',  
            data:{
                id : <?php echo $kegiatan->id_kegiatan; ?>,
                judul : judul_asistensi
            },
            dataType: 'json',
            beforeSend: function() { 
                $("#judul_asistensi").prop( "disabled", true ); 
                $("#btn-tambah-laporan").prop( "disabled", true );
            },
            success: function(response) { 

            	if(response.status == "ok"){

            		swal({
						  title: "Berhasil",
						  text: "Laporan ditambahkan!",
						  type: "success", 
							showConfirmButton:true
						} ).then(function () {
							$('#modalAddLaporan').modal('hide')
							location.reload();
						});

            	} else{
            		$("#addLaporanError").html(response.error);
            	}


                $("#judul_asistensi").prop( "disabled", false ); 
                $("#btn-tambah-asistensi").prop( "disabled", false ); 

                
            }
        });  
    }); 

});
	
</script>


<div class="modal" id="modalAddLaporan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel33">Tambah Laporan</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="judul_asistensi">Judul Laporan</label>
            <textarea class="form-control" id="judul_asistensi"></textarea>
        </div>
        

        <div id="addLaporanError"></div>
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="btn-add-laporan">Selanjutnya</button> 
      </div>
    </div>
  </div>
</div>