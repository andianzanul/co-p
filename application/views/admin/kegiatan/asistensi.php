<style>
	.skin-black-light .wrapper,
	body{
		background: none!important
	}
	.main-footer,
	.main-sidebar,
	.main-header{
		display: none!important
	}
	body .content-wrapper{
		margin-left:0px !important
	}
	.skin-black-light .content-wrapper, .skin-black-light .main-footer {
    	border-left: 0px solid #d2d6de!important;
	}
</style>


<!-- Default box -->
<div class="box box-warning direct-chat direct-chat-warning"> 

    <div class="box-header with-border">
		<h3 class="box-title">Diskusi : <?php echo $asistensi->asistensi; ?> 

		<div class="box-tools pull-right">

			 
		</div>
	</div>
	<!-- /.box-header -->

    <div class="box-body">


        <!-- Conversations are loaded here -->
		<div class="direct-chat-messages" style="height: 400px;">
		<?php 

		foreach ($lbr as $key => $value) {
		    $class = "direct-chat-msg"; // right

		    if($this->session->userdata("id_user") == $value->id_user_dari){
		        $class .= " right";
		    }

		    ?>
		    <!-- Message. Default to the left -->
		    <div class="<?php echo $class; ?>">
		      <div class="direct-chat-info clearfix">
		        <span class="direct-chat-name pull-left"><?php echo $value->user_nama; ?></span>
		        <span class="direct-chat-timestamp pull-right"><?php echo $value->tanggal; ?></span>
		      </div>
		      <!-- /.direct-chat-info -->
		      <img class="direct-chat-img" src="<?php echo base_url("assets/img/user.png"); ?>" alt="Foto">
		      <!-- /.direct-chat-img -->
		      <div class="direct-chat-text">
		        <?php echo $value->isi; ?>
		        <?php if(!empty($value->file)){
			    	echo '<br><a href="'. base_url("uploads/lembar_asistensi/" . $value->file) .'">'.$value->file.'</a>';
			    } ?>
		      </div>
		      <!-- /.direct-chat-text -->
		    </div>
		    <!-- /.direct-chat-msg -->
		<?php } ?> 

		</div>
        <!--/.direct-chat-messages-->
                
         
    </div> 
    
    <div class="box-footer">
        <?php if('open' == $asistensi->status)  { ?>
        <form action="#" method="post" id="form-kirim-asistensi-lbr">
        	<input style="display: none" type="hidden" name="id" value="<?php echo $asistensi->id_asistensi; ?>">
        	<input style="display: none" type="hidden" name="file" value="" id="file-lbr-asistensi">
            <div class="input-group" style="margin-bottom: 5px">
              <input type="text" required name="message" placeholder="Type Message ..." class="form-control" id="message"> 
              <span class="input-group-btn">
                <button type="submit" class="btn btn-warning btn-flat">Kirim</button>
              </span>
            </div>
			
			<a href="#" id="btn-add-file-lbr-asistensi">Tambah File</a>
			<p id="loadingUpload" style="display: none">
				<img width="16" src="<?php echo base_url("assets/img/loading/facebook.gif"); ?>" alt="Loading...">
				<span>Mengunggah file...</span>
			</p>
			
			<p style="display: none" id="boxFileTemp">
				<small>
					<span class="text-muted" style="text-decoration: underline;">nama file</span>
				</small>&nbsp;
				<button type="button" class="btn btn-xs btn-default" id="btnDeleteFileTemp">hapus</button>
			</p>
			
			

          </form>
         <?php } else {  ?>
            <span class="text-danger">Asistensi telah ditutup!</span>
         <?php } ?>
    </div> 
     
</div>
<!-- end Default box -->


<?php if($this->mainlib->cek_level(['direksi','superadmin'])) { ?>
	
	<p class="edit-status">
		 <strong>Status Asistensi :</strong> <?php echo label_status_asistensi($asistensi->status); ?>
		<a href="#" id="edit-status-asistensi"><small class="text-muted">edit</small></a>	
	</p>
	
	<form id="form-edit-status" style="display: none" action="<?php echo site_url("admin/kegiatan/set_status_asistensi"); ?>" method="post">
     	<strong>Status Asistensi :</strong>
     	<input type="hidden" name="id" value="<?php echo $asistensi->id_asistensi; ?>"> 
        <?php echo form_dropdown("status",['open' => "OPEN", 'close' => "Selesai"], $asistensi->status); ?>
        <button>Simpan</button>
    </form>
<?php } ?>

<script>
	
	$(document).ready( function(){

		<?php if('open' == $asistensi->status)  { ?>
	    var data_P          = new Array(); 
	    var btnUpload       = $('#btn-add-file-lbr-asistensi');
	    var loading         = $('#loadingUpload'); 
	    var uploadDoc       = new AjaxUpload(btnUpload, {
	        action: '<?php echo site_url('admin/kegiatan/upload_img_temp_lembar_asistensi'); ?>',  
	        name: 'userfile',
	        autoSubmit: true,
	        multiple:false,
	        data: data_P,
	        responseType: 'json',
	        onSubmit: function(file, ext) {
	            if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
	                alert('Berkas  ' + ext +  ' tidak di izinkan'); 
	                return false;
	            }else{
	                 loading.show(); 
	                 btnUpload.hide(); 
	            }
	        },
	        onChange:function(file, ext) {
	           if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
	                alert('Berkas  ' + ext +  ' tidak di izinkan'); 
	                return false;
	            }else{
	                
	            }


	        },
	        onComplete: function(file, dataResult) {
	            //var dataResult = eval('(' + response + ')');
	            console.log(dataResult);
	            if (dataResult.status == '1') { 

	                 $("#file-lbr-asistensi").val(dataResult.data.file_name); 
	  				$("#boxFileTemp").find("span").html(dataResult.data.file_name);
	            }
	            else { 
	                alert(dataResult.message);  
	            }  

	            loading.hide(); 
	            $("#boxFileTemp").show();
	        }
	    });
	    <?php } ?>
	     

	    $("#edit-status-asistensi").on("click",function(e){
	    	$("p.edit-status").hide(); 
	    	$("#form-edit-status").show();
	    });

	    $("#btnDeleteFileTemp").on("click",function(e){

	    	var file = $("#file-lbr-asistensi").val();
	    	$.ajax({
				type: 'post',
				data:{
					file:file 
				},
				url: '<?php echo site_url("admin/kegiatan/delete_img_temp_lembar_asistensi"); ?>',
				dataType: 'json', 
				success: function(response) {
 
				}
			});


	    	$("#boxFileTemp").hide();
	    	btnUpload.show(); 
	    	$("#file-lbr-asistensi").val("");
	    });

		$("#form-kirim-asistensi-lbr").on("submit",function(e){
			e.preventDefault();
			e.stopPropagation();

			var dataSent 	= $("#form-kirim-asistensi-lbr").find(':input[name]').serialize(); 

			$.ajax({
				type: 'post',
				data: dataSent,
				url: '<?php echo site_url("admin/kegiatan/add_asistensi_lbr"); ?>',
				dataType: 'json',
				beforeSend: function() { 
					$('#message').prop( "disabled", true );
					$('.btn-flat').prop( "disabled", true );
				},
				success: function(response) {

					 
					$("#file-lbr-asistensi").val("");

					$(".direct-chat-messages").append(response.html); 
					$('#message').prop( "disabled", false );
					$('#message').val( "");
					$('.btn-flat').prop( "disabled", false );

					$(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);

					$("#btnDeleteFileTemp").click();
				}
			});
		});
	});
</script>

    
 