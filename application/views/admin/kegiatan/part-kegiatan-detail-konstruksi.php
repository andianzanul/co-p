<div class="detailkonstruksi well">
    <strong>Detail Konstruksi</strong>
    <button type="button" class="btn btn-primary" id="btn-add-detailkonstruksi">+</button> 

    <div id="detailkonstruksi-box"><hr>
        <?php foreach ($kegiatan_detail_konstruksi as $key => $dk) { ?> 
        <div class="row row-detailkonstruksi">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="varchar">Nama Bangunan</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi dengan nama bangunan konstruksi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input name="detail_konstruksi[<?php echo $dk->id_detail_konstruksi; ?>][nama_bangunan]" type="text" class="form-control" placeholder="Nama Bangunan" value="<?php echo $dk->nama_bangunan; ?>" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="varchar">Volume</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka)" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input name="detail_konstruksi[<?php echo $dk->id_detail_konstruksi; ?>][volume]" type="text" class="form-control" placeholder="Volume" value="<?php echo $dk->volume; ?>" />
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label for="varchar">Satuan</label>
                    <span class="fa fa-question-circle help-popup" data-content="Satuan Volume" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <?php echo form_dropdown('detail_konstruksi['.$dk->id_detail_konstruksi.'][satuan]',$jenis_satuan_data, $dk->satuan,'class="form-control"'); ?>
                </div>
            </div>
            

            <div class="col-md-2">
                <div class="form-group">
                    <label> Tahun</label>
                    <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tahun anggaran pelaksanaan kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> 
                    <?php
                        $class = array('class' => "form-control combo-tahun-konstruksi");

                        $tahun = array('' => '-- Pilih -- ');

                        if($kegiatan->multi_years == 'N'){
                            $tahun = array();
                            $tahun[$kegiatan->tahun] = $kegiatan->tahun;

                        }else{ 
                            for ($t = 2031; $t >= 2001; $t--) {
                                $tahun[$t] = label_tahun_current($t);
                            }  
                        }

                        
                        
                        echo form_dropdown('detail_konstruksi['.$dk->id_detail_konstruksi.'][tahun]', $tahun, $dk->tahun, $class);
                    ?> 
                </div>
            </div> 

            <div class="col-md-2">
                <label>&nbsp;</label> <br>
                <button data-id="<?php echo $dk->id_detail_konstruksi; ?>" type="button" class="btn btn-danger btn-delete-konstruksi">-</button>
            </div>





        </div>
        <?php } ?>
    </div>
</div>



<script type="text/javascript">
    $(function() { 


        var cek_tahun_konstruksi = function(){
            var multi_years = $('#multi_years').is(':checked');  
  
            var tahun1 = $('#combo-tahun-anggaran').val();
            var tahun2 = $('#combo-tahun-anggaran2').val();
            
            if(multi_years == false){

                console.log("tidak multi");

                var combo = $('.combo-tahun-konstruksi');
                $.each(combo, function (index, itemCombo) {
                    var item = $(itemCombo);
                    var select_temp = item.val();
                    item.empty();
                    item.append(  $('<option>', { value: "",  text: " -- Pilih -- "  }, '</option>'))
                    item.append( $('<option>', {  value: tahun1,  text: tahun1 }, '</option>'));
                    
                });

            }else{

                var combo = $('.combo-tahun-konstruksi');

                if(tahun2 != ""){ 

                    $.each(combo, function (index, itemCombo) {
                        var item = $(itemCombo);
                        var select_temp = item.val();
                        item.empty();
                        item.append(  $('<option>', { value: "",  text: " -- Pilih -- "  }, '</option>'))
                        var i;
                        var pilih_option = false;
                        for (i = parseInt(tahun2); i  >= parseInt(tahun1); i--) { 
                            label = i;
                             
                            item.append( $('<option>', {  value: i,  text: label }, '</option>'));
                            if(i == parseInt(select_temp)){
                                pilih_option = true;
                            }
                        }

                        if(pilih_option){
                            item.val(select_temp)
                        }
                        
                    });

                }else {

                    $.each(combo, function (index, itemCombo) {
                        var item = $(itemCombo);
                        var select_temp = item.val();
                        item.empty();
                        item.append(  $('<option>', { value: "",  text: " -- Pilih -- "  }, '</option>')) 
                        
                    });

                }
               
            } 
            
        }  

        /* add detail konstruksi */ 
        $('#multi_years').on('change', function(){
            cek_tahun_konstruksi();
        });

        $('#combo-tahun-anggaran2').on('change', function(){
            cek_tahun_konstruksi();
        });       

        /* add detail konstruksi */ 
        $('body').on('click','#btn-add-detailkonstruksi', function(){
            $('#detailkonstruksi-box').append($("#new-item-kegiatan-detailkonstruksi").html());  
            cek_tahun_konstruksi();
        });

        /* hapus detail konstruksi temp  */
        $('body').on('click','.btn-delete-konstruksi-temp', function(){
            $(this).parents('.row-detailkonstruksi').remove(); 
        });

         /* Hapus detail konstruksi di server */
        $('body').on('click','.btn-delete-konstruksi', function(e){
            e.preventDefault();
            e.stopPropagation();
            var tombol = $(this);
            swal({
                title: 'Anda yakin menghapus?',
                text: "data yang telah dihapus tidak bisa di kembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal' 
            }).then(function () {
                $.ajax({
                    type: 'get', 
                    url: appSettings.base_url + 'admin/kegiatan/delete_detail_konstruksi/' + tombol.data('id'),
                    dataType: 'json',
                    beforeSend: function() {},
                    success: function(response) {
                        if(response.status == 1){
                            tombol.parents('.row-detailkonstruksi').slideUp();
                        }
                        swal({
                            title: response.title,
                            text: response.message,
                            type: response.type,
                            timer: 1000,
                            showConfirmButton:false
                        })
                    }
                }); 
            }) 
        });
         
    })
</script>




<script type="text/html" id="new-item-kegiatan-detailkonstruksi">
    <div class="row row-detailkonstruksi">
        <div class="col-md-5">
            <div class="form-group">
                <label for="varchar">Nama Bangunan</label>
                <span class="fa fa-question-circle help-popup" data-content="Diisi dengan nama bangunan konstruksi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input name="new_detail_konstruksi_nama_bangunan[]" type="text" class="form-control" placeholder="Nama Bangunan" value="" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="varchar">Volume</label>
                <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka)" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input name="new_detail_konstruksi_volume[]" type="text" class="form-control" placeholder="Volume" value="" />
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="varchar">Satuan</label>
                <span class="fa fa-question-circle help-popup" data-content="Satuan Volume" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php echo form_dropdown('new_detail_konstruksi_satuan[]',$jenis_satuan_data, '','class="form-control"'); ?>
            </div>
        </div>

        <div class="col-md-2">
                <div class="form-group">
                    <label> Tahun</label>
                    <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tahun anggaran pelaksanaan kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> 
                    <?php
                        $class = array('class' => "form-control combo-tahun-konstruksi");

                        $tahun = array('' => '-- Pilih -- '); 

                        if($kegiatan->multi_years == 'N'){
                            $tahun = array();
                            $tahun[$kegiatan->tahun] = $kegiatan->tahun;

                        }else{ 
                            for ($t = 2031; $t >= 2001; $t--) {
                                $tahun[$t] = label_tahun_current($t);
                            }  
                        }

                        
                        
                        echo form_dropdown('new_detail_konstruksi_tahun[]', $tahun, '', $class);
                    ?> 
                </div>
            </div> 

        <div class="col-md-2">
            <label>&nbsp;</label> <br>
            <button type="button" class="btn btn-warning btn-delete-konstruksi-temp">-</button>
        </div>
    </div>
</script>       