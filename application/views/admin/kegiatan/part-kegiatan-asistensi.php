<div class="box box-info box-warning">
	<div class="box-header with-border">
		<h3 class="box-title">Asistensi</h3>

		<div class="box-tools pull-right">
			<?php if("open" == $laporan->status) { ?>
			<?php if($this->mainlib->cek_level(['superadmin','direksi'])) { ?>
	        <button class="btn btn-primary btn-xs" id="btn-tambah-asistensi" type="button">
	            <i class="fa fa-plus"></i> Tambah Asistensi
	        </button>   
	        <?php } ?> 
	        <?php } ?> 
		</div>
	</div>
	<!-- /.box-header -->

	<div class="table-responsive">
		<table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th colspan="1">Judul Asistensi</th> 
                    <th width="160">Tanggal</th> 
                    <th width="100">Status</th> 
                    <th width="180">Tindak Lanjut</th> 
                </tr>
            </thead>
          <tbody> 
            <?php foreach ($list_asistensi as $dk) { ?>
                <tr>
                     
                    <td><?php echo $dk->asistensi; ?></td> 
                    <td><?php echo $dk->tanggal; ?></td>  
                    <td>
                        <?php echo label_status_asistensi($dk->status); ?>
                    </td>
                    <td>
                       <a href="<?php echo site_url("admin/kegiatan/asistensi/" . $dk->id_asistensi); ?>" target="pageAsistensi" class="btn btn-xs btn-primary">lembaran asistensi</a> 
                        <?php if($this->mainlib->cek_level(['superadmin','direksi'])) { ?>
                            <a href="#" data-id="<?php echo $dk->id_asistensi; ?>" class="btn-hapus-asistensi btn btn-xs btn-danger">hapus</a> 
                        <?php } ?>
                       
                    </td>
                </tr>
            <?php } ?>
          </tbody>
        </table> 
	</div>

	<div class="box-footer clearfix">
		
	</div> 
	<!-- /.box-footer -->
</div>

<!-- <label for="varchar">Laporan</label>
<span class="fa fa-question-circle help-popup" data-content="Diisi sesuai pelaporan yang ada" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>  

 -->
<script>

$(function() {

	/* tambah baru asistensi  */
    $('body').on('click','#btn-tambah-asistensi',function(e){
        $('#modalAddAsistensi').modal('show');
        $('#judul_asistensi').val("");
        $('#judul_asistensi').focus();
    });

     /* simpan data dari modal Add asistensi */
    $('body').on('click','#btn-add-asistensi', function(){
        var judul_asistensi = $("#judul_asistensi").val(); 
        $.ajax({
            type: 'post',
            url:'<?php echo site_url('admin/kegiatan/add_asistensi'); ?>',  
            data:{
                id_laporan : <?php echo $laporan->id_laporan; ?>,
                judul : judul_asistensi
            },
            dataType: 'json',
            beforeSend: function() { 
                $("#judul_asistensi").prop( "disabled", true ); 
                $("#btn-tambah-asistensi").prop( "disabled", true );
            },
            success: function(response) { 

            	if(response.status == "ok"){

            		swal({
						  title: "Berhasil",
						  text: "Asistensi ditambahkan!",
						  type: "success", 
							showConfirmButton:true
						} ).then(function () {
							$('#judul_asistensi').val("");
							$('#modalAddAsistensi').modal('hide')
							location.reload();
						});

            	} else{
            		$("#addLaporanError").html(response.error);
            	}


                $("#judul_asistensi").prop( "disabled", false ); 
                $("#btn-tambah-asistensi").prop( "disabled", false ); 

                
            }
        });  
    }); 

});
	
</script>


<div class="modal" id="modalAddAsistensi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel33">Tambah Asistensi</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="judul_asistensi">Judul Asistensi</label>
            <input class="form-control" id="judul_asistensi" />
        </div>
        

        <div id="addLaporanError"></div>
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="btn-add-asistensi">Selanjutnya</button> 
      </div>
    </div>
  </div>
</div>