<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kegiatan </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                 <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body"> 
            
<?php echo form_open(); ?>
       <!--  <div class="form-group">
            <label for="int">Id Satker <?php echo form_error('id_satker') ?></label>
            <input type="text" class="form-control" name="id_satker" id="id_satker" placeholder="Id Satker" value="<?php echo $id_satker; ?>" />
        </div> -->
        <div class="form-group">
            <label for="varchar">Nm Kegiatan <?php echo form_error('nm_kegiatan') ?></label>
            <input type="text" class="form-control" name="nm_kegiatan" id="nm_kegiatan" placeholder="Nm Kegiatan" value="<?php echo $nm_kegiatan; ?>" />
        </div>

        
        <div class="form-group">
            <label for="varchar">Satuan Kerja <?php echo form_error('nama_satker') ?></label>
            <?php echo form_dropdown('nama_satker',$jenis_satker_data,'','class="form-control" name="nama_satker" id="nama_satker" placeholder="Satker"'); ?>
            
        </div>
        <div class="form-group">
            <label for="varchar">Jenis Kegiatan <?php echo form_error('jenis_kegiatan') ?></label>
            <?php echo form_dropdown('jenis_kegiatan',$jenis_kegiatan_data,'','class="form-control" name="jenis_kegiatan" id="jenis_kegiatan" placeholder="Jenis Kegiatan"'); ?>
 </div>
        <input type="hidden" name="id_kegiatan" value="<?php echo $id_kegiatan; ?>" /> 
        <button type="submit" class="btn btn-primary">Selanjutnya</button> 
        <a href="<?php echo site_url('tbl_kegiatan') ?>" class="btn btn-default">Cancel</a>
    </form>

            </div>
        </div>

    </section>
    <!-- /.content -->