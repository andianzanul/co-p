<script type="text/javascript">
    $(function() {  

                
        var batasi_tahun = function(){ 
            var tahun_sekarang = <?php echo date("Y"); ?>;
            var tahun = $("#combo-tahun-anggaran").val();
            var combo = $('.tahun4');
            $.each(combo, function (index, itemCombo) {
                var item = $(itemCombo);
                var select_temp = item.val();
                item.empty();
                item.append(  $('<option>', { value: "",  text: " -- Pilih -- "  }, '</option>'))
                var i;
                var pilih_option = false;
                for (i = parseInt(tahun); i  >= 2001; i--) { 
                    label = i;
                    if(tahun_sekarang == i){
                        label = i +"";
                    }
                    item.append( $('<option>', {  value: i,  text: label }, '</option>'));
                    if(i == parseInt(select_temp)){
                        pilih_option = true;
                    }
                }

                if(pilih_option){
                    item.val(select_temp)
                }
                
            }); 

        }
                   
                 

		/* add kegiatan konsultansi */ 
        $('body').on('click','#btn-add-kegiatan-konsultansi', function(){
            $('#box-kegiatan-konsultansi').append($("#new-item-kegiatan-konsultansi").html()); 
            batasi_tahun();
        });

        /* add kegiatan konstruksi */ 
        $('body').on('click','#btn-add-kegiatan-konstruksi', function(){
            $('#box-kegiatan-konstruksi').append($("#new-item-kegiatan-konstruksi").html()); 
            batasi_tahun();
        });

        /* add kegiatan supervisi */ 
        $('body').on('click','#btn-add-kegiatan-supervisi', function(){
            $('#box-kegiatan-supervisi').append($("#new-item-kegiatan-supervisi").html()); 
            batasi_tahun();
        });


        /* add kegiatan lanjutan */ 
        $('body').on('click','#btn-add-kegiatan-lanjutan', function(){
            $('#box-kegiatan-lanjutan').append($("#new-item-kegiatan-lanjutan").html()); 
        });
        
        
		/* add kegiatan Terkait */
        $('body').on('click','#btn-add-kegiatan-terkait', function(){
            $('#box-kegiatan-terkait').append($("#new-item-kegiatan-terkait").html()); 
        });



        /* hapus kegiatan relasi temp  */
        $('body').on('click','.bdel-kegiatan-relasi-temp', function(){
            $(this).parents('.item-kegiatan-relasi').remove(); 
        });


        /* Hapus kegiatan relasi di server */
		$('body').on('click','.bdel-kegiatan-relasi', function(e){
			e.preventDefault();
			e.stopPropagation();
			var tombol = $(this);
			swal({
				title: 'Anda yakin menghapus?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Hapus',
				cancelButtonText: 'Batal' 
			}).then(function () {
				$.ajax({
					type: 'get', 
					url: appSettings.base_url + 'admin/kegiatan/delete_kegiatan_relasi/' + tombol.data('kegiatan_relasi'),
					dataType: 'json',
					beforeSend: function() {},
					success: function(response) {
						if(response.status == 1){
							tombol.parents('.item-kegiatan-relasi').slideUp();
						}
						swal({
						  	title: response.title,
						  	text: response.message,
						  	type: response.type,
						  	timer: 1000,
							showConfirmButton:false
						})
					}
				}); 
			}) 
		});
    })
</script>


<script type="text/html" id="new-item-kegiatan-konsultansi">
	<div class="row item-kegiatan-relasi">
        <div class="form-group col-md-8"> 
            <textarea class="form-control" name="kegiatan_konsultansi_isi_new[]" placeholder="konsultansi" rows="1"></textarea>
        </div>
        <div class="col-md-2">
            <div class="form-group"> 
                <?php
                    $class = array('class' => "form-control tahun4");
                    $tahun = array('' => '-- Pilih -- ');
                    for ($t = $maxTahun; $t >= 2001 ; $t--) {
                        $tahun[$t] = label_tahun_current($t);
                    }
                    echo form_dropdown('kegiatan_konsultansi_tahun_new[]', $tahun, '', $class);
                ?>   
            </div>
        </div>
        <div class="col-md-2">
	    	<button type="button" class="btn btn-warning bdel-kegiatan-relasi-temp">-</button>
	    </div>
    </div> 
</script>


<script type="text/html" id="new-item-kegiatan-konstruksi">
	<div class="row item-kegiatan-relasi">
        <div class="form-group col-md-8"> 
            <textarea class="form-control" name="kegiatan_konstruksi_isi_new[]" placeholder="konstruksi" rows="1"></textarea>
        </div>
        <div class="col-md-2">
            <div class="form-group"> 
                <?php
                    $class = array('class' => "form-control tahun4");
                    $tahun = array('' => '-- Pilih -- ');
                    for ($t = $maxTahun; $t >= 2001 ; $t--) {
                        $tahun[$t] = label_tahun_current($t);
                    }
                    echo form_dropdown('kegiatan_konstruksi_tahun_new[]', $tahun, '', $class);
                ?>   
            </div>
        </div>
        <div class="col-md-2">
	    	<button type="button" class="btn btn-warning bdel-kegiatan-relasi-temp">-</button>
	    </div>
    </div> 
</script>


<script type="text/html" id="new-item-kegiatan-supervisi">
	<div class="row item-kegiatan-relasi">
        <div class="form-group col-md-8"> 
            <textarea class="form-control" name="kegiatan_supervisi_isi_new[]" placeholder="Supervisi" rows="1"></textarea>
        </div>
        <div class="col-md-2">
            <div class="form-group"> 
                <?php
                    $class = array('class' => "form-control tahun4");
                    $tahun = array('' => '-- Pilih -- ');
                    for ($t = $maxTahun; $t >= 2001 ; $t--) {
                        $tahun[$t] = label_tahun_current($t);
                    }
                    echo form_dropdown('kegiatan_supervisi_tahun_new[]', $tahun, '', $class);
                ?>   
            </div>
        </div>
        <div class="col-md-2">
	    	<button type="button" class="btn btn-warning bdel-kegiatan-relasi-temp">-</button>
	    </div>
    </div> 
</script>


<script type="text/html" id="new-item-kegiatan-lanjutan">
	<div class="row item-kegiatan-relasi"> 
	    <div class="form-group col-md-8"> 
	        <textarea name="kegiatan_lanjutan_isi_new[]" class="form-control" placeholder="Data Kegiatan lanjutan" rows="1"></textarea>
	    </div>
	    <div class="col-md-2">
	        <div class="form-group"> 
	            <input name="kegiatan_lanjutan_tahun_new[]"  type="text" class="form-control input_tahun" placeholder="Tahun Kegiatan lanjutan" /> 
	        </div>
	    </div>
	    <div class="col-md-2">
	    	<button type="button" class="btn btn-warning bdel-kegiatan-relasi-temp">-</button>
	    </div>
	</div>
</script>


<script type="text/html" id="new-item-kegiatan-terkait">
	<div class="row item-kegiatan-relasi"> 
	    <div class="form-group col-md-8"> 
	        <textarea name="kegiatan_terkait_isi_new[]" class="form-control" placeholder="Data Kegiatan Terkait" rows="1"></textarea>
	    </div>
	    <div class="col-md-2">
	        <div class="form-group"> 
	            <input name="kegiatan_terkait_tahun_new[]"  type="text" class="form-control input_tahun" placeholder="Tahun Kegiatan Terkait" /> 
	        </div>
	    </div>
	    <div class="col-md-2">
	    	<button type="button" class="btn btn-warning bdel-kegiatan-relasi-temp">-</button>
	    </div>
	</div>
</script>