 
<?php 
    


foreach($data as $id_provinsi => $lokasi) { ?>
<div class="well well-sm item-lokasi lokasi-<?php echo $id_provinsi; ?>" > 

    <div class="box-lokasi">
            
         <div class="row row-aja row-lokasi" data-prov="<?php echo $id_provinsi; ?>">
 
         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kabupaten/Kota <?php echo form_error('kabupaten') ?></label><span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> 

                <div class="kabupaten-cek">
                	<strong><?php echo $lokasi['nama']; ?></strong><br>
                	<?php foreach($kabupaten[$id_provinsi] as $kab) { ?>
                            <label class="checkbox-inline">
                                <input class="cekKab" type="checkbox" <?php echo (in_array($kab->id,$lokasi['kabArray']) ? ' checked' : ''); ?> name="cekkab[<?php echo $id_provinsi; ?>][]" value="<?php echo $kab->id; ?>"><span><?php echo $kab->name; ?></span>
                            </label> <br>
                <?php }  ?>
                </div>
                
                <?php /*<select class="form-control combo_kabupaten" multiple  name="lok[<?php echo $lok['id_lokasi']; ?>][id_kabupaten]" id="kabupaten-<?php echo $id_provinsi; ?>">
                    <option value="">-- Pilih --</option>
                    <?php foreach($kabupaten[$key] as $kab) { ?>
                    <option <?php echo set_selected($kab->id,  $lok['id_kabupaten']); ?> value="<?php echo $kab->id; ?>"><?php echo $kab->name; ?></option>
                    <?php } ?>
                </select>*/ ?>



            </div>
         </div>

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kecamatan <?php echo form_error('kecamatan') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php //echo combo_kecamatan( $lok['id_kecamatan'] , $lok['id_kabupaten'] , ' multiple name="lok['.$lok['id_lokasi'].'][id_kecamatan]" class="form-control combo-kecamatan"'); ?>
               <?php /* <input type="text" class="form-control" name="lok[<?php echo $lok['id_lokasi']; ?>][id_kecamatan]" id="kecamatan" placeholder="kecamatan" value="<?php echo $lok['id_kecamatan']; ?>" />*/ ?>
               <div class="kecamatan-cek" style="overflow: auto;max-height: 300px">
                <?php foreach($lokasi['kab'] as $k) { ?>
                    <?php echo list_kec_by_kab($k['id_kabupaten'],$lokasi['kecArray']); ?>
                <?php } ?> 
               </div>
           </div>
         </div>

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kelurahan/Desa<?php echo form_error('desa') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisi secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                 <?php //echo combo_kelurahan( $lok['id_kelurahan'] , $lok['id_kecamatan'] , 'multiple name="lok['.$lok['id_lokasi'].'][id_kelurahan]" class="form-control combo-kelurahan"'); ?>
                <?php /*<input type="text" class="form-control" name="lok[<?php echo $lok['id_lokasi']; ?>][id_kelurahan]" id="desa" placeholder="Desa" value="<?php echo $lok['id_kelurahan']; ?>" />*/ ?>
                <div class="kelurahan-cek" style="overflow: auto;max-height: 300px">
                <?php 

                foreach($lokasi['kab'] as $k) { 
                	if( isset($k['kec']) ){
                		foreach($k['kec'] as $kec) { 
	                        echo list_kal_by_kec($kec,$lokasi['kelArray'], $id_provinsi, $k['id_kabupaten']);
	                    }
                	}
                    
                }

                  ?> 
               </div>
            </div>
         </div>

         <div class="col-md-1">
         <label>&nbsp;</label> <br>
             <?php /*<button type="button" data-id="<?php echo $lok['id_lokasi']; ?>" class="btn btn-danger btn-hapus-lokasi-saat-edit">-</button>   
             <input type="hidden" name="lok[<?php echo $lok['id_lokasi']; ?>][id]" value="<?php echo $lok['id_lokasi']; ?>">
             <input type="hidden" name="lok[<?php echo $lok['id_lokasi']; ?>][id_provinsi]" value="<?php echo $lok['prov']; ?>">*/ ?>
         </div>

    </div>
         
    
        
     
          
     </div>   

     <?php /*<button type="submit" class="btn btn-primary btn-add-lokasi">+</button>  */ ?> 

</div> 
<?php } ?>