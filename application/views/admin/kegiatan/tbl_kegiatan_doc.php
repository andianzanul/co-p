<!doctype html>
<html>
    <head>
        <title>doc</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h3>DESKRIPSI KEGIATAN </h3>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama: Kegiatan</th>
		<th>Jenis Kegiatan</th>
		<th>Thn Anggaran</th>
		<th>Ppk</th>
		<th>Bidang</th>
		<th>Sub Bidang</th>
		<th>Jenis Konstruksi</th>
		<th>Provinsi</th>
		<th>Kabupaten</th>
		<th>Kecamatan</th>
		<th>Desa</th>
		<th>Wl Sungai</th>
		<th>Das</th>
		<th>Tujuan</th>
		<th>Manfaat</th>
		<th>Output</th>
		<th>Outcome</th>
		<th>Data Teknis</th>
		<th>Data Lanjutan</th>
		<th>Th Kegiatanlanjutan</th>
		<th>Sumber Dana</th>
		<th>Status Kegiatan</th>
		<th>Dasar Kegiatan</th>
		<th>Nmp Jasa</th>
		<th>Perencana</th>
		<th>Supervisi</th>
		<th>Tgl Kontrak Awal</th>
		<th>No Kontrak Awal</th>
		<th>Nilai Pagu</th>
		<th>Tgl Spmk</th>
		<th>No Spmk</th>
		<th>Penyerapan Rp</th>
		<th>Keuangan Persen</th>
		<th>Jkwkt Pelaksanaan</th>
		<th>Jml Hari</th>
		<th>Koordinat X</th>
		<th>Koordinat Y</th>
		<th>Youtube Url</th>
		
            </tr><?php
            foreach ($tbl_kegiatan_data as $tbl_kegiatan)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $tbl_kegiatan->nm_kegiatan ?></td>
		      <td><?php echo $tbl_kegiatan->jenis_kegiatan ?></td>
		      <td><?php echo $tbl_kegiatan->thn_anggaran ?></td>
		      <td><?php echo $tbl_kegiatan->ppk ?></td>
		      <td><?php echo $tbl_kegiatan->nama_bidang ?></td>
		      <td><?php echo $tbl_kegiatan->sub_bidang ?></td>
		      <td><?php echo $tbl_kegiatan->jenis_konstruksi ?></td>
		      <td><?php echo $tbl_kegiatan->provinsi ?></td>
		      <td><?php echo $tbl_kegiatan->kabupaten ?></td>
		      <td><?php echo $tbl_kegiatan->kecamatan ?></td>
		      <td><?php echo $tbl_kegiatan->desa ?></td>
		      <td><?php echo $tbl_kegiatan->wl_sungai ?></td>
		      <td><?php echo $tbl_kegiatan->das ?></td>
		      <td><?php echo $tbl_kegiatan->tujuan ?></td>
		      <td><?php echo $tbl_kegiatan->manfaat ?></td>
		      <td><?php echo $tbl_kegiatan->output ?></td>
		      <td><?php echo $tbl_kegiatan->outcome ?></td>
		      <td><?php echo $tbl_kegiatan->data_teknis ?></td>
		      <td><?php echo $tbl_kegiatan->data_kegiatan ?></td>
		      <td><?php echo $tbl_kegiatan->th_kegiatanlanjutan ?></td>
		      <td><?php echo $tbl_kegiatan->sumber_dana ?></td>
		      <td><?php echo $tbl_kegiatan->status_kegiatan ?></td>
		      <td><?php echo $tbl_kegiatan->dasar_kegiatan ?></td>
		      <td><?php echo $tbl_kegiatan->nmp_jasa ?></td>
		      <td><?php echo $tbl_kegiatan->perencana ?></td>
		      <td><?php echo $tbl_kegiatan->supervisi ?></td>
		      <td><?php echo $tbl_kegiatan->tgl_kontrak_awal ?></td>
		      <td><?php echo $tbl_kegiatan->no_kontrak_awal ?></td>
		      <td><?php echo $tbl_kegiatan->nilai_kontrak ?></td>
		      <td><?php echo $tbl_kegiatan->tgl_spmk ?></td>
		      <td><?php echo $tbl_kegiatan->no_spmk ?></td>
		      <td><?php echo $tbl_kegiatan->nilai_penyerapan ?></td>
		      <td><?php echo $tbl_kegiatan->keuangan_persen ?></td>
		      <td><?php echo $tbl_kegiatan->jkwkt_pelaksanaan ?></td>
		      <td><?php echo $tbl_kegiatan->jml_hari ?></td>
		      <td><?php echo $tbl_kegiatan->koordinat_x ?></td>
		      <td><?php echo $tbl_kegiatan->koordinat_y ?></td>
		      <td><?php echo $tbl_kegiatan->youtube_url ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>