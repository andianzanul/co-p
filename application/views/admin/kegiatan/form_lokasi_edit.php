<?php foreach($data as $key => $lokasi) { ?>
<div class="well well-sm item-lokasi lokasi-<?php echo $key; ?>"> 
    <script class="lokasi-tambah-daya" type="text/html">
        <div class="row row-lokasi">
             <div class="col-md-4">
                 <div class="form-group">
                    <label for="varchar">Kabupaten/Kota <?php echo form_error('kabupaten') ?></label> <span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                   
                    <select class="form-control combo_kabupaten" name="kabupaten[<?php echo $key; ?>][]" id="kabupaten-<?php echo $key; ?>">
                        <option value="">-- Pilih --</option>
                        <?php foreach($kabupaten[$key] as $kab) { ?>
                        <option value="<?php echo $kab->id; ?>"><?php echo $kab->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
             </div>

             <div class="col-md-4">
                 <div class="form-group">
                    <label for="varchar">Kecamatan <?php echo form_error('kecamatan') ?></label><span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <?php echo combo_kecamatan( '', '', ' name="kecamatan['. $key .'][]" class="form-control combo-kecamatan"'); ?>
                    <?php /*<input type="text" class="form-control" name="kecamatan[<?php echo $key; ?>][]" id="kecamatan" placeholder="kecamatan" value="" />*/ ?>
               </div>
             </div>

             <div class="col-md-3">
                 <div class="form-group">
                    <label for="varchar">Kelurahan/Desa<?php echo form_error('desa') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <?php echo combo_kelurahan( '' , '' , ' name="desa['. $key .'][]" class="form-control combo-kelurahan"'); ?>
                    <?php /*<input type="text" class="form-control" name="desa[<?php echo $key; ?>][]" id="desa" placeholder="Desa" value="" />*/ ?>
                </div>
             </div>

             <div class="col-md-1">
             <label>&nbsp;</label> <br>
                 <button type="button" class="btn btn-warning btn-hapus-lokasi-123">-</button>   
             </div>

        </div>
    </script>
    <?php 
    $awal = $akhir = '';
    $no = 1;
    foreach ($lokasi as $key_lok => $lok) {
       
        ob_start();
        ?>
         <div class="row row-aja row-lokasi">

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kabupaten/Kota <?php echo form_error('kabupaten') ?></label><span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <select class="form-control combo_kabupaten" name="lok[<?php echo $lok['id_lokasi']; ?>][id_kabupaten]" id="kabupaten-<?php echo $key; ?>">
                    <option value="">-- Pilih --</option>
                    <?php foreach($kabupaten[$key] as $kab) { ?>
                    <option <?php echo set_selected($kab->id,  $lok['id_kabupaten']); ?> value="<?php echo $kab->id; ?>"><?php echo $kab->name; ?></option>
                    <?php } ?>
                </select>
            </div>
         </div>

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kecamatan <?php echo form_error('kecamatan') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php echo combo_kecamatan( $lok['id_kecamatan'] , $lok['id_kabupaten'] , ' name="lok['.$lok['id_lokasi'].'][id_kecamatan]" class="form-control combo-kecamatan"'); ?>
               <?php /* <input type="text" class="form-control" name="lok[<?php echo $lok['id_lokasi']; ?>][id_kecamatan]" id="kecamatan" placeholder="kecamatan" value="<?php echo $lok['id_kecamatan']; ?>" />*/ ?>
           </div>
         </div>

         <div class="col-md-3">
             <div class="form-group">
                <label for="varchar">Kelurahan/Desa<?php echo form_error('desa') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisi secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                 <?php echo combo_kelurahan( $lok['id_kelurahan'] , $lok['id_kecamatan'] , ' name="lok['.$lok['id_lokasi'].'][id_kelurahan]" class="form-control combo-kelurahan"'); ?>
                <?php /*<input type="text" class="form-control" name="lok[<?php echo $lok['id_lokasi']; ?>][id_kelurahan]" id="desa" placeholder="Desa" value="<?php echo $lok['id_kelurahan']; ?>" />*/ ?>
            </div>
         </div>

         <div class="col-md-1">
         <label>&nbsp;</label> <br>
             <button type="button" data-id="<?php echo $lok['id_lokasi']; ?>" class="btn btn-danger btn-hapus-lokasi-saat-edit">-</button>   
             <input type="hidden" name="lok[<?php echo $lok['id_lokasi']; ?>][id]" value="<?php echo $lok['id_lokasi']; ?>">
             <input type="hidden" name="lok[<?php echo $lok['id_lokasi']; ?>][id_provinsi]" value="<?php echo $lok['prov']; ?>">
         </div>

    </div>
        <?php 
       
        if($no == 1){
            $awal .= '<b>'. $lok['nama'] .'</b>'.ob_get_clean();
        }else{ 
            $akhir .= ob_get_clean();
        }

        $no++;
    } 


    echo $awal ;

    ?>
    
        
     <div class="box-lokasi">
         <?php echo $akhir;  ?>
     </div>   

     <button type="button" class="btn btn-primary btn-add-lokasi">+</button>   

</div> 
<?php } ?>