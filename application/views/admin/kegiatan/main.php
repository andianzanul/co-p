<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">
<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>



<!-- SEARCH BEGIN -->
<div class="box box-primary">
  <div class="box-header with-border hide">
    <h3 class="box-title"> <i class="fa fa-search fa-fw"> </i></h3>
  </div>
    <?php if($this->session->userdata('level')=='direksi') { ?> 
    <div class="box-body">
      <form action="" method="get">
          <div class="row">
              <div class="col-md-10">
                  <label>Nama Kegiatan</label>
                  <input type="text" name="nama_kegiatan" class="form-control" value="<?php echo $the_nama_kegiatan; ?>">
              </div>
              <div class="col-md-1">
                  <label style="visibility: hidden;">Cari</label><br>
                  <button   type="submit" class="btn btn-default"> <i class="fa fa-search"></i>  Cari</button>
              </div>
          </div>

          <div class="row" style="margin-top:10px">
              <div class="col-md-4">
                  <label>Tahun Anggaran</label>
                  <?php
                  $class = array(
                      'class' => "form-control"
                  );
                  $tahun = array();
                  for ($t = 2001; $t <= 2031; $t++) {
                      $tahun[$t] = $t;
                  }
                  echo form_dropdown('thn[]', $tahun, $the_tahun, 'class="form-control select2" multiple ');
                  ?>
              </div>
              <!-- <div class="col-md-4">
                  <label>Kegiatan</label>
                  <?php unset($jenis_bidang_data[0]); ?>
                  <?php echo form_dropdown('kegiatan[]', $jenis_bidang_data, $the_kegiatan, 'class="form-control select2" multiple '); ?> 
              </div> -->
              <!-- <div class="col-md-4">
                  <label>Jenis Kegiatan</label>
                  <?php unset($jenis_kegiatan_data[0]); ?>
                  <?php echo form_dropdown('jenis_kegiatan[]',$jenis_kegiatan_data,$the_jenis_kegiatan,'class="form-control select2" multiple name="jenis_kegiatan" id="jenis_kegiatan"'); ?>
              </div> -->
          </div>

          <!-- <div class="row" style="margin-top:10px">
              <div class="col-md-4">
                  <label>Satker</label>
                  <?php echo form_dropdown('satker[]', $jenis_satker_data, $the_satker, 'class="form-control select2" multiple '); ?> 
              </div>
              <div class="col-md-4">
                  <label>Provinsi</label>
                  <?php echo form_dropdown('prov[]', $provinsi, $the_provinsi, 'class="form-control select2" multiple '); ?> 
              </div>
              <div class="col-md-4">
                  <label>Kabupaten</label>
                  <?php echo form_dropdown('kab[]', $kab, $the_kab, 'class="form-control select2" multiple '); ?> 
              </div>
          </div> -->
          <?php echo form_close(); ?>
      </div>

    <?php } else { ?>

      <div class="box-body">
        <form action="" method="get">
            <div class="row">
                <div class="col-md-10">
                    <label>Nama Kegiatan</label>
                    <input type="text" name="nama_kegiatan" class="form-control" value="<?php echo $the_nama_kegiatan; ?>">
                </div>
                <div class="col-md-1">
                    <label style="visibility: hidden;">Cari</label><br>
                    <button   type="submit" class="btn btn-default"> <i class="fa fa-search"></i>  Cari</button>
                </div>
            </div>

            <div class="row" style="margin-top:10px">
                <div class="col-md-4">
                    <label>Tahun Anggaran</label>
                    <?php
                    $class = array(
                        'class' => "form-control"
                    );
                    $tahun = array();
                    for ($t = 2001; $t <= 2031; $t++) {
                        $tahun[$t] = $t;
                    }
                    echo form_dropdown('thn[]', $tahun, $the_tahun, 'class="form-control select2" multiple ');
                    ?>
                </div>
                <!-- <div class="col-md-4">
                    <label>Kegiatan</label>
                    <?php unset($jenis_bidang_data[0]); ?>
                    <?php echo form_dropdown('kegiatan[]', $jenis_bidang_data, $the_kegiatan, 'class="form-control select2" multiple '); ?> 
                </div> -->
                <!-- <div class="col-md-4">
                    <label>Jenis Kegiatan</label>
                    <?php unset($jenis_kegiatan_data[0]); ?>
                    <?php echo form_dropdown('jenis_kegiatan[]',$jenis_kegiatan_data,$the_jenis_kegiatan,'class="form-control select2" multiple name="jenis_kegiatan" id="jenis_kegiatan"'); ?>
                </div> -->
            </div>

            <!-- <div class="row" style="margin-top:10px">
                <div class="col-md-4">
                    <label>Provinsi</label>
                    <?php echo form_dropdown('prov[]', $provinsi, $the_provinsi, 'class="form-control select2" multiple '); ?> 
                </div>
                <div class="col-md-4">
                    <label>Kabupaten</label>
                    <?php echo form_dropdown('kab[]', $kab, $the_kab, 'class="form-control select2" multiple '); ?> 
                </div>
                <div class="col-md-4"></div>
            </div> -->
          <?php echo form_close(); ?>
      </div>
    <?php } ?>
</div> 
<!-- END FORM SEARCH -->



<!-- <h2 style="background: #3C94C3; padding: 5px; color: #ECF0F5;"> DATA KEGIATAN </h2> -->
  <form action="<?php echo site_url('admin/kegiatan/download'); ?>" class="form-inline" method="post">
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title" style="color: #3C94C3;">DATA KEGIATAN</h3>
          <div class="box-tools pull-right">
          <!-- tombol tambah -->
            <!-- <?php //echo anchor(site_url('admin/kegiatan/create'),'Tambah', 'class="btn btn-primary btn-sm"'); ?> -->
            <?php  if($this->mainlib->cek_level(['direksi',"superadmin"])) { ?>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Tambah Paket/Kegiatan</button>
            <?php } ?>
          </div> 
        </div>

        <div class="box-body no-padding">   
          <?php if($total_rows > 0) { ?>          
          <table id="example1" class="table table-striped table-bordered " style="margin-bottom: 0px">
            <thead>
              <tr> 
                 
                <th width="1"><input type="checkbox" name="cekAllIdKegiatan" id="cekAllIdKegiatan" value="1"></th>
                 
                <th>Nama Kegiatan</th>
                <th>Direksi</th>
                  <th>Penyedia Jasa </th>
<!--                 <th width="200" class="hiiden-sm hidden-xs">Nama Satker</th> -->
<!--                 <th class="hiiden-sm hidden-xs">Jenis Kegiatan</th> -->
                <th width="200" class="hiiden-sm hidden-xs">Tahun Anggaran</th>
<!--                 <th class="hiiden-sm hidden-xs">PPK</th> -->
<!--                 <th class="hiiden-sm hidden-xs">Kegiatan</th> -->
                <th width="100" class="hiiden-sm hidden-xs">Last Update</th>
                <th width="90">Action</th>
              </tr>
            </thead>

            <tbody>
              <?php foreach ($tbl_kegiatan_data as $tbl_kegiatan) { ?>
                <tr>
                  
                  <td><input type="checkbox" class="id-kegiatan-main" name="id_kegiatan[]" value="<?php echo $tbl_kegiatan->id_kegiatan; ?>"></td>
                  
                  <td><?php echo $tbl_kegiatan->nm_kegiatan; ?> </td>
                  <td><?php echo $tbl_kegiatan->nama; ?> </td>
                        <td><?php echo $tbl_kegiatan->nama_penyediajasa; ?> </td>
<!--                   <td class="hiiden-sm hidden-xs"><?php echo $tbl_kegiatan->nama_satker; ?></td> -->
<!--                   <td class="hiiden-sm hidden-xs"><?php echo $tbl_kegiatan->jenis_kegiatan; ?></td> -->
                  <td width="200" class="hiiden-sm hidden-xs"><?php echo $tbl_kegiatan->tahun; ?> <?php echo ($tbl_kegiatan->multi_years == 'Y' ? " - " . $tbl_kegiatan->tahun_anggaran_2 : '' ); ?></td>
<!--                   <td class="hiiden-sm hidden-xs"><?php echo $tbl_kegiatan->nama_ppk; ?></td> -->
<!--                   <td class="hiiden-sm hidden-xs"><?php echo $tbl_kegiatan->nama_bidang; ?></td> -->
                  <td class="hiiden-sm hidden-xs"><?php echo tgl_indo($tbl_kegiatan->last_update,2)  ." " .$tbl_kegiatan->nama_lastupdate; ?></td>
                  <td>
                    <?php 
                   // echo anchor(site_url('admin/kegiatan/read/'.$tbl_kegiatan->id_kegiatan),'Baca', 'class="btn btn-success btn-xs"'); 
                    //echo '   '; 
                    //echo anchor(site_url('admin/kegiatan/update/'.$tbl_kegiatan->id_kegiatan),'Edit', 'class="btn btn-warning btn-xs"'); 
                    //echo '  '; 
                   // echo anchor(site_url('admin/kegiatan/delete/'.$tbl_kegiatan->id_kegiatan),'Hapus','class="btn btn-danger btn-hapus-kegiatan btn-xs"'); 
                    ?>
                   <?php  if($this->mainlib->cek_level(['direksi',"superadmin"])) { ?>
            <div class="btn-group">
                      <a href="<?php echo base_url('admin/kegiatan/update/'.$tbl_kegiatan->id_kegiatan); ?>"class="btn btn-success btn-xs" role="button">Edit</a>
                      
                      <button type="button" class="btn btn-success dropdown-toggle btn-xs" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo base_url('admin/kegiatan/read/'.$tbl_kegiatan->id_kegiatan); ?>">Detail</a></li>
                        <li><a data-id="<?php echo $tbl_kegiatan->id_kegiatan; ?>" class="btn-hapus-kegiatan">Hapus</a></li>
                      </ul>
              
                    </div>
            <?php } ?> 
              <?php  if($this->mainlib->cek_level(['konsultan'])) { ?>
            <div class="btn-group">
                      <a href="<?php echo base_url('admin/kegiatan/update/'.$tbl_kegiatan->id_kegiatan); ?>"class="btn btn-success btn-xs" role="button">Detail</a>
                      
                     
                       
                      </ul>
              
                    </div>
            <?php } ?> 
                    
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
  
          <?php } else {  ?>
            <div class="alert alert-warning text-center"> Data tidak ditemukan! </div>
          <?php } ?>
        </div>

        <!-- /.box-body -->
        <?php if($total_rows > 0) { ?> 
        <div class="box-footer">
            <div class="row">
              <div class="col-md-6">
                  <a href="#" class="btn btn-warning">Jumlah Data : <?php echo $total_rows ?></a>
                  <?php echo anchor(site_url('admin/kegiatan/excel'), 'Excel', 'class="btn btn-success"'); ?>
                  <?php //echo anchor(site_url('admin/kegiatan/pdf'), 'Pdf', 'class="btn btn-danger"'); ?>
                  <button type="submit" name="pdf" value="1" class="btn btn-danger">PDF</button>
                  <button type="submit" name="word" value="1" class="btn btn-primary">Word</button>
              </div>
              <div class="col-md-6 text-right">
                  <?php echo $this->pagination->create_links(); ?>
              </div>
          </div>
        </div>
        <?php } ?>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <?php echo form_close(); ?>
</section>
<!-- end content -->

        <script>
    $(function () {
    //$("#example2").DataTable();

     $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
     "order": [[ 0, "DESC" ]],
      "info": true,
      "autoWidth": true
    
    });
    });
    </script>


<?php echo form_open('admin/kegiatan/create'); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Lembar Asistensi</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="nm_kegiatan">Kegiatan <?php echo form_error('nm_kegiatan') ?></label>
            <input required type="text" class="form-control" name="nm_kegiatan" id="nm_kegiatan" placeholder="Nama Kegiatan" value="" />
        </div>

       

        <div class="form-group">
            <label for="konsultan">Penyedia Jasa <?php echo form_error('nm_kegiatan') ?></label>
            <?php echo combo_list_konsultan(); ?>
        </div>
 <div class="form-group">
            <label for="tahun_anggaran">Tahun Anggaran <?php echo form_error('nm_kegiatan') ?></label>
            <?php
                    $class = array('class' => "form-control", 'id' => 'combo-tahun-anggaran','required' => 'required');
                    $tahun = array('' => '-- Pilih -- ');
                    for ($t = 2031; $t >= 2001; $t--) {
                        $tahun[$t] = label_tahun_current($t);
                        
                    }
                    echo form_dropdown('tahun_anggaran', $tahun, "", $class);
                ?> 
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       <button type="submit" class="btn btn-primary">Simpan</button> 
      </div>
    </div>
  </div>
</div>  