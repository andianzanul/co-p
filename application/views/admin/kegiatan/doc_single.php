 
<style>
h3{
    text-align: center
}
h5
{
    font-family: Times New Roman;
    color: white;
    text-align: center;
}
table{
    align:center;
}
td{
    width: 300px;
}

</style>
    

        <h3>DESKRIPSI KEGIATAN </h3>
        <table class="word-table" style="margin-bottom: 10px">
   
        <tr>
            <td style="background-color:#4f81bd; color:white" width="5%">
                <strong><h2 align="center">No</h2></strong>
            </td>
            <td colspan="2"  style="text-align: center;background-color:#4f81bd; color:white; ">
                <strong><h2>Uraian</h2></strong>
            </td>
        </tr>
            <tr><td width="5%" bgcolor="#4f81bd"><h5>1</h5></td> <td bgcolor="a7bfde">Nama Satker <td bgcolor="a7bfde"><?php echo $tbl_kegiatan->nama_satker ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>2</td>  <td bgcolor="d3dfee"> Nama Kegiatan<td bgcolor="d3dfee"><?php echo $tbl_kegiatan->nm_kegiatan ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>3</td><td bgcolor="a7bfde">Jenis Kegiatant</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->jenis_kegiatan ?></td>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>4</td><td bgcolor="d3dfee">Tahun Anggaran</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->tahun ?></td></tr> 

            <tr><td width="5%" bgcolor="#4f81bd"><h5>5</td><td bgcolor="a7bfde">PPK</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->nama_ppk ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>6</td><td bgcolor="d3dfee">Bidang</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->nama_bidang ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>7</td><td bgcolor="a7bfde">Sub Bidang</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->output ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>8</td><td bgcolor="d3dfee">Jenis Konstruksi</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->jenis_konstruksi ?></td></tr>



            <tr><td width="5%" bgcolor="#4f81bd"><h5>9</td><td bgcolor="a7bfde">Provinsi</td><td bgcolor="a7bfde">
                 <?php  
                    $Provinsi = $kabupaten = $kecamatan = $desa = array();
                    foreach($lokasi as $lok){
                        $Provinsi[$lok->provinsi] = $lok->provinsi;
                        $kabupaten[$lok->name] = $lok->name;
                        $kecamatan[$lok->kecamatan] = $lok->kecamatan;
                        $desa[$lok->desa] = $lok->desa;
                    }
                    echo implode(', ',$Provinsi );
                ?>
            </td></tr>



            <tr><td width="5%" bgcolor="#4f81bd"><h5>10</td><td bgcolor="d3dfee">Kabupaten</td><td bgcolor="d3dfee"><?php echo implode(', ',$kabupaten ); ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>11</td><td bgcolor="a7bfde">Kecamatan</td><td bgcolor="a7bfde"><?php echo implode(', ',$kecamatan ); ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>12</td><td bgcolor="d3dfee">Desa</td><td bgcolor="d3dfee"><?php echo implode(', ',$desa ); ?></td></tr>



            <tr>
                <td td width="5%" bgcolor="#4f81bd"><h5>13</td>
                <td bgcolor="a7bfde">Koordinat</td>
                <td>
                    <table class="table">
                        <tr>
                            <th bgcolor="a7bfde">Awal</th>
                            <th bgcolor="a7bfde">Akhir</th>
                        </tr>
                     <?php foreach ($koordinats as $key => $koor) { ?>
                         <tr>
                             <td bgcolor="a7bfde"><?php echo 'X : '.$koor->koordinat_x .', Y : '. $koor->koordinat_y; ?></td>
                             <td bgcolor="a7bfde"><?php echo 'X : '.$koor->koordinat_x1 .', Y : '. $koor->koordinat_y1; ?></td> 
                         </tr>
                     <?php  } ?>
                      </table>
                </td> 
            </tr>



            <tr><td width="5%" bgcolor="#4f81bd"><h5>14</td><td bgcolor="a7bfde">Wilayah Sungai</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->nama_wlsungai ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>14</td><td bgcolor="d3dfee">DAS</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->das ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>15</td><td bgcolor="a7bfde">Tujuan</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->tujuan ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>16</td><td bgcolor="d3dfee">Manfaat</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->manfaat ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>17</td><td bgcolor="a7bfde">Volume Output</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->output ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>18</td><td bgcolor="d3dfee">Volume Outcome</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->outcome ?></td></tr>

            <!-- <tr><td width="5%" bgcolor="#4f81bd"><h5>19</td><td bgcolor="a7bfde">Data Teknis</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->data_teknis ?></td>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>20</td><td bgcolor="d3dfee">Data Kegitan</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->data_kegiatan ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>21</td><td bgcolor="a7bfde">Tahun Kegiatan Lanjutan</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->th_kegiatanlanjutan ?></td></tr> -->

            <tr><td width="5%" bgcolor="#4f81bd"><h5>22</td><td bgcolor="d3dfee">Sumber Dana</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->sumber_dana ?></td></tr>

            <!-- <tr><td width="5%" bgcolor="#4f81bd"><h5>23</td><td bgcolor="a7bfde">Status Kegiatan</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->status_kegiatan ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>24</td><td bgcolor="d3dfee">Dasar Kegiatan</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->dasar_kegiatan ?></td> -->

            <tr><td width="5%" bgcolor="#4f81bd"><h5>25</td><td bgcolor="a7bfde">Penyedia Jasa</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->nmp_jasa ?></td></tr>

            <!-- <tr><td width="5%" bgcolor="#4f81bd"><h5>26</td><td bgcolor="d3dfee">Perencana</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->perencana ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>27</td><td bgcolor="a7bfde">Supervisi</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->supervisi ?></td></tr> -->

            <tr><td width="5%" bgcolor="#4f81bd"><h5>28</td><td bgcolor="d3dfee">Tanggal kontrak</td><td bgcolor="d3dfee"><?php echo tgl_indo($tbl_kegiatan->tgl_kontrak_awal,2); ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>29</td><td bgcolor="a7bfde">No Kontrak</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->no_kontrak_awal ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5><h5>30</td><td bgcolor="d3dfee">Nilai kontrak</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->nilai_kontrak ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>31</td><td bgcolor="a7bfde">Tanggal SPMK</td><td bgcolor="a7bfde"><?php echo tgl_indo($tbl_kegiatan->tgl_spmk,2); ?></td>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>32</td><td bgcolor="d3dfee">Nomor SPMK</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->no_spmk ?></td></tr>



            <tr><td width="5%" bgcolor="#4f81bd"><h5>33</td><td bgcolor="a7bfde">Tanggal Amandemen</td><td bgcolor="a7bfde">
                 <?php 
                 $nomor = $nilai= $tanggal=""  ;
                    foreach ($amandemen as $key => $value) {
                     $nomor .= '<li>'.$value->nomor.'</li>';    
                     $nilai .= '<li>'.$value->nilai.'</li>';
                     $tanggal .= '<li>'. tgl_indo($value->tanggal,2) .'</li>';  
                    } ?>
                    <ol style="padding-left:0px; margin-bottom:0px;" type="a"><?php echo $tanggal ?>   
                    </ol>
            </td></tr>
            <tr><td width="5%" bgcolor="#4f81bd"><h5>34</td><td bgcolor="d3dfee">Nomor Amandemen</td><td bgcolor="d3dfee">
                    <ol style="padding-left:0px; margin-bottom:0px;" type="a"><?php echo $nomor ?>  
                    </ol>
            </td></tr>
                <tr><td width="5%" bgcolor="#4f81bd"><h5>35</td><td bgcolor="a7bfde">Nilai Amandemen</td><td bgcolor="a7bfde">   <ol style="padding-left:0px; margin-bottom:0px;" type="a"><?php echo $nilai ?>  
                    </ol>
                </td></tr>



            <tr><td width="5%" bgcolor="#4f81bd"><h5>35</td><td bgcolor="a7bfde">Tanggal Penyerapan</td><td bgcolor="a7bfde"><?php echo tgl_indo($tbl_kegiatan->tgl_penyerapan,2); ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>35</td><td bgcolor="a7bfde">Nilai Penyerapan (Rp)</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->nilai_penyerapan ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>36</td><td bgcolor="d3dfee">Keuangan (%)</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->keuangan_persen ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>36</td><td bgcolor="d3dfee">fisik (%)</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->fisik_persen ?></td></tr>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>37</td><td bgcolor="a7bfde">Jangka Waktu Pelaksanaan</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->jkwkt_pelaksanaan ?></td></tr>

            <!-- <tr><td width="5%" bgcolor="#4f81bd"><h5>38</td><td bgcolor="d3dfee">Jumlah Hari</td><td bgcolor="d3dfee"><?php echo $tbl_kegiatan->jml_hari ?></td>
            
            <tr><td width="5%" bgcolor="#4f81bd"><h5>41</td><td bgcolor="a7bfde">Video Url</td><td bgcolor="a7bfde"><?php echo $tbl_kegiatan->youtube_url ?></td></tr> -->



            <tr><td width="5%" bgcolor="#4f81bd"><h5>42</td><td bgcolor="d3dfee">Gambar 0 %</td><td bgcolor="d3dfee"><?php
            if (array_key_exists(0, $images)) {
                $img=site_url("thumb?w=300&src=".base_url("uploads/kegiatan/".$images[0]->nama_file));
                $link = site_url('admin/kegiatan/hapus_gbr/' . $images[0]->id_gambar);
                echo '<img src="'.  $img .'">';
            }
            ?>
            </td></tr>
            </div>

            <tr><td width="5%" bgcolor="#4f81bd"><h5>43</td><td bgcolor="a7bfde">Gambar 50 %</td><td bgcolor="a7bfde"><?php
            if (array_key_exists(50, $images)) {
                $img=site_url("thumb?w=300&src=".base_url("uploads/kegiatan/".$images[50]->nama_file));
                $link = site_url('admin/kegiatan/hapus_gbr/' . $images[50]->id_gambar);
                echo '<img src="'.  $img .'">';
            }
            ?>
            </td></tr>
            
            <tr><td width="5%" bgcolor="#4f81bd"><h5>44</td><td bgcolor="d3dfee">Gambar 100 %</td><td bgcolor="d3dfee"><?php
            if (array_key_exists(100, $images)) {
                $img=site_url("thumb?w=300&src=".base_url("uploads/kegiatan/".$images[100]->nama_file));
                $link = site_url('admin/kegiatan/hapus_gbr/' . $images[100]->id_gambar);
                echo '<img src="'.  $img .'">';
            }
            ?>
            </td></tr>
        </table>
        <br style="page-break-before: always">