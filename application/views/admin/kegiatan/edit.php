    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kegiatan <?php echo $this->uri->segment(3); ?></h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                 <i class="fa fa-times"></i></button>
                </div>
            </div>

            <div class="box-body"> 

            <?php echo form_open(current_url(),'',array(
                'id'=> $kegiatan->id_kegiatan,
                'back' => $this->input->get('back')
            )); ?>


 
            <div class="row">
                <div class="col-md-12">  
                    <div class="form-group">
                        <label for="varchar">Nama Pekerjaan <?php echo form_error('nm_kegiatan') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisikan nama kegiatan lengkap sesuai dengan RKA-K/L ataupun POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="nm_kegiatan" id="nm_kegiatan" placeholder="Nama Pekerjaan / Kegiatan" value="<?php echo $kegiatan->nm_kegiatan; ?>" />
                    </div>
                  <div class="form-group" style="display: none;">
                        <label for="varchar">Deskripsi <?php echo form_error('deskripsi') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisikan nama kegiatan lengkap sesuai dengan RKA-K/L ataupun POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi" rows="4"><?php echo $kegiatan->deskripsi; ?></textarea>
                    </div>
                </div>
            </div>



            <!-- <?php $readonly = ($this->mainlib->cek_level(['ppk'])) ? " disabled " : ""; ?> -->

            <!-- <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">Satuan Kerja <?php echo form_error('nama_satker') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih satuan kerja sesuai DIPA" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <?php echo form_dropdown('nama_satker',$jenis_satker_data,$kegiatan->id_satker,'class="form-control" id="nama_satker'.$readonly.'" placeholder="Satker" ' . $readonly ); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">PPK <?php echo form_error('nama_ppk') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih PPK sesuai POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <select class="form-control" name="nama_ppk" id="nama_ppk<?php echo $readonly ?>" <?php echo $readonly; ?>>
                            <option value="">-- pilih --</option>
                            <?php foreach($jenis_ppk_data2 as $ky => $k){ ?>
                            <option <?php echo set_selected($k->id_ppk, $kegiatan->id_ppk ); ?> class="satker-<?php echo $k->id_satker; ?>" value="<?php echo $k->id_ppk; ?>"><?php echo $k->nama_ppk; ?></option>
                            <?php } ?>
                        </select>    
                    </div>
                </div>
            
                <script type="text/javascript">
                    // PILIH  
                    $(document).ready(function() {
                        var showhide_ppk_options = function(input){
                            $("#nama_ppk").find("option").hide(); 
                            $("#nama_ppk").find(".satker-" + input.val()).show();  
            
                            if(input.val() == "0"){
                                $("#nama_ppk").prop( "disabled", true );
                            }else{
                                $("#nama_ppk").prop( "disabled", false );
                            }
                        }
            
                        $("body").on('change','#nama_satker',function(e){
                                 
                            var input = $(this);
                            showhide_ppk_options(input); 
                            $("#nama_ppk").val("");
                        });
            
                        showhide_ppk_options($('#nama_satker'));
                    });
                </script>
            </div> -->
            


            <!-- row -->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="varchar">Sumber Dana <?php echo form_error('sumber_dana') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa asal sumber dana seperti APBN / APBN-P /PHLN / SBSN" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="sumber_dana" id="sumber_dana" placeholder="Sumber Dana" value="<?php echo $kegiatan->sumber_dana; ?>" />
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="varchar">Tahun Anggaran <?php echo form_error('tahun_anggaran') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tahun anggaran pelaksanaan kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> 
                        <?php
                            $class = array('class' => "form-control",'id' => 'combo-tahun-anggaran');
                            $tahun = array('' => '-- Pilih -- ');
                            for ($t = 2031; $t >= 2001; $t--) {
                                $tahun[$t] = label_tahun_current($t);
                                
                            }
                            echo form_dropdown('tahun_anggaran', $tahun, $kegiatan->tahun, $class);
                        ?> 
                    </div>
                </div>
                <div class="col-md-1"> 
                    <div class="form-group">
                        <label for="varchar">Multiyears</label> <br>
                        <input type="checkbox" id="multi_years" name="multi_years" value="1" <?php echo set_checked("Y",$kegiatan->multi_years); ?>> 
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="varchar"> Tahun Anggaran</label> 
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tahun anggaran pelaksanaan kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> 
                        <?php
                            $class = array('class' => "form-control",'id' => 'combo-tahun-anggaran2');

                            $tahun2 = $kegiatan->tahun_anggaran_2;
                            if($kegiatan->multi_years != 'Y'){
                                $class['disabled'] = 'disabled';
                                $tahun2 = '';
                            }

                            $tahun = array('' => '-- Pilih -- ');
                            for ($t = 2031; $t >= 2001; $t--) {
                                $tahun[$t] = label_tahun_current($t);
                            }
                            echo form_dropdown('tahun_anggaran_2', $tahun, $tahun2, $class);
                        ?>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <script type="text/javascript">
            $(function(){

                var set_tahun_anggaran_2 = function(){
                     if( $("#multi_years").prop( "checked" ) ){
                    
                        var item = $('#combo-tahun-anggaran2');
                        var select_temp = item.val();
                        item.empty();
                        item.append(  $('<option>', { value: "",  text: " -- Pilih -- "  }, '</option>'))
                        var i;
                        var pilih_option = false;
                        for (i = 2031; i  >= parseInt($("#combo-tahun-anggaran").val()); i--) { 
                            label = i;
                             
                            item.append( $('<option>', {  value: i,  text: label }, '</option>'));
                            if(i == parseInt(select_temp)){
                                pilih_option = true;
                            }
                        }

                        if(pilih_option){
                            item.val(select_temp)
                        }


                    } 
                }
                
                $("#multi_years").on('change',function(){ 
                   
                    var tombol = $(this);
                    if( tombol.prop( "checked" ) ){
                        $('#combo-tahun-anggaran2').prop( "disabled", false );
                        set_tahun_anggaran_2();
                    }else{
                        $('#combo-tahun-anggaran2').prop( "disabled", true );
                    }

                  
                }); 

                $("#combo-tahun-anggaran").on('change',function(){ 
                   set_tahun_anggaran_2(); 
                }); 

                set_tahun_anggaran_2(); 


            });
            </script>



            <!-- row
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">Jenis Kegiatan <?php echo form_error('jenis_kegiatan') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih jenis kegiatan berupa studi / konstruksi / supervise" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <?php 
                            echo form_dropdown('jenis_kegiatan',$jenis_kegiatan_data,$kegiatan->id_jenis_kegiatan,'class="form-control" id="jenis_kegiatan" placeholder="Jenis Kegiatan"'); 
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">Jenis Konstruksi <?php echo form_error('jenis_konstruksi') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content=" Diisikan jenis konstruksi seperti : tanggul / seawall/ revetmen / pasangan batu / beton / pre cast dll" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="jenis_konstruksi" id="jenis_konstruksi" placeholder="Jenis Konstruksi" value="<?php echo $kegiatan->jenis_konstruksi; ?>" />
                    </div>
                </div>
            </div>
            end row -->
                    


            <!-- row
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="varchar">Volume Output <?php echo form_error('output') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka) dan satuan sesuai dengan capaian kerja" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="output" id="output" placeholder="Nilai Output" value="<?php echo $kegiatan->output; ?>" />
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label for="varchar">Satuan <?php echo form_error('nama_satuan_output') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka) dan satuan sesuai dengan capaian kerja" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <?php echo form_dropdown('nama_satuan_output',$jenis_satuan_output_data,$kegiatan->id_satuan_output,'class="form-control"   id="nama_satuan_output" placeholder="satuan_output"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="varchar">Volume Outcome <?php echo form_error('outcome') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka) dan satuan sesuai dengan capaian kerja" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="outcome" id="outcome" placeholder="Nilai Outcome" value="<?php echo $kegiatan->outcome; ?>" />
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label for="varchar">Satuan <?php echo form_error('nama_satuan_outcome') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka) dan satuan sesuai dengan capaian kerja" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <?php echo form_dropdown('nama_satuan_outcome',$jenis_satuan_outcome_data,$kegiatan->id_satuan_outcome,'class="form-control" id="nama_satuan_outcome" placeholder="satuan_outcome"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">Data Teknis / Uraian Kegiatan <?php echo form_error('data_teknis') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa uraian pelengkap kegiatan seperti lebar saluran, volume tampungan, tinggi tanggul dll" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <textarea class="form-control" id="data_teknis" placeholder="Data Teknis / Uraian Kegiatan" name="data_teknis" rows="4"><?php echo $kegiatan->data_teknis; ?></textarea>
                    </div>
                </div>                  
            </div>
            end row -->


            <!-- <?php $this->load->view('admin/kegiatan/part-kegiatan-detail-konstruksi'); ?> -->

         
            <!-- row
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">Bidang / Kegiatan <i>(sesuai ADIK RKA-KL)</i><?php echo form_error('nama_bidang') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai dengan ADIK pada RKA-K/L" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <?php 
                            echo form_dropdown('nama_bidang',$jenis_bidang_data,$kegiatan->id_bidang,'class="form-control" id="nama_bidang" placeholder="Bidang"'); 
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="varchar">Sub Bidang / Output <i>(sesuai ADIK RKA-KL)</i><?php echo form_error('nama_output') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai Kegiatan dengan ADIK pada RKA-K/L" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <select class="form-control" name="nama_output" id="nama_output">
                            <option value=""> -- pilih -- </option>
                            <?php 
                                foreach($jenis_output_data2 as $ky => $k){ ?>
                                    <option <?php echo set_selected($k->id_output, $kegiatan->id_output ); ?> class="bidang-<?php echo $k->id_bidang; ?>" value="<?php echo $k->id_output; ?>"><?php echo $k->nama_output; ?></option>
                                <?php } 
                            ?>
                        </select>   
                    </div>
            
                    <script type="text/javascript">
                        /* PILIH  */
                        $(document).ready(function() {
                            var showhide_output_options = function(input){
                                $("#nama_output").find("option").hide(); 
                                $("#nama_output").find(".bidang-" + input.val()).show();  
            
                                 if(input.val() == "0"){
                                    $("#nama_output").prop( "disabled", true );
                                }else{
                                    $("#nama_output").prop( "disabled", false );
                                }
                            }
                            $("body").on('change','#nama_bidang',function(e){ 
                                var input = $(this); 
                                $("#nama_output").val("");
                                showhide_output_options( input );
                            });
                            showhide_output_options($('#nama_bidang'));
                        });
                    </script>
                </div>
            </div><hr> 
            end row -->



            <!-- start provinsi 
            <div class="form-group">
                <b>Provinsi</b>  
                <span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> <br>
                    <?php 
                        foreach ($provinsi2 as $key_pr => $prov) 
                        { ?>
                            <label class="checkbox-inline">
                                <input class="cek-provinsi" <?php 
                            echo (in_array($prov->id_provinsi, $provinsinya)) ? set_checked(1,1) : '';?> type="checkbox" name="prov[<?php echo $prov->id_provinsi; ?>]" value="<?php echo $prov->id_provinsi; ?>" data-kegiatan="<?php echo $kegiatan->id_kegiatan; ?>" data-nama="<?php echo $prov->provinsi; ?>"><?php echo $prov->provinsi; ?> 
                            </label>
                    <?php }  ?>
            </div> 
            <div id="box-lokasi">
                <?php echo $lokasi; ?>
            </div>
            <hr>
            CSS untuk lokasi
            <style>
                .kabupaten-cek,
                .kelurahan-cek > div,
                .kecamatan-cek > div{
                    background: #fff;
                    padding: 12px;
                    border:1px solid #eee;
                    margin-bottom: 20px;
                }
            </style>
            end provinsi -->



            <!-- row
            <div class="row"> 
                <div class="col-md-12">
                    <label for="varchar">Wilayah Sungai <?php echo form_error('nama_wlsungai') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <br>
                    <?php foreach ($wilayah_sungai as $ws) { ?>
                        <label class="checkbox-inline">
                                <input class="cek-ws" <?php 
                            echo (in_array($ws->id_wlsungai, $wsnya)) ? set_checked(1,1) : '';?> type="checkbox" name="ws[]" value="<?php echo $ws->id_wlsungai; ?>" data-kegiatan="<?php echo $kegiatan->id_kegiatan; ?>" data-nama="<?php echo $ws->nama_wlsungai; ?>"><?php echo $ws->nama_wlsungai; ?> 
                            </label>
                    <?php } ?>
                        <br><br>
                     <div id="box-ws" class="row">
                        <?php echo $dasnya; ?>
                    </div>
                </div>
            </div>
            <hr>
            end row   
            
            <script type="text/javascript">
                /* PILIH  */
                $(document).ready(function() {
                     $(".selectDAS").select2({
                         placeholder: 'Pilih DAS', 
                    });
                    $("body").on('change','#nama_wlsungai',function(e){
                        var input = $(this); 
                        $.ajax({
                            type: 'post',
                            url:'<?php echo site_url('admin/kegiatan/get_das_by_wilayah_sungai'); ?>',  
                            data:{
                                id : input.val()
                            },
                            dataType: 'html',
                            beforeSend: function() { 
                                $('#box-das').html('<i class="fa fa-spinner fa-pulse fa-fw text-danger"></i> <span class="text-muted">Mohon tunggu, sedang Meminta data das...</span>');
                            },
                            success: function(response) {
                                $('#box-das').html(response);
                            }
                        }); 
                    });
                });
            </script> --> 

            <div class="penyediajasa well">
                <strong>Kontrak :</strong>
                 <div id="penyedia_jasa_box"> 
                    <hr>
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="varchar">Nama Penyedia Jasa <?php echo form_error('nmp_jasa') ?></label>
                                <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual nama perusahaan penyedia jasa konstruksi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                                <input type="text" class="form-control" name="nmp_jasa" id="nmp_jasa" placeholder="Penyedia Jasa" value="<?php echo $kegiatan->nmp_jasa; ?>" /> 
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label for="varchar">KSO/JO</label> <br>
                                <input type="checkbox" id="kso_jo" name="kso_jo" value="kso/jo" <?php echo set_checked("kso/jo",$kegiatan->kso_jo); ?>> 
                            </div>
                        </div>
                        
                        <div class="col-lg-5" id="row-penyedia-jasa-kso-jo" 
                            <?php echo ($kegiatan->kso_jo == 'kso/jo') ? '' : ' style="display: none" ';  ?> > 

                            <div class="row">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary" id="btn-add-perusahaan-kso-jo">+ Perusahaan</button>
                                </div>

                                <div class="col-md-9">
                                    <div id="box-perusahaan-kso-jo">

                                        <?php 
                                        $perusahaan_kso_jo = get_perusahaan_kso_jo_to_array($kegiatan->nmp_jasa2); 
                                        foreach($perusahaan_kso_jo as $item_kso_jo) { ?>
                                        <div class="row item-perusahaan-kso-jo" style="margin-bottom: 15px"> 
                                            <div class="col-md-5">
                                                <strong>Nama Penyedia Jasa</strong><br>
                                                <input type="text" class="form-control" name="nmp_jasa2[]" placeholder="Penyedia Jasa" value="<?php echo $item_kso_jo; ?>" /> 
                                            </div>
                                            <div class="col-md-2">
                                                &nbsp; <br>
                                                <button type="button" class="btn btn-danger btn-delete-perusahaan-kso-jo" title="Hapus">-</button>
                                            </div>
                                        </div> 
                                        <?php } ?>


                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>


<!-- Begin perusahaan kso/jo  -->                 
<script type="text/javascript">
$(function() {  
    /* add perusahaan kso/jo */ 
    $('body').on('click','#btn-add-perusahaan-kso-jo', function(){
        $('#box-perusahaan-kso-jo').append($("#new-item-perusahaan-kso-jo").html()); 
        batasi_tahun();
    });

     /* hapus perusahaan kso/jo  */
    $('body').on('click','.btn-delete-perusahaan-kso-jo', function(){
        $(this).parents('.item-perusahaan-kso-jo').remove(); 
    });
});
</script>
<script type="text/html" id="new-item-perusahaan-kso-jo">
    <div class="row item-perusahaan-kso-jo" style="margin-bottom: 15px"> 
        <div class="col-md-5">
            <strong>Nama Penyedia Jasa</strong><br>
            <input type="text" class="form-control" name="nmp_jasa2[]" placeholder="Penyedia Jasa" /> 
        </div>
        <div class="col-md-2">
            &nbsp; <br>
            <button type="button" class="btn btn-danger btn-delete-perusahaan-kso-jo" title="Hapus">-</button>
        </div>
    </div>
</script>
<!-- END perusahaan kso/jo  --> 

                    

                    <!-- row -->
                    <div class="row">                        
                        <div class="form-group col-lg-4">
                            <label for="varchar">Nomor Kontrak <?php echo form_error('no_kontrak_awal') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nomor kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="no_kontrak_awal" id="no_kontrak_awal" placeholder="No Kontrak Awal" value="<?php echo $kegiatan->no_kontrak_awal; ?>" />
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="varchar">Tanggal Kontrak <?php echo form_error('tgl_kontrak_awal') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tanggal kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="date" class="form-control" name="tgl_kontrak_awal" id="tgl_kontrak_awal" placeholder="Tgl Kontrak Awal" value="<?php echo $kegiatan->tgl_kontrak_awal; ?>" />
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="varchar">Nilai Kontrak (Rp)<?php echo form_error('nilai_kontrak') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai rupiah kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="nilai_kontrak" id="nilai_kontrak" placeholder="Nilai Kontrak (Rp)" value="<?php echo $kegiatan->nilai_kontrak; ?>" />
                            <script>
                                $(function() {
                                    $('#nilai_kontrak').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                                })
                            </script>
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="varchar">Nilai Pagu (Rp)<?php echo form_error('nilai_pagu') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai rupiah kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="nilai_pagu" id="nilai_pagu" placeholder="Nilai Pagu (Rp)" value="<?php echo $kegiatan->nilai_pagu; ?>" />
                            <script>
                                $(function() {
                                    $('#nilai_pagu').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                                })
                            </script>
                        </div>
                    </div>
                    <!-- end row -->



                    <!-- row -->
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label for="varchar">Nomor SPMK <?php echo form_error('no_spmk') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi nomer sesuai SPMK kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="no_spmk" id="no_spmk" placeholder="No SPMK" value="<?php echo $kegiatan->no_spmk; ?>" />
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="date">Tanggal SPMK <?php echo form_error('tgl_spmk') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi tanggal sesuai SPMK kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="date" class="form-control" name="tgl_spmk" id="tgl_spmk" placeholder="Tgl Spmk" value="<?php echo $kegiatan->tgl_spmk; ?>" />
                        </div>

                        <div class="col-md-2 form-group">
                            <label for="varchar">Jenis kontrak</label> 
                            <?php 
                            $class = array('class' => "form-control", 'id' => 'penyediajasa_combo');
                            $jenis_kontrak = array('' => '-- Pilih -- ','Lump Sum' => 'Lump Sum', 'Harga Satuan' => 'Harga Satuan');
                                echo form_dropdown('jenis_kontrak', $jenis_kontrak, $kegiatan->jenis_kontrak, $class);
                            ?>
                        </div>
                    </div> 
                    <!-- end row -->

                    <script>
                        $(function() {
                            $('.input-rupiah').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                        })
                    </script>
                 </div>
            </div>

            <script type="text/javascript">
            $(function(){
                $("#kso_jo").on('change',function(){ 
                   
                    var tombol = $(this);
                    if( tombol.prop( "checked" ) ){
                        $('#row-penyedia-jasa-kso-jo').show();

                    }else{
                        $('#row-penyedia-jasa-kso-jo').hide();
                    }
                });
            });
            </script>



            <!-- start waktu pelaksanaan -->
            <!-- fieldset -->
            <fieldset class="well">      
                <strong>Waktu Pelaksanaan :</strong> <hr>
                <!-- row -->
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="varchar">Mulai</label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal pelaksanaan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="date" class="form-control date44" name="waktu_pelaksanaan_mulai" id="waktu_pelaksanaan_start" placeholder="" value="<?php echo $kegiatan->waktu_pelaksanaan_mulai; ?>" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="varchar">Berakhir</label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal pelaksanaan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="date" class="form-control date44" name="waktu_pelaksanaan_akhir" id="waktu_pelaksanaan_end" placeholder="" value="<?php echo $kegiatan->waktu_pelaksanaan_akhir; ?>" />
                    </div>
                    <div class="form-group col-md-2">
                        <label for="varchar">Bulan</label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal pelaksanaan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input  type="text" class="form-control" name="waktu_pelaksanaan_bulan" id="bulan_kalender" placeholder="Bulan" value="<?php echo $kegiatan->waktu_pelaksanaan_bulan; ?>" />
                    </div>
                    <div class="form-group col-md-2">
                        <label for="varchar">Hari (Kalender)</label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal pelaksanaan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="waktu_pelaksanaan_hari" id="hari_kelender" placeholder="Sesuai Hari Kerja Kalender" value="<?php echo $kegiatan->waktu_pelaksanaan_hari; ?>" />
                    </div>
                </div> <!-- end row -->
            </fieldset><hr> <!-- end fieldset -->
            <!-- end waktu pelaksanaan -->

            <script type="text/javascript">
                $(function(){
                    $(".date44").change(function(){
                        var start = $('#waktu_pelaksanaan_start');
                        var end = $('#waktu_pelaksanaan_end');
                        var hari_kelender = $('#hari_kelender'); 
                        var bulan_kelender = $('#bulan_kalender'); 
                        $.ajax({
                            method: "POST",
                            url: "<?php echo site_url('admin/kegiatan/hitung_hari'); ?>",
                            data:{
                            start:start.val(),
                            end:end.val()
                            },
                            dataType: 'json',
                            success: function(respond){
                                if(respond.status == "1"){
                                    hari_kelender.val(respond.d);
                                    bulan_kelender.val(respond.m);
                                }else{
                                    hari_kelender.val('');
                                    bulan_kelender.val('');
                                } 
                            }
                        })
                    });
                });
            </script>

            <script type="text/javascript">
            $(function(){
                $("#combo-tahun-anggaran").change(function(){
                    var tahun = $(this).val();
                    var combo = $('.tahun4');
                    $.each(combo, function (index, itemCombo) {
                        var item = $(itemCombo);
                        var select_temp = item.val();
                        item.empty();
                        item.append(  $('<option>', { value: "",  text: " -- Pilih -- "  }, '</option>'))
                        var i;
                        var pilih_option = false;
                        for (i = parseInt(tahun); i  >= 2001; i--) { 
                            item.append( $('<option>', {  value: i,  text: i }, '</option>'));
                            if(i == parseInt(select_temp)){
                                pilih_option = true;
                            }
                        }
                        if(pilih_option){
                            item.val(select_temp)
                        }
                        
                    }); 
                });
            });
            </script>



            <?php //$this->load->view('admin/kegiatan/part-kegiatan-relasi'); ?> 



            <!-- <div class="well">
                <div class="row">
                    col-md-12
                    <div class="col-md-12">
                        <label for="varchar">Status Kegiatan <?php echo form_error('status_kegiatan') ?></label>
                        <span class="fa fa-question-circle help-popup" data-content="Pilih button status kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <br>
                            <label class="radio-inline">
                              <input type="radio" name="status_kegiatan" value="Belum Mulai" <?php echo set_checked($kegiatan->status_kegiatan,'Belum Mulai'); ?>>Belum Mulai
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_kegiatan" value="On Going" <?php echo set_checked($kegiatan->status_kegiatan,'On Going'); ?>>On Going
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_kegiatan" value="Selesai" <?php echo set_checked($kegiatan->status_kegiatan,'Selesai'); ?>>Selesai
                            </label>
                    </div> end col-md-12
                </div>
            </div> -->

            
            
            <?php $this->load->view('admin/kegiatan/part-kegiatan-produk-laporan'); ?>


 
            <?php $this->load->view('admin/kegiatan/part-kegiatan-laporan'); ?>


            <?php $this->load->view('admin/kegiatan/part-kegiatan-dokumen-kontrak'); ?>



            <?php $this->load->view('admin/kegiatan/part-kegiatan-progres-pekerjaan'); ?>
            <hr>
            <br>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <?php echo form_close(); ?>
            </div>
        </div>
        <!-- end Default box -->
    </div>
</section>



<script>
    $(function() { 
        $('.percent').mask('##0.00%', {reverse: true});
        $('.input_tahun').mask('0000');
    });
</script>

<!-- Manage Dokumen -->
<script type="text/javascript"> 

/*

- Hapus Dokument (file) Dasar Kegiatan, Dokumen Kontrak , Produk (Laporan/Gambar) & Dokumentasi Lainnya
- Dasar Kegiatan
- Dokumen Kontrak 
- Produk (Laporan/Gambar)
- Kajian Lingkungan
- Dokumentasi Lainnya
- Dokumentasi / Progres Gambar 0%
- Dokumentasi / Progres Gambar 50%
- Dokumentasi / Progres Gambar 100%
- Dokumentasi / Progres Gambar Prespektif
- EDIT Produk (Laporan/Gambar)
- EDIT Kajian Lingkungan

*/

/* Hapus Dokument (file) Dasar Kegiatan, Dokumen Kontrak , Produk (Laporan/Gambar) & Dokumentasi Lainnya */
$(function() {

    /* hapus file di multipel */
    $('body').on('click','.btn_hapus_baris_file_dokument',function(e){
        e.preventDefault();
        e.stopPropagation();
        var tombol = $(this);
        swal({
            title: 'Hapus '+ tombol.data('file_name') +'?',
            text: "Setelah terhapus, tidak bisa di batalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!',
            cancelButtonText: 'Batal!' 
        }).then(function () { 
            $.ajax({
                type: 'post',
                url:'<?php echo site_url('admin/kegiatan/delete_file_di_baris'); ?>',  
                data:{
                    id : tombol.data('file'),
                    file_name : tombol.data('file_name')
                },
                dataType: 'json',
                beforeSend: function() { 
                    tombol.parent('.row-file').slideUp(); 
                } 
            }); 
        }); 
    });
    

    /* hapus file 1 doc */
    $('body').on('click','.btn-delete-file',function(e){
        e.preventDefault();
        e.stopPropagation();
        var tombol = $(this);
        swal({
            title: 'Anda yakin mengahpus?',
            text: "Setelah terhapus, tidak bisa di batalkan",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!',
            cancelButtonText: 'Batal!' 
        }).then(function () { 
            $.ajax({
                type: 'post',
                url:'<?php echo site_url('admin/kegiatan/delete_file'); ?>',  
                data:{
                    id : tombol.data('file')
                },
                dataType: 'json',
                beforeSend: function() { 
                    tombol.parents('tr').slideUp();
                } 
            }); 
        }); 
    }); 
});



/* Produk (Laporan/Gambar) */ 
$(function() { 

    var reset_form_add_laporan_gambar = function(){
        $('#dokumen_note2').val('');
        $('#dokumen_volume').val('');
        $('#dokumen_progres').val('');
    }


    var judul = '';
    var data_P          = new Array();
        data_P['id']    = <?php echo $kegiatan->id_kegiatan; ?>; 
    var btnUpload       = $('#btn-pilih-doc-pelaporan');
    var loading         = $('#loadingUploadDocPelaporan');

    var uploadDoc       = new AjaxUpload(btnUpload, {
        action: '<?php echo site_url('admin/kegiatan/upload_doc_pelaporan'); ?>',  
        name: 'userfile[]',
        autoSubmit: false,
        multiple:true,
        data: data_P,
        onSubmit: function(file, ext) {
            if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                alert('Berkas  ' + ext +  ' tidak di izinkan'); 
                return false;
            }else{
                 loading.show(); 
                 btnUpload.hide(); 
            }
        },
        onChange:function(file, ext) {
           if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                alert('Berkas  ' + ext +  ' tidak di izinkan'); 
                return false;
            }else{
                uploadDoc.setData(data_P);
                uploadDoc.submit(); 
                reset_form_add_laporan_gambar();
            }


        },
        onComplete: function(file, response) {
            var dataResult = eval('(' + response + ')');
            console.log(dataResult);
            if (dataResult.status == '1') { 

                var label = '<span class="label label-danger"><?php echo label_status_file(''); ?></span>'; 
                if(dataResult.data.file_name != ''){
                    var label = '<span class="label label-success"><?php echo label_status_file('ada'); ?></span>'; 
                }

                var file = '';

                $.each(dataResult.files, function(index, d){
                    
                    file += '<div class="row-file">';
                    file += '- <a href="<?php echo base_url('uploads/kegiatan/'); ?>' + d.file_name + '">' + d.file_name + '</a> ';
                    file += '<button data-file="'+ dataResult.id +'" data-file_name="'+ d.file_name +'" type="button" class="btn btn-link btn-xs btn_hapus_baris_file_dokument"><small class="text-danger">x</small></button>';
                    file += '</div>';
                });

                $('#tabel-doc-pelaporan > tbody:last-child').append('<tr class="tr-dokumen" data-file="'+ dataResult.id +'"><td class="doc_name">'+ dataResult.data.nama  +'</td><td class="doc_volume_laporan">'+ dataResult.data.volume_laporan +'</td><td class="doc_progres_laporan">'+ dataResult.label_progress +'</td><td class="doc_file">'+ file +'</td><td class="doc_id_progres_laporan hide">'+ dataResult.data.id_progres_laporan +'</td><td class="doc_status">'+ label +'</td><td><button type="button" class="btn btn-xs btn-primary btn-edit-file_laporan_gambar" data-file="'+ dataResult.id +'">edit</button> <button type="button" class="btn btn-xs btn-success new_btn upload_file" data-file="'+ dataResult.id +'">tambah file</button> <button type="button" class="btn btn-xs btn-danger btn-delete-file" data-file="'+ dataResult.id +'">Hapus</button></td></tr>'); 

                //<td>'+ dataResult.data.tanggal +'</td>

               $.each($('.upload_file'),function(){
                    if($(this).hasClass('new_btn')){
                        var id = $(this).parents('.tr-dokumen').data('file');
                        setUploadDoc($(this),id); 
                    } 
                }); 

            }
            else { 
                alert(dataResult.message);  
            }  

            loading.hide(); 
            btnUpload.show();
        }
    });

    /* inisialisasi jquery upload untuk label belum lengkap */
    var setUploadDoc = function(btnUpload,id_file_kegiatan){
        var data_P          = new Array();
            data_P['id']    = id_file_kegiatan;  
        new AjaxUpload(btnUpload, {
            action: '<?php echo site_url('admin/kegiatan/upload_doc_saja'); ?>',  
            name: 'userfile', 
            multiple:false,
            responseType:"json",
            data: data_P,
            onSubmit: function(file, ext) {
                if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                    alert('Berkas tidak di izinkan'); 
                    return false;
                } 
            },
            onChange:function(file, ext) {
                if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                    alert('Berkas tidak di izinkan'); 
                    return false;
                } 
            },
            onComplete: function(file, dataResult) { 
                console.log(dataResult);
                if (dataResult.status == '1') { 

                    var file = '';

                    $.each(dataResult.files, function(index, d){
                        file += '<div class="row-file">';
                        file += '- <a href="<?php echo base_url('uploads/kegiatan/'); ?>' + d.file_name + '">' + d.file_name + '</a> ';
                        file += '<button data-file="'+ id_file_kegiatan +'" data-index="'+ index +'" type="button" class="btn btn-link btn-xs btn_hapus_baris_file_dokument"><small class="text-danger">x</small></button>';
                        file += '</div>';
                    });
                    btnUpload.parents('.tr-dokumen').find('td.doc_file').html(file);

                    //btnUpload.parents('.tr-dokumen').find('td.doc_ico').html('<img src="'+ dataResult.icon_ext +'">');
                    //btnUpload.parents('.tr-dokumen').find('td.doc_size').html(dataResult.data.ukuran); 
                    btnUpload.parents('.tr-dokumen').find('.doc_status').html('<span class="label label-success"><?php echo label_status_file('a'); ?></span>'); 
                }
                else { 
                    alert(dataResult.message);  
                }   
            }
        });
    }


    /* tambah baru Produk (Laporan/Gambar)  */
    $('body').on('click','#btn-pilih-doc-pelaporan2',function(e){
        $('#modalAddProdukLaporanGambar').modal('show')
        $('#dokumen_note2').focus()
    });

    /* handle  simpan Produk (Laporan/Gambar)*/
    $('body').on('click','#btn-saved-laporan-gambar',function(e){
        judul = $('#dokumen_note2').val();
        volume = $('#dokumen_volume').val();
        progres = $('#dokumen_progres').val(); 

        data_P['name'] = judul;
        data_P['volume'] = volume;
        data_P['progres'] = progres; 
        $('#modalAddProdukLaporanGambar').modal('hide');

        // buka pilihan mau upload gambar atau tidak
        pilih_file_laporan_gambar();

    });


    var pilih_file_laporan_gambar = function(e){
        swal({
              title: 'Upload file?',
              //text:"Anda dapat menambahkan file dokumen",
              type: 'question',
              showCancelButton: true,  
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then(function (txt) {
                 console.log("open file composer")  
                 uploadDoc.clickInput();
            }, function(dimsis){
                if(dimsis == 'cancel'){
                    $.ajax({
                        type: 'post',
                        url:'<?php echo site_url('admin/kegiatan/upload_doc_pelaporan_name_only'); ?>',  
                        data:{
                            id : <?php echo $kegiatan->id_kegiatan; ?>,
                            name : judul,
                            volume : volume,
                            progres : progres
                        },
                        dataType: 'json',
                        beforeSend: function() { 
                            loading.show(); 
                        },
                        success: function(dataResult) {  
                             if (dataResult.status == '1') {  

                                reset_form_add_laporan_gambar();

                                var ext = '-'; 
                                if(dataResult.data.file_name != ''){
                                    ext = '<img src="'+ dataResult.icon_ext +'" alt="img">';
                                } 

                                $('#tabel-doc-pelaporan > tbody:last-child').append('<tr class="tr-dokumen" data-file="'+ dataResult.id +'"><td class="doc_name">'+ dataResult.data.nama +'</td><td class="doc_volume_laporan">'+ dataResult.data.volume_laporan +'</td><td class="doc_progres_laporan">'+ dataResult.label_progress +'</td><td class="doc_file"></td><td class="doc_id_progres_laporan hide">'+ dataResult.data.id_progres_laporan +'</td><td class="doc_status">'+ dataResult.label +'</td><td><button type="button" class="btn btn-xs btn-primary btn-edit-file_laporan_gambar" data-file="'+ dataResult.id +'">edit</button> <button type="button" class="btn btn-xs btn-success upload_file new_btn" data-file="'+ dataResult.id +'">tambah file</button> <button type="button" class="btn btn-xs btn-danger btn-delete-file" data-file="'+ dataResult.id +'">Hapus</button></td></tr>'); 

                               $.each($('.upload_file'),function(){
                                    if($(this).hasClass('new_btn')){
                                        var id = $(this).parents('.tr-dokumen').data('file');
                                        setUploadDoc($(this),id); 
                                    } 
                                }); 
                            }
                            else { 
                                alert(dataResult.message);  
                            }  
                            loading.hide(); 
                        }
                    });
                }
            });
    };

    /*$('body').on('click','#btn-pilih-doc-pelaporan2',function(e){
        swal({
          title: 'Nama Dokumen',
          //text:"Tuliskan penjelasan tentang dokumen ini.",
          input: 'textarea',
          showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Lanjutkan →',
            cancelButtonText: 'Batal',
            progressSteps:[1,2],
            currentProgressStep:0,
            inputPlaceholder:"Ketik keterangan file disini",
            reverseButtons:true 
          
        }).then(function (txt) {
            console.log(txt)
            judul = txt;
            swal({
              title: 'Upload file?',
              //text:"Anda dapat menambahkan file dokumen",
              type: 'question',
              showCancelButton: true,
                progressSteps:[1,2],
                currentProgressStep:1, 
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then(function (txt) {
                 console.log("open file composer")  
                 uploadDoc.clickInput();
            }, function(dimsis){
                if(dimsis == 'cancel'){
                    $.ajax({
                        type: 'post',
                        url:'<?php echo site_url('admin/kegiatan/upload_doc_pelaporan_name_only'); ?>',  
                        data:{
                            id : <?php echo $kegiatan->id_kegiatan; ?>,
                            name : judul
                        },
                        dataType: 'json',
                        beforeSend: function() { 
                            loading.show(); 
                        },
                        success: function(dataResult) {  
                             if (dataResult.status == '1') {  
                                var ext = '-';
                                if(dataResult.data.file_name != ''){
                                    ext = '<img src="'+ dataResult.icon_ext +'" alt="img">';
                                } 

                                $('#tabel-doc-pelaporan > tbody:last-child').append('<tr class="tr-dokumen" data-file="'+ dataResult.id +'"><td class="doc_name">'+ dataResult.data.nama +'</td><td class="doc_file"></td><td>'+ dataResult.data.tanggal +'</td><td class="doc_status">'+ dataResult.label +'</td><td><button type="button" class="btn btn-xs btn-primary btn-edit-file" data-file="'+ dataResult.id +'">edit</button> <button type="button" class="btn btn-xs btn-success upload_file new_btn" data-file="'+ dataResult.id +'">tambah file</button> <button type="button" class="btn btn-xs btn-danger btn-delete-file" data-file="'+ dataResult.id +'">Hapus</button></td></tr>'); 

                               $.each($('.upload_file'),function(){
                                    if($(this).hasClass('new_btn')){
                                        var id = $(this).parents('.tr-dokumen').data('file');
                                        setUploadDoc($(this),id); 
                                    } 
                                }); 
                            }
                            else { 
                                alert(dataResult.message);  
                            }  
                            loading.hide(); 
                        }
                    });
                }
            });
        });
    });*/
});



/* Dokument Kontrak */
$(function() { 
    var data_P          = new Array();
        data_P['id']    = <?php echo $kegiatan->id_kegiatan; ?>; 
    var btnUpload       = $('#btn-pilih-doc-kontrak');
    var loading         = $('#loadingUploadDocKontrak');
    var uploadDoc       = new AjaxUpload(btnUpload, {
        action: '<?php echo site_url('admin/kegiatan/upload_doc_kontrak'); ?>',  
        name: 'userfile',
        autoSubmit: false,
        multiple:false,
        data: data_P,
        onSubmit: function(file, ext) {
            if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                alert('Berkas tidak di izinkan'); 
                return false;
            }else{
                 loading.show(); 
                 btnUpload.hide(); 
            }
        },
        onChange:function(file, ext) {
            if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                alert('Berkas tidak di izinkan'); 
                return false;
            }else{ 
                swal({
                  title: 'Keterangan',
                  text:"Tuliskan Nama dokumen & penjelasan tentang dokumen ini.",
                  input: 'textarea',
                  showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Kirim',
                    cancelButtonText: 'Batal',    
                }).then(function (txt) {
                    data_P['name'] = txt;
                    uploadDoc.setData(data_P);
                    uploadDoc.submit(); 
                });   
            }
        },
        onComplete: function(file, response) {
            var dataResult = eval('(' + response + ')'); 
            if (dataResult.status == '1') {  
                 $('#tabel-doc-kontrak > tbody:last-child').append('<tr><td>'+ dataResult.data.nama +'</td><td>'+ dataResult.data.ukuran +'</td><td>'+ dataResult.data.tanggal +'</td><td><img src="'+ dataResult.icon_ext +'" alt="'+ dataResult.data.ekstensi +'"></td><td><button type="button" class="btn btn-xs btn-danger btn-delete-file" data-file="'+ dataResult.id +'">Hapus</button></td></tr>');
            }
            else { 
                alert(dataResult.message);  
            }  
            loading.hide(); 
            btnUpload.show();
        }
    });
});
</script>



<!-- ADD Produk (Laporan/Gambar)  -->
<div class="modal" id="modalAddProdukLaporanGambar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel33">Tambah Produk (Laporan/Gambar)</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="dokumen_note2">Judul Laporan</label>
            <textarea class="form-control" id="dokumen_note2"></textarea>
        </div>
        <div class="form-group">
            <label for="dokumen_volume">Volume Laporan</label>
            <input class="form-control" id="dokumen_volume"/>
        </div>
        <div class="form-group">
            <label for="dokumen_progres">Progres Laporan</label>
            <?php echo combo_progres_laporan(''); ?> 
        </div> 
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="btn-saved-laporan-gambar">Selanjutnya</button> 
      </div>
    </div>
  </div>
</div>


<!-- MODAL EDIT Produk (Laporan/Gambar)  -->
<div class="modal" id="modalEditFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Dokumen</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="dokumen_note">Nama / Keterangan Dokumen</label>
            <textarea class="form-control" id="dokumen_note"></textarea>
        </div>
        <div class="form-group dokumen_tahun_box">
            <label for="dokumen_tahun">Tahun</label>
            <input class="form-control" id="dokumen_tahun"/>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="btn-save-file">Simpan</button> 
      </div>
    </div>
  </div>
</div>    


<!-- MODAL EDIT 2 Produk (Laporan/Gambar)  -->
<div class="modal" id="modalEditLaporanGambar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Dokumen 2</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="dokumen_note3">Nama / Keterangan Dokumen</label>
            <textarea class="form-control" id="dokumen_note3"></textarea>
        </div>
        <div class="form-group">
            <label for="dokumen_volume2">Volume Laporan</label>
            <input class="form-control" id="dokumen_volume2"/>
        </div>
        <div class="form-group">
            <label for="dokumen_progres2">Progres Laporan</label>
            <?php echo combo_progres_laporan('', 'name="dokumen_progres2" class="form-control" id="dokumen_progres2"'); ?> 
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="btn-save-file_laporan_gambar">Simpan</button> 
      </div>
    </div>
  </div>
</div>  



<!-- EDIT Produk (Laporan/Gambar)  -->
<script>
    $(function() { 
        var txt,tahun,doc_volume_laporan,doc_id_progres_laporan;
        var doc_progres_laporan;
        var id_file = 0;


        /* open modal edit modalEditLaporanGambar */
        $('body').on('click','.btn-edit-file_laporan_gambar', function(){ 

            $("#modalEditLaporanGambar").modal('show');

            var btn = $(this);
            txt = btn.parents('.tr-dokumen').find('.doc_name'); 
            id_file = btn.data('file');

            
            doc_volume_laporan = btn.parents('.tr-dokumen').find('.doc_volume_laporan'); 
            doc_id_progres_laporan = btn.parents('.tr-dokumen').find('.doc_id_progres_laporan');
            doc_progres_laporan = btn.parents('.tr-dokumen').find('.doc_progres_laporan');
             
            $("#dokumen_note3").val(txt.html()).focus();  
            $("#dokumen_volume2").val(doc_volume_laporan.html());
            $("#dokumen_progres2").val(doc_id_progres_laporan.html());
        });

        /* simpan data dari modal edit file_laporan_gambar */
        $('body').on('click','#btn-save-file_laporan_gambar', function(){
            var nameT = $("#dokumen_note3").val();
            var volume = $("#dokumen_volume2").val();
            var progres = $("#dokumen_progres2").val();
            $.ajax({
                type: 'post',
                url:'<?php echo site_url('admin/kegiatan/edit_produk_laporan_gambar'); ?>',  
                data:{
                    id : id_file,
                    nama : nameT,
                    volume : volume,
                    progres : progres
                },
                dataType: 'json',
                beforeSend: function() { 
                     $("#dokumen_note3").prop( "disabled", true );
                     $("#btn-save-file_laporan_gambar").prop( "disabled", true );
                },
                success: function(response) { 
                    $("#dokumen_note3").prop( "disabled", false );
                    $("#btn-save-file_laporan_gambar").prop( "disabled", false );

                    id_file = 0; 
                    $("#modalEditLaporanGambar").modal('hide');

                    txt.html($("#dokumen_note3").val()); 
                    
                    doc_progres_laporan.html(response.label_progress);
                    doc_volume_laporan.html($("#dokumen_volume2").val());
                    doc_id_progres_laporan.html($("#dokumen_progres2").val());

                    $("#dokumen_note3").val('');  
                    $("#dokumen_volume2").val('');
                    $("#dokumen_progres2").val('');

                    
                }
            });  
        }); 

        /* open modal edit file */
        $('body').on('click','.btn-edit-file', function(){

            $(".dokumen_tahun_box").hide();
            $("#dokumen_tahun").val('');

            $("#modalEditFile").modal('show');
            var btn = $(this);
            txt = btn.parents('.tr-dokumen').find('.doc_name');

            if(btn.parents('.tr-dokumen').find('.doc_tahun').size()){
                tahun = btn.parents('.tr-dokumen').find('.doc_tahun');
                $("#dokumen_tahun").val(tahun.html());
                $(".dokumen_tahun_box").show();
            }

            id_file = btn.data('file');
            $("#dokumen_note").val(txt.html()).focus();
            
        });

        /* simpan data dari modal edit file */
        $('body').on('click','#btn-save-file', function(){
            var nameT = $("#dokumen_note").val();
            var tahunT = $("#dokumen_tahun").val();
            $.ajax({
                type: 'post',
                url:'<?php echo site_url('admin/kegiatan/edit_file'); ?>',  
                data:{
                    id : id_file,
                    nama : nameT,
                    tahun : tahunT
                },
                dataType: 'json',
                beforeSend: function() { 
                     $("#dokumen_note").prop( "disabled", true );
                     $("#btn-save-file").prop( "disabled", true );
                },
                success: function(response) { 
                    $("#dokumen_note").prop( "disabled", false );
                    $("#btn-save-file").prop( "disabled", false );

                    id_file = 0; 
                    $("#modalEditFile").modal('hide');
                    txt.html($("#dokumen_note").val());
                    tahun.html($("#dokumen_tahun").val());
                    $("#dokumen_note").val('');
                }
            });  
        }); 

        /* inisialisasi jquery upload untuk label belum lengkap */
        var setUploadDoc = function(btnUpload,id_file_kegiatan){
            var data_P          = new Array();
                data_P['id']    = id_file_kegiatan;  
            new AjaxUpload(btnUpload, {
                action: '<?php echo site_url('admin/kegiatan/upload_doc_saja'); ?>',  
                name: 'userfile', 
                multiple:false,
                responseType:"json",
                data: data_P,
                onSubmit: function(file, ext) {
                    if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                        alert('Berkas tidak di izinkan'); 
                        return false;
                    } 
                },
                onChange:function(file, ext) {
                    if (!(ext && /^(gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx)$/.test(ext)) ) {
                        alert('Berkas tidak di izinkan'); 
                        return false;
                    } 
                },
                onComplete: function(file, dataResult) { 
                    console.log(dataResult);
                    if (dataResult.status == '1') { 
                        var file = '';
                        $.each(dataResult.files, function(index, d){
                            file += '<div class="row-file">';
                            file += '- <a href="<?php echo base_url('uploads/kegiatan/'); ?>' + d.file_name + '">' + d.file_name + '</a> ';
                            file += '<button data-file="'+ id_file_kegiatan +'" data-file_name="'+ d.file_name +'" type="button" class="btn btn-link btn-xs btn_hapus_baris_file_dokument"><small class="text-danger">x</small></button>';
                            file += '</div>';
                        });
                        btnUpload.parents('.tr-dokumen').find('td.doc_file').html(file);
                        btnUpload.parents('.tr-dokumen').find('td.doc_ico').html('<img src="'+ dataResult.icon_ext +'">'); 
                        btnUpload.parents('.tr-dokumen').find('td.doc_size').html(dataResult.data.ukuran); 
                        btnUpload.parents('.tr-dokumen').find('.doc_status').html('<span class="label label-success"><?php echo label_status_file('a');  ?></span>'); 
                    }
                    else { 
                        alert(dataResult.message);  
                    }   
                }
            });
        }
        $.each($('.upload_file'),function(){
            var id = $(this).parents('.tr-dokumen').data('file');
            setUploadDoc($(this),id);
        });
    })
</script>


<?php if($this->mainlib->cek_level(['konsultan'])) { ?>
<script type="text/javascript">
    $(function() { 

        $('#nm_kegiatan').attr('readonly', true); 
        $('#deskripsi').attr('readonly', true); 
        $('#sumber_dana').attr('readonly', true); 
        $('#combo-tahun-anggaran').attr('readonly', true); 
        $('#combo-tahun-anggaran2').attr('readonly', true); 
        $('#multi_years').attr('readonly', true); 
        $('#nmp_jasa').attr('readonly', true); 
        $('#kso_jo').attr('readonly', true); 
        $('#no_kontrak_awal').attr('readonly', true); 
        $('#tgl_kontrak_awal').attr('readonly', true); 
        $('#nilai_kontrak').attr('readonly', true); 
        $('#nilai_pagu').attr('readonly', true); 
        $('#no_spmk').attr('readonly', true); 
        $('#tgl_spmk').attr('readonly', true); 
        $('#penyediajasa_combo').attr('readonly', true); 
        $('#waktu_pelaksanaan_start').attr('readonly', true); 
        $('#waktu_pelaksanaan_end').attr('readonly', true); 
        $('#bulan_kalender').attr('readonly', true); 
        $('#hari_kelender').attr('readonly', true); 
        

        $('#btn-add-realisasipelaksanaan').prop( "disabled", true );
        $('#btn-add-perusahaan-kso-jo').prop( "disabled", true );
        $('#box-perusahaan-kso-jo').find('.form-control').prop( "disabled", true );
        $('.btn-delete-perusahaan-kso-jo').prop( "disabled", true );
        
        $('.btn-delete-realisasi').prop( "disabled", true );

    });
</script>
<?php } ?>