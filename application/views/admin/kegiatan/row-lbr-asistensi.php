 <div class="direct-chat-msg right">
  <div class="direct-chat-info clearfix">
    <span class="direct-chat-name pull-left"><?php echo $user_nama; ?></span>
    <span class="direct-chat-timestamp pull-right"><?php echo $tanggal; ?></span>
  </div>
  <!-- /.direct-chat-info -->
  <img class="direct-chat-img" src="<?php echo base_url("assets/img/user.png"); ?>" alt="Foto">
  <!-- /.direct-chat-img -->
  <div class="direct-chat-text">
    <?php echo $isi; ?>

    <?php if(!empty($file)){
    	echo '<br><a href="'. base_url("uploads/lembar_asistensi/" . $file) .'">'.$file.'</a>';
    } ?>
  </div>
  <!-- /.direct-chat-text -->
</div>