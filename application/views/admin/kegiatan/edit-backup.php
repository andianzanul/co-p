<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kegiatan <?php echo $this->uri->segment(3); ?></h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                 <i class="fa fa-times"></i></button>
                </div>
            </div>

            <div class="box-body"> 


            <?php echo form_open(current_url(),'',array(
                'id'=> $kegiatan->id_kegiatan
            )); ?>

            <div class="col-md-12"> 

                <div class="form-group">
                    <label for="varchar">Nama Kegiatan <?php echo form_error('nm_kegiatan') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisikan nama kegiatan lengkap sesuai dengan RKA-K/L ataupun POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input type="text" class="form-control" name="nm_kegiatan" id="nm_kegiatan" placeholder="Nm Kegiatan" value="<?php echo $kegiatan->nm_kegiatan; ?>" />
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="varchar">Jenis Kegiatan <?php echo form_error('jenis_kegiatan') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih jenis kegiatan berupa studi / konstruksi / supervise" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <?php echo form_dropdown('jenis_kegiatan',$jenis_kegiatan_data,$kegiatan->id_jenis_kegiatan,'class="form-control" name="jenis_kegiatan" id="jenis_kegiatan" placeholder="Jenis Kegiatan"'); ?>
                        </div>
                

                        <div class="form-group">
                            <label for="varchar">Tahun Anggaran <?php echo form_error('nama_tahun') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih tahun anggaran pelaksanaan kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <?php
                                $class = array(
                                    'class' => "form-control"
                                );

                                $tahun = array('' => '-- Pilih -- ');
                                for ($t = 2001; $t <= 2031; $t++) {
                                    $tahun[$t] = $t;
                                }

                                echo form_dropdown('nama_tahun', $tahun, $kegiatan->tahun, $class);
                                ?>   
                        </div>
                

                        <div class="form-group">
                            <label for="varchar">Satuan Kerja <?php echo form_error('nama_satker') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih satuan kerja sesuai DIPA" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <?php echo form_dropdown('nama_satker',$jenis_satker_data,$kegiatan->id_satker,'class="form-control" name="nama_satker" id="nama_satker" placeholder="Satker"'); ?>
                        </div>

                        <div class="form-group">
                            <label for="varchar">Nama PPK<?php echo form_error('nama_ppk') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih PPK sesuai POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <select class="form-control" name="nama_ppk" id="nama_ppk">
                                <option value="">-- pilih --</option>
                                <?php foreach($jenis_ppk_data2 as $ky => $k){ ?>
                                <option <?php echo set_selected($k->id_ppk, $kegiatan->id_ppk ); ?>class="satker-<?php echo $k->id_satker; ?>" value="<?php echo $k->id_ppk; ?>"><?php echo $k->nama_ppk; ?></option>
                                <?php } ?>
                            </select> 
                            
                        </div>

                        <script type="text/javascript">
                            /* PILIH  */
                            $(document).ready(function() {
                                var showhide_mapel_options = function(input){
                                    $("#nama_ppk").find("option").hide(); 
                                    $("#nama_ppk").find(".satker-" + input.val()).show();  

                                    if(input.val() == "0"){
                                        $("#nama_ppk").prop( "disabled", true );
                                    }else{
                                        $("#nama_ppk").prop( "disabled", false );
                                    }
                                }

                                $("body").on('change','#nama_satker',function(e){
                                     
                                    var input = $(this);
                                    showhide_mapel_options(input);
                                    //$("#nama_ppk").find("option").hide(); 
                                    //$("#nama_ppk").find(".satker-" + input.val()).show(); 
                                    $("#nama_ppk").val("");
                                });

                                showhide_mapel_options($('#nama_satker'));

                                
                            });
                        </script>

                        <div class="form-group">
                            <label for="varchar">Kegiatan <?php echo form_error('nama_bidang') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai dengan ADIK pada RKA-K/L" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <?php echo form_dropdown('nama_bidang',$jenis_bidang_data,$kegiatan->id_bidang,'class="form-control" name="nama_bidang" id="nama_bidang" placeholder="Bidang"'); ?>
                        </div>

                        <div class="form-group">
                            <label for="varchar">Output<?php echo form_error('nama_output') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai Kegiatan dengan ADIK pada RKA-K/L" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <select class="form-control" name="nama_output" id="nama_output">
                                <option value=""> -- pilih -- </option>
                                <?php foreach($jenis_output_data2 as $ky => $k){ ?>
                                <option <?php echo set_selected($k->id_output, $kegiatan->id_output ); ?>class="bidang-<?php echo $k->id_bidang; ?>" value="<?php echo $k->id_output; ?>"><?php echo $k->nama_output; ?></option>
                                <?php } ?>
                            </select> 
                            
                        </div>


                        <script type="text/javascript">
                            /* PILIH  */
                            $(document).ready(function() {
                                var showhide_output_options = function(input){
                                    $("#nama_output").find("option").hide(); 
                                    $("#nama_output").find(".bidang-" + input.val()).show();  

                                     if(input.val() == "0"){
                                        $("#nama_output").prop( "disabled", true );
                                    }else{
                                        $("#nama_output").prop( "disabled", false );
                                    }
                                }

                                $("body").on('change','#nama_bidang',function(e){ 
                                    var input = $(this); 
                                    $("#nama_output").val("");
                                    showhide_output_options( input );
                                });

                                showhide_output_options($('#nama_bidang'));

                                
                            });
                        </script>

                        <div class="form-group">
                            <label for="varchar">Jenis Konstruksi <?php echo form_error('jenis_konstruksi') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content=" Diisikan jenis konstruksi seperti : tanggul / seawall/ revetmen / pasangan batu / beton / pre cast dll" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="jenis_konstruksi" id="jenis_konstruksi" placeholder="Jenis Konstruksi" value="<?php echo $kegiatan->jenis_konstruksi; ?>" />
                        </div>

                
                        <div class="form-group">
                            <b>Provinsi</b>  <span class="fa fa-question-circle help-popup" data-content="dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span><br>
                             <?php foreach ($provinsi2 as $key_pr => $prov) { ?>
                                <label class="checkbox-inline"><input class="cek-provinsi" <?php 

                                echo (in_array($prov->id_provinsi, $provinsinya)) ? set_checked(1,1) : '';

                                 ?> type="checkbox" name="prov[<?php echo $prov->id_provinsi; ?>]" value="<?php echo $prov->id_provinsi; ?>" data-kegiatan="<?php echo $kegiatan->id_kegiatan; ?>" data-nama="<?php echo $prov->provinsi; ?>"><?php echo $prov->provinsi; ?> </label>
                            <?php  } ?>
                        </div>

                        <div id="box-lokasi">
                             <?php echo $lokasi; ?>

                        </div>

                        <hr>  
                        <fieldset class="well">
                            <legend>Koordinat:</legend>
                             
                            <strong>Koordinat</strong>  <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai koordinat letak geografis lokasi kegiatan (titik Awal dan akhir)" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>    
                            <button type="button" class="btn btn-primary" id="btn-add-koordinat">+</button>
                        
                            <div id="area-koordinat">
                                <?php foreach($koordinats as $koordinat) { ?>
                                    <div class="form-group well item-koordinat">
                                        <strong>Koordinat Awal</strong>
                                        <button class="btn btn-danger btn-sm btn-hapus-koordinat2" type="button" data-k="<?php echo $kegiatan->id_kegiatan; ?>" data-id="<?php echo $koordinat->id_koordinat; ?>">Hapus</button>
                                        <input type="hidden" name="koor[]"  value="<?php echo $koordinat->id_koordinat; ?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="varchar">Koordinat x <?php echo form_error('koordinat_x') ?></label>
                                                                <input type="text" class="form-control" name="k_x[]"   placeholder="Koordinat X awal" value="<?php echo $koordinat->koordinat_x; ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="varchar">Koordinat y <?php echo form_error('koordinat_y') ?></label>
                                                                <input type="text" class="form-control" name="k_y[]"   placeholder="Koordinat Y awal" value="<?php echo $koordinat->koordinat_y; ?>" />
                                                            </div>
                                                        </div> 
                                                    </div>
                                                        
                                                    <strong>Koordinat Akhir</strong>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="varchar">Koordinat x <?php echo form_error('koordinat_x1') ?></label>
                                                                <input type="text" class="form-control" name="k_x1[]"   placeholder="Koordinat X akhir" value="<?php echo $koordinat->koordinat_x1; ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="varchar">Koordinat y <?php echo form_error('koordinat_y1') ?></label>
                                                                <input type="text" class="form-control" name="k_y1[]"   placeholder="Koordinat Y akhir" value="<?php echo $koordinat->koordinat_y1; ?>" />
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>  
                                    </div>
                                <?php } ?>
                            </div>
                        
                            <script>
                                var jumlah_koordinat = <?php echo count($koordinats); ?>;
                            </script>

                            <script type="text/html" id="new-item-koordinat">
                                <div class="form-group well item-koordinat">
                                    <strong>Koordinat Awal</strong>
                                    <button class="btn btn-danger btn-sm btn-hapus-koordinat" type="button">Hapus</button>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="varchar">Koordinat x <?php echo form_error('koordinat_x') ?></label>
                                                        <input type="text" class="form-control" name="koordinat_x[]"   placeholder="Koordinat X awal" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="varchar">Koordinat y <?php echo form_error('koordinat_y') ?></label>
                                                        <input type="text" class="form-control" name="koordinat_y[]"   placeholder="Koordinat Y awal" value="" />
                                                    </div>
                                                </div> 
                                            </div>
                                                
                                            <strong>Koordinat Akhir</strong>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="varchar">Koordinat x <?php echo form_error('koordinat_x1') ?></label>
                                                        <input type="text" class="form-control" name="koordinat_x1[]"   placeholder="Koordinat X akhir" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="varchar">Koordinat y <?php echo form_error('koordinat_y1') ?></label>
                                                        <input type="text" class="form-control" name="koordinat_y1[]"   placeholder="Koordinat Y akhir" value="" />
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </script>
                        </fieldset>

                        <div class="form-group">
                            <label for="varchar">Wilayah Sungai <?php echo form_error('nama_wlsungai') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <?php echo form_dropdown('nama_wlsungai',$jenis_wlsungai_data,$kegiatan->id_wlsungai,'class="form-control" name="nama_wlsungai" id="nama_wlsungai placeholder="wlsungai"'); ?>
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">DAS<?php echo form_error('das') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan sesuai dengan wilayah sungai" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="das" id="das" placeholder="Das" value="<?php echo $kegiatan->das; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Tujuan<?php echo form_error('tujuan') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual sesuai dengan KAK Usulan Kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <textarea class="form-control" id="tujuan" name="tujuan"  rows="4"><?php echo $kegiatan->tujuan; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="varchar">Manfaat<?php echo form_error('manfaat') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual sesuai dengan KAK Usulan Kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <textarea class="form-control" id="manfaat" name="manfaat"  rows="4"><?php echo $kegiatan->manfaat; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="varchar">Volume Output<?php echo form_error('output') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka) dan satuan sesuai dengan capaian kerja" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <textarea class="form-control" id="output" name="output"  rows="2"><?php echo $kegiatan->output; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="varchar">Volume Outcome<?php echo form_error('outcome') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa besaran nilai (angka) dan satuan sesuai dengan capaian kerja" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <textarea class="form-control" id="outcome" name="outcome"  rows="2"><?php echo $kegiatan->outcome; ?></textarea>
                        </div>

                    </div>
         
<!-- sampai sini -->
                    <div class="col-md-4">

                        <div class="form-group">
                            <label for="varchar">Data Teknis / Uraian Kegiatan<?php echo form_error('data_teknis') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa uraian pelengkap kegiatan seperti lebar saluran, volume tampungan, tinggi tanggul dll" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <textarea class="form-control" id="data_teknis" placeholder="Data Teknis / Uraian Kegiatan" name="data_teknis"  rows="4"><?php echo $kegiatan->data_teknis; ?></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Kegiatan Lanjutan<?php echo form_error('data_kegiatann') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa rencana kegiatan selanjutnya yang perlu dilaksanakan pada lokasi terkait" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="data_kegiatan" id="data_kegiatan" placeholder="Data lanjutan" value="<?php echo $kegiatan->data_kegiatan; ?>" />
                        </div>

                        <div class="form-group">
                            <label for="varchar">Tahun Kegiatan Lanjutan<?php echo form_error('th_kegiatanlanjutan') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa rencana tahun anggaran" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="th_kegiatanlanjutan" id="th_kegiatanlanjutan" placeholder="Tahun Kegiatan lanjutan" value="<?php echo $kegiatan->th_kegiatanlanjutan; ?>" />
                        </div>

                        <div class="form-group">
                            <label for="varchar">Sumber Dana<?php echo form_error('sumber_dana') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual berupa asal sumber dana seperti APBN / APBN-P /PHLN / SBSN" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="sumber_dana" id="sumber_dana" placeholder="Sumber Dana" value="<?php echo $kegiatan->sumber_dana; ?>" />
                        </div>

                        <div class="form-group">
                            <label for="varchar">Status Kegiatan<?php echo form_error('status_kegiatan') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual On Going / Sudah Selesai" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="status_kegiatan" id="status_kegiatan" placeholder="Status Kegiatan" value="<?php echo $kegiatan->status_kegiatan; ?>" />
                        </div> 

                        <div class="form-group">
                            <label for="varchar">Dasar Kegiatan<?php echo form_error('dasar_kegiatan') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content=" Diisi secara manual berupa uraian singkat  kegiatan tersebut apakah usulan daeri daerah (gubernur/bupati/camat/kepala desa)" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="dasar_kegiatan" id="dasar_kegiatan" placeholder="Dasar Kegiatan" value="<?php echo $kegiatan->dasar_kegiatan; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Penyedia Jasa<?php echo form_error('nmp_jasa') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual nama perusahaan penyedia jasa konstruksi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="nmp_jasa" id="nmp_jasa" placeholder="Penyedia Jasa" value="<?php echo $kegiatan->nmp_jasa; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="varchar">Perencana<?php echo form_error('perencana') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual nama perusahaan perencanaan kegiatan terkait" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="perencana" id="perencana" placeholder="Perencana" value="<?php echo $kegiatan->perencana; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Supervisi<?php echo form_error('supervisi') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi secara manual nama perusahaan penyedia jasa supervisi konstruksi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="supervisi" id="supervisi" placeholder="Supervisi" value="<?php echo $kegiatan->supervisi; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Tanggal Kontrak <?php echo form_error('tgl_kontrak_awal') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tanggal kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="date" class="form-control" name="tgl_kontrak_awal" id="tgl_kontrak_awal" placeholder="Tgl Kontrak Awal" value="<?php echo $kegiatan->tgl_kontrak_awal; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">No Kontrak<?php echo form_error('no_kontrak_awal') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nomor kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="no_kontrak_awal" id="no_kontrak_awal" placeholder="No Kontrak Awal" value="<?php echo $kegiatan->no_kontrak_awal; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Nilai Pagu *Khusus Swakelola<?php echo form_error('nilai_pagu') ?>
                                <span class="fa fa-question-circle help-popup" data-content="Diisikan nama kegiatan lengkap sesuai dengan RKA-K/L ataupun POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            </label>
                            <input type="text" class="form-control" name="nilai_pagu" id="nilai_pagu" placeholder="Nilai Pagu *khusus swakelola" value="<?php echo $kegiatan->nilai_pagu; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="varchar">Nilai Kontrak<?php echo form_error('nilai_kontrak') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai rupiah kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="nilai_kontrak" id="nilai_kontrak" placeholder="Nilai Pagu" value="<?php echo $kegiatan->nilai_kontrak; ?>" />
                        </div>
                        
                        <div class="form-group">
                            <label for="date">Tanggal SPMK <?php echo form_error('tgl_spmk') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi tanggal sesuai SPMK kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="date" class="form-control" name="tgl_spmk" id="tgl_spmk" placeholder="Tgl Spmk" value="<?php echo $kegiatan->tgl_spmk; ?>" />
                        </div>
                            
                        <div class="form-group">
                            <label for="varchar">No SPMK <?php echo form_error('no_spmk') ?></label>
                            <span class="fa fa-question-circle help-popup" data-content="Diisi nomer sesuai SPMK kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                            <input type="text" class="form-control" name="no_spmk" id="no_spmk" placeholder="No Spmk" value="<?php echo $kegiatan->no_spmk; ?>" />
                        </div>
                    </div>

        <!-- sampai sini -->

        <div class="col-md-4">        
            <fieldset class="well">
                <legend>Amandemen:</legend>
             
                <strong>Amandemen</strong>
                <button type="button" class="btn btn-primary btn-add-amd">+</button>
                
                <script type="text/javascript">
                    var jumlah_amd = <?php echo count($amandemen) ?>;
                </script>
        
                <div class="box-amd">
                <?php foreach ($amandemen as $key => $amd) { ?>
                    <div class="item-amd">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal Amandemen</label>
                                            <span class="fa fa-question-circle help-popup" data-content="Dipilih sesuai tanggal amandemen kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                                            <input type="date" class="form-control" name="amd_e[<?php echo $amd->id_amd; ?>][tanggal]"  value="<?php echo $amd->tanggal; ?>" />
                                        </div>
                                    </div> 
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nomor Amandemen</label>
                                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nomor amandemen surat kontrak kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                                            <input type="text" class="form-control" name="amd_e[<?php echo $amd->id_amd; ?>][nomor]"  value="<?php echo $amd->nomor; ?>" />
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Nilai Amandemen</label>
                                            <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai rupiah amandemen kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                                            <input type="text" class="form-control" name="amd_e[<?php echo $amd->id_amd; ?>][nilai]"  value="<?php echo $amd->nilai; ?>" />
                                        </div>
                                    </div>   
                                </div>
                            </div>

                            <div class="col-md-1">
                                <label> &nbsp;</label><br />
                                <button data-id="<?php echo $amd->id_amd; ?>" type="button" class="btn btn-danger btn-delete-amd">-</button>
                            </div>
                        </div>  
                    </div>
                <?php } ?>
                </div>
           
            </fieldset>
        

            <fieldset class="well">
                <legend>Progress</legend>

                <div class="form-group">
                    <label for="varchar">Tanggal Penyerapan <?php echo form_error('tgl_penyerapan') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal penyerapan anggaran per tanggal penginputan data" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input type="date" class="form-control" name="tgl_penyerapan" id="tgl_penyerapan" placeholder="tgl_penyerapan" value="<?php echo $kegiatan->tgl_penyerapan; ?>" />
                </div>

                <div class="form-group">
                    <label for="varchar">Nilai Penyerapan (Rp)<?php echo form_error('nilai_penyerapan') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai rupiah penyerapan anggaran per tanggal penginputan data" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input id="nilai_penyerapan" type="text" class="form-control" name="nilai_penyerapan" id="nilai_penyerapan" placeholder="Nilai Penyerapan Rp" value="<?php echo $kegiatan->nilai_penyerapan; ?>" />
                </div>

                <script>
                    $(function() {
                        $('#nilai_penyerapan').maskMoney({prefix:'Rp.', thousands:'.', decimal:',', precision:0});;

                        $("[data-mask]").inputmask();
                    })
                </script>


            <!-- data-inputmask="'mask': ['99,00', '9,00']" data-mask  -->
                <div class="form-group">
                    <label for="varchar">Keuangan Persen (%) <?php echo form_error('keuangan_persen') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen penyerapan anggaran per tanggal penginputan data" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input  type="text" class="form-control" name="keuangan_persen" id="keuangan_persen" placeholder="Keuangan Persen" value="<?php echo $kegiatan->keuangan_persen; ?>" />
                </div>
             

                <div class="form-group">
                    <label for="varchar">fisik Persen (%) <?php echo form_error('fisik_persen') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen kegiatan fisik per tanggal penginputan data" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input type="text" class="form-control" name="fisik_persen" id="fisik_persen" placeholder="fisik Persen" value="<?php echo $kegiatan->fisik_persen; ?>" />
                </div>

            </fieldset>

            <div class="form-group">
                <label for="varchar">Jangka Waktu Pelaksanaan (*hari)<?php echo form_error('jkwkt_pelaksanaan') ?></label>  <span class="fa fa-question-circle help-popup" data-content="Diisikan nama kegiatan lengkap sesuai dengan RKA-K/L ataupun POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input type="text" class="form-control" name="jkwkt_pelaksanaan" id="jkwkt_pelaksanaan" placeholder="Jkwkt Pelaksanaan" value="<?php echo $kegiatan->jkwkt_pelaksanaan; ?>" />
            </div>
           <!--  <div class="form-group">
                <label for="int">Jumlah Hari <?php echo form_error('jml_hari') ?></label>
                <input type="text" class="form-control" name="jml_hari" id="jml_hari" placeholder="Jml Hari" value="<?php echo $kegiatan->jml_hari; ?>" />
            </div> -->
           
            <div class="form-group">
                <label for="varchar">Video Url <?php echo form_error('youtube_url') ?></label>  <span class="fa fa-question-circle help-popup" data-content="Diisikan nama kegiatan lengkap sesuai dengan RKA-K/L ataupun POK" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input type="text" class="form-control" name="youtube_url" id="youtube_url" placeholder="Youtube Url" value="<?php echo $kegiatan->youtube_url; ?>" />
            </div> 
             <div class="form-group">
                <label for="varchar">Progress Gambar</label>  <span class="fa fa-question-circle help-popup" data-content="Upload gambar dokumentasi kegiatan dengan rincian 0, 50 dan 100 %." data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
            </div> 


            <div class="pilih gambar">
                <div class="row">
                    <div class="col-md-4 col-gambar"> 
                    
                        <div class="list-gambar">
                             <?php
                                
                                if (array_key_exists(0, $images)) {
                                        $class=$img=$link="";
                                        foreach($images[0] as $image){
                                            $img=site_url("thumb?w=150&h=150&src=".base_url("uploads/kegiatan/".$image->nama_file));
                                            $link = site_url('admin/kegiatan/hapus_gbr/' . $image->id_gambar);
                                            ?>
                                            <div class="box-btn-gambar">
                                                <div class="gambar"><img src="<?php echo $img; ?>"></div> 
                                                <a href="<?php echo $link; ?>" class="btn btn-danger btn-xs btn-hapus">hapus</a> 
                                            </div>

                                            <?php
                                        }
                                        
                                } 
                            ?> 
                        </div>
                        <span class="loading" style="display:none">loading.....</span>
                        <button type="button" class="btn btn-success btn-pilih" id="btnpilih0">pilih 0%</button>

                    </div>

           
                    <div class="col-md-4 col-gambar">
                        <div class="list-gambar">
                             <?php
                                
                                if (array_key_exists(50, $images)) {
                                        $class=$img=$link="";
                                        foreach($images[50] as $image){
                                            $img=site_url("thumb?w=150&h=150&src=".base_url("uploads/kegiatan/".$image->nama_file));
                                            $link = site_url('admin/kegiatan/hapus_gbr/' . $image->id_gambar);
                                            ?>
                                            <div class="box-btn-gambar">
                                                <div class="gambar"><img src="<?php echo $img; ?>"></div> 
                                                <a href="<?php echo $link; ?>" class="btn btn-danger btn-xs btn-hapus">hapus</a> 
                                            </div>

                                            <?php
                                        }
                                        
                                } 
                            ?> 
                        </div>
                        <span class="loading" style="display:none">loading.....</span>
                        <button type="button" class="btn btn-success btn-pilih" id="btnpilih1">pilih 50%</button>


                         
                    </div>

                    <div class="col-md-4 col-gambar">
                        <div class="list-gambar">
                             <?php
                                
                                if (array_key_exists(100, $images)) {
                                        $class=$img=$link="";
                                        foreach($images[100] as $image){
                                            $img=site_url("thumb?w=150&h=150&src=".base_url("uploads/kegiatan/".$image->nama_file));
                                            $link = site_url('admin/kegiatan/hapus_gbr/' . $image->id_gambar);
                                            ?>
                                            <div class="box-btn-gambar">
                                                <div class="gambar"><img src="<?php echo $img; ?>"></div> 
                                                <a href="<?php echo $link; ?>" class="btn btn-danger btn-xs btn-hapus">hapus</a> 
                                            </div>

                                            <?php
                                        }
                                        
                                } 
                            ?> 
                        </div>
                        <span class="loading" style="display:none">loading.....</span>
                        <button type="button" class="btn btn-success btn-pilih" id="btnpilih2">pilih 100%</button>

                         

                    </div>
                </div>
            </div>

            <br>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
</div>
</div>
</div>
</section>


<script type="text/javascript">
  
  $(function() {
    var btnUpload  = $('#btnpilih0');
    var status    = btnUpload.parent(".col-gambar").find(".loading");
   
    var data_P    = new Array();
       data_P['id']= <?php echo $kegiatan->id_kegiatan; ?>;
       data_P['p']= 0;

    new AjaxUpload(btnUpload, {
      action: '<?php echo site_url('admin/kegiatan/do_upload'); ?>',
      name: 'userfile',
      multiple: false,
      jpgOnly:true,
      data: data_P,
      onSubmit: function(file, ext) {
        if (!(ext && /^(jpg|jpeg|png)$/.test(ext))) {
          alert('file tidak diizinkan!!!');
          return false;
        }else{
        btnUpload.hide();
         status.show();

        }
      },
      onComplete: function(file, response) {
        
        var dataResult = eval('(' + response + ')');
        if (dataResult.status === "ok") {
            
            btnUpload.show();
            status.hide();
            var str = '<div class="box-btn-gambar">'+
            '<div class="gambar"><img src="'+ dataResult.full_url +'" alt="Image"/></div> '+
            '<a href="'+ dataResult.link_hapus +'" class="btn btn-danger btn-xs btn-hapus">hapus</a> ' + 
            '</div>'
            ;

            btnUpload.parent(".col-gambar").find(".list-gambar").append(str);
        }
        else {
          alert(dataResult.message);
        }

        btnUpload.show();
            status.hide();

      }
    });
  });  
</script>


<script type="text/javascript">
  
  $(function() {
    var btnUpload  = $('#btnpilih1');
    var status    = btnUpload.parent(".col-gambar").find(".loading");
   
    var data_P    = new Array();
       data_P['id']= <?php echo $kegiatan->id_kegiatan; ?>;
       data_P['p']= 50;

    new AjaxUpload(btnUpload, {
      action: '<?php echo site_url('admin/kegiatan/do_upload'); ?>',
      name: 'userfile',
      multiple: false,
      jpgOnly:true,
      data: data_P,
      onSubmit: function(file, ext) {
        if (!(ext && /^(jpg|jpeg|png)$/.test(ext))) {
          alert('file tidak diizinkan!!!');
          return false;
        }else{
        btnUpload.hide();
         status.show();

        }
      },
      onComplete: function(file, response) {
        
        var dataResult = eval('(' + response + ')');
        if (dataResult.status === "ok") {
            
            btnUpload.show();
            status.hide();
            var str = '<div class="box-btn-gambar">'+
            '<div class="gambar"><img src="'+ dataResult.full_url +'" alt="Image"/></div> '+
            '<a href="'+ dataResult.link_hapus +'" class="btn btn-danger btn-xs btn-hapus">hapus</a> ' + 
            '</div>'
            ;

            btnUpload.parent(".col-gambar").find(".list-gambar").append(str);
        }
        else {
          alert(dataResult.message);
        }

        btnUpload.show();
            status.hide();

      }
    });
  });  
</script>


<script type="text/javascript">
   
  $(function() {
    var btnUpload  = $('#btnpilih2');
    var status    = btnUpload.parent(".col-gambar").find(".loading");
   
    var data_P    = new Array();
       data_P['id']= <?php echo $kegiatan->id_kegiatan; ?>;
       data_P['p']= 100;

    new AjaxUpload(btnUpload, {
      action: '<?php echo site_url('admin/kegiatan/do_upload'); ?>',
      name: 'userfile',
      multiple: false,
      jpgOnly:true,
      data: data_P,
      onSubmit: function(file, ext) {
        if (!(ext && /^(jpg|jpeg|png)$/.test(ext))) {
          alert('file tidak diizinkan!!!');
          return false;
        }else{
        btnUpload.hide();
         status.show();

        }
      },
      onComplete: function(file, response) {
        
        var dataResult = eval('(' + response + ')');
        if (dataResult.status === "ok") {
            
            btnUpload.show();
            status.hide();
            var str = '<div class="box-btn-gambar">'+
            '<div class="gambar"><img src="'+ dataResult.full_url +'" alt="Image"/></div> '+
            '<a href="'+ dataResult.link_hapus +'" class="btn btn-danger btn-xs btn-hapus">hapus</a> ' + 
            '</div>'
            ;

            btnUpload.parent(".col-gambar").find(".list-gambar").append(str);
        }
        else {
          alert(dataResult.message);
        }

        btnUpload.show();
            status.hide();

      }
    });
  });  
</script>
<?php 



/* <!-- tes tes -->

<script type="text/javascript">

$(function(){

$.ajaxSetup({
type:"POST",
url: "<?php echo base_url('admin/kegiatan/update') ?>",
cache: false,
});

$("#provinsi").change(function(){

var value=$(this).val();
if(value>0){
$.ajax({
data:{modul:'kabupaten',id:value},
success: function(respond){
$("#kabupaten-kota").html(respond);
}
})
}

});


$("#kabupaten-kota").change(function(){
var value=$(this).val();
if(value>0){
$.ajax({
data:{modul:'kecamatan',id:value},
success: function(respond){
$("#kecamatan").html(respond);
}
})
}
})

$("#kecamatan").change(function(){
var value=$(this).val();
if(value>0){
$.ajax({
data:{modul:'kelurahan',id:value},
success: function(respond){
$("#kelurahan-desa").html(respond);
}
})
} 
})

})

</script>


<div class='form-group'>
<label>Provinsi</label>
<select class='form-control' id='provinsi'>
<option value='0'>--pilih--</option>
<?php 
foreach ($provinsi as $prov) {
echo "<option value='$prov[id_provinsi]'>$prov[provinsi]</option>";
}
?>
</select>
</div>

<div class='form-group'>
<label>Kabupaten/kota</label>
<select class='form-control' id='kabupaten-kota'>
<option value='0'>--pilih--</option>
</select>
</div>


<div class='form-group'>
<label>Kecamatan</label>
<select class='form-control' id='kecamatan'>
<option value='0'>--pilih--</option>
</select>
</div>


<div class='form-group'>
<label>Kelurahan/desa</label>
<select class='form-control' id='kelurahan-desa'>
<option value='0'>--pilih--</option>
</select>
</div>
<!-- tes tes -->*/








 ?>



