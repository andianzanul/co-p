<div class="well well-sm item-lokasi lokasi-<?php echo $prov; ?>">
    <b><?php echo $nama; ?></b>
    
    <div class="row row-lokasi">

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kabupaten/Kota <?php echo form_error('kabupaten') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    
                <select class="form-control combo_kabupaten" name="kabupaten[<?php echo $prov; ?>][]" id="kabupaten-<?php echo $prov; ?>">
                    <option value="">-- Pilih --</option>
                	<?php foreach($kabupaten as $kab) { ?>
                	<option <?php echo set_selected($kab->id, $id_kabupaten); ?> value="<?php echo $kab->id; ?>"><?php echo $kab->name; ?></option>
                	<?php } ?>
                </select>
            </div>
         </div>

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kecamatan <?php echo form_error('kecamatan') ?></label>
                <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php echo combo_kecamatan($id_kecamatan, $id_kabupaten, ' name="kecamatan['. $prov .'][]" class="form-control combo-kecamatan"'); ?> 
                <?php /*<input type="text" class="form-control" name="kecamatan[<?php echo $prov; ?>][]" id="kecamatan" placeholder="kecamatan" value="<?php echo $kecamatan; ?>" />*/ ?>
           </div>
         </div>

         <div class="col-md-3">
             <div class="form-group">
                <label for="varchar">Kelurahan/Desa<?php echo form_error('desa') ?></label>
                <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php echo combo_kelurahan($id_kelurahan, $id_kecamatan, ' name="desa['. $prov .'][]" class="form-control combo-kelurahan" id="kelurahan-'. $id_kabupaten .'" '); ?>
                <?php /*<input type="text" class="form-control" name="desa[<?php echo $prov; ?>][]" id="desa" placeholder="Desa" value="<?php echo $desa; ?>" />*/ ?>
            </div>
         </div>

         <div class="col-md-1">
         <label>&nbsp;</label> <br>
             <button type="button" class="btn btn-danger btn-hapus-lokasi-123">-</button>   
         </div>

    </div> 


    <script class="lokasi-tambah-daya" type="text/html">
        <div class="row row-lokasi">

             <div class="col-md-4">
                 <div class="form-group">
                    <label for="varchar">Kabupaten/Kota <?php echo form_error('kabupaten') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <select class="form-control combo_kabupaten" name="kabupaten[<?php echo $prov; ?>][]" id="kabupaten-<?php echo $prov; ?>">
                        <option value="">-- Pilih --</option>
                        <?php foreach($kabupaten as $kab) { ?>
                        <option value="<?php echo $kab->id; ?>"><?php echo $kab->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
             </div>

             <div class="col-md-4">
                 <div class="form-group">
                    <label for="varchar">Kecamatan <?php echo form_error('kecamatan') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <?php echo combo_kecamatan( '', '', ' name="kecamatan['. $prov .'][]" class="form-control combo-kecamatan"'); ?>
                    <?php /*<input type="text" class="form-control" name="kecamatan[<?php echo $prov; ?>][]" id="kecamatan" placeholder="kecamatan" value="" />*/ ?>
               </div>
             </div>

             <div class="col-md-3">
                 <div class="form-group">
                    <label for="varchar">Kelurahan/Desa<?php echo form_error('desa') ?></label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <?php echo combo_kelurahan( '', '', ' name="desa['. $prov .'][]" class="form-control combo-kelurahan"'); ?>
                    <?php /*<input type="text" class="form-control" name="desa[<?php echo $prov; ?>][]" id="desa" placeholder="Desa" value="" />*/ ?>
                </div>
             </div>

             <div class="col-md-1">
             <label>&nbsp;</label> <br>
                 <button type="button" class="btn btn-warning btn-hapus-lokasi-123">-</button>   
             </div>

        </div>
    </script> 

    
        
     <div class="box-lokasi">
         
     </div>   

     <button type="button" class="btn btn-primary btn-add-lokasi">+</button>   

</div> 