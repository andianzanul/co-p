<!-- start manage dokument kontrak -->
            <div class="row"> 
                <div class="col-md-12"> 
                    <?php /*<div class="form-group">
                        <label for="varchar"><i>Dokumen Kontrak</i></label>
                        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal dokumentasi kontrak" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                        <input type="text" class="form-control" name="" id="" placeholder="Dokumen Kontrak" value="" />
                    </div>*/ ?> 
                    <label for="varchar">Dokumen Kontrak</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai tanggal dokumentasi kontrak" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>

                    <button class="btn btn-primary btn-xs" id="btn-pilih-doc-kontrak" type="button">
                        <i class="fa fa-plus"></i> Tambah Dokumen Kontrak
                    </button>
                    <span style="display: none"  id="loadingUploadDocKontrak" class="text-danger">
                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </span>
                    <table id="tabel-doc-kontrak" class="table table-striped table-bordered" style="border: 1px solid #ddd;">
                        <thead>
                            <tr>
                                <th>Nama Dokumen</th>
                                <th width="140">Ukuran</th> 
                                <th width="140">Tanggal</th> 
                                <th width="140">Extensi</th> 
                                 <th width="140">FIle</th> 
                                <th width="140">Aksi</th> 
                            </tr>
                        </thead>
                      <tbody> 
                        <?php foreach ($doc_kontrak as $dk) { ?>
                            <tr>
                                <td><?php echo $dk->nama; ?></td>
                                <td><?php echo $dk->ukuran; ?></td>
                                <td><?php echo $dk->tanggal; ?></td>
                                <td>
                                    <img src="<?php echo base_url('assets/img/file_icon/'. $dk->ekstensi .'.gif'); ?>" alt="<?php echo $dk->ekstensi; ?>" />
                                </td>
                                <td>
                                     <a class="btn btn-xs btn-default" target="_blank" href="<?php echo base_url('uploads/kegiatan/' . $dk->file_name); ?>">lihat</a>
                                </td>
                                <td>
                                   <button type="button" class="btn btn-xs btn-danger btn-delete-file" data-file="<?php echo $dk->id_file_kegiatan; ?>">Hapus</button>
                                </td>
                            </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    
                </div>
            </div>
            <!-- end manage dokument kontrak -->