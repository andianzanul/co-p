<!-- start manage Pelaporan/Gambar/Peta/KAK/RAB -->
<div class="row"> 
    <div class="col-md-12">
        <label for="varchar">Kajian Lingkungan</label>
        <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai pelaporan yang ada" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span> 


        <button class="btn btn-default hide" id="btn-pilih-doc-kajian-lingkungan" type="button">
            <i class="fa fa-plus"></i> Pilih File Kajian Lingkungan
        </button>


        <button class="btn btn-primary btn-xs" id="btn-pilih-doc-kajian-lingkungan2" type="button">
            <i class="fa fa-plus"></i> Tambah Kajian Lingkungan
        </button>

        <span style="display: none"  id="loadingUploadDocKajianLingkungan" class="text-danger">
            <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
            <span class="sr-only">Loading...</span>
        </span>   

        <table id="tabel-doc-kajian-lingkungan" class="table table-striped table-bordered" style="border: 1px solid #ddd;">
            <thead>
                <tr>
                    <th colspan="1">Nama Dokumen</th>
                    <th width="120">Volume Laporan</th>
                    <th width="120">Progres Laporan</th>
                    <th width="380">File</th> 
                    <th class="hide">Progres Laporan</th>  
                    <th width="130">Status</th> 
                    <th width="200">Aksi</th> 
                </tr>
            </thead>
          <tbody> 
            <?php foreach ($doc_kangkung as $dk) { ?>
                <tr class="tr-dokumen" data-file="<?php echo $dk->id_file_kegiatan; ?>">
                     
                    <td class="doc_name"><?php echo $dk->nama; ?></td>
                    <td class="doc_volume_laporan"><?php echo $dk->volume_laporan; ?></td>
                    <td class="doc_kajian_lingkungan"><?php echo $dk->kajian_lingkungan; ?></td>
                    <td class="doc_file"><?php  echo tampil_edit_file_dokument_multi($dk); ?></td>
                    <td class="doc_id_kajian_lingkungan hide"><?php echo $dk->id_kajian_lingkungan; ?></td>
                    <td class="doc_status">
                        <?php echo label_status_file($dk->file_name); ?>
                    </td>
                    <td>
                       <button type="button" class="btn btn-xs btn-primary btn-edit-file_kajian_lingkungan" data-file="<?php echo $dk->id_file_kegiatan; ?>">edit</button>
                       <button type="button" class="btn btn-xs btn-success upload_file" data-file="<?php echo $dk->id_file_kegiatan; ?>">tambah file</button>
                       <button type="button" class="btn btn-xs btn-danger btn-delete-file" data-file="<?php echo $dk->id_file_kegiatan; ?>">Hapus</button>
                    </td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
        
    </div>
</div>