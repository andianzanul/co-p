<section class="content">
    <div class="row">

        <div class="col-md-12">
            <h2>Laporan : <?php print_r($laporan->judul); ?></h2>

        </div>

        <div class="col-md-6"> 
            <?php $this->load->view('admin/kegiatan/part-kegiatan-asistensi'); ?>  
            <strong>Status Laporan :</strong>
            <form  action="<?php echo site_url("admin/kegiatan/set_status_laporan"); ?>" method="post"> 
                <input type="hidden" name="id" value="<?php echo $laporan->id_laporan; ?>"> 
                <input type="hidden" name="kegiatan" value="<?php echo $laporan->id_kegiatan; ?>"> 
                <?php echo form_dropdown("status",['open' => "OPEN", 'close' => "Selesai"], $laporan->status); ?>
                <button>Simpan</button>
            </form>
        </div>

    	<div class="col-md-6">
            <iframe name="pageAsistensi" src="" width="100%" height="800" frameborder="0"></iframe> 

            <script>
                $('iframe').load(function(){ 
                   //alert("iframe is done loading")
                });
            </script>
             
    	
    		<?php /*<!-- Default box -->
    <div class="box box-warning direct-chat direct-chat-warning">
        <div class="box-header with-border">
            <h1 class="box-title">
                <strong>Diskusi</strong>
                <h2><?php echo $asistensi->asistensi; ?></h2>
        <p><?php echo $asistensi->desc; ?></p>
            </h1>
            <div class="box-tools pull-right"> 
                <a href="<?php echo site_url("admin/kegiatan/update/" . $kegiatan->id_kegiatan ); ?>" class="btn btn-primary btn-sm">Kembali</a>
            </div> 
        </div> 

        <div class="box-body">


            <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" style="height: 400px;">
    
                    <?php 

                    foreach ($lbr as $key => $value) {
                        $class = "direct-chat-msg"; // right

                        if($this->session->userdata("id_user") == $value->id_user_dari){
                            $class .= " right";
                        }

                        ?>
                        <!-- Message. Default to the left -->
                        <div class="<?php echo $class; ?>">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left"><?php echo $value->user_nama; ?></span>
                            <span class="direct-chat-timestamp pull-right"><?php echo $value->tanggal; ?></span>
                          </div>
                          <!-- /.direct-chat-info -->
                          <img class="direct-chat-img" src="<?php echo base_url("assets/img/user.png"); ?>" alt="Foto">
                          <!-- /.direct-chat-img -->
                          <div class="direct-chat-text">
                            <?php echo $value->isi; ?>
                          </div>
                          <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                    <?php } ?> 

                </div>
                  <!--/.direct-chat-messages-->
                    
             
        </div> 
        
        <div class="box-footer">
            <?php if('open' == $asistensi->status)  { ?>
            <form action="#" method="post" id="form-kirim-asistensi-lbr">
            <input type="hidden" name="id" value="<?php echo $asistensi->id_asistensi; ?>">
                <div class="input-group">
                  <input type="text" required name="message" placeholder="Type Message ..." class="form-control" id="message">
                  
                  <span class="input-group-btn">
                        <button type="submit" class="btn btn-warning btn-flat">Send</button>
                      </span>
                </div>
              </form>
             <?php } else {  ?>
                <span class="text-danger">Asistensi telah ditutup!</span>
             <?php } ?>
        </div> 
         
    </div> <!-- end Default box -->

    <?php if($this->mainlib->cek_level(['direksi','superadmin'])) { ?>
    	<strong>Status Asistensi :</strong>
		<form action="<?php echo site_url("admin/kegiatan/set_status_asistensi"); ?>" method="post">
         	<input type="hidden" name="id" value="<?php echo $asistensi->id_asistensi; ?>">
         	<input type="hidden" name="id_kegiatan" value="<?php echo $asistensi->id_kegiatan; ?>">
            <?php echo form_dropdown("status",['open' => "OPEN", 'close' => "Selesai"], $asistensi->status); ?>
            <button>Simpan</button>
        </form>
    <?php } ?>

    
    	</div>
    	 
    		*/ ?>
    	 
    	
    </div>
</section>




 