<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h1 class="box-title">
                <strong><?php echo $tbl_kegiatan->nm_kegiatan; ?></strong>
            </h1>
            <div class="box-tools pull-right">
                <?php  if(!$this->mainlib->cek_level(['balai','satker'])) { ?>
                    <a class="btn btn-danger btn-xs" href="<?php echo site_url('admin/kegiatan/update/' . $tbl_kegiatan->id_kegiatan. '?back=detail'); ?>">Edit</a> 
                <?php } ?>
            </div> 
        </div> 
        
        <table class="table table-bordered table-striped" style="margin-bottom: 0">

             <tr><td width="300">Nama Pekerjaan / Kegiatan</td><td><?php echo $tbl_kegiatan->nm_kegiatan; ?></td></tr>

             <tr><td>Tahun Anggaran</td><td>
                <?php echo $tbl_kegiatan->tahun; ?> 
                <?php echo ($tbl_kegiatan->multi_years == 'Y' ? " - " . $tbl_kegiatan->tahun_anggaran_2 : '' ); ?>
             </td></tr> 
           
             <tr><td>
                 <strong>Kontrak :</strong>
             </td><td style="padding: 0">
                    <table class="table table-striped table-bordered">
                        <tr><td>Nama Penyedia Jasa</td><td><?php echo $tbl_kegiatan->nmp_jasa; ?></td></tr> 
                        <tr><td width="150">KSO/JO</td><td><?php echo ($tbl_kegiatan->kso_jo == 'kso/jo') ? 'YA - (' . display_perusahaan_kso_jo($tbl_kegiatan->nmp_jasa2) .')'  : ''; ?></td></tr> 
                        <tr><td>Nomor Kontrak</td><td><?php echo $tbl_kegiatan->no_kontrak_awal; ?></td></tr>
                        <tr><td>Tanggal Kontrak</td><td><?php echo tgl_indo($tbl_kegiatan->tgl_kontrak_awal,2); ?></td></tr>
                        <tr><td>Nilai Kontrak (Rp)</td><td><?php echo $tbl_kegiatan->nilai_kontrak; ?></td></tr>
                        <tr><td>Nilai Pagu (Swakelola)</td><td><?php echo $tbl_kegiatan->nilai_pagu; ?></td></tr>
                        <tr><td>Nomor SPMK</td><td><?php echo $tbl_kegiatan->no_spmk; ?></td></tr>
                        <tr><td>Tanggal SPMK</td><td><?php echo tgl_indo($tbl_kegiatan->tgl_spmk,2); ?></td></tr>
                        <tr><td>Jenis Kontrak</td><td><?php echo $tbl_kegiatan->jenis_kontrak; ?></td></tr>
                    </table> 
             </td></tr>

             <tr><td height="20" colspan="2"><h4><strong>Waktu Pelaksanaan :</strong></h4></td></tr> 
                 <tr><td>Mulai</td><td><?php echo tgl_indo($tbl_kegiatan->waktu_pelaksanaan_mulai,2); ?></td></tr>
                 <tr><td>Berakhir</td><td><?php echo tgl_indo($tbl_kegiatan->waktu_pelaksanaan_akhir,2); ?></td></tr>
                 <tr><td>Bulan</td><td><?php echo $tbl_kegiatan->waktu_pelaksanaan_bulan; ?> <span class="text-muted">bulan</span></td></tr>
                 <tr><td>Hari (Kalender)</td><td><?php echo $tbl_kegiatan->waktu_pelaksanaan_hari; ?> <span class="text-muted">hari</span></td></tr>

              <tr>
                <td><strong>Realisasi Pelaksanaan :</strong></td>
                <td style="padding: 0">
                    <table class="table table-striped table-bordered" style="margin-bottom: 0">
                        <tr>
                            <th>Periode</th>
                            <th>Rencana (%)</th> 
                            <th>Realisasi (%)</th>
                            <th>Deviasi (%)</th> 
                        </tr>
                        <?php foreach($realisasi_pelaksanaan as $rp){ ?>
                        <tr>
                            <td><?php echo tgl_indo($rp->progres_fisik_tanggal,2); ?></td>
                            <td><?php echo $rp->progres_fisik_rencana; ?></td>
                            <td><?php echo $rp->progres_fisik_realisasi; ?></td> 
                            <td><?php echo $rp->progres_fisik_deviasi; ?></td>
                        </tr>
                        <?php } ?>
                    </table>   
                    <?php   
                ?></td> 
             </tr>
    </div> <!-- end Default box -->
            
             <tr><td><strong>Produk (Laporan/Gambar) :</strong></td><td style="padding: 0">
                <?php if($doc_pelaporan) { ?>
                 <table class="table table-striped table-bordered" style="margin-bottom: 0">
                        <thead>
                            <tr> 
                                <th>Dokumen</th>
                                <th>File</th> 
                                <th width="120">Volume Laporan</th>
                                <th width="120">Progres Laporan</th>
                                <!-- <th width="150">Tanggal</th> --> 
                                <th width="90">Status</th>  
                            </tr>
                        </thead>
                      <tbody> 
                        <?php foreach ($doc_pelaporan as $dk) { ?>
                            <tr> 
                                <td><?php echo $dk->nama; ?></td>
                                <td class="doc_file"><?php echo tampilkan_file_dokument_multi($dk->file_name); ?></td>
                                <td class="doc_volume_laporan"><?php echo $dk->volume_laporan; ?></td>
                                <td class="doc_progres_laporan"><?php echo $dk->progres_laporan; ?></td>
                                <!-- <td><?php echo $dk->tanggal; ?></td> -->
                                <td><?php echo label_status_file($dk->file_name); ?></td>  
                            </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    <?php } ?>
             </td></tr>
             <!-- <tr><td height="20" colspan="2"></td></tr> -->
        </table>

        <div class="box-footer">
            <?php  echo anchor(site_url('admin/kegiatan'),'kembali', 'class="btn btn-warning"'); 
            echo '   '; ?>
            <?php echo anchor(site_url('admin/kegiatan/word/'.$tbl_kegiatan->id_kegiatan),'Word', 'class="btn btn-primary"'); 
            echo '   '; ?>
        </div> 
    </div>
</section>