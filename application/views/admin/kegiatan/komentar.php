
                <div class="box">


                   <div class="box-header with-border">
                          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
                        <h3 class="box-title">Komentar</h3> 
                    </div>

                       <div class="box-footer box-comments" style="max-height: 300px; overflow: auto;">
                          <?php
foreach ($komentars as $komentar) {
 

  ?>
              <div class="box-comment">
                <!-- User image -->
                <img class="img-circle img-sm"  width="128" src="<?php echo base_url('data/profil/' . $komentar->foto ); ?>">
          

                <div class="comment-text">
                      <span class="username">
                        <a target="_blank" href="<?php echo site_url('admin/profil/detail/' . $komentar->id); ?>"><?php echo $komentar->nama; ?></a>
                        <span class="text-muted pull-right"><?php echo $komentar->tanggal; ?></span>
                      </span>  <?php echo $komentar->isi; ?>
                      <a href="<?php echo base_url('admin/laporan/hapuskomen/'.$komentar->id_laporan .'/'.$komentar->id_komentar_laporan); ?>" class="fa fa-trash-o" role="button"></a>
                </div>
                <!-- /.comment-text -->
              </div>
                     <?php } ?> 

            </div>
            <div class="box-footer">
                <?php echo form_open('admin/laporan/komentar','',['id' => $laporan->id_laporan]); ?>
              <form action="#" method="post">
                <img src="<?php echo base_url('data/profil/' . $this->session->userdata("foto") ); ?>" width="128" class="img-responsive img-circle img-sm" alt="User Image">
        
                <!-- .img-push is used to add margin to elements next to floating images -->
              

                <div class="img-push">
                   <div class="row">
            <div class="col-sm-11">
                  <input type="text" name="pesan" class="form-control input-sm" placeholder="Masukkan Komentar">     </div>      <button type="submit" class="btn btn-primary btn-sm">Kirim</button>
                </div>
            </div>
              </form>
                <?php echo form_close(); ?>
            </div>

                    </div>
                </div>