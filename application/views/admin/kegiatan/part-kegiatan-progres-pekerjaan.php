<?php 

$rencana = "";
$realisasi = "";
$periode="";


if($this->mainlib->cek_level(['direksi','ppk'])) {
    $periode = "";
    $rencana = "";
    $realisasi = " readonly ";

}

else if($this->mainlib->cek_level(['konsultan'])) {

    $periode = "";
    $rencana = " readonly ";
    $realisasi = "";

}


 ?><div class="realisasipelaksanaan well">
    <strong>Realisasi Pelaksanaan</strong>
    <button type="button" class="btn btn-primary" id="btn-add-realisasipelaksanaan">+</button> 

    <div id="realisasipelaksanaan-box"><hr>
        <?php foreach ($kegiatan_realisasi_pelaksanaan as $key => $rp) { ?> 
        <div class="row row-realisasipelaksanaan">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="varchar">Periode</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen dari rencana yang dibuat" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input <?php echo $periode; ?> type="date" class="form-control" name="realisasi_pelaksanaan[<?php echo $rp->id_realisasi_pelaksanaan; ?>][progres_fisik_tanggal]" id="progres_fisik_tanggal" value="<?php echo $rp->progres_fisik_tanggal; ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="varchar">Rencana (%)</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen dari rencana yang dibuat" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input <?php echo $rencana; ?> type="text" class="form-control progres_fisik_col percent progres_fisik_rencana" name="realisasi_pelaksanaan[<?php echo $rp->id_realisasi_pelaksanaan; ?>][progres_fisik_rencana]" placeholder="Rencana" value="<?php echo $rp->progres_fisik_rencana; ?>"  />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="varchar">Realisasi (%)</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen kegiatan fisik per tanggal penginputan data" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input <?php echo $realisasi; ?> type="text" class="form-control progres_fisik_col percent progres_fisik_realisasi" name="realisasi_pelaksanaan[<?php echo $rp->id_realisasi_pelaksanaan; ?>][progres_fisik_realisasi]"  placeholder="Realisasi" value="<?php echo $rp->progres_fisik_realisasi; ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="varchar">Deviasi (%)</label>
                    <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen deviasi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                    <input readonly type="text" class="form-control progres_fisik_deviasi" name="realisasi_pelaksanaan[<?php echo $rp->id_realisasi_pelaksanaan; ?>][progres_fisik_deviasi]" placeholder="Deviasi" value="<?php echo (!empty($rp->progres_fisik_deviasi) ? $rp->progres_fisik_deviasi /*. '%'*/ : ''); ?>" />
                </div>
            </div>
            <div class="col-md-1">
                <label>&nbsp;</label> <br>
                <button data-id="<?php echo $rp->id_realisasi_pelaksanaan; ?>" type="button" class="btn btn-danger btn-delete-realisasi">-</button>
            </div>
        </div>
        <?php } ?>
    </div>
</div>



<script type="text/javascript"> 
    $(function(){ 
        $("body").on("keyup",".progres_fisik_col", function(){

            var parent = $(this).parents(".row-realisasipelaksanaan");

            console.log(parent)

            var rencana = parseFloat( parent.find('.progres_fisik_rencana').val()); 
            var realisasi = parseFloat( parent.find('.progres_fisik_realisasi').val());
                deviasi = realisasi - rencana;
                if(!isNaN(deviasi)){
                    parent.find('.progres_fisik_deviasi').val(deviasi.toFixed(2) + '%');   
                }else{
                    parent.find('.progres_fisik_deviasi').val("");  
                }  
         });

         

    });


        /* add realisasi pelaksanaan */ 
        $('body').on('click','#btn-add-realisasipelaksanaan', function(){
            $('#realisasipelaksanaan-box').append($("#new-item-kegiatan-realisasipelaksanaan").html());
        });

        /* hapus realisasi pelaksanaan temp  */
        $('body').on('click','.btn-delete-realisasi-temp', function(){
            $(this).parents('.row-realisasipelaksanaan').remove();
        });

         /* Hapus detail konstruksi di server */
        $('body').on('click','.btn-delete-realisasi', function(e){
            e.preventDefault();
            e.stopPropagation();
            var tombol = $(this);
            swal({
                title: 'Anda yakin menghapus?',
                text: "data yang telah dihapus tidak bisa di kembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal' 
            }).then(function () {
                $.ajax({
                    type: 'get', 
                    url: appSettings.base_url + 'admin/kegiatan/delete_realisasi_pelaksanaan/' + tombol.data('id'),
                    dataType: 'json',
                    beforeSend: function() {},
                    success: function(response) {
                        if(response.status == 1){
                            tombol.parents('.row-realisasipelaksanaan').slideUp();
                        }
                        swal({
                            title: response.title,
                            text: response.message,
                            type: response.type,
                            timer: 1000,
                            showConfirmButton:false
                        })
                    }
                }); 
            }) 
        });
</script>



<script type="text/html" id="new-item-kegiatan-realisasipelaksanaan">
    <div class="row row-realisasipelaksanaan">
        <div class="col-md-2">
            <div class="form-group">
                <label for="varchar">Periode</label>
                <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen dari rencana yang dibuat" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input type="date" class="form-control" name="new_realisasi_pelaksanaan_progres_fisik_tanggal[]" id="progres_fisik_tanggal" value="" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="varchar">Rencana (%)</label>
                <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen dari rencana yang dibuat" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input type="text" class="form-control progres_fisik_col progres_fisik_rencana percent" name="new_realisasi_pelaksanaan_progres_fisik_rencana[]" placeholder="Rencana" value=""  />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="varchar">Realisasi (%)</label>
                <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen kegiatan fisik per tanggal penginputan data" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input type="text" class="form-control progres_fisik_col progres_fisik_realisasi percent" name="new_realisasi_pelaksanaan_progres_fisik_realisasi[]" placeholder="Realisasi" value="" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="varchar">Deviasi (%)</label>
                <span class="fa fa-question-circle help-popup" data-content="Diisi sesuai nilai persen deviasi" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <input readonly type="text" class="form-control progres_fisik_deviasi percent" name="new_realisasi_pelaksanaan_progres_fisik_deviasi[]" placeholder="Deviasi" value="" />
            </div>
        </div>
        <div class="col-md-1">
            <label>&nbsp;</label> <br>
            <button type="button" class="btn btn-warning btn-delete-realisasi-temp">-</button>
        </div>
    </div>
</script>       