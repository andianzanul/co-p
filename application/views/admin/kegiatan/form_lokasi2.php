<div class="well well-sm item-lokasi lokasi-<?php echo $prov; ?>">
    <div class="box-lokasi">
    
    
    <div class="row row-lokasi" data-prov="<?php echo $prov; ?>">

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kabupaten/Kota <?php echo form_error('kabupaten') ?></label><span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span><br>
                 <div class="kabupaten-cek">
                    <b><?php echo $nama; ?></b> <br>
                    <?php foreach($kabupaten as $kab) { ?>
                            <label class="checkbox-inline">
                                <input class="cekKab" type="checkbox" <?php echo (in_array($kab->id,array()) ? ' checked' : ''); ?> name="cekkab[<?php echo $prov; ?>][]" value="<?php echo $kab->id; ?>"><span><?php echo $kab->name; ?></span>
                            </label> <br>
                <?php }  ?>
                 </div>   
                
                <?php /*<select class="form-control combo_kabupaten" multiple  name="lok[<?php echo $lok['id_lokasi']; ?>][id_kabupaten]" id="kabupaten-<?php echo $prov; ?>">
                    <option value="">-- Pilih --</option>
                    <?php foreach($kabupaten[$key] as $kab) { ?>
                    <option <?php echo set_selected($kab->id,  $lok['id_kabupaten']); ?> value="<?php echo $kab->id; ?>"><?php echo $kab->name; ?></option>
                    <?php } ?>
                </select>*/ ?>
            </div>
         </div>

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kecamatan <?php echo form_error('kecamatan') ?></label>
                <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php //echo combo_kecamatan($id_kecamatan, $id_kabupaten, ' name="kecamatan['. $prov .'][]" class="form-control combo-kecamatan"'); ?> 
                <?php /*<input type="text" class="form-control" name="kecamatan[<?php echo $prov; ?>][]" id="kecamatan" placeholder="kecamatan" value="<?php echo $kecamatan; ?>" />*/ ?>
                <div class="kecamatan-cek" style="overflow: auto;max-height: 300px"></div>
           </div>
         </div>

         <div class="col-md-4">
             <div class="form-group">
                <label for="varchar">Kelurahan/Desa<?php echo form_error('desa') ?></label>
                <span class="fa fa-question-circle help-popup" data-content="Diisikan secara manual sesuai lokasi kegiatan" data-placement="right" data-container="body" data-toggle="popover" data-original-title="Keterangan"></span>
                <?php //echo combo_kelurahan($id_kelurahan, $id_kecamatan, ' name="desa['. $prov .'][]" class="form-control combo-kelurahan" id="kelurahan-'. $id_kabupaten .'" '); ?>
                <?php /*<input type="text" class="form-control" name="desa[<?php echo $prov; ?>][]" id="desa" placeholder="Desa" value="<?php echo $desa; ?>" />*/ ?>
                <div class="kelurahan-cek" style="overflow: auto;max-height: 300px"></div>
            </div>
         </div>

         <?php /*<div class="col-md-1">
         <label>&nbsp;</label> <br>
             <button type="button" class="btn btn-danger btn-hapus-lokasi-123">-</button>   
         </div>*/ ?>

    </div> 


   

    
        
     
         
     </div>   

     <?php /*<button type="button" class="btn btn-primary btn-add-lokasi">+</button>   */ ?>

</div> 