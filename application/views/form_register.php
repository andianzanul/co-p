<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<!--  -->
		<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Registrasi Pengguna</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>"
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>"

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	</head>
	<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-box-body">
    <div class="login-logo">
    <!-- <a href="<?=config_item('asset')?>/index2.html"> -->
    <b>Registrasi</b>
  </div>
    <p class="login-box-msg">Silahkan daftarkan pengguna baru</p>
    <!--  -->

<?php echo validation_errors();?>
    <form action="<?php echo base_url('user/register');?>" method="POST">
		<!--  -->

      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <!-- tambahan -->
      <div class="form-group has-feedback">
        <?php 
        echo form_dropdown('satker',$jenis_satker_data,"",'class="form-control" name="satker" id="satker" placeholder="Satker"'); 
              ?>
        <!-- <select name="satker" class="form-control" required>
            <option value="" disabled selected style="color: #999;"> -- Pilih Satker --</option>
            <option value="1">SATUAN KERJA BALAI WILAYAH SUNGAI SULAWESI III</option>
            <option value="2">SATUAN KERJA OPERASI DAN PEMELIHARAAN SUMBER DAYA AIR SULAWESI III</option>
            <option value="3">SNVT PELAKSANAAN JARINGAN PEMANFAATAN AIR WS. KALUKU-KARAMA, WS. PALU-LARIANG PROVINSI SULAWESI BARAT</option>
            <option value="4">SNVT PELAKSANAAN JARINGAN PEMANFAATAN AIR WS. PALU-LARIANG, WS. PARIGI-POSO, WS. KALUKU-KARAMA PROVINSI SULAWESI TENGAH</option>
            <option value="5">SNVT PELAKSANAAN JARINGAN SUMBER AIR WS. KALUKU-KARAMA, WS. PALU-LARIANG PROVINSI SULAWESI BARAT</option>
            <option value="6">SNVT PELAKSANAAN JARINGAN SUMBER AIR WS. PALU-LARIANG, WS. PARIGI-POSO, WS. KALUKU-KARAMA PROVINSI SULAWESI TENGAH</option>
        </select> -->
      </div>
      <div class="form-group has-feedback">
        <?php echo combo_level_user('', 'name="level" class="form-control" required'); ?>
        <!-- <select name="level" class="form-control" required>
            <option value="" disabled selected style="color: #999;"> -- Pilih Level User -- </option>
            <option value="superadmin">Superadmin</option>
            <option value="admin">Admin</option>
            <option value="user">User</option>
        </select> -->
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="submit" value="Login!" class="btn btn-primary btn-block btn-flat">Registrer</button>
        </div>
      </div>
    </form>
</div>

</body>
</html>
<!--  
<body>
    <h1>Form Login</h1>
    <?php echo validation_errors();?>
    <form action="<?php echo base_url('user/login');?>" method="POST">
      <label>Username:</label><input type="text" name="username" /><br />
      <label>Password:</label><input type="password" name="password" /><br />
      <input type="submit" name="submit" value="Login!" />
    </form>
  </body>
  -->