<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BWS Sulawesi III - e-SISDA</title>
 	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/bootstrap/css/bootstrap.css'); ?>"> -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/landing-page.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/customku.css'); ?>">


  <style>

img.iconSda{
  border-radius: 50%;
    align-items: center;
   display: block;
  margin-left: auto;
  margin-right: auto;
  width: 40%;
}
p.texttengah{
  color:#fff;
  text-align: center;
}
img.ikonnya{
    align-items: center;
   display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}
img.myClass {
  align-items: center;
  width: 200px;
   display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

    body {
       background-image: url("<?php echo base_url(); ?>assets/img/bakground.jpg");
    }
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: black;
  color: white;
  text-align: center;
}

    p {
      color: white;
      text-align: center;
      margin-top: 10px;
    }

    .gambar {
      border-radius: 50%;
      width: 25%;
      display: block;
      margin-left: auto;
      margin-right: auto;
    }

    .navbar-nav {
      flex-direction: row;
    }

    .nav-link {
      padding-right: .5rem !important;
      padding-left: .5rem !important;
    }

    /* Fixes dropdown menus placed on the right side */
    .ml-auto .dropdown-menu {
      left: auto !important;
      right: 0px;
    }

    .navbar .navbar-expand-lg .navbar-dark .bg-primary .rounded {
      border-width: 0px;
      -webkit-box-shadow: 0px 0px;
      box-shadow: 0px 0px;
      background-color: rgba(0, 0, 0, 0.0);
      background-image: -webkit-gradient(linear, 50.00% 0.00%, 50.00% 100.00%, color-stop(0%, rgba(0, 0, 0, 0.00)), color-stop(100%, rgba(0, 0, 0, 0.00)));
      background-image: -webkit-linear-gradient(270deg, rgba(0, 0, 0, 0.00) 0%, rgba(0, 0, 0, 0.00) 100%);
      background-image: linear-gradient(180deg, rgba(0, 0, 0, 0.00) 0%, rgba(0, 0, 0, 0.00) 100%);
    }
  </style>

</head>

<body>
  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark rounded">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
         <a class="nav-link" data-toggle="modal" data-target="#modalAbout" href="#">Tentang</a>

      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#modalContact" href="#">Hubungi Kami</a>
      </li>
    </ul>
  </nav>
  <!-- //content -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-4"></div>
          <div align="middle" class="col-md-4 offset-5" style="margin-top: 50px;">
            <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 220px;     padding-bottom: 80px; ">
          </div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
      <div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-md-4">
          <a href="<?=base_url('admin');?>">
          <img alt="" src="<?php echo base_url(); ?>assets/img/1.png" class="iconSda" >
          <p class="texttengah">
            Work
          </p>
        </div>
        <div class="col-md-4">
          <a href="<?=base_url('suaraanda');?>">
          <img alt="" src="<?php echo base_url(); ?>assets/img/2.png" class="iconSda">
          <p class="texttengah">
            Suara Anda
          </p>
        </div>
        <div class="col-md-4">
          <a href="#">
          <img alt="" src="<?php echo base_url(); ?>assets/img/3.png" class="iconSda" data-toggle="modal" data-target="#modalSDA">
        </a>
          <p class="texttengah">
            Asset SDA
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <img alt="" src="<?php echo base_url(); ?>assets/img/4.png" class="iconSda"data-toggle="modal" data-target="#modalPeta">
          <p class="texttengah">
            Peta
          </p>
        </div>
        <div class="col-md-4">
          <img alt="" src="<?php echo base_url(); ?>assets/img/5.png" class="iconSda"data-toggle="modal" data-target="#modalPustaka">
          <p class="texttengah">
            Pustaka
          </p>
        </div>
        <div class="col-md-4">
          <img alt="" src="<?php echo base_url(); ?>assets/img/6.png" class="iconSda"data-toggle="modal" data-target="#modalOrganisasi">
          <p class="texttengah">
            Organisasi
          </p>
        </div>
      </div>
    </div>
  <div class="col-md-4">
  </div>
  </div>
  </div>
 

      <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-2.2.3.min.js'); ?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/js/bootstrap.min.js'); ?>">

</body>


<div class="footer">
  <p text-align: right><img width="24px" src="<?php echo base_url(); ?>assets/img/puicon.jpg" alt="Smiley face" align="middle">   © Balai Wilayah Sungai Sulawesi III Palu</p>
  
</div>
<!-- modal -->
    <div class="modal" id="modalAbout" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="display: inline-block; border-radius: 5px; background: #334875; color: #eeeeee; padding: 10px;">Tentang e-SISDA</h3>
                </div>

                <div class="modal-body">
                    <p style="text-align: justify; color: #334875; margin: 20px;"> e-SISDA merupakan aplikasi berbasis website yang bertujuan untuk meningkatkan performa kinerja sekaligus secara bertahap memperbaiki sistem dalam hal pengelolaan data dan informasi Sumber Daya Air di lingkungan Balai Wilayah Sungai Sulawesi III, diharapkan dengan adanya aplikasi ini dapat mempermudah tugas dan fungsi di tingkat pemegang data (wali data) sebagai pihak yang diamanahkan dalam mencatat/merekam, mengolah serta menyajikan berbagai data dan informasi Sumber Daya Air di lingkup wilayah kerja Balai Wilayah Sungai Sulawesi III.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal"> CLOSE </button>
                </div>    
            </div>
        </div>
    </div>
    <div class="modal" id="modalContact" tabindex="-1">
        <div class="modal-dialog mofal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="display: inline-block; border-radius: 5px; background: #334875; color: #eeeeee; padding: 10px;">Kontak</h3>
                </div>

                <div class="modal-body">
                    <p style="text-align: justify; color: #334875; margin: 20px;"> <b> Balai Wilayah Sungai Sulawesi III </b> <br>
                        Jl. Abdurachman Saleh No. 230 Palu <br>
                        Provinsi Sulawesi Tengah, Indonesia <br> 
                        Telp. (0451) 482147 <br>
                        Fax. (0451) 482101 </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal"> CLOSE </button>
                </div>    
            </div>
        </div>
    </div>
    <div class="modal" id="modalSDA" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="display: inline-block; border-radius: 5px; background: #EEAB4E; color: #eeeeee; padding: 10px;">Tentang SDA</h3>
                </div>

                <div class="modal-body">
                    <p style="text-align: justify; color: #334875; margin: 20px;">Merupakan fitur untuk mencatat data aset (konstruksi maupun non konstruksi) serta data teknis terkait Sumber Daya Air di wilayah kewenangan BWS Sulawesi III. Fitur ini juga sebagai alat pendukung untuk aplikasi PDSDA 6.0.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal"> CLOSE </button>
                </div>    
            </div>
        </div>
    </div><div class="modal" id="modalPustaka" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="display: inline-block; border-radius: 5px; background: #9A59B5; color: #eeeeee; padding: 10px;">Tentang Pustaka</h3>
                </div>

                <div class="modal-body">
                    <p style="text-align: justify; color: #334875; margin: 20px;">Merupakan fitur untuk menunjang keperluan perpustakaan (simpan-pinjam dokumen) di lingkungan BWS Sulawesi III. Fitur ini nantinya bisa dikembangkan dengan menerapkan teknologi elektronika untuk index penyimpanan dokumen dalam bentuk hard copy.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal"> CLOSE </button>
                </div>    
            </div>
        </div>
    </div>
    <!-- <div class="modal" data-keyboard="false" data-backdrop="static" id="modalAspirasi" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" style="background: #FB6648; padding: 5px; color: #FFF;">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Login Aspirasi</h3>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="inputUsername">Username</label>
                            <input class="form-control" type="text" name="username" placeholder="username" id="inputUsername">    
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input class="form-control" type="password" name="username" placeholder="password" id="inputPassword">    
                        </div>
                        <hr> 
                    </form>
                </div>
                <blockquote>
                    <small> &nbsp;<a href="#">Registrasi</a> jika belum memiliki akun.</small>
                </blockquote>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalDaftar">Login</button>
                    <button class="btn btn-primary btn-sm" data-dismiss="modal"> Close </button>
                </div>

            </div>
        </div>
    </div> -->
    <div class="modal" id="modalOrganisasi" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="display: inline-block; border-radius: 5px; background: #28608F; color: #eeeeee; padding: 10px;">Tentang Organisasi</h3>
                </div>

                <div class="modal-body">
                    <p style="text-align: justify; color: #334875; margin: 20px;">Merupakan fitur untuk mencatat Struktur Organisasi kepegawaian di lingkungan BWS Sulawesi III dan pihak-pihak rekanan/penyedia jasa yang pernah terlibat.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal"> CLOSE </button>
                </div>    
            </div>
        </div>
    </div>
    <div class="modal" id="modalPeta" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="display: inline-block; border-radius: 5px; background: #60BA61; color: #eeeeee; padding: 10px;">Tentang Peta</h3>
                </div>

                <div class="modal-body">
                    <p style="text-align: justify; color: #334875; margin: 20px;">Merupakan sistem informasi geospasial yang dapat menyajikan data-data pengelolaan Sumber Daya Air di BWS Sulawesi III dalam bentuk peta online. Contoh peta yang bisa disajikan antara lain, peta administrasi, peta irigasi, sebaran bangunan pengendali banjir, dsb.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal"> CLOSE </button>
                </div>    
            </div>
        </div>
    </div>

<!-- akhir Modal -->

</html>