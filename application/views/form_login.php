<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
		<meta charset="utf-8">
		<meta name="theme-color" content="#C5501F">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#C5501F">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#C5501F">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title>Login CO-P</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	</head>


  <style>
.gambarcenter {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 10%;
}
  .itembgkiri{
  padding-top:15%;
  overflow: auto;
  margin: auto;
  
}
.itembgkanan{
   width: 50%;
  height: 35%;
  overflow: auto;
  margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;
  ;
}
  .logocop{
    display: block;
  margin-right: auto;
  margin-left: auto;;
  }
.buttonlogin {


  width: 300px;
  background-color: #C5501F;
  border: none;
  border-radius: 50px;
  color: white;
  padding: 20px;
  display: block;
  margin-right: auto;
  margin-left: auto;;
  padding: 20px; 
  width: 300px;
  height: auto; 

}
.inputlogin
{
  border-radius: 25px;
  border: 1px solid #757475;
  display: block;
  margin-right: auto;
  margin-left: auto;;
  padding: 20px; 
  width: 300px;
  height: 15px; 
}
  .bgkiri{
background:#eeef;
min-height: 100vh;
  }
  .bgkanan{
  min-height: 100vh;
   background-image:
linear-gradient(45deg, #C5501F, rgba(187, 41, 59, 0.40)),
url("<?php echo base_url(); ?>assets/img/bgkanan.jpg");
background-size: cover;
  }

img.iconSda{
  border-radius: 50%;
    align-items: center;
   display: block;
  margin-left: auto;
  margin-right: auto;
  width: 40%;
}
p.texttengah{
  color:#fff;
  text-align: center;
}
img.ikonnya{
    align-items: center;
   display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}
img.myClass {
  align-items: center;
  width: 200px;
   display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

    body {
      background:#eeef;
    }
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: black;
  color: white;
  text-align: center;
}

    p {
      color: white;
      text-align: center;
      margin-top: 10px;
    }
     p#kiri {
      color: black;
      text-align: center;
      margin-top: 10px;
    }

    .gambar {
      border-radius: 50%;
      width: 25%;
      display: block;
      margin-left: auto;
      margin-right: auto;
    }


    .navbar-nav {
      flex-direction: row;
    }

    .nav-link {
      padding-right: .5rem !important;
      padding-left: .5rem !important;
    }

    /* Fixes dropdown menus placed on the right side */
    .ml-auto .dropdown-menu {
      left: auto !important;
      right: 0px;
    }

    .navbar .navbar-expand-lg .navbar-dark .bg-primary .rounded {
      border-width: 0px;
      -webkit-box-shadow: 0px 0px;
      box-shadow: 0px 0px;
      background-color: rgba(0, 0, 0, 0.0);
      background-image: -webkit-gradient(linear, 50.00% 0.00%, 50.00% 100.00%, color-stop(0%, rgba(0, 0, 0, 0.00)), color-stop(100%, rgba(0, 0, 0, 0.00)));
      background-image: -webkit-linear-gradient(270deg, rgba(0, 0, 0, 0.00) 0%, rgba(0, 0, 0, 0.00) 100%);
      background-image: linear-gradient(180deg, rgba(0, 0, 0, 0.00) 0%, rgba(0, 0, 0, 0.00) 100%);
    }
  </style>

</head>

<body>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4">
               <div class="itembgkiri">
          <h3 align="center">
            WELCOME TO
          </h3>
        </form>
        <img class="logocop" width="100px" alt="Bootstrap Image Preview" src="<?php echo base_url(); ?>assets/img/logocop.png" />
          <br>
          <strong><p id="kiri" align="center">CONTROL PAPER</p></strong>
          <P id="kiri" align="center"><i>PPK Perencanaan dan Program</i></p>
          
            <?php echo validation_errors();?> 
    <form action="<?php echo base_url('login');?>" method="POST">
            <div class="form-group">
               
              
              <input type="text" class="form-control inputlogin"  name="username" placeholder="username" id="exampleInputEmail1" />
            </div>
            <div class="form-group">
              <input type="password" placeholder="password" class="form-control inputlogin"  name="password" id="exampleInputPassword1" />
            </div>
            <button style="align-items: "center" type="submit" name="submit" class="btn btn-primary btn-sm buttonlogin">
              Login
            </button>

          </form>
          
            <p style="color:black" align="center">
              Forgot Password?
              </p>
      <br>
          <br>
          <br>
          <br>
        </div>
         
        <img class="gambarcenter" src="<?php echo base_url(); ?>assets/img/pu.jpg" />
          <p id="kiri">
            KEMENTERIAN PEKERJAAN UMUM DAN PERUMAHAN RAKYAT</br>
DIREKTORAT JENDERAL SUMBER DAYA AIR</br>
BALAI WILAYAH SUNGAI SULAWESI III</br>
@2019</br>

          </p>
        </div>
      
        <div style="background: " class="col-md-8 bgkanan" >
          <div class="itembgkanan">
      <img class="logocop" width="100px" alt="Bootstrap Image Preview" src="<?php echo base_url(); ?>assets/img/logocop2.png" /> <br>    
      <p><strong>CONTROL PAPER<br></strong><small>PPK Perencanaan dan Program</br>
      Satker Balai Wilayah Sungai Sulawesi III</small></small> </p>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
 </body>
</html>
<!--  
<body>
    <h1>Form Login</h1>
    <?php echo validation_errors();?>
    <form action="<?php echo base_url('user/login');?>" method="POST">
      <label>Username:</label><input type="text" name="username" /><br />
      <label>Password:</label><input type="password" name="password" /><br />
      <input type="submit" name="submit" value="Login!" />
    </form>
  </body>
  -->