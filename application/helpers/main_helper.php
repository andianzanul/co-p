<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function rupiahkan($nominal){
	return 'Rp '.number_format($nominal, 0, ',', '.');
}


function label_tahun_current($tahun){
	

	/*if(date("Y") == $tahun){
		return $tahun . '  - current';
	}*/

	return $tahun;
}


function combo_list_konsultan($cek = '',$attr = 'required name="konsultan" class="form-control" id="konsultan"'){
	$CI =& get_instance();


	$out = '<select '.$attr.'>';
	$out .= '<option value=""> -- Pilih -- </option>';

	$users = $CI->db->order_by("username","ASC")->where('level',"konsultan")->get('tbl_user')->result_object();

	foreach($users as $user){
		 
		$out .= '<option '. set_selected($cek,$user->user_id) .' value="'.$user->user_id.'">'.$user->nama.' ('.$user->username.')</option>';
	}  

	
	$out .= '</select>';
	return $out;
}


function combo_ppk( $cek = '',$attr = 'name="ppk" class="form-control" id="ppk"' ){
	$CI =& get_instance();


	$out = '<select '.$attr.'>';
	$out .= '<option value=""> -- Pilih -- </option>';

	$cats = $CI->db->order_by("nama_satker","ASC")->get('tbl_satker')->result_object();

	foreach($cats as $cat){
		$out .= '<optgroup label="'. $cat->nama_satker .'">';

		$data 	= $CI->db->where('id_satker',$cat->id_satker)->order_by("nama_ppk","ASC")->get('tbl_ppk')->result_object(); 
		foreach($data as $item){
			$out .= '<option '. set_selected($cek,$item->id_ppk) .' value="'.$item->id_ppk.'">'.$item->nama_ppk.'</option>';
		}

		$out .= '</optgroup>';
	}  

	
	$out .= '</select>';
	return $out;
}

function print_r2($a){
	echo '<pre>';
	print_r($a);
	echo '</pre>';
}

// checked input
function set_checked($str1,$str2){
	if($str1==$str2){
		return 'checked="checked"';
	}
}

// selected dropdown
function set_selected($str1,$str2){
	if($str1==$str2){
		return 'selected="selected"';
	}
}

// echo  rupiahkan(1000000) --> Rp. 1.000.000

function combo_kelurahan($cek = '', $by = '', $attr = 'name="kelurahan" class="form-control" id="form-kelurahan"'){
	$CI =& get_instance(); 
	$dass = $CI->db->where('id_kecamatan',$by)->order_by('name','ASC')->get('kelurahan')->result_object(); 
	
	$out = '<select '.$attr.'>';
	$out .= '<option value="">-- pilih --</option>';

	if($by != ''){
		foreach($dass as $item){
			$out .= '<option '. set_selected($cek,$item->id) .' value="'.$item->id.'">'.$item->name.'</option>';
		}
	}
	
	$out .= '</select>';
	return $out;
}
function list_kal_by_kec($kec,$kelurahan,$id_prv, $id_kabupaten){
	$CI =& get_instance(); 
	$dass = $CI->db->where('id_kecamatan',$kec['id_kecamatan'])->order_by('name','ASC')->get('kelurahan')->result_object(); 

	 
	 
	
	$out = '<div id="box-list-kelurahan-kec-'.$kec['id_kecamatan'].'">'; 
	$out .= '<b>'. $kec['nama'].'</b><br>'; 

			
	 
	foreach($dass as $item){ 
		$out .= '<label  class="checkbox-inline" id="item-kel-'.$item->id.'">';
			$out .='<input class="cekKel" '.  ( in_array($item->id, $kelurahan) ? ' checked="checked" ' : '') .' type="checkbox" name="cekkel['.$kec['id_kecamatan'].'][]" value="'.$item->id.'"><span>'.$item->name.'</span></label><br>';

	}
	 
	
	$out .= '</div>';
	return $out;
}
function list_kec_by_kab($id_kabupaten,$kec){
	$CI =& get_instance(); 
	$dass = $CI->db->where('id_kabupaten',$id_kabupaten)->order_by('name','ASC')->get('kecamatan')->result_object(); 

	 
	$kab = $CI->db->where('id',$id_kabupaten)->get('kabupaten')->row_object(); 

	if($kab){
	
	$out = '<div id="box-list-kec-kab-'.$id_kabupaten.'">'; 
	$out .= '<b>'.$kab->name.'</b><br>'; 

			
	 
	foreach($dass as $item){
		 
		//$out .= '<option '. set_selected($cek,$item->id) .' value="'.$item->id.'">'.$item->name.'</option>'; 
		$out .= '<label  class="checkbox-inline" id="item-kec-'.$item->id.'">';
			$out .='<input data-kab='.$id_kabupaten.' class="cekKec" '.  ( in_array($item->id, $kec) ? ' checked="checked" ' : '') .' type="checkbox" name="cekkec['.$id_kabupaten.'][]" value="'.$item->id.'"><span>'.$item->name.'</span></label><br>';

	}
	 
	
	$out .= '</div>';
	return $out;
	}

	return '';
}

function combo_kecamatan($cek = '', $by = '', $attr = 'name="kecamatan" class="form-control" id="form-kecamatan"'){
	$CI =& get_instance(); 
	$dass = $CI->db->where('id_kabupaten',$by)->order_by('name','ASC')->get('kecamatan')->result_object(); 
	
	$out = '<select '.$attr.'>';
	$out .= '<option value="">-- pilih --</option>';

	if($by != ''){
		foreach($dass as $item){
			$out .= '<option '. set_selected($cek,$item->id) .' value="'.$item->id.'">'.$item->name.'</option>';
		}
	}
	
	$out .= '</select>';
	return $out;
}

function combo_das($cek = '', $id_wilayah_sungai, $attr = 'name="das" class="form-control" id="form-das"'){
	$CI =& get_instance(); 
	$dass = $CI->db->where('id_wlsungai',$id_wilayah_sungai)->order_by('nama_das','ASC')->get('tbl_das')->result_object(); 
	
	$out = '<select '.$attr.'>';
	$out .= '<option value="">-- pilih --</option>';
	foreach($dass as $item){
		$out .= '<option '. set_selected($cek,$item->id_das) .' value="'.$item->id_das.'">'.$item->nama_das.'</option>';
	}
	$out .= '</select>';
	return $out;
}

function combo_kajian_lingkungan($cek = '',  $attr = 'name="dokumen_kajian_lingkungan" class="form-control" id="dokumen_kajian_lingkungan"'){
    $CI =& get_instance(); 
    $dass = $CI->db->order_by('kajian_lingkungan','ASC')->get('tbl_kajian_lingkungan')->result_object(); 
    
    $out = '<select '.$attr.'>';
    $out .= '<option value="">-- pilih --</option>';
    foreach($dass as $item){
        $out .= '<option '. set_selected($cek,$item->id_kajian_lingkungan) .' value="'.$item->id_kajian_lingkungan.'">'.$item->kajian_lingkungan.'</option>';
    }
    $out .= '</select>';
    return $out;
}

function combo_level_user($cek = '',  $attr = 'name="level" class="form-control" id="level_user"'){
	$out = '<select '.$attr.'>';
	$out .= '<option value="">-- Pilih --</option>';
	$out .= '<option '. set_selected($cek,"superadmin") .' value="superadmin">Super Admin</option>';
	$out .= '<option '. set_selected($cek,"direksi") .' value="direksi">Akun direksi</option>';
	$out .= '<option '. set_selected($cek,"konsultan") .' value="konsultan">Akun konsultan</option>';
	

	return $out . '</select>'; 
}

function combo_level_userumum($cek = '',  $attr = 'name="level" class="form-control" id="level_user"'){
	$out = '<select '.$attr.'>';
	$out .= '<option '. set_selected($cek,"direksi") .' value="direksi">Akun direksi</option>';
	$out .= '<option '. set_selected($cek,"konsultan") .' value="konsultan">Akun konsultan</option>';

	return $out . '</select>'; 
}

function combo_progres_laporan($cek = '',  $attr = 'name="dokumen_progres" class="form-control" id="dokumen_progres"'){
	$CI =& get_instance(); 
	$dass = $CI->db->order_by('progres_laporan','ASC')->get('tbl_progres_laporan')->result_object(); 
	
	$out = '<select '.$attr.'>';
	$out .= '<option value="">-- pilih --</option>';
	foreach($dass as $item){
		$out .= '<option '. set_selected($cek,$item->id_progres_laporan) .' value="'.$item->id_progres_laporan.'">'.$item->progres_laporan.'</option>';
	}
	$out .= '</select>';
	return $out;
}

function combo_das_multi($cek = array(), $id_wilayah_sungai, $attr = 'name="das[]" multiple class="form-control" id="form-das"'){
	$CI =& get_instance(); 
	$dass = $CI->db->where('id_wlsungai',$id_wilayah_sungai)->order_by('nama_das','ASC')->get('tbl_das')->result_object(); 
	
	$out = '<select '.$attr.'>';
	//$out .= '<option value="">-- pilih --</option>';
	foreach($dass as $item){
		$out .= '<option ';

		if(in_array($item->id_das,$cek)){
			$out .= set_selected(1,1);
		}

		$out .= ' value="'.$item->id_das.'">'.$item->nama_das.'</option>';
	}
	$out .= '</select>';
	return $out;
}

function img_extensi($data){

	if(!empty($data->ekstensi)){
		return '<img src="' . base_url('assets/img/file_icon/'. $data->ekstensi .'.gif') .'" alt=" ' . $data->ekstensi .'" />';
	}

	return '-';

	
}

function label_status_asistensi($nama){
	if( $nama == "open"){
		return '<span class="label label-danger">Belum Selesai</span>';
	} else if( $nama == "close"){
		return '<span class="label label-success">Selesai</span>';
	}

	return '<span class="label label-primary">'. $nama .'</span>';
}


function label_status_file($nama){
	if(!empty($nama)){
		return '<span class="label label-success">Ada File</span>';
	}

	return '<span class="label label-danger">Belum Ada File</span>';
}


function tampil_edit_file_dokument_multi($dk){
	$result = '';

	if(is_serialized($dk->file_name)){
		$files = unserialize($dk->file_name);

		if($files){

			foreach($files as $key => $file){
				 

				$result .= '<div class="row-file">- <a href="'. base_url('uploads/kegiatan/' . $file['file_name']) .'" target="_blank">'. $file['file_name'] .'</a>';
				$result .= ' <button data-file="'. $dk->id_file_kegiatan .'" data-file_name="'.$file['file_name'].'" type="button" class="btn btn-link btn-xs btn_hapus_baris_file_dokument"><small class="text-danger">x</small></button></div>';
			}
		}
	}
	

	return $result;

}


function tampilkan_file_dokument_multi($file){
	$result = '';

	if(is_serialized($file)){
		$files = unserialize($file);

		if($files){

			foreach($files as $key => $file){

				if($key != 0){
					$result .= '<br>';
				}

				$result .= '- <a href="'. base_url('uploads/kegiatan/' . $file['file_name']) .'" target="_blank">'. $file['file_name'] .'</a>';
			}
		}
	}
	

	return $result;

}


function display_perusahaan_kso_jo($kso_jo, $pemisah = ", "){
	$items = get_perusahaan_kso_jo_to_array($kso_jo);
	return implode($pemisah,$items);
}

function get_perusahaan_kso_jo_to_array($kso_jo){

	if(empty($kso_jo)){
		return [];
	}

	if(is_serialized($kso_jo)){
		$items = unserialize($kso_jo);

		if($items){
			return $items;
		}
	}

	return [$kso_jo];
}



function maybe_unserialize( $original ) {
	if ( is_serialized( $original ) ) // don't attempt to unserialize data that wasn't serialized going in
		return @unserialize( $original );
	return $original;
}
	
	
function is_serialized( $data ) {
	// if it isn't a string, it isn't serialized
	if ( ! is_string( $data ) )
		return false;
	$data = trim( $data );
 	if ( 'N;' == $data )
		return true;
	$length = strlen( $data );
	if ( $length < 4 )
		return false;
	if ( ':' !== $data[1] )
		return false;
	$lastc = $data[$length-1];
	if ( ';' !== $lastc && '}' !== $lastc )
		return false;
	$token = $data[0];
	switch ( $token ) {
		case 's' :
			if ( '"' !== $data[$length-2] )
				return false;
		case 'a' :
		case 'O' :
			return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
		case 'b' :
		case 'i' :
		case 'd' :
			return (bool) preg_match( "/^{$token}:[0-9.E-]+;\$/", $data );
	}
	return false;
}

 