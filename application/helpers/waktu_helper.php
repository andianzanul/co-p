<?php 
/* MENAMPILKAN TANGGAL DALAM FORMAT INDONESIA */
function tgl_indo($data,$bulan = 1){

	if(empty($data)){
		return '';
	}

	if(is_null($data)){
		return '';
	}

	if($data == '0000-00-00'){
		return '';
	}

	$tgl 	= substr($data,8,2);
	if($bulan == 2){
		$bln 	= getBulan2(substr($data,5,2));
	}else{
		$bln 	= getBulan(substr($data,5,2));
	}
	
	$thn	= substr($data,0,4);
	return $tgl.' '.$bln.' '.$thn;		 
} 
	
/* MENDAPATKAN BULAN DALAM FORMAT INDONEISA */
function getBulan($bln){
	switch ($bln){
		case 1: 	return "Jan"; 	break;
		case 2: 	return "Feb"; 	break;
		case 3: 	return "Mar"; 	break;
		case 4: 	return "Apr"; 	break;
		case 5: 	return "Mei"; 		break;
		case 6: 	return "Jun"; 		break;
		case 7: 	return "Jul"; 		break;
		case 8: 	return "Agt"; 	break;
		case 9: 	return "Sept"; break;
		case 10: 	return "Okt"; 	break;
		case 11: 	return "Nov"; 	break;
		case 12: 	return "Des"; 	break;
	}
}
function getBulan2($bln){
	switch ($bln){
		case 1: 	return "Januari"; 	break;
		case 2: 	return "Februari"; 	break;
		case 3: 	return "Maret"; 	break;
		case 4: 	return "April"; 	break;
		case 5: 	return "Mei"; 		break;
		case 6: 	return "Juni"; 		break;
		case 7: 	return "Juli"; 		break;
		case 8: 	return "Agustus"; 	break;
		case 9: 	return "September"; break;
		case 10: 	return "Oktober"; 	break;
		case 11: 	return "November"; 	break;
		case 12: 	return "Desember"; 	break;
	}
} 
function waktu_post($tgl){
	$now 		= time();
	$tanggal1 	= substr($tgl,0,10);
	$jam1		= substr($tgl,11,8);
	$pecah_tanggal 	= explode("-",$tanggal1);
	$pecah_jam 		= explode(":",$jam1);
	/* mktime(int hour, int min, int sec, int mon, int day, int year); */
	$kembali=mktime($pecah_jam[0],$pecah_jam[1],$pecah_jam[2],$pecah_tanggal[1],$pecah_tanggal[2],$pecah_tanggal[0]);
	
	$selisih= $now - $kembali;
	$hari_int = date("w",$kembali);
	
	if($selisih<=5){
		return 'sesaat yang lalu ';
	}
	
	elseif($selisih<=60){
		return $selisih.' detik yang lalu';
	}
	
	elseif($selisih<=3600){
		$menit=round($selisih/60);
		return $menit. ' Menit yang lalu';
	}
	
	elseif($selisih<=86400){
		$jam=round($selisih/3600);
		return $jam.' Jam yang lalu';
	}
	
	else{
		$TglJam = date("d-M-Y, h:i",$kembali);
		return $TglJam;
	}	 
}
