<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_satuan_outcome extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_satuan_outcome')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_satuan_outcome')
	 		->where('id_satuan_outcome', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_satuan_outcome',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_satuan_outcome', $id)
	 		->update('tbl_satuan_outcome',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_satuan_outcome', $id)
	 		->delete('tbl_satuan_outcome');

	 }

}