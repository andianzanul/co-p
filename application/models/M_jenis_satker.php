<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_satker extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_satker')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_satker')
	 		->where('id_satker', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_satker',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_satker', $id)
	 		->update('tbl_satker',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_satker', $id)
	 		->delete('tbl_satker');

	 }



}