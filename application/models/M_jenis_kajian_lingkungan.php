<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_jenis_kajian_lingkungan extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	var $table_name = 'tbl_kajian_lingkungan';

    public function get_name_by_id_kajian($id) {
		$this->db->select('*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('id_kajian_lingkungan',$id ); 
		$this->db->limit(1);

		if($a = $this->db->get()->row_object()){
			return $a->kajian_lingkungan;
		}

		return '-';
    }

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_kajian_lingkungan')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_kajian_lingkungan')
	 		->where('id_kajian_lingkungan', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_kajian_lingkungan',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_kajian_lingkungan', $id)
	 		->update('tbl_kajian_lingkungan',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_kajian_lingkungan', $id)
	 		->delete('tbl_kajian_lingkungan');

	 }



}