<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_amandemen extends CI_Model{

	public $tbl_name = 'tbl_amandemen';
	public function __construct(){
		parent::__construct();
	}

	public function add($data){
		return $this->db->insert($this->tbl_name,$data);
	}

	public function get_by_kegiatan($id){
		return $this->db->where('id_kegiatan',$id)
			->select('*')
			->from($this->tbl_name)
			->get();
	}

	function update($data,$id){
	 	return $this->db->where('id_amd', $id)
	 		->update($this->tbl_name,$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_amd', $id)
	 		->delete($this->tbl_name);

	 }

	 

	 

	 

}