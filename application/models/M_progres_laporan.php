<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_progres_laporan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    var $table_name = 'tbl_progres_laporan';
	
    public function get_name_by_id($id) {
		$this->db->select('*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('id_progres_laporan',$id ); 
		$this->db->limit(1); 
		
		if($a = $this->db->get()->row_object()){
			return $a->progres_laporan;
		} 

		return '-';
    }
	
	 
	
}

