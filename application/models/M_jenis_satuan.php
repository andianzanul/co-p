<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_satuan extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_satuan')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_satuan')
	 		->where('id_satuan', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_satuan',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_satuan', $id)
	 		->update('tbl_satuan',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_satuan', $id)
	 		->delete('tbl_satuan');

	 }

}