<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_tahun extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_tahun')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_tahun')
	 		->where('id_thn', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_tahun',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_thn', $id)
	 		->update('tbl_tahun',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_thn', $id)
	 		->delete('tbl_tahun');

	 }



}