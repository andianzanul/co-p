<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_das extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    var $table_name = 'tbl_das';
	
    public function getById($par) {
		$this->db->select('*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('id_das',$par ); 
		$this->db->limit(1); 
		return  $this->db->get(); 
    }
	
	public function getByWs($par) { 
		$this->db->select('*')
			->from($this->table_name )
			->where('id_wlsungai',$par );
		return $this->db->get(); 
    }

    function insert($par) {
		if($this->db->insert($this->table_name, $par)){
			return $this->db->insert_id();
	   }else{
			return false;
	   }
    }
	 
    function update($data, $id) {
        $this->db->where('id_das', $id);
		if( $this->db->update($this->table_name, $data)){
			return true;
		}else{
			return false;
		}
    }
	 
    function delete($id) {
		$this->db->where('id_das', $id);
		if( $this->db->delete($this->table_name) ){
			return true;
		}else{
			return false;
		}
    }
	
}

