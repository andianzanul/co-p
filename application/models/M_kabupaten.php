<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_kabupaten extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    var $table_name = 'kabupaten';

	
    public function getById($par) {
		$this->db->select('*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('id',$par ); 
		$this->db->limit(1); 
		return  $this->db->get(); 
    }
	
	public function getByProvinsi($par) { 
		$this->db->select('*')
			->from($this->table_name )
			->where('id_provinsi',$par );
			
		return $this->db->get(); 
    }
	
	
	function get(){
		$this->db->select('*'); 
		$this->db->from($this->table_name .' k');
		$this->db->join('provinsi p ', 'ON k.id_provinsi=p.id_provinsi'); 
		return $this->db->get();
	}
	
	
	function getKecamatan($id_kota=0){
		$this->db->select('kec_id as id,kecamatan as n'); 
		$this->db->from('kecamatan'); 
		$this->db->order_by('kecamatan','ASC');
		$this->db->where('id_kota',$id_kota);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
			$temp = array();
			foreach($result as $item){
				$temp[$item['id']] = $item;
				$temp[$item['id']]['kel'] =  $this->getKelurahan($item['id']);
			}
            $query->free_result();
			
            return $temp;
		}
		return array();
	}
	
	function getKecamatanOnly($id_kota=0){
		return $this->db->select('kec_id as id,kecamatan as n')
		->from('kecamatan')
		->order_by('kecamatan','ASC')
		->where('id_kota',$id_kota)
        ->get()->result_array(); 
	}
	
	
	function getKelurahan($kec_id = 0){
		$this->db->select('kelurahan_id as id,kelurahan as n'); 
		$this->db->from('kelurahan'); 
		$this->db->order_by('kelurahan','ASC');
		$this->db->where('kec_id',$kec_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
			$query->free_result();
			return $data;
		}
		return array();
	}
	
	
	function getCombo(){
		$kota = $this->get();
		$temp = array();
		foreach($kota as $k){
			$temp[$k['id_kota']] = $k['kota'];
		}
		unset($kota);
		
		return $temp;
	}
	
	
	
	function get_count()
    {
        $query = $this->db->get($this->table_name);
        return $query->num_rows();
    }
	
	
	
	// digunakan untuk insert 
    function insert($par) {
		if($this->db->insert($this->table_name, $par)){
			return $this->db->insert_id();
	   }else{
			return false;
	   }
    }

	// digunakan untuk update 
    function update($data, $id) {
        $this->db->where('id', $id);
		if( $this->db->update($this->table_name, $data)){
			return true;
		}else{
			return false;
		}
    }
	
	
	
	// digunakan untuk menghapus
    function delete($id) {
		$this->db->where('id', $id);
		
		if( $this->db->delete($this->table_name) ){
			return true;
		}else{
			return false;
		}
    }

	
	
}

