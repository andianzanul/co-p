<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_ppk extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_ppk p')
	 		->join('tbl_satker s','p.id_satker=s.id_satker','LEFT')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_ppk')
	 		->where('id_ppk', $id)
	 		->get();
	 }
 
	 public function insert($data){
	 	if($this->db->insert('tbl_ppk',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_ppk', $id)
	 		->update('tbl_ppk',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_ppk', $id)
	 		->delete('tbl_ppk');

	 }

	 public function get_for_combo(){
	 	return $this->db->select("*")
	 		->order_by("nama_ppk","ASC") 
	 		->get('tbl_ppk p');
	 }



}