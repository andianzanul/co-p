<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_output extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_output p')
	 		->join('tbl_bidang b','p.id_bidang=b.id_bidang','LEFT')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_output')
	 		->where('id_output', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_output',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_output', $id)
	 		->update('tbl_output',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_output', $id)
	 		->delete('tbl_output');

	 }
}