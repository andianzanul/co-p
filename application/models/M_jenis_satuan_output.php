<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_satuan_output extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_satuan_output')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_satuan_output')
	 		->where('id_satuan_output', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_satuan_output',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_satuan_output', $id)
	 		->update('tbl_satuan_output',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_satuan_output', $id)
	 		->delete('tbl_satuan_output');

	 }

}