<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_kecamatan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    var $table_name = 'kecamatan';

	function get(){
		$this->db->select( '*'); 
		$this->db->from($this->table_name ); 
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			$data = $query->result_array();
			$query->free_result();
			return $data;
		}

		$query->free_result();
		return false;
	}


	function kecamatan($id){
		return $this->db->select('*')
			->where('id',$id)
			->from('kecamatan')
			->get();
	}
	
	function get_kecamatan_by_name($name = ''){
		if(empty($name)) return false;
		
		$this->db->select( '*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('kecamatan',$name);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$result = $query->row_array(); 
            $query->free_result();
			
            return $result;
		}
		$query->free_result();
		return false;
	}
	
    public function getById($par) {
		$this->db->select( '*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('id',$par ); 
		$this->db->limit(1); 
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
		return false;
    }
	
	public function getByKota($par) { 
		$this->db->select('kecamatan.*, kabupaten.name as kab'); 
		$this->db->join('kabupaten', 'kabupaten.id=kecamatan.id_kabupaten','LEFT');
		$this->db->from($this->table_name ); 
		

		if(is_array($par)){
			$this->db->where_in('id_kabupaten',$par );
			$this->db->order_by('id_kabupaten',"DESC" );
		}else{
			$this->db->where('id_kabupaten',$par );
		}
		
        return $this->db->get();
    }


	
	function get_count()
    {
        $query = $this->db->get($this->table_name);
        return $query->num_rows();
    }

	// digunakan untuk insert 
    function insert($par) {
		if($this->db->insert($this->table_name, $par)){
			return $this->db->insert_id();
		}else{
			return false;
		}
    }

	// digunakan untuk update 
    function update($data, $id) {
        $this->db->where('id', $id);
		if( $this->db->update($this->table_name, $data)){
			return true;
		}else{
			return false;
		}
    }
	
	// digunakan untuk menghapus
    function delete($id) {
		$this->db->where('id', $id);
		if( $this->db->delete($this->table_name) ){
			return true;
		}else{
			return false;
		}
    }
}

