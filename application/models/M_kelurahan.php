<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_kelurahan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    var $table_name = 'kelurahan';

	function get(){

		$this->db->select( '*'); 
		$this->db->from($this->table_name ); 
		
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			
			$data = $query->result_array();
			
			$query->free_result();
			return $data;
		}
		$query->free_result();
		return false;
	}
	
	function get_by_name($name = ''){
		if(empty($name)) return false;
		
		$this->db->select( '*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('kelurahan',$name);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			
			$result = $query->row_array(); 
            $query->free_result();
			
            return $result;
		}
		$query->free_result();
		return false;
	}
	
    public function getById($par = '') {
		$this->db->select( '*'); 
		$this->db->from($this->table_name ); 
		$this->db->where('id',$par ); 
		$this->db->limit(1); 
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        }
		return false;
    }
	
	public function getByKec($par) { 
		
		$this->db->select( 'kelurahan.*, kecamatan.name as kec'); 
		$this->db->join('kecamatan', 'kecamatan.id=kelurahan.id_kecamatan','LEFT');
		$this->db->from($this->table_name );  

		if(is_array($par)){
			$this->db->where_in('id_kecamatan',$par );
			$this->db->order_by('id_kecamatan',"DESC" );
		}else{
			$this->db->where('id_kecamatan',$par );
		}

		return $this->db->get();
    }
	
	function get_count()
    {
        $query = $this->db->get($this->table_name);
        return $query->num_rows();
    }
	
	
	
	// digunakan untuk insert 
    function insert($par) {
		if($this->db->insert($this->table_name, $par)){
			return $this->db->insert_id();
		}else{
			return false;
		}
    }

	// digunakan untuk update 
    function update($data, $id) {
        $this->db->where('id', $id);
		if( $this->db->update($this->table_name, $data)){
			return true;
		}else{
			return false;
		}
    }
	
	
	
	// digunakan untuk menghapus
    function delete($id) {
		$this->db->where('id', $id);
		
		if( $this->db->delete($this->table_name) ){
			return true;
		}else{
			return false;
		}
    }

	
	
}

