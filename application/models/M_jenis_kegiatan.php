<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_kegiatan extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_jenis_kegiatan')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_jenis_kegiatan')
	 		->where('id_jenis_kegiatan', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_jenis_kegiatan',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_jenis_kegiatan', $id)
	 		->update('tbl_jenis_kegiatan',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_jenis_kegiatan', $id)
	 		->delete('tbl_jenis_kegiatan');

	 }



}