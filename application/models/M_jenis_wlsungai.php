<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_wlsungai extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_wlsungai')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_wlsungai')
	 		->where('id_wlsungai', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_wlsungai',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_wlsungai', $id)
	 		->update('tbl_wlsungai',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_wlsungai', $id)
	 		->delete('tbl_wlsungai');

	 }

}