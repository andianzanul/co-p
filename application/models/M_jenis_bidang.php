<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_bidang extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	 public function get(){
	 	return $this->db->select("*")
	 		->from('tbl_bidang')
	 		->get();
	 }

	 function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_bidang')
	 		->where('id_bidang', $id)
	 		->get();
	 }

	 public function insert($data){
	 	if($this->db->insert('tbl_bidang',$data)){
	 		return $this->db->insert_id();
	 	}

	 	return false;
	 }

	  function update($data,$id){

	 	return $this->db->where('id_bidang', $id)
	 		->update('tbl_bidang',$data);

	 }

	 function delete($id){

	 	return $this->db->where('id_bidang', $id)
	 		->delete('tbl_bidang');

	 }



}