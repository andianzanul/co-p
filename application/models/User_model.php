<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get() {
		return $this->db->select('*')
			->from('tbl_user u')
			->join('tbl_satker s', 'u.id_satker=s.id_satker', 'LEFT')
			->get();
	}

	function get_by_id($id){
	 	return $this->db->select("*")
	 		->from('tbl_user')
	 		->where('user_id', $id)
	 		->get();
	}

	public function user_register($input){
		$this->load->helper('site_helper');
		$encrypt_password = bCrypt($input['password'],12);

		$array_user = array(
			'nama' => $input['nama'],
			'username' => $input['username'],
			'password' => $encrypt_password,
			'email' => $input['email'],
			//'id_satker' => $input['satker'],
			'level' => $input['level'],
			'active_since' => date('Y-m-d')
		);

		if(isset($input['satker'])){
			$array_user['id_satker'] = $input['satker'];
		}

		if(isset($input['ppk'])){

			$ppk = $this->db->where("id_ppk",$input['ppk'])->get('tbl_ppk')->row_object();

			$array_user['id_satker'] = $ppk->id_satker;
			$array_user['id_ppk'] = $input['ppk'];
		}


		$this->db->insert('tbl_user', $array_user);
	}

	public function exist_row_check($field,$data){
		$this->db->where($field,$data);
		$this->db->from('tbl_user');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function get_user_detail($username){
		$this->db->where("username", $username);
		$query = $this->db->get('tbl_user');

		if($query->num_rows() > 0){
			$data = $query->row();
			$query->free_result();
		}
		else{
			$data = NULL;
		} 

		return $data;
	}

	// public function tampil_user(){
	// 	$hsl=$this->db->query("SELECT tbl_user.user_id,tbl_user.username,tbl_user.email,tbl_user.level,tbl_user.active_since,tbl_user.id_satker,tbl_satker.nama_satker FROM tbl_user, tbl_satker WHERE tbl_user.id_satker=tbl_satker.id_satker");
	// 	return $hsl;
	// }

	

	function update($data,$id){
	 	return $this->db->where('user_id', $id)
	 		->update('tbl_user',$data);
	}

	// function update_pengguna($user_id,$username,$id_satker,$email,$level,$password){
	// 	$hsl=$this->db->query("UPDATE tbl_user SET username='$username',id_satker='$id_satker', email='$email', password='$password', level='$level' WHERE user_id='$user_id'");
	// 	return $hsl;
	// }

	function delete($id){

	 	return $this->db->where('user_id', $id)
	 		->delete('tbl_user');
	 }
}