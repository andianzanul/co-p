<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kegiatan_model extends CI_Model
{

    public $table = 'tbl_kegiatan';
    public $id    = 'id_kegiatan';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }


    /*** DETAIL KONSTRUKSI ***/
    function get_detail_konstruksi($id){
        $this->db->select('dk.*, tbl_satuan.nama_satuan') 
            ->join('tbl_satuan','tbl_satuan.id_satuan=dk.satuan','left')
            ->order_by("id_detail_konstruksi", "ASC")
            ->where('id_kegiatan', $id); 
        return $this->db->get('tbl_kegiatan_detail_konstruksi dk');
    }
    function delete_detail_konstruksi($id){
        $this->db->where('id_detail_konstruksi', $id);
        return   $this->db->delete('tbl_kegiatan_detail_konstruksi');
    }
    function add_detail_konstruksi($data){
        return $this->db->insert('tbl_kegiatan_detail_konstruksi',$data);
    }
    function update_detail_konstruksi($id, $data){
        $this->db->where('id_detail_konstruksi', $id);
        return $this->db->update('tbl_kegiatan_detail_konstruksi', $data);
    }

    /* laporan */
    function get_laporan($id){
        $this->db->select('*')
            ->order_by("id_laporan", "DESC")
            ->where('id_kegiatan', $id);
        return $this->db->get('tbl_laporan');
    }

    function add_laporan($data){
        return $this->db->insert('tbl_laporan',$data);
    }


    function update_laporan($id_laporan, $data){
        $this->db->where('id_laporan', $id_laporan);
        return $this->db->update('tbl_laporan', $data);
    }

    function delete_laporan($id){
        $this->db->where('id_laporan', $id);

        if($this->db->delete('tbl_laporan')){

            # hapus juga data asistensinya
            $this->db->where('id_laporan', $id)->delete('tbl_asistensi');

            return true;
        }
        return  false;
    }

    /* end laporan */



    
    /* asistensi */
    function get_asistensi_by_laporan($id_laporan){
        return$this->db->select('*') 
            ->where('id_laporan', $id_laporan)
            ->get('tbl_asistensi');
    }

    function add_asistensi($data){
        return $this->db->insert('tbl_asistensi',$data);
    }

    function update_asistensi($id, $data){
        $this->db->where('id_asistensi', $id);
        return $this->db->update('tbl_asistensi', $data);
    }

    function add_asistensi_lbr($data){
        return $this->db->insert('tbl_asistensi_lbr',$data);
    }

    function delete_asistensi($id){
        $this->db->where('id_asistensi', $id);

        if($this->db->delete('tbl_asistensi')){
            # hapus juga data lembar asistensinya
            $this->db->where('id_asistensi', $id)->delete('tbl_asistensi_lbr');

            return true;
        }
        return  false;
    }

    /* end asistensi */
    


    /*** REALISASI PELAKSANAAN ***/
    function get_realisasi_pelaksanaan($id){
        $this->db->select('*')
            ->order_by("id_realisasi_pelaksanaan", "ASC")
            ->where('id_kegiatan', $id);
        return $this->db->get('tbl_kegiatan_realisasi_pelaksanaan rp');
    }
    function delete_realisasi_pelaksanaan($id){
        $this->db->where('id_realisasi_pelaksanaan', $id);
        return   $this->db->delete('tbl_kegiatan_realisasi_pelaksanaan');
    }
    function add_realisasi_pelaksanaan($data){
        return $this->db->insert('tbl_kegiatan_realisasi_pelaksanaan',$data);
    }
    function update_realisasi_pelaksanaan($id, $data){
        $this->db->where('id_realisasi_pelaksanaan', $id);
        return $this->db->update('tbl_kegiatan_realisasi_pelaksanaan', $data);
    }


    /* kegiatan relasi */
    function get_relasi($id, $name){
        $this->db->select('*')
            ->order_by("id", "ASC")
            ->where('id_kegiatan', $id)
            ->where('name', $name); 
        return $this->db->get('tbl_kegiatan_relasi');
    }

    function add_relasi($data){
        return $this->db->insert('tbl_kegiatan_relasi',$data);
    }

    function delete_relasi($id){
        $this->db->where('id', $id);
        return   $this->db->delete('tbl_kegiatan_relasi');
    }

    function update_relasi($id, $data){
        $this->db->where('id', $id);
        return $this->db->update('tbl_kegiatan_relasi', $data);
    }

    // get all
    function get_all()
    {  
        $this->db->select('k.*, jk.*');
        $this->db->order_by($this->id, $this->order);
        $this->db->join("tbl_jenis_kegiatan jk",'ON jk.id_jenis_kegiatan=k.id_jenis_kegiatan');
        return $this->db->get($this->table . 'k')->result();
    }

    // get data by id
    function read_by_id($id) {
        $this->db->select('tbl_kegiatan.*, tbl_jenis_kegiatan.*,tbl_satker.*,tbl_bidang.*,tbl_ppk.*,tbl_wlsungai.*,tbl_output.*,tbl_satuan_output.*, tbl_satuan_outcome.*'); //, tbl_satuan.*
        $this->db->order_by($this->id, $this->order);
        $this->db->join("tbl_jenis_kegiatan",'ON tbl_jenis_kegiatan.id_jenis_kegiatan=tbl_kegiatan.id_jenis_kegiatan','LEFT');
        $this->db->join("tbl_satker",'ON tbl_satker.id_satker=tbl_kegiatan.id_satker','LEFT');   
        $this->db->join("tbl_bidang",'ON tbl_bidang.id_bidang=tbl_bidang.id_bidang','LEFT');  
        $this->db->join("tbl_ppk",'ON tbl_ppk.id_ppk=tbl_kegiatan.id_ppk','LEFT');  
        // $this->db->join("tbl_tahun",'ON tbl_tahun.id_thn=tbl_kegiatan.id_thn');  
        $this->db->join("tbl_wlsungai",'ON tbl_wlsungai.id_wlsungai=tbl_kegiatan.id_wlsungai','LEFT');
        $this->db->join("tbl_output",'ON tbl_output.id_output=tbl_kegiatan.id_output','LEFT');
        // $this->db->join("tbl_kajian_lingkungan",'ON tbl_kajian_lingkungan.id_kajian_lingkungan=tbl_kegiatan.id_kajian_lingkungan','LEFT');
        //$this->db->join("tbl_satuan", 'ON tbl_satuan.id_satuan=tbl_kegiatan.id_satuan', 'LEFT');
        $this->db->join("tbl_satuan_output", 'ON tbl_satuan_output.id_satuan_output=tbl_kegiatan.id_satuan_output', 'LEFT');
        $this->db->join("tbl_satuan_outcome", 'ON tbl_satuan_outcome.id_satuan_outcome=tbl_kegiatan.id_satuan_outcome', 'LEFT');
        $this->db->select('tbl_das.nama_das, tbl_das.tipe_das')->join("tbl_das", 'ON tbl_kegiatan.das=tbl_das.id_das', 'LEFT');

        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_satker()
    {
        $this->db->select('*');
        $this->db->where($this->id_satker,$id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
     function get_limit_data($konsultan,$limit=0, $start = 0,$par = array()) {

    		$this->db->select('k.*,uk.user_id as user_id_pembuat,uk.nama,u.nama as nama_lastupdate,up.nama as nama_penyediajasa')
        ->from($this->table . ' k');
        $this->db->order_by('k.id_kegiatan', $this->order);
        $this->db->join("tbl_jenis_kegiatan jk",'ON jk.id_jenis_kegiatan=k.id_jenis_kegiatan','left');
        $this->db->join("tbl_satker s",'ON k.id_satker=s.id_satker','left');
        $this->db->join("tbl_bidang b",'ON k.id_bidang=b.id_bidang','left');
        $this->db->join("tbl_ppk p",'ON k.id_ppk=p.id_ppk','left');
        $this->db->join("tbl_output o",'ON k.id_output=o.id_output','left');
        $this->db->join("tbl_user u",'ON k.last_update_user=u.user_id','left');
        $this->db->join("tbl_user uk",'ON k.user_id=uk.user_id','left');
        $this->db->join("tbl_user up",'ON k.konsultan=up.user_id','left');
        $this->db->join("tbl_wlsungai wl",'ON k.id_wlsungai=wl.id_wlsungai','left');
        // $this->db->join("tbl_kajian_lingkungan kl",'ON k.id_kajian_lingkungan=kl.id_kajian_lingkungan','left');
        $this->db->join("tbl_satuan_output sp", 'ON k.id_satuan_output=sp.id_satuan_output', 'left');
        $this->db->join("tbl_satuan_outcome sc", 'ON k.id_satuan_outcome=sc.id_satuan_outcome', 'left');
        
        if(array_key_exists('konsultan', $par)){
            if($par['tahun']){
                $this->db->group_start();
                foreach( $par['tahun'] as $tt){
                    $this->db->or_where('k.tahun', $tt);
                }
                $this->db->group_end();
            }
        }

        if(array_key_exists('tahun', $par)){
            if($par['tahun']){
                $this->db->group_start();
                foreach( $par['tahun'] as $tt){
                    $this->db->or_where('k.tahun', $tt);
                }
                $this->db->group_end();
            }
        }

        if(array_key_exists('nama_kegiatan', $par)){
            $this->db->like('k.nm_kegiatan', $par['nama_kegiatan']);
        }

        if(array_key_exists('kegiatan', $par)){
            if($par['kegiatan']){
                $this->db->group_start();
                foreach( $par['kegiatan'] as $bb){
                    $this->db->or_where('k.id_bidang', $bb);
                }
                $this->db->group_end();
            } 
        }

        if(array_key_exists('jenis_kegiatan', $par)){
            if($par['jenis_kegiatan']){
                $this->db->group_start();
                foreach( $par['jenis_kegiatan'] as $jj){
                    $this->db->or_where('k.id_jenis_kegiatan', $jj);
                }
                $this->db->group_end();
            } 
        }
       
        $belum_join_kl = true;
        if(array_key_exists('prov', $par)){
            if($par['prov']){
                $this->db->join("tbl_kegiatan_lokasi kl",'ON k.id_kegiatan=kl.id_kegiatan','left');
                $belum_join_kl = false;
                $this->db->group_start();
                foreach( $par['prov'] as $pr){
                    $this->db->or_where('kl.id_provinsi', $pr);
                }
                $this->db->group_end();
            } 
        }

         if(array_key_exists('kab', $par)){
            if($par['kab']){
                if($belum_join_kl ){
                $this->db->join("tbl_kegiatan_lokasi kl",'ON k.id_kegiatan=kl.id_kegiatan','left');
                }
                $this->db->group_start();
                foreach( $par['kab'] as $pr){
                    $this->db->or_where('kl.id_kabupaten', $pr);
                }
                $this->db->group_end();
            } 
        }

        if(array_key_exists('satker', $par)){
            if($par['satker']){
                $this->db->group_start();
                foreach( $par['satker'] as $cc){
                    $this->db->or_where('k.id_satker', $cc);
                }
                $this->db->group_end();
            }
        }

        
        // limit
        if(array_key_exists('limit' , $par)){
            if(is_array($par['limit'])){
                $this->db->limit( $par['limit'][0] , $par['limit'][1] );
            }else{
                $this->db->limit( $par['limit']);
            }
        }

        if(array_key_exists('id_kegiatan' , $par)){
            if(is_array($par['id_kegiatan'])){
                $this->db->where_in( 'k.id_kegiatan',$par['id_kegiatan'] );
            }else{
                $this->db->where( 'k.id_kegiatan',$par['id_kegiatan']);
            }
        }

		//$this->db->limit($limit, $start);
        return $this->db->get();
    }

// get data with limit and search bysatker
    function get_limit_data_bysatker($limit=0, $start = 0,$par = array()) {
        $this->db->select('*')->from($this->table . ' k');
        $this->db->order_by('k.id_kegiatan', $this->order);
        $this->db->where('s.id_satker',$this->session->userdata('id_satker'));
        $this->db->join("tbl_jenis_kegiatan jk",'ON jk.id_jenis_kegiatan=k.id_jenis_kegiatan','left');
        $this->db->join("tbl_satker s",'ON k.id_satker=s.id_satker','left');
        $this->db->join("tbl_bidang b",'ON k.id_bidang=b.id_bidang','left');
        $this->db->join("tbl_ppk p",'ON k.id_ppk=p.id_ppk','left');
        $this->db->join("tbl_output o",'ON k.id_output=o.id_output','left');
        $this->db->join("tbl_user u",'ON k.last_update_user=u.user_id','left');
        $this->db->join("tbl_wlsungai wl",'ON k.id_wlsungai=wl.id_wlsungai','left');
        // $this->db->join("tbl_kajian_lingkungan kl",'ON k.id_kajian_lingkungan=kl.id_kajian_lingkungan','left');
        /*$this->db->join("tbl_satuan st", 'ON k.id_satuan=st.id_satuan', 'left');*/

        if(array_key_exists('tahun', $par)){
            if($par['tahun']){
                $this->db->group_start();
                foreach( $par['tahun'] as $tt){
                    $this->db->or_where('k.tahun', $tt);
                }
                $this->db->group_end();
            }
        }

        if(array_key_exists('nama_kegiatan', $par)){
            $this->db->like('k.nm_kegiatan', $par['nama_kegiatan']);
        }

        if(array_key_exists('kegiatan', $par)){
            if($par['kegiatan']){
                $this->db->group_start();
                foreach( $par['kegiatan'] as $bb){
                    $this->db->or_where('k.id_bidang', $bb);
                }
                $this->db->group_end();
            } 
        }

        if(array_key_exists('jenis_kegiatan', $par)){
            if($par['jenis_kegiatan']){
                $this->db->group_start();
                foreach( $par['jenis_kegiatan'] as $jj){
                    $this->db->or_where('k.id_jenis_kegiatan', $jj);
                }
                $this->db->group_end();
            } 
        }
        $belum_join_kl = true;
        if(array_key_exists('prov', $par)){
            if($par['prov']){
                $this->db->join("tbl_kegiatan_lokasi kl",'ON k.id_kegiatan=kl.id_kegiatan','left');
                $belum_join_kl = false;
                $this->db->group_start();
                foreach( $par['prov'] as $pr){
                    $this->db->or_where('kl.id_provinsi', $pr);
                }
                $this->db->group_end();
            } 
        }

         if(array_key_exists('kab', $par)){
            if($par['kab']){
                if($belum_join_kl ){
                $this->db->join("tbl_kegiatan_lokasi kl",'ON k.id_kegiatan=kl.id_kegiatan','left');
                }
                $this->db->group_start();
                foreach( $par['kab'] as $pr){
                    $this->db->or_where('kl.id_kabupaten', $pr);
                }
                $this->db->group_end();
            } 
        }

        if(array_key_exists('satker', $par)){
            if($par['satker']){
                $this->db->group_start();
                foreach( $par['satker'] as $cc){
                    $this->db->or_where('k.id_satker', $cc);
                }
                $this->db->group_end();
            }
        }
        
        // limit
        if(array_key_exists('limit' , $par)){
            if(is_array($par['limit'])){
                $this->db->limit( $par['limit'][0] , $par['limit'][1] );
            }else{
                $this->db->limit( $par['limit']);
            }
        }

        if(array_key_exists('id_kegiatan' , $par)){
            if(is_array($par['id_kegiatan'])){
                $this->db->where_in( 'k.id_kegiatan',$par['id_kegiatan'] );
            }else{
                $this->db->where( 'k.id_kegiatan',$par['id_kegiatan']);
            }
        }
        //$this->db->limit($limit, $start);
        return $this->db->get();
    }
// akhir get_limit_data_bysatker



    // get data with limit and search bysatker
    function get_limit_data_by_ppk($limit=0, $start = 0,$par = array()) {
        $this->db->select('*')->from($this->table . ' k');
        
        $this->db->join("tbl_jenis_kegiatan jk",'ON jk.id_jenis_kegiatan=k.id_jenis_kegiatan','left');
        $this->db->join("tbl_satker s",'ON k.id_satker=s.id_satker','left');
        $this->db->join("tbl_bidang b",'ON k.id_bidang=b.id_bidang','left');
        $this->db->join("tbl_ppk p",'ON k.id_ppk=p.id_ppk','left');
        $this->db->join("tbl_output o",'ON k.id_output=o.id_output','left');
        $this->db->join("tbl_user u",'ON k.last_update_user=u.user_id','left');
        $this->db->join("tbl_wlsungai wl",'ON k.id_wlsungai=wl.id_wlsungai','left');
        // $this->db->join("tbl_kajian_lingkungan kl",'ON k.id_kajian_lingkungan=kl.id_kajian_lingkungan','left');
        /*$this->db->join("tbl_satuan st", 'ON k.id_satuan=st.id_satuan', 'left');*/


        $this->db->order_by('k.id_kegiatan', $this->order);
        $this->db->where('k.konsultan',$this->session->userdata('id_user'));

        if(array_key_exists('tahun', $par)){
            if($par['tahun']){
                $this->db->group_start();
                foreach( $par['tahun'] as $tt){
                    $this->db->or_where('k.tahun', $tt);
                }
                $this->db->group_end();
            }
        }

        if(array_key_exists('nama_kegiatan', $par)){
            $this->db->like('k.nm_kegiatan', $par['nama_kegiatan']);
        }

        if(array_key_exists('kegiatan', $par)){
            if($par['kegiatan']){
                $this->db->group_start();
                foreach( $par['kegiatan'] as $bb){
                    $this->db->or_where('k.id_bidang', $bb);
                }
                $this->db->group_end();
            } 
        }

        if(array_key_exists('jenis_kegiatan', $par)){
            if($par['jenis_kegiatan']){
                $this->db->group_start();
                foreach( $par['jenis_kegiatan'] as $jj){
                    $this->db->or_where('k.id_jenis_kegiatan', $jj);
                }
                $this->db->group_end();
            } 
        }
        $belum_join_kl = true;
        if(array_key_exists('prov', $par)){
            if($par['prov']){
                $this->db->join("tbl_kegiatan_lokasi kl",'ON k.id_kegiatan=kl.id_kegiatan','left');
                $belum_join_kl = false;
                $this->db->group_start();
                foreach( $par['prov'] as $pr){
                    $this->db->or_where('kl.id_provinsi', $pr);
                }
                $this->db->group_end();
            } 
        }

         if(array_key_exists('kab', $par)){
            if($par['kab']){
                if($belum_join_kl ){
                $this->db->join("tbl_kegiatan_lokasi kl",'ON k.id_kegiatan=kl.id_kegiatan','left');
                }
                $this->db->group_start();
                foreach( $par['kab'] as $pr){
                    $this->db->or_where('kl.id_kabupaten', $pr);
                }
                $this->db->group_end();
            } 
        }

        if(array_key_exists('satker', $par)){
            if($par['satker']){
                $this->db->group_start();
                foreach( $par['satker'] as $cc){
                    $this->db->or_where('k.id_satker', $cc);
                }
                $this->db->group_end();
            }
        }
        
        // limit
        if(array_key_exists('limit' , $par)){
            if(is_array($par['limit'])){
                $this->db->limit( $par['limit'][0] , $par['limit'][1] );
            }else{
                $this->db->limit( $par['limit']);
            }
        }

        if(array_key_exists('id_kegiatan' , $par)){
            if(is_array($par['id_kegiatan'])){
                $this->db->where_in( 'k.id_kegiatan',$par['id_kegiatan'] );
            }else{
                $this->db->where( 'k.id_kegiatan',$par['id_kegiatan']);
            }
        }
        //$this->db->limit($limit, $start);
        return $this->db->get();
    }
// akhir get_limit_data_bysatker

    // insert data
    public function insert($data){
        if($this->db->insert($this->table,$data)){
            return $this->db->insert_id();
        }
        return false;
     }

    // update data
    function update($data,$id)
    {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        return   $this->db->delete($this->table);
    }

    function get_image($id){
        return $this->db->select('*')
            ->where('id_kegiatan',$id)
            ->from('tbl_gambar')
            ->get();
    }

    public function add_image($data){
        if($this->db->insert('tbl_gambar',$data)){
            return $this->db->insert_id();
        }
        return false;
    }

    /* FILES */
    public function add_file($data){
        if($this->db->insert('tbl_kegiatan_files',$data)){
            return $this->db->insert_id();
        }
        return false;
    }

    public function get_files($id_kegiatan,$jenis){
        return $this->db->where('id_kegiatan',$id_kegiatan)
        ->where('jenis',$jenis)
        ->order_by('nama','ASC')
        ->join('tbl_progres_laporan','tbl_progres_laporan.id_progres_laporan=tbl_kegiatan_files.id_progres_laporan','LEFT')
       // ->join('tbl_kajian_lingkungan','tbl_kajian_lingkungan.id_kajian_lingkungan=tbl_kegiatan_files.id_kajian_lingkungan','LEFT')
        ->get('tbl_kegiatan_files');
    }
    
        public function get_files2($id_kegiatan,$jenis){
        return $this->db->where('id_kegiatan',$id_kegiatan)
        ->where('jenis',$jenis)
        ->order_by('nama','ASC')
        ->join('tbl_kajian_lingkungan','tbl_kajian_lingkungan.id_kajian_lingkungan=tbl_kegiatan_files.id_kajian_lingkungan','LEFT')
       // ->join('tbl_kajian_lingkungan','tbl_kajian_lingkungan.id_kajian_lingkungan=tbl_kegiatan_files.id_kajian_lingkungan','LEFT')
        ->get('tbl_kegiatan_files');
    }

    public function get_file($id_file_kegiatan){
        return $this->db->where('id_file_kegiatan',$id_file_kegiatan)->get('tbl_kegiatan_files');
    }

    function delete_file($id_file){
        $file = $this->db->where('id_file_kegiatan',$id_file)->get('tbl_kegiatan_files')->row_object();
        if($file){

            if(!empty($file->file_name)){

                if(is_serialized($file->file_name)){

                    // hapus banyak file
                    $files = unserialize($file->file_name);
                    foreach ($files as $file) {
                        $path = 'uploads/kegiatan/' . $file['file_name'];
                        if(file_exists($path)){
                            unlink($path);
                        }
                    }

                }else{

                    // hapus 1 file saja
                    $path = 'uploads/kegiatan/' . $file->file_name;
                    if(file_exists($path)){
                        unlink($path);
                    }
                }


                
            }
            return $this->db->where('id_file_kegiatan',$id_file)->delete('tbl_kegiatan_files');
        }
        return false;
    }

    function update_file($id_file_kegiatan,$data){
         $this->db->where('id_file_kegiatan',$id_file_kegiatan);
        return $this->db->update('tbl_kegiatan_files',$data);
    }

    /* start provinsi*/
    function provinsi(){
        return $this->db->order_by('id_provinsi','ASC')->get('provinsi')->result_array();
    } /*end provinsi*/

    /*function kabupaten($provId){

        $kabupaten="<option value='0'>--pilih--</pilih>";

        $this->db->order_by('name','ASC');
        $kab= $this->db->get_where('kabupaten',array('id_provinsi'=>$provId));

        foreach ($kab->result_array() as $data ){
            $kabupaten.= "<option value='$data[id]'>$data[name]</option>";
        }

        return $kabupaten;

    }

    function kecamatan($kabId){
        $kecamatan="<option value='0'>--pilih--</pilih>";

        $this->db->order_by('name','ASC');
        $kec= $this->db->get_where('kecamatan',array('id_kabupaten'=>$kabId));
        foreach ($kec->result_array() as $data ){
        $kecamatan.= "<option value='$data[id]'>$data[name]</option>";
        }

        return $kecamatan;
    }

    function kelurahan($kecId){
        $kelurahan="<option value='0'>--pilih--</pilih>";

        $this->db->order_by('name','ASC');
        $kel= $this->db->get_where('kelurahan',array('id_kecamatan  '=>$kecId));

        foreach ($kel->result_array() as $data ){
        $kelurahan.= "<option value='$data[id]'>$data[name]</option>";
        }

        return $kelurahan;
    }*/

    /* WILAYAH SUNGAI */
    function delete_ws($id_kegiatan,$id_wlsungai){
        return $this->db->where('id_wlsungai',$id_wlsungai)->where('id_kegiatan',$id_kegiatan)->delete('tbl_kegiatan_ws');
    }

    function get_ws($id_kegiatan){
        return $this->db->select('tbl_kegiatan_ws.*,tbl_wlsungai.nama_wlsungai')
            ->where('id_kegiatan',$id_kegiatan)
            ->join('tbl_wlsungai','tbl_wlsungai.id_wlsungai=tbl_kegiatan_ws.id_wlsungai')
            ->get('tbl_kegiatan_ws');
    }

    function get_ws_das($id_kegiatan){
        $ws = $this->get_ws($id_kegiatan)->result_object();
        foreach($ws as $key => $wss){
            $ws[$key]->das = $this->get_das($id_kegiatan,$wss->id_wlsungai)->result_object();
        }
        return $ws;
    }

    function add_ws($id_kegiatan, $ws){
        $data = array();
        foreach ($ws as $d) {
            $jumlah = $this->db->where('id_wlsungai',$d)->where('id_kegiatan',$id_kegiatan)->get('tbl_kegiatan_ws')->num_rows();
            if($jumlah == 0){
                $this->db->insert('tbl_kegiatan_ws', array(
                    'id_kegiatan' => $id_kegiatan,
                    'id_wlsungai' => $d
                )) ;
            }
        }
    } /*end wilayah sungai*/

    /* DAS */
    function delete_das($id_kegiatan,$id_wlsungai){
        return $this->db->where('id_wlsungai',$id_wlsungai)->where('id_kegiatan',$id_kegiatan)->delete('tbl_kegiatan_das');
    }

    function get_das_array($id_kegiatan,$id_wlsungai){
        $das = $this->get_das($id_kegiatan,$id_wlsungai)->result_object();
        $temp = array();
        foreach ($das as $value) {
            $temp[] = $value->id_das;
        }
        return $temp;
    }

    function get_das($id_kegiatan,$id_wlsungai){
        return $this->db->select('tbl_kegiatan_das.*,tbl_das.nama_das')
            ->where('id_kegiatan',$id_kegiatan)
            ->where('tbl_kegiatan_das.id_wlsungai',$id_wlsungai) 
            ->join('tbl_das','tbl_das.id_das=tbl_kegiatan_das.id_das')
            ->get('tbl_kegiatan_das');
    }

    function set_das($id_kegiatan, $id_wlsungai, $das){ 
        $this->db->trans_begin();
        /* HAPUS SEMUA DAS */
        $sql_delete = $this->db->where('id_kegiatan',$id_kegiatan)->where('id_wlsungai',$id_wlsungai)->get_compiled_delete('tbl_kegiatan_das');
        $this->db->query($sql_delete);
        /* TAMBAH NEW DAS */
        foreach($das as $d){ 
            $sql_insert = $this->db->set(array(
                'id_wlsungai' => $id_wlsungai,
                'id_kegiatan' => $id_kegiatan,
                'id_das' => $d
            ))->get_compiled_insert('tbl_kegiatan_das');
            $this->db->query($sql_insert);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }

// start koordinat Zikra
    function set_koordinat($data){
        return $this->db->insert('tbl_koordinat',$data);
    }
    function set_koordinat_garis($data){
        return $this->db->insert('tbl_koordinat',$data);
    }
    function set_koordinat_luasan($data){
        return $this->db->insert('tbl_koordinat',$data);
    }
    function get_koordinat($id_kegiatan){
        return $this->db->select('*')
            ->from('tbl_koordinat')
            ->where('id_kegiatan',$id_kegiatan)
            ->where("tipe='titik'")
            ->get();
    }
    function get_koordinat_garis($id_kegiatan){
        return $this->db->select('id_koordinat,id_kegiatan,koordinat_x AS koordinat_x_garis,koordinat_y as koordinat_y_garis,ket_lokasi AS ket_lokasi_garis,tipe AS tipe_garis')
            ->from('tbl_koordinat')
            ->where('id_kegiatan',$id_kegiatan)
            ->where("tipe='garis'")
            ->get();
    }
    function get_koordinat_luasan($id_kegiatan){
        return $this->db->select('id_koordinat,id_kegiatan,koordinat_x AS koordinat_x_luasan,koordinat_y as koordinat_y_luasan,ket_lokasi AS ket_lokasi_luasan,tipe AS tipe_luasan')
            ->from('tbl_koordinat')
            ->where('id_kegiatan',$id_kegiatan)
            ->where("tipe='luasan'")
            ->get();
    }
    function delete_koordinat($id_kegiatan,$id_koordinat){
        return $this->db->where('id_koordinat',$id_koordinat)->where('id_kegiatan',$id_kegiatan)->delete('tbl_koordinat');
    }
    function update_koordinat($data,$id_koordinat){
         $this->db->where('id_koordinat',$id_koordinat);
        return $this->db->update('tbl_koordinat',$data);
    }/*end koordinat*/
    function update_koordinat_garis($data,$id_koordinat){
         $this->db->where('id_koordinat',$id_koordinat);
        return $this->db->update('tbl_koordinat',$data);
    }/*end koordinat*/
    function update_koordinat_luasan($data,$id_koordinat){
         $this->db->where('id_koordinat',$id_koordinat);
        return $this->db->update('tbl_koordinat',$data);
    }/*end koordinat*/  // end koordinat Zikra



    function set_lokasi($data){
        if($this->db->where('id_provinsi',$data['id_provinsi'])
            ->where('id_kegiatan',$data['id_kegiatan'])
            ->where('id_kabupaten',$data['id_kabupaten'])
            ->where('id_kecamatan',$data['id_kecamatan'])
            ->where('id_kelurahan',$data['id_kelurahan'])
            ->get('tbl_kegiatan_lokasi')->num_rows() == 0)
        {
            // tambah baru, jika record belum ada
            return $this->db->insert('tbl_kegiatan_lokasi',$data);
        } 
    }
    function get_locations($id_kegiatan){
        return $this->db->select('kl.id as id_lokasi,kl.id_kegiatan, kl.id_provinsi,kl.id_kabupaten, kl.id_kecamatan, kl.id_kelurahan,p.provinsi,kb.name as kabupaten,kec.name as kecamatan, kelurahan.name as kelurahan')
            ->from('tbl_kegiatan_lokasi kl')
            ->where('id_kegiatan',$id_kegiatan)
            ->join("provinsi p",'ON kl.id_provinsi=p.id_provinsi','left') 
            ->join("kabupaten kb",'ON kb.id=kl.id_kabupaten','left') 
            ->join("kecamatan kec",'ON kec.id=kl.id_kecamatan','left') 
            ->join("kelurahan",'ON kelurahan.id=kl.id_kelurahan','left') 
            ->get();
    }
    function delete_lokasi($id_kegiatan,$id_provinsi){
        return $this->db->where('id_provinsi',$id_provinsi)->where('id_kegiatan',$id_kegiatan)->delete('tbl_kegiatan_lokasi');
    }
    function delete_lokasi_by_id($id_lokasi){
        return $this->db->where('id',$id_lokasi)->delete('tbl_kegiatan_lokasi');
    }
    function update_lokasi($data,$id_lokasi){
         if($this->db->where('id_provinsi',$data['id_provinsi'])
            ->where('id_kegiatan != ',$data['id_kegiatan'])
            ->where('id_kabupaten',$data['id_kabupaten'])
            ->where('id_kecamatan',$data['id_kecamatan'])
            ->where('id_kelurahan',$data['id_kelurahan'])
            ->get('tbl_kegiatan_lokasi')->num_rows() == 0)
        {
            // tambah baru, jika record belum ada
            return $this->db->where('id',$id_lokasi)->update('tbl_kegiatan_lokasi',$data);
        } 
        
    }
}