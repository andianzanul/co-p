<?php 
require_once APPPATH.'libraries/fpdf16/fpdf.php';
class Pupdf extends FPDF{
	var $y0 = 0; // tabel pengembangan diri
	var $y1 = 0; // catatan PU
	var $y2 = 0; // tabel akhal mulia dan kepribadian
	
	public $show_page_number;
	
	private $kegiatan;
	
	function set_data_gegiatan($kegiatan){
		$this->kegiatan = $kegiatan;
	}
	
	 
	 
	/* PRIVATE FUNCTION  */
	
	function init(){
		
		$this->CI =& get_instance();
		 
		$this->show_page_number();
		$this->setRaporProperties();
	}	
	
	function show_page_number(){
		$this->show_page_number	= true;
	}
	
	function hide_page_number(){
		$this->show_page_number	= false;
	}
	
	function garis(){
		$this->SetLineWidth(1);
		$this->Line(10,36,200,36);
		$this->SetLineWidth(0);
		$this->Line(10,37,200,37);	
	}
	
	// Page header
	function Header()
	{
		 
		  
	}
 
	function buat_pdf(){
		$this->init();
		
	 	$this->hide_page_number();	  
		
		$this->_buat_pdf();   
		$this->output('Database Kegiatan Balai Sungai III.pdf','i');
	}


	private function _buat_pdf(){
		foreach ($this->kegiatan as $key => $kegiatan) {
			$this->buat_individu($kegiatan);
		}
		 
	}


	private function buat_individu($kegiatan){
		$this->TambahHalaman();
		$this->setY(15);		
		//$this->setX(20);

		$this->SetLeftMargin(20); 

			$this->SetFont('Times','',15);
		
		// Title
		$this->Cell(170,5,'DESKRIPSI KEGIATAN ',0,1,'C');
		$this->Ln(2);

		//$this->SetFont('Arial','','10'); 
		$this->SetFont('Times','','11'); 
		$this->SetFillColor(79,129,189); // biru
		$this->SetTextColor(000); 
		$this->SetDrawColor(000);  
		 
		
	 	
		$th_nomor = 10;
		$height_default = 7;
		$border = 1;
		$lebar_nama = 45;
		$lebar_titik2 = 3;
		$lebar_value = 112;
		
		// header
		$this->Cell($th_nomor, $height_default, 'No', $border, 0, 'C', true); 
		$this->Cell(160, $height_default, 'Urian', $border, 0, 'C', true);
		 
		 
		
	  	$row_kegiatan = array(
	  			'nama_satker' => 'Nama Satker',
	  			'nm_kegiatan' => 'Nama Kegiatan',
	  			'jenis_kegiatan' => 'Jenis Kegiatan',
	  		
	            'tahun' => 'Tahun Anggaran',
	            'nama_ppk'	=> 'PPK',
	            'nama_bidang' => 'Bidang',
	            'output' => 'Sub Bidang',
	            'jenis_konstruksi' => 'Jenis Konstruksi',
	            'prov'	=>	'Provinsi',

	            'kordinat' => 'Koordinat',

	            'nama_wlsungai' => 'Wilayah Sungai',
	           	'das' => 'DAS',
	            'tujuan' => 'Tujuan',
	            'manfaat' => 'Manfaat',
	            'output' => 'Volume Output',
	            'outcome' => 'Volume Outcome',
	            'sumber_dana' => 'Sumber Dana',

	            'nmp_jasa' => 'Penyedia Jasa',
	            'tgl_kontrak_awal' => 'Tanggal kontrak',
	            'no_kontrak_awal' => 'No Kontrak',
	            'nilai_kontrak' => 'Nilai kontrak',
	            'tgl_spmk' => 'Tanggal SPMK',
	            'no_spmk' => 'Nomor SPMK',

	            'tgl_amandemen' => 'Tanggal Amandemen',
	            'no_amandemen' => 'Nomor Amandemen',
	            'nilai_amandemen' => 'Nilai Amandemen',
 
	            'tgl_penyerapan' => 'Tanggal Penyerapan',
	            'nilai_penyerapan' => 'Nilai Penyerapan (Rp)',
	            'keuangan_persen' => 'Keuangan (%)',
	            'fisik_persen' => 'Fisik (%)',
	            'jkwkt_pelaksanaan' => 'Jangka Waktu Pelaksanaan',
	  			//'amandemen_nomor' => 'Nomor Amandemen',
	  			//'amandemen_nilai' => 'Nilai Amandemen',
	           // 'id_satker' => 'Nama',
	           // 'nm_kegiatan' => 'Nama',
	          //  'id_jenis_kegiatan' => 'Nama',
	          //  'id_ppk' => 'Nama',
	           // 'id_bidang' => 'Nama', 
	           // 'sub_bidang' => 'Nama',
	           // 'jenis_konstruksi' => 'Nama',
	           // 'provinsi' => 'Provinsi',
	           // 'kabupaten' => 'Kabupaten',
	           // 'kecamatan' => 'Kecamatan',
	           // 'desa' => 'Desa',
	          //  'wl_sungai' => 'Wilayah Sungai',
	            
	            
	            'data_teknis' => 'Data Teknis',
	            'data_kegiatan' => 'Data Kegitan',
	            'th_kegiatanlanjutan' => 'Tahun Kegiatan Lanjutan',
	            
	            'status_kegiatan' => 'Status Kegiatan',
	            'dasar_kegiatan' => 'Dasar Kegiatan',
	           
	            'perencana' => 'Perencana',
	            'supervisi' => 'Supervisi',
	         
	           
	            // 'jml_hari' => 'Jumlah Hari',
	           // 'koordinat_x' => 'Koordinat X',
	           // 'koordinat_y' => 'Koordinat Y',
	           // 'youtube_url' => 'Video Url', 

	  		);

	  	$abc = array(
	  		'',
	  		'A','B','C','D','E','F','G','H','I','J','K','L','M',
	  		);

	  	$no = 1;
		$H = $this->GetY() + $height_default;

		$this->SetFillColor(211,223,238); // kuning
		$belang = false;
	 	foreach($row_kegiatan as $key_row => $row){
	 		
			$Y = $H ;
			$this->SetXY(78,$Y); 

			 if($Y >= 270){
			 	$this->TambahHalaman();
				$this->setY(15);	 

				$this->SetLeftMargin(20);

				 $H = $this->GetY() + $height_default;
				 $Y= $H ;
				 $this->SetXY(78,$Y); 
			 } 

			 $val = '';

			 if('amandemen_nomor' == $key_row){
			 	$val = '';
			 	$no_a = 1;
			 	foreach ($kegiatan->amandemen as $key_a => $value_a) {
			 		$val .= $abc[$no_a]. '. ' .$value_a->nomor ."\n";
			 		$no_a++;
			 	}
			 }

			 else if('amandemen_nilai' == $key_row){
			 	$val = '';
			 	$no_a = 1;
			 	foreach ($kegiatan->amandemen as $key_a => $value_a) {
			 		$val .= $abc[$no_a]. '. ' . $value_a->nilai ."\n";
			 		$no_a++;
			 	}
			 }
			 
			 else if('prov' == $key_row){

			 }

			 else if('kordinat' == $key_row){

			 }

			 else if('tgl_kontrak_awal' == $key_row || 'tgl_spmk' == $key_row || 'tgl_penyerapan' == $key_row){ 
			 	$val = tgl_indo( $kegiatan->$key_row ,2); 
			 }

			 
			 else if('tgl_amandemen' == $key_row){
			 	$no_amd = 1;
                foreach ($kegiatan->amandemen as $key => $value) { 
                 $val .=  strtolower($abc[$no_amd]) . '. '.tgl_indo($value->tanggal,2) ."\n";
                 $no_amd++; 
                }
			 }

			 else if('no_amandemen' == $key_row){
			 	$no_amd = 1;
				foreach ($kegiatan->amandemen as $key => $value) {
                 $val .= strtolower($abc[$no_amd]) . '. '. $value->nomor. "\n";
                 $no_amd++;
                } 
			 }

			 else if('nilai_amandemen' == $key_row){
			 	$no_amd = 1;
				foreach ($kegiatan->amandemen as $key => $value) {
                 $val .= strtolower($abc[$no_amd]) . '. '. $value->nilai. "\n";
                 $no_amd++;
                } 
			 }



			 
			 else{
			 	$val = $kegiatan->$key_row;
			 }

			 if('prov' == $key_row){


			 	$Provinsi = $kabupaten = $kecamatan = $desa = array();

                foreach($kegiatan->lokasi as $lok){
                    $Provinsi[$lok->provinsi] = $lok->provinsi;
                    $kabupaten[$lok->name] = $lok->name;
                    $kecamatan[$lok->kecamatan] = $lok->kecamatan;
                    $desa[$lok->desa] = $lok->desa;
                }

                    //echo implode(', ',$Provinsi );
               	$pp = array(
               			'prov' => array('Provinsi',$Provinsi),
               			'kab' => array('Kabupaten',$kabupaten),
               			'kec' => array('Kecamatan',$kecamatan),
               			'desa' => array('Desa',$desa),
               		);

               	foreach($pp as $c){

               		$Y = $H ;
					$this->SetXY(78,$Y); 

					 if($Y >= 270){
					 	$this->TambahHalaman();
						$this->setY(15);	 

						$this->SetLeftMargin(20);

						 $H = $this->GetY() + $height_default;
						 $Y= $H ;
						 $this->SetXY(78,$Y); 
					 }


               		$this->MultiCell($lebar_value, $height_default, implode(', ',$c[1] ) , $border,  'L', true);
					
					$H = $this->GetY();

					

				    $height= $H-$Y;

				    $this->SetXY(20,$Y); 
					$this->Cell($th_nomor, $height, $no, $border, 0, 'C', true);  
					$this->Cell($lebar_nama, $height, $c[0], $border, 0, 'L', true);
					$this->Cell($lebar_titik2, $height, ":", $border, 0, 'L', true);
				
				 	$no++;

				 	if($belang){
				 		$belang = false;
				 		$this->SetFillColor(211,223,238); // kuning
				 	}else{
				 		$belang = true;
				 		$this->SetFillColor(79,129,189); // biru
				 	}
               	}


              } else if('kordinat' == $key_row){
              		$this->SetXY(78, $this->GetY() );
			 	   	$this->Cell( $lebar_value/2 , $height_default, 'Awal' , $border,0,  'L', true);
               		$this->Cell( $lebar_value/2  , $height_default, 'Akhir' , $border, 0, 'L', true);
               		

               		foreach ( $kegiatan->koordinats as $key => $koor) {
				    	$this->SetXY(78, $this->GetY() + $height_default ); 

               			$this->Cell( $lebar_value/2  , $height_default, 'X : '.$koor->koordinat_x .', Y : '. $koor->koordinat_y , $border,0,  'L', true);
               			$this->Cell( $lebar_value/2  , $height_default, 'X : '.$koor->koordinat_x1 .', Y : '. $koor->koordinat_y1 , $border, 0, 'L', true);
               		}
               		 
               		 
               		 $this->SetXY(78, $this->GetY() + $height_default ); 

               		

               		$H = $this->GetY();

				

				    $height= $H-$Y;

				    $this->SetXY(20,$Y); 
					$this->Cell($th_nomor, $height, $no, $border, 0, 'C', true);  
					$this->Cell($lebar_nama, $height, $row, $border, 0, 'L', true);
					$this->Cell($lebar_titik2, $height, ":", $border, 0, 'L', true);
                
			 		$no++;

			 		if($belang){
				 		$belang = false;
				 		$this->SetFillColor(211,223,238); // kuning
				 	}else{
				 		$belang = true;
				 		$this->SetFillColor(79,129,189); // biru
				 	}

			 }else {

				$this->MultiCell($lebar_value, $height_default, $val, $border,  'L', true);
					


				$H = $this->GetY();

				

			    $height= $H-$Y;

			    $this->SetXY(20,$Y); 
				$this->Cell($th_nomor, $height, $no, $border, 0, 'C', true);  
				$this->Cell($lebar_nama, $height, $row, $border, 0, 'L', true);
				$this->Cell($lebar_titik2, $height, ":", $border, 0, 'L', true);
			
			 	$no++;

			 	if($belang){
			 		$belang = false;
			 		$this->SetFillColor(211,223,238); // kuning
			 	}else{
			 		$belang = true;
			 		$this->SetFillColor(79,129,189); // biru
			 	}

		 	 }

		}

		$H = $this->GetY() + $height_default + 5;
		ksort ($kegiatan->images);
		foreach($kegiatan->images as $image){
			$foto = 'uploads/kegiatan/'. $image->nama_file;
			 
			if(!file_exists($foto)){
				$foto = 'uploads/kegiatan.jpg';
			}

		 	
		 	 

			$im_src = getimagesize($foto);

		 


		 	$src_width 	= $im_src[0];
			$src_height = $im_src[1];

			$ext = 'PNG';

			if($im_src['mime'] == "image/jpg"){
				$ext = 'jpg';
			} else if($im_src['mime'] == "image/jpeg"){
				$ext = 'jpg';
			} else if($im_src['mime'] == "image/png"){
				$ext = 'PNG';
			}

				 

			$yy =  ($src_width / ($src_height/40));
			
			$this->Image($foto,20,$H,$yy,40, $ext);
			$this->SetXY(20,$H+40); 
			$this->Cell($lebar_nama, $height, 'Gambar Progres '.$image->proses .' %' , false, 0, 'L', false);
			$H = $H+48;
		}
		

		  
	}



	
	
	public function ttd(){
		
		$this->Ln(12);
		
		$border = 0;
		
		// Tanggal
		$this->Cell(126, 5);
		$this->Cell(63, 5, $this->CI->config->item('lokasi_ttd').', '.$this->CI->config->item('tgl_ttd'), $border, 0, 'C', false);
		$this->Ln(6);
		
		 
		$this->Ln(20);
		
		$this->Cell(126, 5); 
		$this->Cell(63, 5, $this->CI->config->item('nama_ttd'), $border, 0, 'C', false);	
		$this->Ln(5);
		
		
		if(!empty($this->CI->config->item('nip_ttd'))){
			$this->Cell(126, 5); 
			$this->Cell(63, 5, 'NIP : '. $this->CI->config->item('nip_ttd') , $border, 0, 'C', false);
		}		
		
	}
	
	
	# COVER
	public function cover(){
		$this->TambahHalaman();	
		$this->SetFont('Arial','','10'); 
		$this->SetFillColor(207,223,233);
		$this->SetTextColor(000); 
		$this->SetDrawColor(000);  
		$this->SetLeftMargin(10);
		$this->SetTopMargin(10);
		$this->Ln(45);
		// kop rapot
		$this->Image(APPPATH.'libraries/fpdf16/garudapancasila.png',95,20,20);
		$this->Cell(190, 5, 'LAPORAN HASIL BELAJAR PESERTA DIDIK', 0, 0, 'C', false);
		$this->Ln(5);
		// sekolah menengah atas
		$this->Cell(190, 5, 'SEKOLAH MENENGAH ATAS', 0, 0, 'C', false);
		$this->Ln(20);
		// nama sekolah
		$this->SetFont('Arial','B','18'); 
		$this->Cell(190, 5, 'Nama Sekolah' , 0, 0, 'C', false);
		// alamat sekolah
		$this->SetFont('Arial','I','8');
		$this->Ln(8);
		$this->Cell(190, 5, 'Alamat' , 0, 0, 'C', false);
		$this->SetFont('Arial','','10');
		// logo sekolah
		//$this->Image('logo_pb.png',80,105,50,50,'','http://sman1bambanglipuro.sch.id');
		$this->Image(APPPATH.'libraries/fpdf16/logo_pb.png',80,105,50);
		// nama peserta didik
		$this->Ln(90);
		$this->SetLeftMargin(35);
		$this->Cell(50, 5, 'Nama Peserta Didik', 0, 0, 'L', false);
		$this->Cell(5, 5, ' : ', 0, 0, 'L', false);
		$this->Cell(75, 5, 'fsdfsdfsdf', 0, 0, 'L', false);
		$this->Ln(5);
		// nis peserta didik
		$this->Cell(50, 5, 'No. Induk  Peserta Didik', 0, 0, 'L', false);
		$this->Cell(5, 5, ' : ', 0, 0, 'L', false);
		$this->SetFont('Arial','B','12'); 
		$this->Cell(75, 5, 'fsdfsdfsdf', 0, 0, 'L', false);
		$this->SetFont('Arial','','12'); 	
	}
	
	 
	function setRaporProperties(){
		$this->SetTitle('Database Kegiatan Balai Wilayah Sungai Sulawesi III');
		$this->SetSubject('Database Kegiatan');
		$this->SetAuthor('BWSSIII');
		$this->SetKeywords('');
		$this->SetCreator('');
	}
	
	
	public function TambahHalaman(){
		$this->AddPage('P','A4');
	}
	
	
/* = Fungsi Umum FPDF
*****************************************************************/

	function Footer2(){
		
		if(true == $this->show_page_number){
			// Go to 1.5 cm from bottom
			$this->SetY(-15);
			// Select Arial italic 8
			$this->SetFont('Arial','I',8);
			// Print centered page number
			$this->Cell(0,10,'Halaman '.$this->PageNo(),0,0,'R' ,false);
		}
		
		if(true){
			// Go to 1.5 cm from bottom
			$this->SetY(-15);
			// Select Arial italic 8
			$this->SetFont('Arial','I',6);
			
			// Text color
			$this->SetTextColor(111);
			
			// Print centered page number
			$this->Cell(0,10,'Dibuat pada : ' . date("d M Y H:i",now()) ,0,0,'L' ,false);
		}
	}	
	
}