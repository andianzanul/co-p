<?php 
require_once APPPATH.'libraries/fpdf16/fpdf.php';
class Pupdf extends FPDF{
	var $y0 = 0; // tabel pengembangan diri
	var $y1 = 0; // catatan guru
	var $y2 = 0; // tabel akhal mulia dan kepribadian
	
	public $show_page_number;
	
	private $kegiatan;
	
	function set_data_gegiatan($kegiatan){
		$this->kegiatan = $kegiatan;
	}
	
	 
	
	
	
	
	
	
	
	
	/* PRIVATE FUNCTION  */
	
	function init(){
		
		$this->CI =& get_instance();
		 
		$this->show_page_number();
		$this->setRaporProperties();
	}
	
	
	
	
	
	function show_page_number(){
		$this->show_page_number	= true;
	}
	
	
	
	function hide_page_number(){
		$this->show_page_number	= false;
	}
	
	
	 
	
	
	function garis(){
		$this->SetLineWidth(1);
		$this->Line(10,36,200,36);
		$this->SetLineWidth(0);
		$this->Line(10,37,200,37);	
	}
	
	// Page header
	function Header2()
	{
		// Logo
		$foto = 'data/kop-logo.jpg';
		$this->Image($foto,10,7,30);
		// Times bold 15
		$this->SetFont('Times','',15);
		// Move to the right
		$this->Cell(40);
		// Title
		$this->Cell(150,5,'KEMENTERIAN AGAMA REPUBLIK INDONESIA',0,1,'C');
		
		$this->Cell(40);
		$this->Cell(150,6,'KANTOR WILAYAH PROVINSI SULAWESI TENGAH',0,1,'C');
		$this->Ln(1);
		$this->SetFont('Times','',10);
		
		$this->Cell(40);
		$this->Cell(150,4,'Jl. Prof. Dr. Moh. Yamin No. 42 ',0,1,'C');
		
		$this->Cell(40);
		$this->Cell(150,4,'Tel (0451) 488921 Fax (0451) 488920',0,1,'C');
		
		$this->Cell(40);
		$this->Cell(150,4,'E-mail kanwilsulteng@kemenag.go.id Web http//sulteng.kemenag.go.id',0,1,'C');
		// Line break
		$this->Ln(20);
		$this->garis();
	}
 
	function buat_pdf(){
		$this->init();
		
	 	$this->hide_page_number();	  
		
		$this->_buat_pdf();   
		$this->output('Database Kegiatan Balai Sungai III.pdf','I');
	}


	private function _buat_pdf(){
		foreach ($this->kegiatan as $key => $kegiatan) {
			$this->buat_individu($kegiatan);
		}
		 
	}


	private function buat_individu($kegiatan){
		$this->TambahHalaman();
		$this->setY(20);		
		//$this->setX(20);

		$this->SetLeftMargin(20); 


		//$this->SetFont('Arial','','10'); 
		$this->SetFont('Times','','11'); 
		$this->SetFillColor(207,223,233);
		$this->SetTextColor(000); 
		$this->SetDrawColor(000);  
		 
		
	 
		
		 
		
		$th_nomor = 10;
		$height = 7;
		$border = 1;
		$lebar_nama = 45;
		$lebar_titik2 = 3;
		$lebar_value = 112;
		
		// header
		$this->Cell($th_nomor, $height, 'No', $border, 0, 'C', true); 
		$this->Cell(160, $height, 'Urian', $border, 0, 'C', true);
		$this->Ln($height);
		 
		
	 	$this->Cell($th_nomor, $height, '1', $border, 0, 'C', true);  
		$this->Cell($lebar_nama, $height, 'Nama Satker', $border, 0, 'L', false);
		$this->Cell($lebar_titik2, $height, ":", $border, 0, 'C', false);
		$this->Cell($lebar_value, $height, $kegiatan->nama_satker, $border, 0, 'L', false);
		$this->Ln($height);

		$this->Cell($th_nomor, $height, '2', $border, 0, 'C', true);  
		$this->Cell($lebar_nama, $height, 'Nama Kegiatan', $border, 0, 'L', false);
		$this->Cell($lebar_titik2, $height, ":", $border, 0, 'C', false);
		$this->Cell($lebar_value, $height, $kegiatan->nm_kegiatan, $border, 0, 'L', false);
		$this->Ln($height);

		$this->Cell($th_nomor, $height, '3', $border, 0, 'C', true);  
		$this->Cell($lebar_nama, $height, 'Jenis Kegiatan', $border, 0, 'L', false);
		$this->Cell($lebar_titik2, $height, ":", $border, 0, 'C', false);
		$this->Cell($lebar_value, $height, $kegiatan->jenis_kegiatan, $border, 0, 'L', false);
		$this->Ln($height);

		$this->Cell($th_nomor, $height, '3', $border, 0, 'C', true);  
		  
		$this->Cell($lebar_nama, $height, 'Jangka Waktu Pelaksanaan', $border, 0, 'L', false);
		 
		$this->Cell($lebar_titik2, $height, ":", $border, 0, 'C', false);
		$this->MultiCell($lebar_value, $height, $kegiatan->jenis_kegiatan, $border,  'L', false);
		$this->Ln($height);


		
		
		/*$this->Cell($th_width, $height, 'NIP', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height, $guru['nip'], $border, 0, 'L', false);
		$this->Ln($height);
		
		
		
		$this->Cell($th_width, $height, 'Tempat, Tanggal Lahir', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height, (!empty($guru['tempat_lahir']) ? $guru['tempat_lahir'].', ' : '- , ') . tgl_indo($guru['tanggal_lahir'],2) , $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Jenis Kelamin', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height, get_kelamin($guru['jenis_kelamin']), $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'No Telp/Hp', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height, $guru['no_telp'], $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Alamat', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->MultiCell(140, $height, $guru['alamat'], $border, 'L', false);
		//$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Pangkat', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$pangkat , $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Golongan', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$golongan , $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Gaji', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height, format_rupiah((int) $gaji) , $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Masa Kerja', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$masa_kerja, $border, 0, 'L', false);
		$this->Ln($height);
		
		
		$this->Cell($th_width, $height, 'Jabatan', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$guru['jabatan'], $border, 0, 'L', false);
		$this->Ln($height);
		
		# SK PERTAMA
		$this->Cell($th_width, $height, 'SK Pertama', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$number_sk_pertama, $border, 0, 'L', false);
		$this->Ln($height);
		
		# SK TERAKHIR
		$this->Cell($th_width, $height, 'SK Terakhir', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$number_sk_terakhir, $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'Nama Sekolah', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->MultiCell(140, $height,$nama_sekolah, $border, 'L', false);
		//$this->Ln($height); 
		
		$this->Cell($th_width, $height, 'Kota/Kabupaten	', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$guru['kota'], $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'NUPTK	', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$guru['no_nuptk'], $border, 0, 'L', false);
		$this->Ln($height);
		
		$this->Cell($th_width, $height, 'NRG	', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->Cell(140, $height,$guru['nrg'], $border, 0, 'L', false);
		$this->Ln($height);*/
		
		/* $this->Cell($th_width, $height, 'Keterangan	', $border, 0, 'L', false);
		$this->Cell(5, $height, ":", $border, 0, 'C', false);
		$this->MultiCell(140, $height,$guru['keterangan'], $border,  'L', false); */
		 
		
		#HISTORI SEKOLAH
		/*$this->Cell($th_width, $height, 'History Sekolah	', $border, 0, 'L', false);
		$tulis = false;
		foreach($guru['sekolah'] as $sklh) { 
			if($tulis){
				$this->Cell($th_width, $height, '', $border, 0, 'L', false);
			}
			
			
			$this->Cell(5, $height, ":", $border, 0, 'C', false);
			$this->Cell(140, $height,$sklh['nama_sekolah'], $border, 0, 'L', false);
			$this->Ln($height);
			$tulis = true; 
		}*/
		
		#HISTORI SEKOLAH
		/*$this->Cell($th_width, $height, 'History SK	', $border, 0, 'L', false);
		$tulis = false;
		foreach($guru['sk'] as $sk) { 
			if($tulis){
				$this->Cell($th_width, $height, '', $border, 0, 'L', false);
			}
			
			
			$this->Cell(5, $height, ":", $border, 0, 'C', false);
			$this->Cell(140, $height,$sk['no_sk'], $border, 0, 'L', false);
			$this->Ln($height);
			$tulis = true; 
		}*/
		
		//$this->ttd();
	}



	
	
	public function ttd(){
		
		$this->Ln(12);
		
		$border = 0;
		
		// Tanggal
		$this->Cell(126, 5);
		$this->Cell(63, 5, $this->CI->config->item('lokasi_ttd').', '.$this->CI->config->item('tgl_ttd'), $border, 0, 'C', false);
		$this->Ln(6);
		
		 
		$this->Ln(20);
		
		$this->Cell(126, 5); 
		$this->Cell(63, 5, $this->CI->config->item('nama_ttd'), $border, 0, 'C', false);	
		$this->Ln(5);
		
		
		if(!empty($this->CI->config->item('nip_ttd'))){
			$this->Cell(126, 5); 
			$this->Cell(63, 5, 'NIP : '. $this->CI->config->item('nip_ttd') , $border, 0, 'C', false);
		}		
		
	}
	
	
	
	# COVER
	public function cover(){
		$this->TambahHalaman();	
		$this->SetFont('Arial','','10'); 
		$this->SetFillColor(207,223,233);
		$this->SetTextColor(000); 
		$this->SetDrawColor(000);  
		$this->SetLeftMargin(10);
		$this->SetTopMargin(10);
		$this->Ln(45);
		// kop rapot
		$this->Image(APPPATH.'libraries/fpdf16/garudapancasila.png',95,20,20);
		$this->Cell(190, 5, 'LAPORAN HASIL BELAJAR PESERTA DIDIK', 0, 0, 'C', false);
		$this->Ln(5);
		// sekolah menengah atas
		$this->Cell(190, 5, 'SEKOLAH MENENGAH ATAS', 0, 0, 'C', false);
		$this->Ln(20);
		// nama sekolah
		$this->SetFont('Arial','B','18'); 
		$this->Cell(190, 5, 'Nama Sekolah' , 0, 0, 'C', false);
		// alamat sekolah
		$this->SetFont('Arial','I','8');
		$this->Ln(8);
		$this->Cell(190, 5, 'Alamat' , 0, 0, 'C', false);
		$this->SetFont('Arial','','10');
		// logo sekolah
		//$this->Image('logo_pb.png',80,105,50,50,'','http://sman1bambanglipuro.sch.id');
		$this->Image(APPPATH.'libraries/fpdf16/logo_pb.png',80,105,50);
		// nama peserta didik
		$this->Ln(90);
		$this->SetLeftMargin(35);
		$this->Cell(50, 5, 'Nama Peserta Didik', 0, 0, 'L', false);
		$this->Cell(5, 5, ' : ', 0, 0, 'L', false);
		$this->Cell(75, 5, 'fsdfsdfsdf', 0, 0, 'L', false);
		$this->Ln(5);
		// nis peserta didik
		$this->Cell(50, 5, 'No. Induk  Peserta Didik', 0, 0, 'L', false);
		$this->Cell(5, 5, ' : ', 0, 0, 'L', false);
		$this->SetFont('Arial','B','12'); 
		$this->Cell(75, 5, 'fsdfsdfsdf', 0, 0, 'L', false);
		$this->SetFont('Arial','','12'); 	
	}
	
	 
	 
	 
	 
	 
	function setRaporProperties(){
		$this->SetTitle('Laporan Guru Agama Hindu');
		$this->SetSubject('Biodata guru agama hindu');
		$this->SetAuthor('I Wayan Mastrayasa, S.Kom');
		$this->SetKeywords('Laporan,biodata,data guru agama hindu, sigrah, sistem informasi guru agama hindu');
		$this->SetCreator('www.sigrahsulteng.com');
	}
	
	
	
	
	
	public function TambahHalaman(){
		$this->AddPage('P','A4');
	}
	
	
/* = Fungsi Umum FPDF
*****************************************************************/

	function Footer2(){
		
		if(true == $this->show_page_number){
			// Go to 1.5 cm from bottom
			$this->SetY(-15);
			// Select Arial italic 8
			$this->SetFont('Arial','I',8);
			// Print centered page number
			$this->Cell(0,10,'Halaman '.$this->PageNo(),0,0,'R' ,false);
		}
		
		if(true){
			// Go to 1.5 cm from bottom
			$this->SetY(-15);
			// Select Arial italic 8
			$this->SetFont('Arial','I',6);
			
			// Text color
			$this->SetTextColor(111);
			
			// Print centered page number
			$this->Cell(0,10,'Dibuat pada : ' . date("d M Y H:i",now()) ,0,0,'L' ,false);
		}
	}
	
	
	
}



