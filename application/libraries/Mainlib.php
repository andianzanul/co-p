<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainlib{


	public function logged_in(){
		$_this =& get_instance();
		$_this->load->helper('url');
		if($_this->session->userdata('login_status') != TRUE){
			redirect(base_url('login'));
		}
	}


	public function cek_level($array = array()){
		$_this =& get_instance();
		if(in_array($_this->session->userdata('level'), $array)){
			return true;
		}
		return false;
	}


	public function batasi_akses_user($array = array()){
		$_this =& get_instance();
		if(!in_array($_this->session->userdata('level'), $array)){
			 show_error("Akses di tolak!",403,"Perhatian");
		}
	}


	public function file_upload_load(){
		$_this =& get_instance();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 1024*10;
		$config['max_width'] = 2000;
		$config['max_height'] = 2000;
		$_this->load->library('upload', $config);
	}


	public function pagination_load(){
		$_this =& get_instance();
		$config['base_url'] = base_url('front/daftar_artikel/hal/');
		$config['total_rows'] = $_this->Post_model->total_rows('tbl_post');
		$config['per_page'] = 5;
		/* config */
		$config['full_tag_open'] = '<div class="paging">';
		$config['full_tag_close'] = '</div>';
		$config['first_url'] = '';
		$_this->pagination->initialize($config);			
	}
}